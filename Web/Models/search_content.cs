namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class search_content
    {
      
        public int id { get; set; }

       
        public string guid { get; set; }

        public string title { get; set; }
        public string notes { get; set; }
        public string content { get; set; }
        public string url { get; set; }
        public string tables { get; set; }
        public DateTime? modifydate { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? create_date { get; set; }


        [StringLength(30)]
        public string lang { get; set; }

        public string category { get; set; }
        public int sortIndex { get; set; }

    }
}
