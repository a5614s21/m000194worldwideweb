namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class web_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        [StringLength(200)]
        public string title { get; set; }

        [Column(TypeName = "text")]
        public string url { get; set; }

        [StringLength(255)]
        public string phone { get; set; }

        [StringLength(255)]
        public string fax { get; set; }

        [StringLength(255)]
        public string servicemail { get; set; }

        [StringLength(255)]
        public string ext_num { get; set; }

        [StringLength(255)]
        public string address { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string seo_keywords { get; set; }

        public string seo_description { get; set; }


        public string register_url { get; set; }

        public string reference_url { get; set; }
        public string symptom_url { get; set; }
        public string sp_url { get; set; }
        public string International_url { get; set; }
        public string health_url { get; set; }
        public string medical_url { get; set; }
        public string care_url { get; set; }

        public string csr_url { get; set; }

        public string refs_url { get; set; }//轉診作業
        public string webwork_url { get; set; }//健康存摺上傳
                                        
        public string fidrug_url { get; set; }//藥品引進資訊
        public string material_url { get; set; }//材料資訊

        public string department_url { get; set; }//社服處官網
        public string donate1_url { get; set; }//補助與捐贈

        public string en_url { get; set; }//英文版網址

        public string share_url { get; set; }//醫病共享決策
    }
}
