namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class historys_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }
        public string day { get; set; }

        public string title { get; set; }

        [StringLength(64)]
        public string category { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(1)]
        public string status { get; set; }


        public DateTime? create_date { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
        public int sortIndex { get; set; }

    }
}
