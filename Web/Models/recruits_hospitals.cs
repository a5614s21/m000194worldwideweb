namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class recruits_hospitals
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }
        public string name { get; set; }

        public string tel { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string sp_url { get; set; }

        public string job_url { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }
        public DateTime? modifydate { get; set; }
        
        [StringLength(1)]
        public string status { get; set; }
        

        public DateTime? create_date { get; set; }

        [StringLength(64)]
        public string category { get; set; }

        [StringLength(30)]
        public string lang { get; set; }


        public int sortIndex { get; set; }

     
    }
}
