namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class recruits
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string top_notes { get; set; }

        public string notes { get; set; }
        public string content { get; set; }    



        public DateTime? modifydate { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(64)]
        public string category { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
        
        public int sortIndex { get; set; }


        public string resume_url { get; set; }

        public string admittance_url { get; set; }


        public string link_list { get; set; }

    }
}
