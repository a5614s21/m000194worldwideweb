﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(null, "connector", new { controller = "Files", action = "Index" });
            routes.MapRoute(null, "Thumbnails/{tmb}", new { controller = "Files", action = "Thumbs", tmb = UrlParameter.Optional });



            routes.MapRoute(
              name: "asproot_03",
                  url: "asproot/{path1}/{path2}/{uri}",
                  defaults: new { controller = "OldPath", action = "Situation_C", path1 = UrlParameter.Optional, path2 = UrlParameter.Optional, uri = UrlParameter.Optional }
              );

            routes.MapRoute(
              name: "asproot_02",
                  url: "asproot/{path}/{uri}",
                  defaults: new { controller = "OldPath", action = "Situation_B", path = UrlParameter.Optional, uri = UrlParameter.Optional }
              );

            routes.MapRoute(
            name: "asproot_01",
                url: "asproot",
                defaults: new { controller = "OldPath", action = "Situation_A" }
            );


            routes.MapRoute(
                    name: "apple-app-site-association",
                    url: "apple-app-site-association",
                    defaults: new { controller = "Home", action = "AppleApp" }
                );



            routes.MapRoute(
                    name: "analyticsDatatable",
                    url: "siteadmin/analyticsDatatable/{key}/{subkey}",
                    defaults: new { controller = "AnalyticsDatatable", action = "Index", key = UrlParameter.Optional, subkey = UrlParameter.Optional }
                );
             
            routes.MapRoute(
              name: "analytics",
              url: "siteadmin/analytics/{key}/{subkey}",
              defaults: new { controller = "Siteadmin", action = "analytics", key = UrlParameter.Optional, subkey = UrlParameter.Optional }
          );

       
            
            routes.MapRoute(
                   name: "AnalyticsList",
                   url: "siteadmin/AnalyticsList/{act}/{id}",
                   defaults: new { controller = "Siteadmin", action = "AnalyticsList", act = UrlParameter.Optional, id = UrlParameter.Optional }
               );
               

            routes.MapRoute(
                     name: "SiteadminCkAccount",
                     url: "siteadmin/ckAccount/{tokens}",
                     defaults: new { controller = "Siteadmin", action = "CkAccount", tokens = UrlParameter.Optional }
                 );


            routes.MapRoute(
                  name: "SiteadminUploadToWeb",
                  url: "siteadmin/fileUpload",
                  defaults: new { controller = "Upload", action = "Upload", id = UrlParameter.Optional, FilePath = "" }
              );


            routes.MapRoute(
                name: "SiteadminList",
                url: "siteadmin/{tables}/{action}/{id}",
                defaults: new { controller = "Siteadmin", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Siteadmin",
                url: "siteadmin/{action}/{id}",
                defaults: new { controller = "Siteadmin", id = UrlParameter.Optional }
            );


            routes.MapRoute(
             name: "SiteadminIndex",
             url: "siteadmin",
             defaults: new { controller = "Siteadmin", action = "Index", id = UrlParameter.Optional }
         );

            routes.MapRoute(
              name: "ClearCache",
              url: "ClearCache",
              defaults: new { controller = "Home", action = "ClearCache", id = UrlParameter.Optional }
          );



            routes.MapRoute(
                name: "HomeAjax",
                url: "Ajax",
                defaults: new { controller = "Home", action = "Ajax", id = UrlParameter.Optional }
            );

            routes.MapRoute(
             name: "Home",
             url: "{lang}",
             defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
         );
            routes.MapRoute(
           name: "LangToController4",
           url: "{lang}/{controller}/{action}/{id}/{guid}/{subid}/{lastid}",
           defaults: new { id = UrlParameter.Optional, guid = UrlParameter.Optional, subid = UrlParameter.Optional, lastid = UrlParameter.Optional }
       );

            routes.MapRoute(
              name: "LangToController3",
              url: "{lang}/{controller}/{action}/{id}/{guid}/{subid}",
              defaults: new { id = UrlParameter.Optional, guid = UrlParameter.Optional, subid = UrlParameter.Optional }
          );

            routes.MapRoute(
               name: "LangToController2",
               url: "{lang}/{controller}/{action}/{id}/{guid}",
               defaults: new { id = UrlParameter.Optional, guid = UrlParameter.Optional }
           );

            routes.MapRoute(
               name: "LangToController",
               url: "{lang}/{controller}/{action}/{id}",
               defaults: new { id = UrlParameter.Optional }
           );

            /*
            routes.MapRoute(
             name: "Lang",
             url: "{lang}/{action}/{id}",
             defaults: new { controller = "Home", id = UrlParameter.Optional }
         );
         */



            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
