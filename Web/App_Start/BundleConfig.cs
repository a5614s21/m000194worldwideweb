﻿using System.Web;
using System.Web.Optimization;

namespace Web
{
    public class BundleConfig
    {
        // 如需統合的詳細資訊，請瀏覽 https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // 使用開發版本的 Modernizr 進行開發並學習。然後，當您
            // 準備好可進行生產時，請使用 https://modernizr.com 的建置工具，只挑選您需要的測試。
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Styles/plugin/fontello.css",
                      "~/Styles/style.css",
                       "~/Styles/sweetalert.css"));

            bundles.Add(new ScriptBundle("~/Scripts/plugins").Include(
                 "~/Scripts/js/plugin/jquery.js",
                 "~/Scripts/js/plugin/imgLiquid.js",
                 "~/Scripts/js/plugin/slick.min.js",
                 "~/Scripts/js/main.js",
                 "~/Scripts/sweetalert.min.js",
                 "~/Scripts/jquery.blockUI.js",
                 "~/Scripts/js/plugin/picturefill.min.js",
                 "~/Content/Design/home.js",
                 "~/Scripts/js/plugin/jquery.mCustomScrollbar.concat.min.js"));
        }
    }
}