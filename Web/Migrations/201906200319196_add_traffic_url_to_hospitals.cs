namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_traffic_url_to_hospitals : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hospitals", "traffic_url", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'永續經營連結' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'traffic_url'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.hospitals", "traffic_url");
        }
    }
}
