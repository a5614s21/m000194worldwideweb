namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_search_content_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.search_content",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(),
                        title = c.String(),
                        notes = c.String(),
                        content = c.String(),
                        url = c.String(),
                        tables = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        category = c.String(),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'簡述' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'相關連結' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'來源資料表' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'tables'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'類別' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'search_content', 'COLUMN', N'sortIndex'");
        }
        
        public override void Down()
        {
            DropTable("dbo.search_content");
        }
    }
}
