namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_any_url_to_web_data_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "register_url", c => c.String());
            AddColumn("dbo.web_data", "reference_url", c => c.String());
            AddColumn("dbo.web_data", "symptom_url", c => c.String());
            AddColumn("dbo.web_data", "sp_url", c => c.String());
            AddColumn("dbo.web_data", "International_url", c => c.String());
            AddColumn("dbo.web_data", "health_url", c => c.String());
            AddColumn("dbo.web_data", "medical_url", c => c.String());
            AddColumn("dbo.web_data", "care_url", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'即時掛號連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'register_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'看哪一科連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'reference_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'病症參考連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'symptom_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'特色醫療中心連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'sp_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'國際醫療連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'International_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'健康促進連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'health_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'長庚醫訊連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'medical_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'預立醫療照護連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'care_url'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "care_url");
            DropColumn("dbo.web_data", "medical_url");
            DropColumn("dbo.web_data", "health_url");
            DropColumn("dbo.web_data", "International_url");
            DropColumn("dbo.web_data", "sp_url");
            DropColumn("dbo.web_data", "symptom_url");
            DropColumn("dbo.web_data", "reference_url");
            DropColumn("dbo.web_data", "register_url");
        }
    }
}
