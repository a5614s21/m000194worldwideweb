namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_department_url_to_web_data : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "department_url", c => c.String());
            AddColumn("dbo.web_data", "donate1_url", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'社服處官網' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'department_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'補助與捐贈連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'donate1_url'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "donate1_url");
            DropColumn("dbo.web_data", "department_url");
        }
    }
}
