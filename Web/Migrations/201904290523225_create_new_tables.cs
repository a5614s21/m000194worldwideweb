namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_new_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.app_inquiry",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        url = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });



            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'連結' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry', 'COLUMN', N'url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'分類' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry', 'COLUMN', N'sortIndex'");


            CreateTable(
                "dbo.app_inquiry_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        sortIndex = c.Int(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry_category', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry_category', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry_category', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry_category', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry_category', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'app_inquiry_category', 'COLUMN', N'sortIndex'");

            CreateTable(
                "dbo.edu_resource",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'edu_resource', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'edu_resource', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'edu_resource', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'edu_resource', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'edu_resource', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'edu_resource', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'列表圖' ,'SCHEMA', N'dbo','TABLE', N'edu_resource', 'COLUMN', N'pic'");

            CreateTable(
                "dbo.edu_resource_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        url = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'單位/名稱' ,'SCHEMA', N'dbo','TABLE', N'edu_resource_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'連結' ,'SCHEMA', N'dbo','TABLE', N'edu_resource_data', 'COLUMN', N'url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'edu_resource_data', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'edu_resource_data', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'edu_resource_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'分類' ,'SCHEMA', N'dbo','TABLE', N'edu_resource_data', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'edu_resource_data', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'edu_resource_data', 'COLUMN', N'sortIndex'");

            CreateTable(
                "dbo.edu_service",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        url = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'洽詢單位' ,'SCHEMA', N'dbo','TABLE', N'edu_service', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'連結' ,'SCHEMA', N'dbo','TABLE', N'edu_service', 'COLUMN', N'url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'edu_service', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'edu_service', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'edu_service', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'edu_service', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'edu_service', 'COLUMN', N'sortIndex'");

            CreateTable(
                "dbo.edu_train",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'edu_train', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'edu_train', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'edu_train', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'edu_train', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'edu_train', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'edu_train', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'edu_train', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'列表圖' ,'SCHEMA', N'dbo','TABLE', N'edu_train', 'COLUMN', N'pic'");

            CreateTable(
                "dbo.res_resource",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });



            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'res_resource', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'res_resource', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'res_resource', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'res_resource', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'res_resource', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'res_resource', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'res_resource', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'列表圖' ,'SCHEMA', N'dbo','TABLE', N'res_resource', 'COLUMN', N'pic'");



            CreateTable(
                "dbo.res_resource_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        url = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });



            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'res_resource_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'連結' ,'SCHEMA', N'dbo','TABLE', N'res_resource_data', 'COLUMN', N'url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'res_resource_data', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'res_resource_data', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'res_resource_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'res_resource_data', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'res_resource_data', 'COLUMN', N'sortIndex'");

            CreateTable(
                "dbo.res_results",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });



            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'res_results', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'res_results', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'res_results', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'res_results', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'res_results', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'res_results', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'res_results', 'COLUMN', N'sortIndex'");
        }
        
        public override void Down()
        {
            DropTable("dbo.res_results");
            DropTable("dbo.res_resource_data");
            DropTable("dbo.res_resource");
            DropTable("dbo.edu_train");
            DropTable("dbo.edu_service");
            DropTable("dbo.edu_resource_data");
            DropTable("dbo.edu_resource");
            DropTable("dbo.app_inquiry_category");
            DropTable("dbo.app_inquiry");
        }
    }
}
