namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_recruits_hospitals_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.recruits_hospitals",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        name = c.String(),
                        tel = c.String(),
                        email = c.String(),
                        address = c.String(),
                        sp_url = c.String(),
                        job_url = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'院區名稱' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'聯絡人' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'name'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'電話' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'tel'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'E-mail' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'email'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'地址' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'address'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'院區特色' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'sp_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'缺額單位內容' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'job_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'列表圖' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬職缺' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'recruits_hospitals', 'COLUMN', N'sortIndex'");

        }
        
        public override void Down()
        {
            DropTable("dbo.recruits_hospitals");
        }
    }
}
