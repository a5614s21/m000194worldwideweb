namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_recruits_recruits_data_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.recruits",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        top_notes = c.String(),
                        notes = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                        resume_url = c.String(),
                        admittance_url = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'職缺標題' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'招聘作業說明' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'top_notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'列表敘述' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'詳細內容' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'職類' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'列表圖' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'pic'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'履歷登錄(網址)' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'resume_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'錄取人員資料登錄(網址)' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'admittance_url'");

            CreateTable(
                "dbo.recruits_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'recruits_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'recruits_data', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'recruits_data', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'recruits_data', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'recruits_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'職類' ,'SCHEMA', N'dbo','TABLE', N'recruits_data', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'recruits_data', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'recruits_data', 'COLUMN', N'sortIndex'");

        }
        
        public override void Down()
        {
            DropTable("dbo.recruits_data");
            DropTable("dbo.recruits");
        }
    }
}
