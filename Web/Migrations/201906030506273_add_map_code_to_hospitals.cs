namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_map_code_to_hospitals : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hospitals", "map_code", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'GoogleMap�O�J�X' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'map_code'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.hospitals", "map_code");
        }
    }
}
