namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_medical_guide_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.medical_guide",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'詳細內容' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬類別' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO關鍵字' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'seo_keywords'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO敘述' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'seo_description'");

        }
        
        public override void Down()
        {
            DropTable("dbo.medical_guide");
        }
    }
}
