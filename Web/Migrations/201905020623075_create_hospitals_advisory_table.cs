namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_hospitals_advisory_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.hospitals_advisory",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        subject = c.String(),
                        ext = c.String(),
                        service_time = c.String(),
                        area = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'單位' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'服務項目' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'subject'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'分機號碼' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'ext'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'服務時段' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'service_time'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所在位置' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'area'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'hospitals_advisory', 'COLUMN', N'sortIndex'");

        }
        
        public override void Down()
        {
            DropTable("dbo.hospitals_advisory");
        }
    }
}
