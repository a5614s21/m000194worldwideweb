namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_link_to_news_category : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news_category", "link", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�~�s��' ,'SCHEMA', N'dbo','TABLE', N'news_category', 'COLUMN', N'link'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.news_category", "link");
        }
    }
}
