namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_about_overview_data_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.about_overview_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'類別' ,'SCHEMA', N'dbo','TABLE', N'about_overview_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'about_overview_data', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'about_overview_data', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'about_overview_data', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'about_overview_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'about_overview_data', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'類別' ,'SCHEMA', N'dbo','TABLE', N'about_overview_data', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'about_overview_data', 'COLUMN', N'sortIndex'");


        }

        public override void Down()
        {
            DropTable("dbo.about_overview_data");
        }
    }
}
