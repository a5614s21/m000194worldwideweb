namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_medical_other_data : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.medical_other_data", "file", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.medical_other_data", "file");
        }
    }
}
