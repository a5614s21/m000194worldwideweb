// <auto-generated />
namespace Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class add_sortIndex_to_medical_guide_table : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(add_sortIndex_to_medical_guide_table));
        
        string IMigrationMetadata.Id
        {
            get { return "201904090845361_add_sortIndex_to_medical_guide_table"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
