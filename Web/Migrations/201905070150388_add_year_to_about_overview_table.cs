namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_year_to_about_overview_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.about_overview", "start_year", c => c.String());
            AddColumn("dbo.about_overview", "end_year", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'起始年(西元)' ,'SCHEMA', N'dbo','TABLE', N'about_overview', 'COLUMN', N'start_year'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'結束年(西元)' ,'SCHEMA', N'dbo','TABLE', N'about_overview', 'COLUMN', N'end_year'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.about_overview", "end_year");
            DropColumn("dbo.about_overview", "start_year");
        }
    }
}
