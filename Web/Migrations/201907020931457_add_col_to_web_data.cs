namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_col_to_web_data : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "refs_url", c => c.String());
            AddColumn("dbo.web_data", "webwork_url", c => c.String());
            AddColumn("dbo.web_data", "fidrug_url", c => c.String());
            AddColumn("dbo.web_data", "material_url", c => c.String());


            Sql("execute sp_addextendedproperty 'MS_Description', N'轉診作業連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'refs_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'健康存摺上傳連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'webwork_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'藥品引進資訊連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'fidrug_url'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'材料資訊連結' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'material_url'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "material_url");
            DropColumn("dbo.web_data", "fidrug_url");
            DropColumn("dbo.web_data", "webwork_url");
            DropColumn("dbo.web_data", "refs_url");
        }
    }
}
