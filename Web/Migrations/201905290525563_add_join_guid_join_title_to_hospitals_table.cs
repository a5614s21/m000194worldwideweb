namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_join_guid_join_title_to_hospitals_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.hospitals", "join_guid", c => c.String());
            AddColumn("dbo.hospitals", "join_title", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'合併單位ID' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'join_guid'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'合併後顯示標題' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'join_title'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.hospitals", "join_title");
            DropColumn("dbo.hospitals", "join_guid");
        }
    }
}
