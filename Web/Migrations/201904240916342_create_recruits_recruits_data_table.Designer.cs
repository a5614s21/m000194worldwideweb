// <auto-generated />
namespace Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class create_recruits_recruits_data_table : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(create_recruits_recruits_data_table));
        
        string IMigrationMetadata.Id
        {
            get { return "201904240916342_create_recruits_recruits_data_table"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
