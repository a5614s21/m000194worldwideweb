namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_sortIndex_to_medical_guide_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.medical_guide", "sortIndex", c => c.Int(nullable: false));
            Sql("execute sp_addextendedproperty 'MS_Description', N'�Ƨ�' ,'SCHEMA', N'dbo','TABLE', N'medical_guide', 'COLUMN', N'sortIndex'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.medical_guide", "sortIndex");
        }
    }
}
