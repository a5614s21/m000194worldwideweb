namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_en_url_to_web_data : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "en_url", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�^�媩���}' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'en_url'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "en_url");
        }
    }
}
