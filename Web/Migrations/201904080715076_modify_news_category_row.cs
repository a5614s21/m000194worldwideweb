namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modify_news_category_row : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news_category", "banner_sub_title", c => c.String());
            AddColumn("dbo.news_category", "view_type", c => c.String(maxLength: 1));
            DropColumn("dbo.news_category", "link");

            Sql("execute sp_addextendedproperty 'MS_Description', N'Banner副標題' ,'SCHEMA', N'dbo','TABLE', N'news_category', 'COLUMN', N'banner_sub_title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'顯示樣式' ,'SCHEMA', N'dbo','TABLE', N'news_category', 'COLUMN', N'view_type'");

        }
        
        public override void Down()
        {
            AddColumn("dbo.news_category", "link", c => c.String());
            DropColumn("dbo.news_category", "view_type");
            DropColumn("dbo.news_category", "banner_sub_title");
        }
    }
}
