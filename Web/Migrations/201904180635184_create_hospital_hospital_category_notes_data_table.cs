namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_hospital_hospital_category_notes_data_table : DbMigration
    {
        public override void Up()
        {

            Sql("execute sp_addextendedproperty 'MS_Description', N'分類名稱' ,'SCHEMA', N'dbo','TABLE', N'hospital_category', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'hospital_category', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'hospital_category', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'hospital_category', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'hospital_category', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'hospital_category', 'COLUMN', N'modifydate'");


            Sql("execute sp_addextendedproperty 'MS_Description', N'院區名稱' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'API hospitalID' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'hospitalID'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬分類' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'地址' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'address'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'電話' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'tel'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'傳真' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'fax'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'病床總床數' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'bedQty'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'專科醫師人數' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'doctorQty'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'醫事人員數' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'medicalQty'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'院區簡介' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'profile'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'院區官網網址' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'website'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'列表圖片' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'pic'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'sortIndex'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'hospitals', 'COLUMN', N'modifydate'");



            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'副標題' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'sub_title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容介紹' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'notes_data', 'COLUMN', N'modifydate'");


        }

        public override void Down()
        {
        }
    }
}
