namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_cm_list_tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.cm_list",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });


            Sql("execute sp_addextendedproperty 'MS_Description', N'中心名' ,'SCHEMA', N'dbo','TABLE', N'cm_list', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'cm_list', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'cm_list', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'cm_list', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'cm_list', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'cm_list', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'cm_list', 'COLUMN', N'sortIndex'");



        }

        public override void Down()
        {
            DropTable("dbo.cm_list");
        }
    }
}
