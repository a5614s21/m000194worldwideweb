namespace Web.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Web.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Models.Model>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Web.Models.Model context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            /* var Users = new List<user>
              {
                  new user { guid = "001",   username = "sysadmin", password = "HpUf8bETpVZWULhCfdbVIg==", rolename = "" , role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",
                             name = "系統管理員" , email = "design7@e-ceative.tw", phone = "", mobile="", address="", note="",status="Y",
                             logindate = DateTime.Now , modifydate = DateTime.Now ,create_date = DateTime.Now  },

                  new user { guid = "002",   username = "admin", password = "4fPH8SWr/V1MMXXg3X8p9A==", rolename = "" , role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",
                             name = "系統管理員" , email = "design7@e-ceative.tw", phone = "", mobile="", address="", note="",status="Y",
                             logindate = DateTime.Now , modifydate = DateTime.Now ,create_date = DateTime.Now  }

              };
             if (context.user.ToList().Count == 0)
             {
                 Users.ForEach(s => context.user.Add(s));
                 context.SaveChanges();
             }

             //群組
             var Roles = new List<roles>
             {
                 new roles { guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   title = "總管理", site="", rolenote = "",status="Y", modifydate = DateTime.Now ,create_date = DateTime.Now  },
                 new roles { guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   title = "查核人員", site="", rolenote = "",status="Y", modifydate = DateTime.Now ,create_date = DateTime.Now  },

             };
             if (context.roles.ToList().Count == 0)
             {
                 Roles.ForEach(s => context.roles.Add(s));
                 context.SaveChanges();
             }

             //群組權限
             var Role_permissions = new List<role_permissions>
             {
                 new role_permissions { role_guid = "cd836750-dfaf-4587-8236-f020fea0cb53",   permissions_guid = "AF126AAF-A167-4725-BA01-281D9DDAF934", permissions_status = "F", guid = Guid.Parse("00000000-0000-0000-0000-000000000000")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "9F09A958-5AA9-4315-A590-2147A3966FCD", permissions_status = "F", guid = Guid.Parse("E8750DC0-7FD1-46B7-BBFE-04228960C217")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "433E38F7-2FE3-4784-9787-243557E6A89E", permissions_status = "F", guid = Guid.Parse("B7BC1224-DBCB-408B-8A65-0D070CD89BF3")  },
                 new role_permissions { role_guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   permissions_guid = "9F09A958-5AA9-4315-A590-2147A3966FCD", permissions_status = "F", guid = Guid.Parse("0F511AD3-C005-40C5-90FA-23ADD2457F5B")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "DC64C91E-B9A6-45DB-A285-0D3DC37D7C84", permissions_status = "F", guid = Guid.Parse("BB64CE33-FC73-42DB-A9D3-249BCAF1EBBD")  },
                 new role_permissions { role_guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   permissions_guid = "F6E78285-C7F6-45FB-9708-9636D017570C", permissions_status = "F", guid = Guid.Parse("B9202D95-7C97-4C11-A985-2FF6727A9F9F")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "96FC5DD0-CCC7-4ECC-9749-0E32056C25A9", permissions_status = "F", guid = Guid.Parse("66AD7E73-87DF-4E04-A6DE-53921FF49D95")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "6CB42205-6E48-4301-AD10-4DBB628F12A1", permissions_status = "F", guid = Guid.Parse("4D0419AF-FDFB-4A8F-BF6F-57E1C3EFEFB3")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "2369C9AD-F291-45FD-963E-1A31FD4DA8F7", permissions_status = "F", guid = Guid.Parse("935C7B04-ADEB-49E6-8674-58F58D05B8B8")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "ABED3072-52E7-4733-A85E-C7B8B19A4658", permissions_status = "F", guid = Guid.Parse("329F8063-0A22-4F4D-86E1-7F9A8D9831DB")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "BBFB5DA8-F47C-4FF3-A7FC-7D7DD9B8613A", permissions_status = "F", guid = Guid.Parse("293FEB7B-14EF-4CBF-B81C-85482FB1F841")  },
                 new role_permissions { role_guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   permissions_guid = "54307B50-7189-4FFF-8AE3-7AD885FE3E29", permissions_status = "F", guid = Guid.Parse("7AC90982-2554-47E7-9719-C799639EBEE1")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "EB3C5C45-1921-4B8D-8434-41B692EA9F75", permissions_status = "F", guid = Guid.Parse("B6F2B5C5-D3FC-480F-BCCA-F058FC34510A")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "F67F3C1B-0262-4D6A-AF99-D62F5ABB2ACA", permissions_status = "F", guid = Guid.Parse("54982587-2732-48D7-9388-F3F26DCE6F08")  },
                 new role_permissions { role_guid = "c0d924b9-e52f-4280-9ab9-aad722626cff",   permissions_guid = "ABED3072-52E7-4733-A85E-C7B8B19A4658", permissions_status = "F", guid = Guid.Parse("7A397B2A-6413-4488-91D7-F95FE6D2D021")  },
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "E72C63AD-EB05-49AA-A3AE-C3C18517C993", permissions_status = "F", guid = Guid.Parse("D532AE73-53DC-4E29-8EDB-FE07FF71432A")  },

                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "2E019597-6B11-4E25-95D8-F9667085AEE0", permissions_status = "F", guid = Guid.NewGuid()},
                 new role_permissions { role_guid = "da66d7ed-6bce-42ef-ac85-6773ff6a95dc",   permissions_guid = "F5326200-B462-426B-88DB-5C9C072696C7", permissions_status = "F", guid = Guid.NewGuid()},

             };

             if (context.role_permissions.ToList().Count == 0)
             {
                 Role_permissions.ForEach(s => context.role_permissions.Add(s));
                 context.SaveChanges();
             }
             */
            /*
           //信件樣板
           var Mail_contents = new List<mail_contents>
           {
             new mail_contents { guid = "1",  lang="tw" ,title="忘記密碼",content="<!DOCTYPE html><html><head><meta charset=\"utf-8\" /><title></title></head><body><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#f5f5f5; width:100%\"><tbody><tr><td><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:700px\"><tbody><tr><td> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin-top:35px; width:680px\"><tbody><tr><td><h1>{[websiteName]}</h1></td><td style=\"text-align:right\"><span style=\"font-size:18px\"><strong>{[title]}</strong></span></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-bottom:1px solid #b4b4b4; border-left:1px solid #cecece; border-radius:7px; border-right:1px solid #cecece; border-top:1px solid #e1e1e1; margin:15px 0 15px 0; padding:0px; width:100%\"><tbody><tr><td style=\"text-align:right\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin:15px auto; width:650px\"><tbody><tr><td style=\"text-align:left\"><div style=\"display:block; font-size:18px; padding-bottom:15px; padding-top:15px\"><strong>親愛的 <span style=\"color:#125773\">{[cName]}</span> ，您好</strong></div><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; width:100%\">&nbsp;</div><p>您有一封 {[websiteName]} - {[title]} 通知。</p><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; width:100%\">&nbsp;</div><p>日期：{[date]}</p>{[Content]}<p>謝謝您對{[websiteName]}的支持<br />敬祝 順利 平安</p><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; margin-top:25px; width:100%\">&nbsp;</div></td></tr></tbody></table></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-bottom:1px solid #b4b4b4; border-left:1px solid #cecece; border-radius:7px; border-right:1px solid #cecece; border-top:1px solid #e1e1e1; margin:15px 0 35px 0; padding:0px; width:100%\"><tbody><tr><td style=\"text-align:right\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin:15px auto; width:650px\"><tbody><tr><td style=\"text-align:center\"><div style=\"display:block; font-size:16px\"><span style=\"color:#c0392b\"><strong>本信件為系統自動發出，請勿直接回覆！</strong></span></div><div style=\"display:block; font-size:12px; text-align:center\"><span style=\"color:#666666; font-family:Tahoma,Geneva,sans-serif; font-size:12px\">Copyright &copy; <a href=\"{$websiteUrl}\">{[websiteName]}</a>, All rights reserved.</span></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></body></html>" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
             new mail_contents { guid = "2",  lang="tw" ,title="會員啟用信件",content="<!DOCTYPE html><html><head><meta charset=\"utf-8\" /><title></title></head><body><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#f5f5f5; width:100%\"><tbody><tr><td><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:700px\"><tbody><tr><td> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin-top:35px; width:680px\"><tbody><tr><td><h1>{[websiteName]}</h1></td><td style=\"text-align:right\"><span style=\"font-size:18px\"><strong>{[title]}</strong></span></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-bottom:1px solid #b4b4b4; border-left:1px solid #cecece; border-radius:7px; border-right:1px solid #cecece; border-top:1px solid #e1e1e1; margin:15px 0 15px 0; padding:0px; width:100%\"><tbody><tr><td style=\"text-align:right\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin:15px auto; width:650px\"><tbody><tr><td style=\"text-align:left\"><div style=\"display:block; font-size:18px; padding-bottom:15px; padding-top:15px\"><strong>親愛的 <span style=\"color:#125773\">{[cName]}</span> ，您好</strong></div><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; width:100%\">&nbsp;</div><p>您有一封 {[websiteName]} - {[title]} 通知。</p><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; width:100%\">&nbsp;</div><p>日期：{[date]}</p>{[Content]}<p>謝謝您對{[websiteName]}的支持<br />敬祝 順利 平安</p><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; margin-top:25px; width:100%\">&nbsp;</div></td></tr></tbody></table></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-bottom:1px solid #b4b4b4; border-left:1px solid #cecece; border-radius:7px; border-right:1px solid #cecece; border-top:1px solid #e1e1e1; margin:15px 0 35px 0; padding:0px; width:100%\"><tbody><tr><td style=\"text-align:right\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin:15px auto; width:650px\"><tbody><tr><td style=\"text-align:center\"><div style=\"display:block; font-size:16px\"><span style=\"color:#c0392b\"><strong>本信件為系統自動發出，請勿直接回覆！</strong></span></div><div style=\"display:block; font-size:12px; text-align:center\"><span style=\"color:#666666; font-family:Tahoma,Geneva,sans-serif; font-size:12px\">Copyright &copy; <a href=\"{$websiteUrl}\">{[websiteName]}</a>, All rights reserved.</span></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></body></html>" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
             new mail_contents { guid = "1",  lang="en" ,title="忘記密碼",content="<!DOCTYPE html><html><head><meta charset=\"utf-8\" /><title></title></head><body><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#f5f5f5; width:100%\"><tbody><tr><td><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:700px\"><tbody><tr><td> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin-top:35px; width:680px\"><tbody><tr><td><h1>{[websiteName]}</h1></td><td style=\"text-align:right\"><span style=\"font-size:18px\"><strong>{[title]}</strong></span></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-bottom:1px solid #b4b4b4; border-left:1px solid #cecece; border-radius:7px; border-right:1px solid #cecece; border-top:1px solid #e1e1e1; margin:15px 0 15px 0; padding:0px; width:100%\"><tbody><tr><td style=\"text-align:right\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin:15px auto; width:650px\"><tbody><tr><td style=\"text-align:left\"><div style=\"display:block; font-size:18px; padding-bottom:15px; padding-top:15px\"><strong>親愛的 <span style=\"color:#125773\">{[cName]}</span> ，您好</strong></div><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; width:100%\">&nbsp;</div><p>您有一封 {[websiteName]} - {[title]} 通知。</p><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; width:100%\">&nbsp;</div><p>日期：{[date]}</p>{[Content]}<p>謝謝您對{[websiteName]}的支持<br />敬祝 順利 平安</p><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; margin-top:25px; width:100%\">&nbsp;</div></td></tr></tbody></table></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-bottom:1px solid #b4b4b4; border-left:1px solid #cecece; border-radius:7px; border-right:1px solid #cecece; border-top:1px solid #e1e1e1; margin:15px 0 35px 0; padding:0px; width:100%\"><tbody><tr><td style=\"text-align:right\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin:15px auto; width:650px\"><tbody><tr><td style=\"text-align:center\"><div style=\"display:block; font-size:16px\"><span style=\"color:#c0392b\"><strong>本信件為系統自動發出，請勿直接回覆！</strong></span></div><div style=\"display:block; font-size:12px; text-align:center\"><span style=\"color:#666666; font-family:Tahoma,Geneva,sans-serif; font-size:12px\">Copyright &copy; <a href=\"{$websiteUrl}\">{[websiteName]}</a>, All rights reserved.</span></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></body></html>" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
             new mail_contents { guid = "2",  lang="en" ,title="會員啟用信件",content="<!DOCTYPE html><html><head><meta charset=\"utf-8\" /><title></title></head><body><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#f5f5f5; width:100%\"><tbody><tr><td><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:700px\"><tbody><tr><td> <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin-top:35px; width:680px\"><tbody><tr><td><h1>{[websiteName]}</h1></td><td style=\"text-align:right\"><span style=\"font-size:18px\"><strong>{[title]}</strong></span></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-bottom:1px solid #b4b4b4; border-left:1px solid #cecece; border-radius:7px; border-right:1px solid #cecece; border-top:1px solid #e1e1e1; margin:15px 0 15px 0; padding:0px; width:100%\"><tbody><tr><td style=\"text-align:right\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin:15px auto; width:650px\"><tbody><tr><td style=\"text-align:left\"><div style=\"display:block; font-size:18px; padding-bottom:15px; padding-top:15px\"><strong>親愛的 <span style=\"color:#125773\">{[cName]}</span> ，您好</strong></div><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; width:100%\">&nbsp;</div><p>您有一封 {[websiteName]} - {[title]} 通知。</p><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; width:100%\">&nbsp;</div><p>日期：{[date]}</p>{[Content]}<p>謝謝您對{[websiteName]}的支持<br />敬祝 順利 平安</p><div style=\"border-left:none; border-right:none; border-top:dashed #cccccc 1px; margin-top:25px; width:100%\">&nbsp;</div></td></tr></tbody></table></td></tr></tbody></table><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#ffffff; border-bottom:1px solid #b4b4b4; border-left:1px solid #cecece; border-radius:7px; border-right:1px solid #cecece; border-top:1px solid #e1e1e1; margin:15px 0 35px 0; padding:0px; width:100%\"><tbody><tr><td style=\"text-align:right\"><table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"swe-header\" style=\"margin:15px auto; width:650px\"><tbody><tr><td style=\"text-align:center\"><div style=\"display:block; font-size:16px\"><span style=\"color:#c0392b\"><strong>本信件為系統自動發出，請勿直接回覆！</strong></span></div><div style=\"display:block; font-size:12px; text-align:center\"><span style=\"color:#666666; font-family:Tahoma,Geneva,sans-serif; font-size:12px\">Copyright &copy; <a href=\"{$websiteUrl}\">{[websiteName]}</a>, All rights reserved.</span></div></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></body></html>" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


           };
           if (context.mail_contents.ToList().Count == 0)
           {
               Mail_contents.ForEach(s => context.mail_contents.Add(s));
               context.SaveChanges();
           }
           */

            /*
             //選單
             var System_menu = new List<system_menu>
             {


                 new system_menu { guid = "7E9F3B64-5090-48E9-8256-104A2D9232BB", title = "關於長庚", category = "0",tables = "",sortindex=1,status="Y",act_path="",icon="icon-sphere",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "宗旨與理念", category = "7E9F3B64-5090-48E9-8256-104A2D9232BB",tables = "abouts",sortindex=1,status="Y",act_path="list/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "大事記", category = "7E9F3B64-5090-48E9-8256-104A2D9232BB",tables = "historys",sortindex=2,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "大事記細項", category = "7E9F3B64-5090-48E9-8256-104A2D9232BB",tables = "historys_data",sortindex=2,status="N",act_path="list",icon="",area="",category_table="historys",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "服務概況", category = "7E9F3B64-5090-48E9-8256-104A2D9232BB",tables = "about_overview",sortindex=3,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "服務概況細項", category = "7E9F3B64-5090-48E9-8256-104A2D9232BB",tables = "about_overview_data",sortindex=5,status="N",act_path="list",icon="",area="",category_table="about_overview",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "病人安全", category = "7E9F3B64-5090-48E9-8256-104A2D9232BB",tables = "abouts",sortindex=4,status="Y",act_path="list/2",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },



                 new system_menu { guid = "1FEFC89F-A289-4D66-98B6-E22375266C4C", title = "就醫指南", category = "0",tables = "",sortindex=2,status="Y",act_path="",icon="icon-file-text",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "", },




                 new system_menu { guid = "D6049E75-227B-4A05-B9D0-15C79116FBD2", title = "廣告模組", category = "0",tables = "",sortindex=3,status="N",act_path="",icon="icon-map",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C", title = "控制台", category = "0",tables = "",sortindex=1,status="Y",act_path="",icon="icon-cog",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},
                 new system_menu { guid = "2F9FD9B5-B5D5-416E-91D9-7F97EF63BF3B", title = "帳戶資訊", category = "0",tables = "",sortindex=2,status="Y",act_path="",icon="icon-person_pin",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = "2372C0B8-33C5-4A7B-93DB-08D23611A3B9", title = "系統整合", category = "0",tables = "",sortindex=3,status="N",act_path="",icon="icon-handshake-o",area="system",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "門診", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "medical_guide",sortindex=1,status="Y",act_path="list/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "急診", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "medical_guide",sortindex=2,status="Y",act_path="list/2",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "住院", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "medical_guide",sortindex=3,status="Y",act_path="list/3",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "轉診就醫流程", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "medical_guide",sortindex=4,status="Y",act_path="list/4",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "",},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "文件申請", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "medical_guide",sortindex=5,status="Y",act_path="list/5",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "檢驗/檢查", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "medical_other",sortindex=6,status="Y",act_path="list/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "常見問題", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "qas",sortindex=7,status="Y",act_path="list",icon="",area="",category_table="news_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "收費標準", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "medical_other",sortindex=8,status="Y",act_path="list/2",icon="",area="",category_table="news_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },


                 new system_menu { guid = Guid.NewGuid().ToString(), title = "相關連結", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "medical_other_data",sortindex=999,status="N",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "常見問題內容", category = "1FEFC89F-A289-4D66-98B6-E22375266C4C",tables = "qas_data",sortindex=999,status="N",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },


                 new system_menu { guid = "ABED3072-52E7-4733-A85E-C7B8B19A4658", title = "帳戶管理", category = "2F9FD9B5-B5D5-416E-91D9-7F97EF63BF3B",tables = "user",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="roles",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},
                 new system_menu { guid = "9F09A958-5AA9-4315-A590-2147A3966FCD", title = "群組管理", category = "2F9FD9B5-B5D5-416E-91D9-7F97EF63BF3B",tables = "roles",sortindex=2,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},
                 new system_menu { guid = "F67F3C1B-0262-4D6A-AF99-D62F5ABB2ACA", title = "網站基本資訊", category = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C",tables = "web_data",sortindex=1,status="Y",act_path="edit/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},
                 new system_menu { guid = "E72C63AD-EB05-49AA-A3AE-C3C18517C993", title = "SMTP資料管理", category = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C",tables = "smtp_data",sortindex=2,status="Y",act_path="edit/b9732bfe-238d-4e4e-9114-8e6d30c34022",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = "433E38F7-2FE3-4784-9787-243557E6A89E", title = "資源回收桶", category = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C",tables = "ashcan",sortindex=90,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "N",can_del="Y",prev_table = "", },

                  new system_menu { guid = "2E019597-6B11-4E25-95D8-F9667085AEE0", title = "防火牆設定", category = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C",tables = "firewalls",sortindex=91,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},
                  new system_menu { guid = "F5326200-B462-426B-88DB-5C9C072696C7", title = "系統日誌", category = "E8CB7F71-F98E-4CCE-93B5-DFB957F8EC0C",tables = "system_log",sortindex=92,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "N",can_del="Y" ,prev_table = "",},

                 new system_menu { guid = "37c85623-3ad2-43af-82cf-0f5d5c5868bf", title = "模組管理", category = "0",tables = "",sortindex=99,status="Y",act_path="",icon="icon-filter",area="admin",category_table="",index_view_url="", can_add = "Y" ,can_edit = "N",can_del="Y",prev_table = "", },



                 new system_menu { guid = "4795dabf-18de-490e-9bb2-d57b4d99c127", title = "系統參數", category = "37c85623-3ad2-43af-82cf-0f5d5c5868bf",tables = "system_data",sortindex=1,status="Y",act_path="edit/4795dabf-18de-490e-9bb2-d57b4d99c127",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "",},


                 new system_menu { guid = "A3C4D491-8C06-45D7-9DF1-72A44A5CD91F", title = "院區管理", category = "0",tables = "",sortindex=3,status="Y",act_path="",icon="icon-office",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "組織架構", category = "A3C4D491-8C06-45D7-9DF1-72A44A5CD91F",tables = "notes_data",sortindex=1,status="Y",act_path="edit/1",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "院區類別", category = "A3C4D491-8C06-45D7-9DF1-72A44A5CD91F",tables = "hospital_category",sortindex=2,status="N",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "院區資料", category = "A3C4D491-8C06-45D7-9DF1-72A44A5CD91F",tables = "hospitals",sortindex=3,status="Y",act_path="list",icon="",area="",category_table="hospital_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "院區醫療諮詢", category = "A3C4D491-8C06-45D7-9DF1-72A44A5CD91F",tables = "hospitals_advisory",sortindex=4,status="N",act_path="list",icon="",area="",category_table="hospital_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },



                 new system_menu { guid = "96A9D124-B1CA-48F6-B550-6160171940B4", title = "研究教學", category = "0",tables = "",sortindex=5,status="Y",act_path="",icon="icon-book2",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "研究成果", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "res_results",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "研究資源", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "res_resource",sortindex=2,status="Y",act_path="list",icon="",area="",category_table="res_resource",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "研究資源相關連結", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "res_resource_data",sortindex=2,status="N",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "服務諮詢", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "notes_data",sortindex=3,status="Y",act_path="edit/2",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "教育培訓", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "edu_train",sortindex=4,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "教學資源", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "edu_resource",sortindex=5,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "教學資源單位", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "edu_resource_data",sortindex=99,status="N",act_path="list",icon="",area="",category_table="edu_resource",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "各院區醫教業務洽詢單位", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "edu_service",sortindex=6,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "申請與查詢", category = "96A9D124-B1CA-48F6-B550-6160171940B4",tables = "app_inquiry",sortindex=7,status="Y",act_path="list",icon="",area="",category_table="app_inquiry_category",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = "B4AA7DAC-727A-47F2-A75F-C508C38B1020", title = "社會公益", category = "0",tables = "",sortindex=6,status="Y",act_path="",icon="icon-heart",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "公益活動", category = "B4AA7DAC-727A-47F2-A75F-C508C38B1020",tables = "public_welfare",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "長庚社福資訊", category = "B4AA7DAC-727A-47F2-A75F-C508C38B1020",tables = "notes_data",sortindex=2,status="Y",act_path="edit/3",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "社會服務", category = "B4AA7DAC-727A-47F2-A75F-C508C38B1020",tables = "social_service",sortindex=3,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },



                 new system_menu { guid = "112148B8-B1A3-4FE7-B4D9-574B7503802A", title = "網站資訊", category = "0",tables = "",sortindex=99,status="Y",act_path="",icon="icon-flag3",area="modules",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y" ,prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "人才招募資訊", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "recruits",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "薪資與福利", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "recruits_data",sortindex=2,status="N",act_path="list",icon="",area="",category_table="recruits",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "職缺院區資訊", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "recruits_hospitals",sortindex=3,status="N",act_path="list",icon="",area="",category_table="recruits",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },


                 new system_menu { guid = Guid.NewGuid().ToString(), title = "各入口頁面資訊管理", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "index_info",sortindex=1,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "入口其他資訊", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "index_info_data",sortindex=2,status="N",act_path="list",icon="",area="",category_table="recruits",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "服務滿意度問卷調查", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "notes_data",sortindex=1,status="Y",act_path="edit/4",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N",prev_table = "", },

                 new system_menu { guid = Guid.NewGuid().ToString(), title = "附屬網站", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "subsidiary_web",sortindex=4,status="Y",act_path="list/1",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "友善連結", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "subsidiary_web",sortindex=5,status="Y",act_path="list/2",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "員工專區", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "staff_area",sortindex=6,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },
                 new system_menu { guid = Guid.NewGuid().ToString(), title = "首頁上板公告", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "notes_data",sortindex=8,status="Y",act_path="edit/6",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N",prev_table = "", },

                  new system_menu { guid = Guid.NewGuid().ToString(), title = "隱私權聲明", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "notes_data",sortindex=8,status="Y",act_path="edit/5",icon="",area="",category_table="",index_view_url="", can_add = "N" ,can_edit = "Y",can_del="N",prev_table = "", },

                   new system_menu { guid = Guid.NewGuid().ToString(), title = "特色醫療中心", category = "112148B8-B1A3-4FE7-B4D9-574B7503802A",tables = "cm_list",sortindex=10,status="Y",act_path="list",icon="",area="",category_table="",index_view_url="", can_add = "Y" ,can_edit = "Y",can_del="Y",prev_table = "", },

             };
             if (context.system_menu.ToList().Count == 0)
             {
                 System_menu.ForEach(s => context.system_menu.Add(s));
                 context.SaveChanges();
             }*/
            //特色醫療中心
            var Cm_list = new List<cm_list>
            {
                  new cm_list { guid = "1",  lang="tw" , sortIndex=1 ,title="健診中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"桃園長庚\",\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

                  new cm_list { guid = "2",  lang="tw" , sortIndex=2 ,title="美容醫學中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"桃園長庚\",\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

                  new cm_list { guid = "3",  lang="tw" , sortIndex=3 ,title="癌症中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"林口長庚\",\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "4",  lang="tw" , sortIndex=4 ,title="質子暨放射治療中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "5",  lang="tw" , sortIndex=5 ,title="屈光雷射矯正中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "6",  lang="tw" , sortIndex=6 ,title="生殖醫學中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "7",  lang="tw" , sortIndex=7 ,title="骨震波治療中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "8",  lang="tw" , sortIndex=8 ,title="睡眠醫學中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "9",  lang="tw" , sortIndex=9 ,title="腦中風中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "10",  lang="tw" , sortIndex=10 ,title="器官移植中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "11",  lang="tw" , sortIndex=11 ,title="顱顏中心",content="[{\"key\":\"title\",\"val\":[\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

                  new cm_list { guid = "12",  lang="tw" , sortIndex=12 ,title="兒童過敏氣喘中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "13",  lang="tw" , sortIndex=13 ,title="高壓氧治療中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"桃園長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "14",  lang="tw" , sortIndex=14 ,title="正子中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"林口長庚\",\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "15",  lang="tw" , sortIndex=15 ,title="外傷急重症中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "16",  lang="tw" , sortIndex=16 ,title="胎兒醫學中心",content="[{\"key\":\"title\",\"val\":\"台北長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "17",  lang="tw" , sortIndex=17 ,title="失智症中心",content="[{\"key\":\"title\",\"val\":[\"桃園長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "18",  lang="tw" , sortIndex=18 ,title="心臟衰竭中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "19",  lang="tw" , sortIndex=19 ,title="藥物過敏中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "20",  lang="tw" , sortIndex=20 ,title="血友病及血栓防治中心",content="[{\"key\":\"title\",\"val\":[\"嘉義長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "21",  lang="tw" , sortIndex=21 ,title="慢性腎病防治中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "22",  lang="tw" , sortIndex=22 ,title="近視防治中心",content="[{\"key\":\"title\",\"val\":[\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "23",  lang="tw" , sortIndex=23 ,title="免疫腫瘤學卓越中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "24",  lang="tw" , sortIndex=24 ,title="臨床毒物中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "25",  lang="tw" , sortIndex=25 ,title="人工耳蝸暨聽語診療中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

                    new cm_list { guid = "1",  lang="en" , sortIndex=1 ,title="健診中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"桃園長庚\",\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

                  new cm_list { guid = "2",  lang="en" , sortIndex=2 ,title="美容醫學中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"桃園長庚\",\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

                  new cm_list { guid = "3",  lang="en" , sortIndex=3 ,title="癌症中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"林口長庚\",\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "4",  lang="en" , sortIndex=4 ,title="質子暨放射治療中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "5",  lang="en" , sortIndex=5 ,title="屈光雷射矯正中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "6",  lang="en" , sortIndex=6 ,title="生殖醫學中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "7",  lang="en" , sortIndex=7 ,title="骨震波治療中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "8",  lang="en" , sortIndex=8 ,title="睡眠醫學中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "9",  lang="en" , sortIndex=9 ,title="腦中風中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"台北長庚\",\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "10",  lang="en" , sortIndex=10 ,title="器官移植中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "11",  lang="en" , sortIndex=11 ,title="顱顏中心",content="[{\"key\":\"title\",\"val\":[\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

                  new cm_list { guid = "12",  lang="en" , sortIndex=12 ,title="兒童過敏氣喘中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "13",  lang="en" , sortIndex=13 ,title="高壓氧治療中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"桃園長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "14",  lang="en" , sortIndex=14 ,title="正子中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\",\"林口長庚\",\"嘉義長庚\",\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\",\"#\",\"#\",\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "15",  lang="en" , sortIndex=15 ,title="外傷急重症中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "16",  lang="en" , sortIndex=16 ,title="胎兒醫學中心",content="[{\"key\":\"title\",\"val\":\"[台北長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "17",  lang="en" , sortIndex=17 ,title="失智症中心",content="[{\"key\":\"title\",\"val\":[\"桃園長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "18",  lang="en" , sortIndex=18 ,title="心臟衰竭中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "19",  lang="en" , sortIndex=19 ,title="藥物過敏中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "20",  lang="en" , sortIndex=20 ,title="血友病及血栓防治中心",content="[{\"key\":\"title\",\"val\":[\"嘉義長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "21",  lang="en" , sortIndex=21 ,title="慢性腎病防治中心",content="[{\"key\":\"title\",\"val\":[\"基隆長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "22",  lang="en" , sortIndex=22 ,title="近視防治中心",content="[{\"key\":\"title\",\"val\":[\"高雄長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "23",  lang="en" , sortIndex=23 ,title="免疫腫瘤學卓越中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "24",  lang="en" , sortIndex=24 ,title="臨床毒物中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                  new cm_list { guid = "25",  lang="en" , sortIndex=25 ,title="人工耳蝸暨聽語診療中心",content="[{\"key\":\"title\",\"val\":[\"林口長庚\"]},{\"key\":\"url\",\"val\":[\"#\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },



            };
            if (context.cm_list.ToList().Count == 0)
            {
                Cm_list.ForEach(s => context.cm_list.Add(s));
                context.SaveChanges();
            }

            /*
            //服務概況
            var About_overview = new List<about_overview>
            {
              new about_overview { guid = "1",  lang="tw" , sortIndex=1 ,title="醫療服務人次",content="[{\"key\":\"X\",\"val\":[\"門診\",\"急診\",\"住院\"]},{\"key\":\"Color\",\"val\":[\"#F38826\",\"#90B64C\",\"#40BFCB\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview { guid = "2",  lang="tw" , sortIndex=2 ,title="門診人次趨勢圖",content="[{\"key\":\"X\",\"val\":[\"林口台北\",\"桃園\",\"基隆\",\"嘉義\",\"雲林\",\"高雄\",\"鳳山\"]},{\"key\":\"Color\",\"val\":[\"#F38826\",\"#DFB67\",\"#90B64C\",\"#30B7C3\",\"#63AAD6\",\"#A261D5\",\"#E36DBA\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview { guid = "3",  lang="tw" , sortIndex=3 ,title="急診人次趨勢圖",content="[{\"key\":\"X\",\"val\":[\"林口台北\",\"基隆\",\"嘉義\",\"雲林\",\"高雄\",\"鳳山\"]},{\"key\":\"Color\",\"val\":[\"#F38826\",\"#90B64C\",\"#30B7C3\",\"#63AAD6\",\"#A261D5\",\"#E36DBA\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview { guid = "4",  lang="tw" , sortIndex=4 ,title="住院人次趨勢圖",content="[{\"key\":\"X\",\"val\":[\"林口台北\",\"桃園\",\"基隆\",\"嘉義\",\"雲林\",\"高雄\",\"鳳山\"]},{\"key\":\"Color\",\"val\":[\"#F38826\",\"#DFB67\",\"#90B64C\",\"#30B7C3\",\"#63AAD6\",\"#A261D5\",\"#E36DBA\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview { guid = "1",  lang="en" , sortIndex=1 ,title="醫療服務人次",content="[{\"key\":\"X\",\"val\":[\"門診\",\"急診\",\"住院\"]},{\"key\":\"Color\",\"val\":[\"#F38826\",\"#90B64C\",\"#40BFCB\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview { guid = "2",  lang="en" , sortIndex=2 ,title="門診人次趨勢圖",content="[{\"key\":\"X\",\"val\":[\"林口台北\",\"桃園\",\"基隆\",\"嘉義\",\"雲林\",\"高雄\",\"鳳山\"]},{\"key\":\"Color\",\"val\":[\"#F38826\",\"#DFB67\",\"#90B64C\",\"#30B7C3\",\"#63AAD6\",\"#A261D5\",\"#E36DBA\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview { guid = "3",  lang="en" , sortIndex=3 ,title="急診人次趨勢圖",content="[{\"key\":\"X\",\"val\":[\"林口台北\",\"基隆\",\"嘉義\",\"雲林\",\"高雄\",\"鳳山\"]},{\"key\":\"Color\",\"val\":[\"#F38826\",\"#90B64C\",\"#30B7C3\",\"#63AAD6\",\"#A261D5\",\"#E36DBA\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview { guid = "4",  lang="en" , sortIndex=4 ,title="住院人次趨勢圖",content="[{\"key\":\"X\",\"val\":[\"林口台北\",\"桃園\",\"基隆\",\"嘉義\",\"雲林\",\"高雄\",\"鳳山\"]},{\"key\":\"Color\",\"val\":[\"#F38826\",\"#DFB67\",\"#90B64C\",\"#30B7C3\",\"#63AAD6\",\"#A261D5\",\"#E36DBA\"]}]" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


            };
            if (context.about_overview.ToList().Count == 0)
            {
                About_overview.ForEach(s => context.about_overview.Add(s));
                context.SaveChanges();
            }


            //服務概況(細項)
            var About_overview_data = new List<about_overview_data>
            {
              new about_overview_data { guid = "1",  lang="tw"  ,title="2011" ,category="1",content="710,150,80" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "2",  lang="tw"  ,title="2012" ,category="1",content="820,160,83" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "3",  lang="tw"  ,title="2013" ,category="1",content="930,100,86" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "4",  lang="tw"  ,title="2014" ,category="1",content="935,120,90" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "5",  lang="tw"  ,title="2015" ,category="1",content="950,120,95" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new about_overview_data { guid = "6",  lang="tw"  ,title="2011" ,category="2",content="320,60,80,70,10,230,30" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "7",  lang="tw"  ,title="2012" ,category="2",content="330,70,90,80,15,245,35" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "8",  lang="tw"  ,title="2013" ,category="2",content="345,80,100,90,20,250,40" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "9",  lang="tw"  ,title="2014" ,category="2",content="350,90,110,100,25,255,45" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "10",  lang="tw"  ,title="2015" ,category="2",content="345,80,100,90,15,250,40" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


              new about_overview_data { guid = "11",  lang="tw"  ,title="2011" ,category="3",content="24,7,8,2,14,1" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "12",  lang="tw"  ,title="2012" ,category="3",content="23,8,9,3,13,2" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "13",  lang="tw"  ,title="2013" ,category="3",content="21,7,8,2,12,1" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "14",  lang="tw"  ,title="2014" ,category="3",content="22,8,9,3,13,2" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "15",  lang="tw"  ,title="2015" ,category="3",content="21,7,8,2,12,1" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


              new about_overview_data { guid = "16",  lang="tw"  ,title="2011" ,category="4",content="12.5,1,2.3,3,0.9,7.2,0.5" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "17",  lang="tw"  ,title="2012" ,category="4",content="13,1.2,2.1,3.6,0.8,7.5,0.5" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "18",  lang="tw"  ,title="2013" ,category="4",content="12.7,1.5,2.1,3.6,0.8,7.5,0.6" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "19",  lang="tw"  ,title="2014" ,category="4",content="13.5,1.3,2.2,3.9,0.8,7,0.5" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "20",  lang="tw"  ,title="2015" ,category="4",content="13.6,1,2.1,3.8,0.7,7.7,0.2" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new about_overview_data { guid = "1",  lang="en"  ,title="2011" ,category="1",content="710,150,80" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "2",  lang="en"  ,title="2012" ,category="1",content="820,160,83" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "3",  lang="en"  ,title="2013" ,category="1",content="930,100,86" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "4",  lang="en"  ,title="2014" ,category="1",content="935,120,90" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "5",  lang="en"  ,title="2015" ,category="1",content="950,120,95" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new about_overview_data { guid = "6",  lang="en"  ,title="2011" ,category="2",content="320,60,80,70,10,230,30" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "7",  lang="en"  ,title="2012" ,category="2",content="330,70,90,80,15,245,35" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "8",  lang="en"  ,title="2013" ,category="2",content="345,80,100,90,20,250,40" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "9",  lang="en"  ,title="2014" ,category="2",content="350,90,110,100,25,255,45" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "10",  lang="en"  ,title="2015" ,category="2",content="345,80,100,90,15,250,40" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


              new about_overview_data { guid = "11",  lang="en"  ,title="2011" ,category="3",content="24,7,8,2,14,1" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "12",  lang="en"  ,title="2012" ,category="3",content="23,8,9,3,13,2" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "13",  lang="en"  ,title="2013" ,category="3",content="21,7,8,2,12,1" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "14",  lang="en"  ,title="2014" ,category="3",content="22,8,9,3,13,2" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "15",  lang="en"  ,title="2015" ,category="3",content="21,7,8,2,12,1" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


              new about_overview_data { guid = "16",  lang="en"  ,title="2011" ,category="4",content="12.5,1,2.3,3,0.9,7.2,0.5" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "17",  lang="en"  ,title="2012" ,category="4",content="13,1.2,2.1,3.6,0.8,7.5,0.5" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "18",  lang="en"  ,title="2013" ,category="4",content="12.7,1.5,2.1,3.6,0.8,7.5,0.6" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "19",  lang="en"  ,title="2014" ,category="4",content="13.5,1.3,2.2,3.9,0.8,7,0.5" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new about_overview_data { guid = "20",  lang="en"  ,title="2015" ,category="4",content="13.6,1,2.1,3.8,0.7,7.7,0.2" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


            };
            if (context.about_overview_data.ToList().Count == 0)
            {
                About_overview_data.ForEach(s => context.about_overview_data.Add(s));
                context.SaveChanges();
            }


            /*
            //附屬網站&友善連結
            var Subsidiary_web = new List<subsidiary_web>
            {
              new subsidiary_web { guid = "1",  lang="tw" , sortIndex=1 , category="1",title="產學合作中心", url="https://www2.cgmh.org.tw/intr/01p00/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "2",  lang="tw" , sortIndex=2 , category="1",title="臨床試驗中心", url="https://www1.cgmh.org.tw/intr/intr2/c3s400/index.html"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "3",  lang="tw" , sortIndex=3 , category="1",title="人體試驗倫理委員會", url="https://www1.cgmh.org.tw/intr/intr1/c0040/web/C/C.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "4",  lang="tw" , sortIndex=4 , category="1",title="遺傳諮詢中心", url="https://www1.cgmh.org.tw/chldhos/intr/c4ad0/1.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "5",  lang="tw" , sortIndex=5 , category="1",title="優生保健中心", url="https://www1.cgmh.org.tw/mombaby/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "6",  lang="tw" , sortIndex=6 , category="1",title="早產防治網", url="https://www1.cgmh.org.tw/prpr/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "7",  lang="tw" , sortIndex=7 , category="1",title="家庭暴力防治中心", url="https://www1.cgmh.org.tw/wmchld/Home/index.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "8",  lang="tw" , sortIndex=8 , category="1",title="林口實證醫學中心", url="https://www1.cgmh.org.tw/intr/intr2/ebmlink/index.asp"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "9",  lang="tw" , sortIndex=9 , category="1",title="嘉義實證醫學中心", url="https://www1.cgmh.org.tw/intr/intr5/ebmjia/index.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "10",  lang="tw" , sortIndex=10 , category="1",title="醫療擴增實境研究中心", url="https://www1.cgmh.org.tw/mar/index.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "11",  lang="tw" , sortIndex=11 , category="1",title="癌症分子診斷檢驗室", url="https://www1.cgmh.org.tw/intr/intr2/cgmh-p/index.asp?PID=3s240"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "1",  lang="en" , sortIndex=1 , category="1",title="產學合作中心", url="https://www2.cgmh.org.tw/intr/01p00/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "2",  lang="en" , sortIndex=2 , category="1",title="臨床試驗中心", url="https://www1.cgmh.org.tw/intr/intr2/c3s400/index.html"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "3",  lang="en" , sortIndex=3 , category="1",title="人體試驗倫理委員會", url="https://www1.cgmh.org.tw/intr/intr1/c0040/web/C/C.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "4",  lang="en" , sortIndex=4 , category="1",title="遺傳諮詢中心", url="https://www1.cgmh.org.tw/chldhos/intr/c4ad0/1.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "5",  lang="en" , sortIndex=5 , category="1",title="優生保健中心", url="https://www1.cgmh.org.tw/mombaby/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "6",  lang="en" , sortIndex=6 , category="1",title="早產防治網", url="https://www1.cgmh.org.tw/prpr/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "7",  lang="en" , sortIndex=7 , category="1",title="家庭暴力防治中心", url="https://www1.cgmh.org.tw/wmchld/Home/index.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "8",  lang="en" , sortIndex=8 , category="1",title="林口實證醫學中心", url="https://www1.cgmh.org.tw/intr/intr2/ebmlink/index.asp"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "9",  lang="en" , sortIndex=9 , category="1",title="嘉義實證醫學中心", url="https://www1.cgmh.org.tw/intr/intr5/ebmjia/index.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "10",  lang="en" , sortIndex=10 , category="1",title="醫療擴增實境研究中心", url="https://www1.cgmh.org.tw/mar/index.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new subsidiary_web { guid = "11",  lang="en" , sortIndex=11 , category="1",title="癌症分子診斷檢驗室", url="https://www1.cgmh.org.tw/intr/intr2/cgmh-p/index.asp?PID=3s240"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new subsidiary_web { guid = "12",  lang="tw" , sortIndex=1 , category="2",title="長庚大學", url="http://www.cgu.edu.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "13",  lang="tw" , sortIndex=2 , category="2",title="長庚科技大學", url="http://www.cgit.edu.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "14",  lang="tw" , sortIndex=3 , category="2",title="汎航通運", url="https://www1.cgmh.org.tw/frms/frms1.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "15",  lang="tw" , sortIndex=4 , category="2",title="長庚生物科技", url="http://www.cgb.com.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "16",  lang="tw" , sortIndex=5 , category="2",title="長庚醫學科技", url="http://www.cgmc.com.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "17",  lang="tw" , sortIndex=6 , category="2",title="台塑企業", url="http://www.fpg.com.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new subsidiary_web { guid = "12",  lang="en" , sortIndex=1 , category="2",title="長庚大學", url="http://www.cgu.edu.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "13",  lang="en" , sortIndex=2 , category="2",title="長庚科技大學", url="http://www.cgit.edu.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "14",  lang="en" , sortIndex=3 , category="2",title="汎航通運", url="https://www1.cgmh.org.tw/frms/frms1.htm"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "15",  lang="en" , sortIndex=4 , category="2",title="長庚生物科技", url="http://www.cgb.com.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "16",  lang="en" , sortIndex=5 , category="2",title="長庚醫學科技", url="http://www.cgmc.com.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new subsidiary_web { guid = "17",  lang="en" , sortIndex=6 , category="2",title="台塑企業", url="http://www.fpg.com.tw/"   , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

            };
            if (context.subsidiary_web.ToList().Count == 0)
            {
                Subsidiary_web.ForEach(s => context.subsidiary_web.Add(s));
                context.SaveChanges();
            }


            /*
            //職缺院區資訊
            var Recruits_hospitals = new List<recruits_hospitals>
            {
                new recruits_hospitals { guid = "1",  lang="tw" , sortIndex=1,category="1" ,title="基隆院區",name="林妙蕙",tel="02–24313131分機2511",email="lin3555@cgmh.org.tw",address="基隆市麥金路 222 號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "2",  lang="tw" , sortIndex=2,category="1" ,title="林口院區",name="曾雅婷",tel="03–3281200分機2810",email="alu660109@cgmh.org.tw",address="桃園縣龜山鄉復興街 5 號",sp_url="",job_url="",pic="images/02-service/team/2.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "3",  lang="tw" , sortIndex=3,category="1" ,title="桃園院區",name="高薏茹",tel="03-3196200分機3336",email="amykao@adm.cgmh.org.tw",address="桃園縣龜山鄉舊路村頂湖路123號",sp_url="",job_url="",pic="images/02-service/team/3.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "4",  lang="tw" , sortIndex=4,category="1" ,title="嘉義院區",name="郭秋雅",tel="05-3621000分機3956",email="choya@cgmh.org.tw",address="嘉義縣朴子市嘉朴路西段 6 號",sp_url="",job_url="",pic="images/02-service/team/4.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "5",  lang="tw" , sortIndex=5,category="1" ,title="雲林院區",name="蔡麗琴",tel="05–6915151分機2887",email="chin0916@cgmh.org.tw",address="雲林縣麥寮鄉三盛村工業路707號",sp_url="",job_url="images/02-service/team/5.jpg",pic=""  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "6",  lang="tw" , sortIndex=6,category="1" ,title="高雄院區",name="陳美琴",tel="07–7317123分機2824",email="clairechen@cgmh.org.tw",address="高雄縣鳥松鄉大埤路 123 號",sp_url="",job_url="",pic="images/02-service/team/6.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

                new recruits_hospitals { guid = "1",  lang="en" , sortIndex=1,category="1" ,title="基隆院區",name="林妙蕙",tel="02–24313131分機2511",email="lin3555@cgmh.org.tw",address="基隆市麥金路 222 號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "2",  lang="en" , sortIndex=2,category="1" ,title="林口院區",name="曾雅婷",tel="03–3281200分機2810",email="alu660109@cgmh.org.tw",address="桃園縣龜山鄉復興街 5 號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "3",  lang="en" , sortIndex=3,category="1" ,title="桃園院區",name="高薏茹",tel="03-3196200分機3336",email="amykao@adm.cgmh.org.tw",address="桃園縣龜山鄉舊路村頂湖路123號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "4",  lang="en" , sortIndex=4,category="1" ,title="嘉義院區",name="郭秋雅",tel="05-3621000分機3956",email="choya@cgmh.org.tw",address="嘉義縣朴子市嘉朴路西段 6 號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "5",  lang="en" , sortIndex=5,category="1" ,title="雲林院區",name="蔡麗琴",tel="05–6915151分機2887",email="chin0916@cgmh.org.tw",address="雲林縣麥寮鄉三盛村工業路707號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "6",  lang="en" , sortIndex=6,category="1" ,title="高雄院區",name="陳美琴",tel="07–7317123分機2824",email="clairechen@cgmh.org.tw",address="高雄縣鳥松鄉大埤路 123 號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


                new recruits_hospitals { guid = "7",  lang="tw" , sortIndex=1,category="2" ,title="基隆院區",name="林妙蕙",tel="02–24313131分機2511",email="lin3555@cgmh.org.tw",address="基隆市麥金路 222 號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "8",  lang="tw" , sortIndex=2,category="2" ,title="林口院區",name="曾雅婷",tel="03–3281200分機2810",email="alu660109@cgmh.org.tw",address="桃園縣龜山鄉復興街 5 號",sp_url="",job_url="",pic="images/02-service/team/2.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "9",  lang="tw" , sortIndex=3,category="2" ,title="桃園院區",name="高薏茹",tel="03-3196200分機3336",email="amykao@adm.cgmh.org.tw",address="桃園縣龜山鄉舊路村頂湖路123號",sp_url="",job_url="",pic="images/02-service/team/3.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "10",  lang="tw" , sortIndex=4,category="2" ,title="嘉義院區",name="郭秋雅",tel="05-3621000分機3956",email="choya@cgmh.org.tw",address="嘉義縣朴子市嘉朴路西段 6 號",sp_url="",job_url="",pic="images/02-service/team/4.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "11",  lang="tw" , sortIndex=5,category="2" ,title="雲林院區",name="蔡麗琴",tel="05–6915151分機2887",email="chin0916@cgmh.org.tw",address="雲林縣麥寮鄉三盛村工業路707號",sp_url="",job_url="",pic="images/02-service/team/5.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "12",  lang="tw" , sortIndex=6,category="2" ,title="高雄院區",name="陳美琴",tel="07–7317123分機2824",email="clairechen@cgmh.org.tw",address="高雄縣鳥松鄉大埤路 123 號",sp_url="",job_url="",pic="images/02-service/team/6.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new recruits_hospitals { guid = "7",  lang="en" , sortIndex=1,category="2" ,title="基隆院區",name="林妙蕙",tel="02–24313131分機2511",email="lin3555@cgmh.org.tw",address="基隆市麥金路 222 號",sp_url="",job_url="",pic="images/02-service/team/1.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "8",  lang="en" , sortIndex=2,category="2" ,title="林口院區",name="曾雅婷",tel="03–3281200分機2810",email="alu660109@cgmh.org.tw",address="桃園縣龜山鄉復興街 5 號",sp_url="",job_url="",pic="images/02-service/team/2.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "9",  lang="en" , sortIndex=3,category="2" ,title="桃園院區",name="高薏茹",tel="03-3196200分機3336",email="amykao@adm.cgmh.org.tw",address="桃園縣龜山鄉舊路村頂湖路123號",sp_url="",job_url="",pic="images/02-service/team/3.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "10",  lang="en" , sortIndex=4,category="2" ,title="嘉義院區",name="郭秋雅",tel="05-3621000分機3956",email="choya@cgmh.org.tw",address="嘉義縣朴子市嘉朴路西段 6 號",sp_url="",job_url="",pic="images/02-service/team/4.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "11",  lang="en" , sortIndex=5,category="2" ,title="雲林院區",name="蔡麗琴",tel="05–6915151分機2887",email="chin0916@cgmh.org.tw",address="雲林縣麥寮鄉三盛村工業路707號",sp_url="",job_url="",pic="images/02-service/team/5.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
                new recruits_hospitals { guid = "12",  lang="en" , sortIndex=6,category="2" ,title="高雄院區",name="陳美琴",tel="07–7317123分機2824",email="clairechen@cgmh.org.tw",address="高雄縣鳥松鄉大埤路 123 號",sp_url="",job_url="",pic="images/02-service/team/6.jpg"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

            };
            if (context.recruits_hospitals.ToList().Count == 0)
            {
                Recruits_hospitals.ForEach(s => context.recruits_hospitals.Add(s));
                context.SaveChanges();
            }
            */


            /*
          //各入口頁面資訊管理
          var Index_info = new List<index_info>
          {
              new index_info { guid = "1",  lang="tw" , sortIndex=1,title="醫療服務", subject="科際合作，提升服務" ,notes="有鑑於科際整合治療的重要性，本院於八十七年七月起開辦聯合門診，由各科主治醫師聯合診察，以提升服務品質，目前已經成立的有：頭頸部腫瘤聯合門診、乳房腫瘤聯合門診、糖尿病聯合門診、骨質疏鬆聯合門診…等。", category="1"  , content="<section class=\"index__team layout__container-full bg-grey\"><div class=\"layout__container-1400 por\"><div class=\"layout__page-title\"><h3 class=\"fz-32 fwb\">醫療團隊</h3><p class=\"fz-20\">醫院是直接面對人的服務，本院對於醫護人員的訓練不僅重視專業能力，也強調醫德的培養。讓每位病患都能獲得最全面、最完整的照顧。</p></div><a href=\"/tw/Services/Team\" class=\"btn__round-transparent\">了解更多 </a></div><div class=\"slider\"><ul class=\"ul-reset slick-triple slick-arrow-round\"><li><div class=\"imgLiquidFill\"><img src=\"/images/02-service/index/1.jpg\" alt=\"\" srcset=\"\"></div></li><li><div class=\"imgLiquidFill\"><img src=\"/images/02-service/index/2.jpg\" alt=\"\" srcset=\"\"></div></li><li><div class=\"imgLiquidFill\"><img src=\"/images/02-service/index/3.jpg\" alt=\"\" srcset=\"\"></div></li><li><div class=\"imgLiquidFill\"><img src=\"/images/02-service/index/1.jpg\" alt=\"\" srcset=\"\"></div></li></ul></div></section>",content2="<ul class=\"index__guide layout__block-left-right ul-reset\"><li><div class=\"layout__container-1400\"><div class=\"block__text\"><h3 class=\"fz-36 fwb\">就醫指南 </h3><p class=\"fz-20\">病人與醫療團隊 充分合作是疾病治療成功的重要因素之一，醫病雙方瞭解病人的權利與義務，有助於良好的醫病溝通增進雙方合作與互信。</p><a href=\"/tw/Research/Index\" class=\"btn__round-transparent\">了解更多 </a></div></div><div class=\"bgimage imgLiquidFill\"><img src=\"/images/02-service/index/12.jpg\" alt=\"\"></div></li></ul>",content3="<section class=\"layout__container-1400 pd__section\"><div class=\"layout__page-title\"><h3 class=\"fz-32 fwb\">線上服務</h3><p class=\"fz-20\">提供病人病情相關資訊供醫護人員參考，並配合執行診療，善用有限的醫療資源，<br>共同維護醫療環境與全體人員健康。</p></div><div class=\"index__grid\"><div class=\"col\"><div class=\"col__child col-lg\"><a href=\"/tw/Services/Login\" class=\"child\"><div class=\"text\"><h4 class=\"fz-24 fwb\">網站會員</h4><p class=\"fz-17\">加入長庚網站會員，您將可以<br>得到完整的網站服務。</p><div class=\"btn__round-transparent\">了解更多 </div></div><div class=\"bgimage imgLiquidFill\"><img src=\"/images/02-service/index/4.jpg\" alt=\"\"></div></a></div><div class=\"col__child\"><a href=\"/tw/Services/Guardian\" class=\"child\"><div class=\"text\"><h4 class=\"fz-24 fwb\">衛教園地</h4><p class=\"fz-17\">衛教類文章搜尋。</p></div><div class=\"bgimage imgLiquidFill\"><img src=\"/images/02-service/index/5.jpg\" alt=\"\"></div></a><a href=\"/tw/Services/Advisory\" class=\"child\"><div class=\"text\"><h4 class=\"fz-24 fwb\">醫療諮詢</h4><p class=\"fz-17\">各院區相關服務<br>台諮詢資訊。</p></div><div class=\"bgimage imgLiquidFill\"><img src=\"/images/02-service/index/6.jpg\" alt=\"\"></div></a></div></div><div class=\"col\"><div class=\"col__child col-lg\"><a href=\"/tw/Services/Drug\" class=\"child\"><div class=\"text\"><h4 class=\"fz-24 fwb\">藥品綜合查詢</h4><p class=\"fz-17\">藥品詳細介紹，對應症狀以<br>及副作用反應</p><div class=\"btn__round-transparent\">了解更多 </div></div><div class=\"bgimage imgLiquidFill\"><img src=\"/images/02-service/index/8.jpg\" alt=\"\"></div></a></div><div class=\"col__child\"><a href=\"/tw/Services/Consultation\" class=\"child\"><div class=\"text\"><h4 class=\"fz-24 fwb\">用藥諮詢</h4><p class=\"fz-17\">這是一個免費提供民眾線上用藥<br>諮詢的園地，由本院各院區藥師<br>諮詢團隊駐站諮詢。</p><div class=\"btn__circle\"><i class=\"icon-plus\"></i></div></div><div class=\"bgimage imgLiquidFill\"><img src=\"/images/02-service/index/14.jpg\" alt=\"\"></div></a><a href=\"/tw/Services/ConsultationContact\" class=\"child\"><div class=\"text\"><h4 class=\"fz-24 fwb\">我要發問</h4><p class=\"fz-17\">立刻詢問相關<br>用藥資訊。</p></div><div class=\"bgimage imgLiquidFill\"><img src=\"/images/02-service/index/7.jpg\" alt=\"\"></div></a></div></div><div class=\"col col-fix\"><div class=\"col__child\"><a href=\"/tw/Services/Questionnaire\" class=\"child\"><div class=\"text\"><h4 class=\"fz-24 fwb\">服務滿意度問卷調查</h4><p class=\"fz-17\">為提高醫療品質，創造更好的環境。</p><div class=\"btn__circle\"><i class=\"icon-plus\"></i></div></div><div class=\"bgimage\"><div class=\"imgLiquidFill\"><img src=\"/images/02-service/index/11.jpg\" alt=\"\"></div></div></a></div><div class=\"col__child\"><a href=\"/tw/Services/DrugGuide\" class=\"child\"><div class=\"text\"><h4 class=\"fz-24 fwb\">用藥指導單張</h4><p class=\"fz-17\">藥品使用須知。</p></div><div class=\"bgimage imgLiquidFill fix-before\"><img src=\"/images/02-service/index/10.jpg\" alt=\"\"></div></a></div></div></div></section>"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new index_info { guid = "2",  lang="tw" , sortIndex=2,title="研究教學", subject="一點一滴，追根究底" ,notes="長庚醫研部支持的研究來自各獨立研究主持人的個別型計畫或研究團隊的整合型計畫。目前許多長庚的研究計畫是受到核心實驗室資源的幫助。已建立一個生物銀行做為容納研究中捐獻者們的組織及體液標本。提供做臨床試驗研究的中心也已經成立。醫研部也支援動物照護及使用委員會以及人體試驗倫理委員會和智慧財產權等。", category="1"  , content="<section class=\"index__innovation layout__container-full\"><ul class=\"layout__block-img-lr ul-reset\"><li><div class=\"bgimage imgLiquidFill\"><img src=\"/images/03-research/index/5.jpg\" alt=\"\"></div><div class=\"layout__container-1400\"><div class=\"text\"><h3 class=\"fz-36 fwb\">創新研究</h3><div class=\"fz-20\">一點一滴、追根究底、止於至善，<br>追求卓越，要做就做最好的！</div><a href=\"/tw/Research/Innovation\" class=\"btn__round-transparent\">了解更多 </a></div></div></li></ul></section>",content2="<section class=\"index__edu layout__container-1400 tac\"><h3 class=\"fz-36 fwb\">醫學教育</h3><div class=\"fz-18\">本院每年培育醫師 及醫事人員超過 3,000 人,持續為 提升醫學教育、研究及醫療服務水 準做出重要貢獻。</div><div class=\"blocks\"><a href=\"/tw/Research/Education#train\" class=\"blocks__block\"><div class=\"wrap\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/03-research/index/6.jpg\" alt=\"\"></div><h4 class=\"fz-24 fwb\">教育培訓</h4><div class=\"btn\"><i class=\"icon-plus\"></i></div></div></a><a href=\"/tw/Research/Education#resource\" class=\"blocks__block\"><div class=\"wrap\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/03-research/index/7.jpg\" alt=\"\"></div><h4 class=\"fz-24 fwb\">教學資源</h4><div class=\"btn\"><i class=\"icon-plus\"></i></div></div></a><a href=\"/tw/Research/Education#service\" class=\"blocks__block\"><div class=\"wrap\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/03-research/index/8.jpg\" alt=\"\"></div><h4 class=\"fz-24 fwb\">服務諮詢</h4><div class=\"btn\"><i class=\"icon-plus\"></i></div></div></a></div></section>",content3="<section class=\"index__search layout__container-full tac\"><a href=\"/tw/Research/Activity\" class=\"block\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/03-research/index/9.jpg\" alt=\"\"></div><div class=\"text\"><h4 class=\"fz-32 fwb\">會議與活動</h4><div class=\"fz-20\">研討會、學術研究的第一手詳細資訊！</div></div><div class=\"btn__round-transparent fz-17 fwb\">了解更多 </div></a><a href=\"/tw/Research/Search\" class=\"block\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/03-research/index/10.jpg\" alt=\"\"></div><div class=\"text\"><h4 class=\"fz-32 fwb\">申請與查詢</h4><div class=\"fz-20\">即時查閱各類研究計畫與送檢訊息！</div></div><div class=\"btn__round-transparent fz-17 fwb\">了解更多 </div></a></section>"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new index_info { guid = "3",  lang="tw" , sortIndex=3,title="長庚體系", subject="勤勞樸實，深耕生根" ,notes="本院規劃時係以國際大型醫院標準設計，除了購置各種精密儀器外，並細分各專科，從事更專業的醫療服務，此外更強調團隊醫療照顧，由醫師、護理人員、社會工作員、營養師、復健師等共同提供服務，以達到全人照護的理念，兼顧病患之生理、心理、社會各層面的照顧。", category="1"  , content="<section class=\"index__about layout__container-full bg-grey\"><div class=\"layout__textimg\"><div class=\"col\"><div class=\"col__img imgLiquidFill\"><img src=\"/images/05-system/1-1.jpg\" alt=\"\"></div><div class=\"col__text\"><h4 class=\"fz-36 fwb\">關於長庚 </h4><div class=\"fz-24\">服務、教學、研究</div><div class=\"fz-17\">我們必須與時俱進，永無止盡的謀求改善、再改善，以此奠定良好經營基礎，確保—「回饋社會」，「利益眾生」的事業可以永續發展，充分冠測醫療服務之職志。</div><div class=\"btns fz-17 fwb\"><a href=\"/tw/Systems/About\" class=\"btn__round-transparent\">了解更多</a></div></div></div></div></section>",content2="<section class=\"index__info layout__container-full\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/05-system/1-10.jpg\" alt=\"\"></div><div class=\"layout__page-title\"><h3 class=\"fz-36 fwb\">醫療體系</h3><div class=\"fz-18\">從事醫療事業，促進全民健康 ; 提供全人、團隊、延續性醫療照護。</div></div><div class=\"layout__container-1400\"><div class=\"block block-row\"><a href=\"/tw/Systems/Area\" class=\"col col-half\"><div class=\"col__img imgLiquidFill\"><img src=\"/images/05-system/1-6.jpg\" alt=\"\"></div><div class=\"col__text\"><h4 class=\"fz-24 fwb\">組織架構</h4><div class=\"fz-17\">培育卓越的醫護人員、提供病患最佳的醫療服務為目的</div></div></a><a href=\"system-area.php#area__intro\" class=\"col col-full\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/05-system/1-7.jpg\" alt=\"\"></div><div class=\"col__text\"><h4 class=\"fz-24 fwb\">院區簡介</h4><div class=\"fz-17\">聯絡資訊、規模、交通、官網</div><div href=\"/tw/Systems/Area#area__intro\" class=\"btn__round-transparent hide-phone-under\">了解更多</div></div></a></div><div class=\"block block-dec\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/05-system/1-8.jpg\" alt=\"\"></div></div></div></section>",content3="<section class=\"index__recruit layout__container-full tac\"><div class=\"bgimage imgLiquidFill\"><img src=\"/images/05-system/1-9.jpg\" alt=\"\"></div><div class=\"text\"><h4 class=\"fz-36 fwb\">人員招募</h4><div class=\"fz-24\">掌握自己、邁向卓越、共創未來!</div><div class=\"fz-17\">體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人生階段，如：繼續進修、結婚、生子等之工作地點變更需要。</div><a href=\"/tw/Systems/Recruit\" class=\"btn__round-transparent\">了解更多</a></div></section>"  , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },



          };
          if (context.index_info.ToList().Count == 0)
          {
              Index_info.ForEach(s => context.index_info.Add(s));
              context.SaveChanges();
          }



          //醫療諮詢
          var Hospitals_advisory = new List<hospitals_advisory>
          {
              new hospitals_advisory { guid = "1",  lang="tw" , category="1" ,title="服務台" , sortIndex=1, subject="掛號、繳費、住出院等醫療事務諮詢服務",ext="2102",service_time="09：00 ~ 16：30",area="一樓櫃檯區" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "2",  lang="tw" , category="1",title="精神科諮詢室", sortIndex=2, subject="精神科相關醫療諮詢及衛教服務，醫療事務諮詢服務",ext="2721",service_time="08：30 ~ 17：30",area="八樓精神科服務區" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "3",  lang="tw" , category="1",title="轉介服務櫃檯", sortIndex=3, subject="各類醫療諮詢服務，病人轉介諮詢服務",ext="2173",service_time="08：00 ~ 16：00",area="一樓轉介服務櫃檯" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "4",  lang="tw" , category="1",title="社會服務組", sortIndex=4, subject="社會福利諮詢及家庭暴力個案服務",ext="2028",service_time="08：30 ~ 17：00",area="地下室一樓" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "5",  lang="tw" , category="1",title="藥物諮詢室", sortIndex=5, subject="藥物諮詢及用藥指導服務",ext="2192",service_time="09：00 ~ 17：00",area="一樓大廳藥局" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "1",  lang="en" , category="1",title="服務台" , sortIndex=1, subject="掛號、繳費、住出院等醫療事務諮詢服務",ext="2102",service_time="09：00 ~ 16：30",area="一樓櫃檯區" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "2",  lang="en" , category="1",title="精神科諮詢室", sortIndex=2, subject="精神科相關醫療諮詢及衛教服務，醫療事務諮詢服務",ext="2721",service_time="08：30 ~ 17：30",area="八樓精神科服務區" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "3",  lang="en" , category="1",title="轉介服務櫃檯", sortIndex=3, subject="各類醫療諮詢服務，病人轉介諮詢服務",ext="2173",service_time="08：00 ~ 16：00",area="一樓轉介服務櫃檯" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "4",  lang="en" , category="1",title="社會服務組", sortIndex=4, subject="社會福利諮詢及家庭暴力個案服務",ext="2028",service_time="08：30 ~ 17：00",area="地下室一樓" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new hospitals_advisory { guid = "5",  lang="en" , category="1",title="藥物諮詢室", sortIndex=5, subject="藥物諮詢及用藥指導服務",ext="2192",service_time="09：00 ~ 17：00",area="一樓大廳藥局" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },



          };
          if (context.hospitals_advisory.ToList().Count == 0)
          {
              Hospitals_advisory.ForEach(s => context.hospitals_advisory.Add(s));
              context.SaveChanges();
          }


          //社會服務
          var Social_service = new List<social_service>
          {
              new social_service { guid = "1",  lang="tw" , sortIndex=1, pic="images/04-welfare/page2/2.jpg",title="病友團體",notes="戰勝疾病不孤單，同理、支持，迎光明 。",content="<p>佳和，有著一位共同奮鬥的老婆與一對可愛雙胞胎女兒，他曾想像著未來要如何經營一個幸 福的家庭，<br>最重要的就是牽著女兒的手步入禮堂以及與老婆一起白頭到老。</p><p>以為日子就這樣幸福美滿的下去，沒想到上天突然開了佳和一個玩笑，經檢查確定佳和罹患了鼻 咽癌。<br>治療過程他擔心未知的變化、擔心死神找上門、擔心未竟之事。</p><p>幸在旁人介紹下，佳和來到了長庚醫院癌症資源中心，並參加相關病友的活動，在這裡有相 同疾病的朋友相互支持鼓勵，<br>有醫療團隊提供疾病照顧、營養諮詢及各種紓壓管道，病友志工帶 領手工 DIY 活動等，在醫療團隊及家人的支持下，<br>佳和終完成了 6 次化學治療與 33 次放射治療 這條漫漫長路的治療過程。 </p><img src=\"images/04-welfare/page2/6.jpg\" alt=\"\"><p>本院為促進病患、家屬與醫療團隊間的良好互動，提升其對疾病的正確認知，以維持健康與 生活品質，<br>並提供病患間能有彼此正向經驗分享、相互支持鼓勵的機會，各院區陸續成立支持性 病友團體及舉辦各類醫療座談會，年度辦理約 700 多場。</p><div class=\"hr\"></div><p class=\"fz-20\">支持性團體一覽表 ：</p><div class=\"layout__tablewrap\"><table class=\"table fz-17\"><tr><th width=\"10%\" class=\"tac\">項次</th><th width=\"15%\"class=\"tac\">院區</th><th width=\"20%\">科別</th><th width=\"40%\">團體名稱</th><th width=\"15%\"class=\"tac\">成立時間</th></tr><tr><td class=\"tac\">1</td><td class=\"tac bdr\" rowspan=\"3\">基隆</td><td>呼吸胸腔科</td><td>基隆市呼吸健康協會 </td><td class=\"tac\">87.08</td></tr><tr><td class=\"tac\">2</td><td>新陳代謝科</td><td>基糖族病友聯誼會</td><td class=\"tac\">97.11</td></tr><tr><td class=\"tac\">3</td><td>心臟內科</td><td>心臟強健病友聯誼會</td><td class=\"tac\">100.07</td></tr><tr><td class=\"tac\">4</td><td class=\"tac bdr\" rowspan=\"16\">林口</td><td>泌尿科</td><td>基隆市呼吸健康協會 </td><td class=\"tac\">87.08</td></tr><tr><td class=\"tac\">5</td><td>新陳代謝科</td><td>林長好哇胰聯誼會(糖尿病友)</td><td class=\"tac\">80.05</td></tr><tr><td class=\"tac\">6</td><td>血液腫瘤科</td><td>骨髓移植病友聯誼會</td><td class=\"tac\">84.08</td></tr><tr><td rowspan=\"3\" class=\"tac\">7</td><td>血液腫瘤科</td><td rowspan=\"3\">林長好哇胰聯誼會(糖尿病友)</td><td rowspan=\"3\" class=\"tac\">87.09</td></tr><tr><td class=\"bdn\">一般外科</td></tr><tr><td class=\"bdn\">放射腫瘤科</td></tr><tr><td class=\"tac\">8</td><td>整形外科</td><td>台灣乳房重建協會 (原名福爾摩沙乳房重建協會)</td><td class=\"tac\">91.07</td></tr><tr><td class=\"tac\">9</td><td>風濕免疫科</td><td>彩虹之友聯誼會（紅斑性狼瘡病友）</td><td class=\"tac\">91.09</td></tr><tr><td class=\"tac\">10</td><td>放射腫瘤科</td><td>彼愛關懷聯誼會 （鼻咽癌、咽喉癌病友）</td><td class=\"tac\">99.08</td></tr><tr><td class=\"tac\">11</td><td>一般外科</td><td>彼愛關懷聯誼會 （鼻咽癌、咽喉癌病友）</td><td class=\"tac\">99.08</td></tr><tr><td class=\"tac\">12</td><td>兒童神經內科</td><td>跳躍的音符兒童發展協進會（癲癇）</td><td class=\"tac\">88.07</td></tr><tr><td class=\"tac\">13</td><td>兒童過敏氣喘風濕科</td><td>台灣兒童氣(棄)喘之友會</td><td class=\"tac\">90.12</td></tr><tr><td class=\"tac\">14</td><td>兒童神經內科</td><td>台灣妥瑞症協會</td><td class=\"tac\">91.05</td></tr><tr><td class=\"tac\">15</td><td>兒童血液腫瘤科</td><td>長頸鹿關懷聯誼會（兒癌病童）</td><td class=\"tac\">91.11</td></tr><tr><td class=\"tac\">16</td><td>兒童血液腫瘤科</td><td>期待再生聯誼會（臍帶血移植病友）</td><td class=\"tac\">95.10</td></tr><tr><td class=\"tac\">17</td><td>兒童內分泌科</td><td>台灣兒童糖尿病關懷協會</td><td class=\"tac\">95.11</td></tr><tr><td class=\"tac\">18</td><td class=\"tac bdr\" rowspan=\"4\">嘉義</td><td>新陳代謝科</td><td>糖小鴨俱樂部（第一型糖尿病）</td><td class=\"tac\">98.02</td></tr><tr><td class=\"tac\">19</td><td>新陳代謝科</td><td>糖糖俱樂部（第二型糖尿病）</td><td class=\"tac\">94.05</td></tr><tr><td class=\"tac\">20</td><td>泌尿科</td><td>珍愛生命聯誼會（腎移植）</td><td class=\"tac\">101.10</td></tr><tr><td class=\"tac\">21</td><td>一般外科</td><td>微笑關懷聯誼會(乳癌病友) </td><td class=\"tac\">102.12</td></tr><tr><td class=\"tac\">22</td><td class=\"tac bdr\" rowspan=\"10\">高雄</td><td>神經內科</td><td>聰動成長協會(神經退化性疾病)</td><td class=\"tac\">98.03</td></tr><tr><td class=\"tac\">23</td><td>腎臟科</td><td>腹膜透析腎友聯誼會</td><td class=\"tac\">99.05</td></tr><tr><td class=\"tac\">24</td><td>一般外科</td><td>攜手關懷聯誼會(乳癌)</td><td class=\"tac\">99.08</td></tr><tr><td class=\"tac\">25</td><td>兒童血液腫瘤科</td><td>劦愛病友會（兒癌）</td><td class=\"tac\">99.10</td></tr><tr><td class=\"tac\">26</td><td>婦產科</td><td>喜樂病友會（婦癌）</td><td class=\"tac\">99.11</td></tr><tr><td class=\"tac\">27</td><td>新陳代謝科</td><td>糖尿病友聯誼會</td><td class=\"tac\">99.12</td></tr><tr><td class=\"tac\">28</td><td>大腸直腸肛門外科</td><td>腸安會（直腸癌）</td><td class=\"tac\">99.12</td></tr><tr><td class=\"tac\">29</td><td>耳鼻喉科</td><td>笑口常開聯誼會（頭頸癌）</td><td class=\"tac\">100.05</td></tr><tr><td class=\"tac\">30</td><td>泌尿外科</td><td>胡桃聯誼會（泌尿腫瘤）</td><td class=\"tac\">100.12</td></tr><tr><td class=\"tac\">31</td><td>兒科</td><td>中華川崎症關懷協會（川崎症）</td><td class=\"tac\">100.11</td></tr></table></div><p>相關病友活動訊息，可至<span><a href=\"javascript:;\">社服活動</a></span>(社服活動連結網址)了解更多，或與各院區社服課聯繫。 </p><img src=\"images/04-welfare/page2/7.jpg\" alt=\"\">" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new social_service { guid = "2",  lang="tw" , sortIndex=2, pic="images/04-welfare/page2/3.jpg",title="志願服務",notes="志工心，服務情，熱情服務溫暖你心。",content="志工心，服務情，熱情服務溫暖你心。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new social_service { guid = "3",  lang="tw" , sortIndex=3, pic="images/04-welfare/page2/4.jpg",title="遺愛人間",notes="愛傳承，延長生命，開啟另一人生的樂章。",content="愛傳承，延長生命，開啟另一人生的樂章。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new social_service { guid = "4",  lang="tw" , sortIndex=4, pic="images/04-welfare/page2/5.jpg",title="社區服務",notes="在地人關心在地事，守護民眾健康是咱ㄟ責任。",content="在地人關心在地事，守護民眾健康是咱ㄟ責任。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


         };
          if (context.social_service.ToList().Count == 0)
          {
              Social_service.ForEach(s => context.social_service.Add(s));
              context.SaveChanges();
          }

          //公益活動
          var Public_welfare = new List<public_welfare>
          {
              new public_welfare { guid = "1",  lang="tw" , sortIndex=1 ,title="手握夢想不放手-「拔」出一片天",pic="images/04-welfare/page1/1.jpg",notes="多年來累積的專業運動醫學經驗，今年度開始推 廣拔河運動計畫，實際贊助拔河隊裝備並辦理訓 練營與比賽，結合本院醫療專業教導選手有關運 動防護課程。選手們的轉變與拔河堅韌故事，邀 請你我一同支持台灣拔河運動。",content="<p>一位媽媽領著甫將小學畢業的小女孩詢問了拔河隊教練：「教練，我女兒可以加入拔河隊嗎?」，<br>教練不假思索的回答：「可以」!便開啟了小女孩的拔河路，迎接而來的是一段辛苦的訓練，手掌反覆起水泡已是常有的事情，<br>有時甚至訓練到流血使拔河繩上沾染了血漬，但小女孩不怕苦，帶著勇敢以及滿腔的熱血繼續練習，用行動證明她愛拔河，而且想要繼續拔下去。</p><p>1997 年臺北市萬人拔河比賽發生震驚國內外的斷臂事件，社會各界均誤解拔河為異常危險的運動，造成拔河運動日益式微，<br>政府及民間單位對於拔河的推廣及贊助銳減。本院結合運動醫學專業及經費支持景美拔河隊迄今 4 年。<br>有感於拔河運動有別於其他運動，幾乎沒有個人明星，透過團隊合作展現團隊的精神，故於今年度開始向下扎根推廣拔河運動計畫，<br>希望能夠發揚拔河運動，使國人重視此項運動，同時也能建立拔河運動防護知能，預防運動傷害。</p><img class=\"dib\" src=\"images/04-welfare/page1/6.jpg\" alt=\"\"><img class=\"dib\" src=\"images/04-welfare/page1/7.jpg\" alt=\"\"><div class=\"hr\"></div><p>計畫目的<br><ul><li>贊助偏鄉基層學校拔河隊拔河鞋，並提供運動防護講座。</li><li>舉辦拔河訓練營，提升基層拔河隊技巧，及預防運動傷害。</li><li>舉辦永慶盃拔河比賽，推廣拔河運動，使國人重視拔河運動。</li></ul></p><p>計畫成果<br><ul><li>贊助偏遠地區國小、國中拔河隊計 4 間，提供拔河鞋每人每年 2 雙，計 96 雙。</li><li>贊助學校運動防護講座：每校每學年 2 場，計 8 場/年。</li><li>舉辦拔河訓練營，提升國內拔河隊技巧，及預防運動傷害，邀請國內基層學校拔河隊參加拔河訓練營，於長庚養生村舉辦 2 梯次(每梯次 5 天)，<br>共訓練 30 隊、388 人，訓練課程包括學科、術科及正確運動傷害防護觀念。</li><li>12/15-12/16 將舉辦永慶盃拔河比賽，推廣拔河運動，以國中小為參賽對象，舉辦校隊組(48 隊)及推廣組(48 隊)拔河比賽，並以院區為單位，<br>舉辦院內員工拔河賽，凝聚同仁向心力。</li></ul></p><img class=\"dib\" src=\"images/04-welfare/page1/8.jpg\" alt=\"\"><img class=\"dib\" src=\"images/04-welfare/page1/9.jpg\" alt=\"\"><p>相關媒體報導(可連結網址) :<br><ul><li><a href=\"javascript\">運動防護從小扎根 長庚醫院辦拔河訓練營 </a></li><li><a href=\"javascript\">《桃園》長庚醫院首辦拔河營 景美國手「愛相隨」</a></li><li><a href=\"javascript\">長庚拔河訓練營開訓 專業技能醫療防護並重 </a></li><li><a href=\"javascript\">長庚拔河訓練營 學生以繩會友圓滿落幕 </a></li><li><a href=\"javascript\">長庚 800 萬辦訓練營 大方贈國家隊球鞋強化運動防護 </a></li></ul></p><img src=\"images/04-welfare/page1/10.jpg\" alt=\"\"><p class=\"fwb\">拔河運動員回饋與感謝</p><img class=\"dib\" src=\"images/04-welfare/page1/11.jpg\" alt=\"\"><img class=\"dib\" src=\"images/04-welfare/page1/12.jpg\" alt=\"\">" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "2",  lang="tw" , sortIndex=2 ,title="運動扎根，成為運動明日之星推手",pic="images/04-welfare/page1/2.jpg",notes="長庚有感資金贊助對體育發展固然重要，但體育 選手更需要的是「運動傷害醫療」和「運動防護 員」的支援，因此成立運動醫學照護小組幫助選 手。除成為頂尖選手後盾外，更向下扎根，將過 去服務經驗，服務更多基層選手，栽培未來明日之星。",content="長庚有感資金贊助對體育發展固然重要，但體育 選手更需要的是「運動傷害醫療」和「運動防護 員」的支援，因此成立運動醫學照護小組幫助選 手。除成為頂尖選手後盾外，更向下扎根，將過 去服務經驗，服務更多基層選手，栽培未來明日之星。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "3",  lang="tw" , sortIndex=3 ,title="關懷偏鄉「宜」起來",pic="images/04-welfare/page1/3.jpg",notes="為以實際行動關心社會大眾健康，除長期營造鄰 近社區健康環境及養成健康習慣外，今年度帶領 專業醫療團隊，走入宜蘭偏鄉學校進行巡迴關懷 及協助體育班學童運動防護，透過本院醫療團隊 介入與關懷，孩童身體健康漸有改善並注重自我 衛生，長庚醫院邀請你我一同關心偏鄉學童!",content="為以實際行動關心社會大眾健康，除長期營造鄰 近社區健康環境及養成健康習慣外，今年度帶領 專業醫療團隊，走入宜蘭偏鄉學校進行巡迴關懷 及協助體育班學童運動防護，透過本院醫療團隊 介入與關懷，孩童身體健康漸有改善並注重自我 衛生，長庚醫院邀請你我一同關心偏鄉學童!" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "4",  lang="tw" , sortIndex=4 ,title="雲林好厝邊社區健康作伙來",pic="images/04-welfare/page1/4.jpg",notes="鑑於雲林地區醫療資源缺乏，為深耕雲林，照護 社區民眾，今年成立雲林社區健康中心，主動深 入社區健康評估，規劃各項篩檢、衛教健康講 座、偏鄉義診服務，並輔導社區推動健康營造， 達到提升當地居民健康和生活品質的目標。",content="於雲林地區醫療資源缺乏，為深耕雲林，照護 社區民眾，今年成立雲林社區健康中心，主動深 入社區健康評估，規劃各項篩檢、衛教健康講 座、偏鄉義診服務，並輔導社區推動健康營造， 達到提升當地居民健康和生活品質的目標。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "5",  lang="tw" , sortIndex=5 ,title="兒少保護「庚」要守護",pic="images/04-welfare/page1/5.jpg",notes="為保護國家未來主人翁，本院積極推動「兒少保 護計畫」，建構長庚醫療體系兒少保護防治及追 蹤機制，建置各項專業標準教材，已成立全國性 兒少保護訓練中心，並進一步與政府各兒少保護 機關合作，建立更完善的全國兒少保護體系和制 度，邀請大家一起成為兒少守護者。",content="為保護國家未來主人翁，本院積極推動「兒少保 護計畫」，建構長庚醫療體系兒少保護防治及追 蹤機制，建置各項專業標準教材，已成立全國性 兒少保護訓練中心，並進一步與政府各兒少保護 機關合作，建立更完善的全國兒少保護體系和制 度，邀請大家一起成為兒少守護者。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "1",  lang="en" , sortIndex=1 ,title="手握夢想不放手-「拔」出一片天",pic="images/04-welfare/page1/1.jpg",notes="多年來累積的專業運動醫學經驗，今年度開始推 廣拔河運動計畫，實際贊助拔河隊裝備並辦理訓 練營與比賽，結合本院醫療專業教導選手有關運 動防護課程。選手們的轉變與拔河堅韌故事，邀 請你我一同支持台灣拔河運動。",content="<p>一位媽媽領著甫將小學畢業的小女孩詢問了拔河隊教練：「教練，我女兒可以加入拔河隊嗎?」，<br>教練不假思索的回答：「可以」!便開啟了小女孩的拔河路，迎接而來的是一段辛苦的訓練，手掌反覆起水泡已是常有的事情，<br>有時甚至訓練到流血使拔河繩上沾染了血漬，但小女孩不怕苦，帶著勇敢以及滿腔的熱血繼續練習，用行動證明她愛拔河，而且想要繼續拔下去。</p><p>1997 年臺北市萬人拔河比賽發生震驚國內外的斷臂事件，社會各界均誤解拔河為異常危險的運動，造成拔河運動日益式微，<br>政府及民間單位對於拔河的推廣及贊助銳減。本院結合運動醫學專業及經費支持景美拔河隊迄今 4 年。<br>有感於拔河運動有別於其他運動，幾乎沒有個人明星，透過團隊合作展現團隊的精神，故於今年度開始向下扎根推廣拔河運動計畫，<br>希望能夠發揚拔河運動，使國人重視此項運動，同時也能建立拔河運動防護知能，預防運動傷害。</p><img class=\"dib\" src=\"images/04-welfare/page1/6.jpg\" alt=\"\"><img class=\"dib\" src=\"images/04-welfare/page1/7.jpg\" alt=\"\"><div class=\"hr\"></div><p>計畫目的<br><ul><li>贊助偏鄉基層學校拔河隊拔河鞋，並提供運動防護講座。</li><li>舉辦拔河訓練營，提升基層拔河隊技巧，及預防運動傷害。</li><li>舉辦永慶盃拔河比賽，推廣拔河運動，使國人重視拔河運動。</li></ul></p><p>計畫成果<br><ul><li>贊助偏遠地區國小、國中拔河隊計 4 間，提供拔河鞋每人每年 2 雙，計 96 雙。</li><li>贊助學校運動防護講座：每校每學年 2 場，計 8 場/年。</li><li>舉辦拔河訓練營，提升國內拔河隊技巧，及預防運動傷害，邀請國內基層學校拔河隊參加拔河訓練營，於長庚養生村舉辦 2 梯次(每梯次 5 天)，<br>共訓練 30 隊、388 人，訓練課程包括學科、術科及正確運動傷害防護觀念。</li><li>12/15-12/16 將舉辦永慶盃拔河比賽，推廣拔河運動，以國中小為參賽對象，舉辦校隊組(48 隊)及推廣組(48 隊)拔河比賽，並以院區為單位，<br>舉辦院內員工拔河賽，凝聚同仁向心力。</li></ul></p><img class=\"dib\" src=\"images/04-welfare/page1/8.jpg\" alt=\"\"><img class=\"dib\" src=\"images/04-welfare/page1/9.jpg\" alt=\"\"><p>相關媒體報導(可連結網址) :<br><ul><li><a href=\"javascript\">運動防護從小扎根 長庚醫院辦拔河訓練營 </a></li><li><a href=\"javascript\">《桃園》長庚醫院首辦拔河營 景美國手「愛相隨」</a></li><li><a href=\"javascript\">長庚拔河訓練營開訓 專業技能醫療防護並重 </a></li><li><a href=\"javascript\">長庚拔河訓練營 學生以繩會友圓滿落幕 </a></li><li><a href=\"javascript\">長庚 800 萬辦訓練營 大方贈國家隊球鞋強化運動防護 </a></li></ul></p><img src=\"images/04-welfare/page1/10.jpg\" alt=\"\"><p class=\"fwb\">拔河運動員回饋與感謝</p><img class=\"dib\" src=\"images/04-welfare/page1/11.jpg\" alt=\"\"><img class=\"dib\" src=\"images/04-welfare/page1/12.jpg\" alt=\"\">" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "2",  lang="en" , sortIndex=2 ,title="運動扎根，成為運動明日之星推手",pic="images/04-welfare/page1/2.jpg",notes="長庚有感資金贊助對體育發展固然重要，但體育 選手更需要的是「運動傷害醫療」和「運動防護 員」的支援，因此成立運動醫學照護小組幫助選 手。除成為頂尖選手後盾外，更向下扎根，將過 去服務經驗，服務更多基層選手，栽培未來明日之星。",content="長庚有感資金贊助對體育發展固然重要，但體育 選手更需要的是「運動傷害醫療」和「運動防護 員」的支援，因此成立運動醫學照護小組幫助選 手。除成為頂尖選手後盾外，更向下扎根，將過 去服務經驗，服務更多基層選手，栽培未來明日之星。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "3",  lang="en" , sortIndex=3 ,title="關懷偏鄉「宜」起來",pic="images/04-welfare/page1/3.jpg",notes="為以實際行動關心社會大眾健康，除長期營造鄰 近社區健康環境及養成健康習慣外，今年度帶領 專業醫療團隊，走入宜蘭偏鄉學校進行巡迴關懷 及協助體育班學童運動防護，透過本院醫療團隊 介入與關懷，孩童身體健康漸有改善並注重自我 衛生，長庚醫院邀請你我一同關心偏鄉學童!",content="為以實際行動關心社會大眾健康，除長期營造鄰 近社區健康環境及養成健康習慣外，今年度帶領 專業醫療團隊，走入宜蘭偏鄉學校進行巡迴關懷 及協助體育班學童運動防護，透過本院醫療團隊 介入與關懷，孩童身體健康漸有改善並注重自我 衛生，長庚醫院邀請你我一同關心偏鄉學童!" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "4",  lang="en" , sortIndex=4 ,title="雲林好厝邊社區健康作伙來",pic="images/04-welfare/page1/4.jpg",notes="鑑於雲林地區醫療資源缺乏，為深耕雲林，照護 社區民眾，今年成立雲林社區健康中心，主動深 入社區健康評估，規劃各項篩檢、衛教健康講 座、偏鄉義診服務，並輔導社區推動健康營造， 達到提升當地居民健康和生活品質的目標。",content="於雲林地區醫療資源缺乏，為深耕雲林，照護 社區民眾，今年成立雲林社區健康中心，主動深 入社區健康評估，規劃各項篩檢、衛教健康講 座、偏鄉義診服務，並輔導社區推動健康營造， 達到提升當地居民健康和生活品質的目標。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new public_welfare { guid = "5",  lang="en" , sortIndex=5 ,title="兒少保護「庚」要守護",pic="images/04-welfare/page1/5.jpg",notes="為保護國家未來主人翁，本院積極推動「兒少保 護計畫」，建構長庚醫療體系兒少保護防治及追 蹤機制，建置各項專業標準教材，已成立全國性 兒少保護訓練中心，並進一步與政府各兒少保護 機關合作，建立更完善的全國兒少保護體系和制 度，邀請大家一起成為兒少守護者。",content="為保護國家未來主人翁，本院積極推動「兒少保 護計畫」，建構長庚醫療體系兒少保護防治及追 蹤機制，建置各項專業標準教材，已成立全國性 兒少保護訓練中心，並進一步與政府各兒少保護 機關合作，建立更完善的全國兒少保護體系和制 度，邀請大家一起成為兒少守護者。" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

         };
          if (context.public_welfare.ToList().Count == 0)
          {
              Public_welfare.ForEach(s => context.public_welfare.Add(s));
              context.SaveChanges();
          }


          /*
         //申請與查詢
         var App_inquiry = new List<app_inquiry>
         {
              new app_inquiry { guid = "1",  lang="tw" , sortIndex=1,category="1",title="研究計畫申請",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "2",  lang="tw" , sortIndex=2,category="1",title="研究實驗室送檢服務",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "3",  lang="tw" , sortIndex=3,category="1",title="人體生物資料庫",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "4",  lang="tw" , sortIndex=4,category="1",title="長庚醫學研究資料",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "5",  lang="tw" , sortIndex=5,category="1",title="臨床試驗研究資訊",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new app_inquiry { guid = "6",  lang="tw" , sortIndex=6,category="2",title="研究計畫申請",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "7",  lang="tw" , sortIndex=7,category="2",title="研究實驗室送檢服務",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "8",  lang="tw" , sortIndex=8,category="2",title="人體生物資料庫",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "9",  lang="tw" , sortIndex=9,category="2",title="長庚醫學研究資料",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "10",  lang="tw" , sortIndex=10,category="2",title="臨床試驗研究資訊",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "11",  lang="tw" , sortIndex=11,category="2",title="人體生物資料庫",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new app_inquiry { guid = "1",  lang="en" , sortIndex=1,category="1",title="研究計畫申請",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "2",  lang="en" , sortIndex=2,category="1",title="研究實驗室送檢服務",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "3",  lang="en" , sortIndex=3,category="1",title="人體生物資料庫",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "4",  lang="en" , sortIndex=4,category="1",title="長庚醫學研究資料",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "5",  lang="en" , sortIndex=5,category="1",title="臨床試驗研究資訊",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new app_inquiry { guid = "6",  lang="en" , sortIndex=6,category="2",title="研究計畫申請",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "7",  lang="en" , sortIndex=7,category="2",title="研究實驗室送檢服務",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "8",  lang="en" , sortIndex=8,category="2",title="人體生物資料庫",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "9",  lang="en" , sortIndex=9,category="2",title="長庚醫學研究資料",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "10",  lang="en" , sortIndex=10,category="2",title="臨床試驗研究資訊",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry { guid = "11",  lang="en" , sortIndex=11,category="2",title="人體生物資料庫",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

       };
         if (context.app_inquiry.ToList().Count == 0)
         {
             App_inquiry.ForEach(s => context.app_inquiry.Add(s));
             context.SaveChanges();
         }




         //申請與查詢-類別
         var App_inquiry_category = new List<app_inquiry_category>
         {
              new app_inquiry_category { guid = "1",  lang="tw" , sortIndex=1,title="研究", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry_category { guid = "2",  lang="tw" , sortIndex=2,title="教學", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry_category { guid = "1",  lang="en" , sortIndex=1,title="研究", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new app_inquiry_category { guid = "2",  lang="en" , sortIndex=2,title="教學", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

       };
         if (context.app_inquiry_category.ToList().Count == 0)
         {
             App_inquiry_category.ForEach(s => context.app_inquiry_category.Add(s));
             context.SaveChanges();
         }



         //各院區醫教業務洽詢單位
         var Edu_service = new List<edu_service>
         {
              new edu_service { guid = "1",  lang="tw" , sortIndex=1,title="基隆長庚教學部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_service { guid = "2",  lang="tw" , sortIndex=2,title="林口長庚教學部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_service { guid = "3",  lang="tw" , sortIndex=3,title="桃園教研部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_service { guid = "4",  lang="tw" , sortIndex=4,title="嘉義長庚教學部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_service { guid = "5",  lang="tw" , sortIndex=5,title="高雄長庚教學部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new edu_service { guid = "1",  lang="en" , sortIndex=1,title="基隆長庚教學部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_service { guid = "2",  lang="en" , sortIndex=2,title="林口長庚教學部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_service { guid = "3",  lang="en" , sortIndex=3,title="桃園教研部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_service { guid = "4",  lang="en" , sortIndex=4,title="嘉義長庚教學部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_service { guid = "5",  lang="en" , sortIndex=5,title="高雄長庚教學部",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

       };
         if (context.edu_service.ToList().Count == 0)
         {
             Edu_service.ForEach(s => context.edu_service.Add(s));
             context.SaveChanges();
         }

         //教學資源單位
         var Edu_resource_data = new List<edu_resource_data>
         {
              new edu_resource_data { guid = "1",  lang="tw" , sortIndex=1,category="1",title="基隆長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new edu_resource_data { guid = "1",  lang="en" , sortIndex=1,category="1",title="基隆長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new edu_resource_data { guid = "2",  lang="tw" , sortIndex=2,category="1",title="林口長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "2",  lang="en" , sortIndex=2,category="1",title="林口長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new edu_resource_data { guid = "3",  lang="tw" , sortIndex=3,category="1",title="桃園長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "3",  lang="en" , sortIndex=3,category="1",title="桃園長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new edu_resource_data { guid = "4",  lang="tw" , sortIndex=4,category="1",title="嘉義長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new edu_resource_data { guid = "4",  lang="en" , sortIndex=4,category="1",title="嘉義長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new edu_resource_data { guid = "5",  lang="tw" , sortIndex=5,category="1",title="高雄長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new edu_resource_data { guid = "5",  lang="en" , sortIndex=5,category="1",title="高雄長庚圖書館",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


              new edu_resource_data { guid = "6",  lang="tw" , sortIndex=1,category="2",title="長庚醫訊",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "7",  lang="tw" , sortIndex=2,category="2",title="長庚學報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "8",  lang="tw" , sortIndex=3,category="2",title="林口分子轉譯影像中心電子報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "9",  lang="tw" , sortIndex=4,category="2",title="林口兒童過敏氣喘風濕電子報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "10",  lang="tw" , sortIndex=5,category="2",title="嘉義貴重儀器實驗室電子報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "11",  lang="tw" , sortIndex=6,category="2",title="Biomedical Journal",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "12",  lang="tw" , sortIndex=7,category="2",title="長庚醫訊",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "13",  lang="tw" , sortIndex=8,category="2",title="長庚藥學學報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },



              new edu_resource_data { guid = "6",  lang="en" , sortIndex=1,category="2",title="長庚醫訊",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "7",  lang="en" , sortIndex=2,category="2",title="長庚學報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "8",  lang="en" , sortIndex=3,category="2",title="林口分子轉譯影像中心電子報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "9",  lang="en" , sortIndex=4,category="2",title="林口兒童過敏氣喘風濕電子報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "10",  lang="en" , sortIndex=5,category="2",title="嘉義貴重儀器實驗室電子報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "11",  lang="en" , sortIndex=6,category="2",title="Biomedical Journal",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "12",  lang="en" , sortIndex=7,category="2",title="長庚醫訊",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource_data { guid = "13",  lang="en" , sortIndex=8,category="2",title="長庚藥學學報",url="javascript:;" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

       };
         if (context.edu_resource_data.ToList().Count == 0)
         {
             Edu_resource_data.ForEach(s => context.edu_resource_data.Add(s));
             context.SaveChanges();
         }



         //教學資源
         var Edu_resource = new List<edu_resource>
         {
              new edu_resource { guid = "1",  lang="tw" , sortIndex=1,title="各院區圖書館" , pic="images/03-research/education/4.jpg",pic_alt="各院區圖書館", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource { guid = "1",  lang="en" , sortIndex=1,title="各院區圖書館" , pic="images/03-research/education/4.jpg",pic_alt="各院區圖書館", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new edu_resource { guid = "2",  lang="tw" , sortIndex=2,title="體系期刊" , pic="images/03-research/education/5.jpg",pic_alt="體系期刊", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_resource { guid = "2",  lang="en" , sortIndex=2,title="體系期刊" , pic="images/03-research/education/5.jpg",pic_alt="體系期刊", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

         };
         if (context.edu_resource.ToList().Count == 0)
         {
             Edu_resource.ForEach(s => context.edu_resource.Add(s));
             context.SaveChanges();
         }


         //教育培訓
         var Edu_train = new List<edu_train>
         {
              new edu_train { guid = "1",  lang="tw" , sortIndex=1,title="醫師人員培育" , pic="images/03-research/education/1.jpg",pic_alt="醫師人員培育",content="本院培育之醫師類人員包括：各專科住院醫師、畢業後一般醫學訓練(PGY)學員、長庚大學及各建教合作醫學系(北醫、高醫、中國、中山、陽明、慈濟、義守、輔仁)之實習醫學生，每年訓練之人數超過1,500人。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_train { guid = "1",  lang="en" , sortIndex=1,title="醫師人員培育" , pic="images/03-research/education/1.jpg",pic_alt="醫師人員培育",content="本院培育之醫師類人員包括：各專科住院醫師、畢業後一般醫學訓練(PGY)學員、長庚大學及各建教合作醫學系(北醫、高醫、中國、中山、陽明、慈濟、義守、輔仁)之實習醫學生，每年訓練之人數超過1,500人。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new edu_train { guid = "2",  lang="tw" , sortIndex=2,title="國內外專業人員代訓" , pic="images/03-research/education/2.jpg",pic_alt="國內外專業人員代訓",content="本院為協助國內外醫療機構提升診療水準，每年接受委託代訓之專科醫師及醫事人員人數超過500人。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_train { guid = "2",  lang="en" , sortIndex=2,title="國內外專業人員代訓" , pic="images/03-research/education/2.jpg",pic_alt="國內外專業人員代訓",content="本院為協助國內外醫療機構提升診療水準，每年接受委託代訓之專科醫師及醫事人員人數超過500人。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


              new edu_train { guid = "3",  lang="tw" , sortIndex=3,title="醫事人員培育" , pic="images/03-research/education/3.jpg",pic_alt="醫事人員培育",content="本院培育之醫事類人員含括：護理、藥事、醫事放射、醫事檢驗、職能治療、物理治療、呼吸治療、聽力治療、語言治療、營養、臨床心理、諮商心理及牙體技術等十三個職類，每年訓練參與「衛生福利部臨床醫事人員培訓計畫」之新進醫事人員人數超過1,400人。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new edu_train { guid = "3",  lang="en" , sortIndex=3,title="醫事人員培育" , pic="images/03-research/education/3.jpg",pic_alt="醫事人員培育",content="本院培育之醫事類人員含括：護理、藥事、醫事放射、醫事檢驗、職能治療、物理治療、呼吸治療、聽力治療、語言治療、營養、臨床心理、諮商心理及牙體技術等十三個職類，每年訓練參與「衛生福利部臨床醫事人員培訓計畫」之新進醫事人員人數超過1,400人。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

       };
         if (context.edu_train.ToList().Count == 0)
         {
             Edu_train.ForEach(s => context.edu_train.Add(s));
             context.SaveChanges();
         }






         //研究資源相關連結
         var Res_resource_data = new List<res_resource_data>
         {
              new res_resource_data { guid = "1",  lang="tw" , sortIndex=1,category="1",title="依姓名查詢",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/name.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource_data { guid = "1",  lang="en" , sortIndex=1,category="1",title="依姓名查詢",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/name.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new res_resource_data { guid = "2",  lang="tw" , sortIndex=2,category="1",title="依領域查詢",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/specialist.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource_data { guid = "2",  lang="en" , sortIndex=2,category="1",title="依領域查詢",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/specialist.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new res_resource_data { guid = "3",  lang="tw" , sortIndex=3,category="1",title="依院區查詢",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/sub_main.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource_data { guid = "3",  lang="en" , sortIndex=3,category="1",title="依院區查詢",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/sub_main.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new res_resource_data { guid = "4",  lang="tw" , sortIndex=1,category="2",title="了解更多",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/resource1.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource_data { guid = "4",  lang="en" , sortIndex=1,category="2",title="了解更多",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/resource1.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

              new res_resource_data { guid = "5",  lang="tw" , sortIndex=1,category="3",title="了解更多",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/researchcenter.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource_data { guid = "5",  lang="en" , sortIndex=1,category="3",title="了解更多",url="https://www1.cgmh.org.tw/intr/intr2/c3s000/research/researchcenter.html" , status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

       };
         if (context.res_resource_data.ToList().Count == 0)
         {
             Res_resource_data.ForEach(s => context.res_resource_data.Add(s));
             context.SaveChanges();
         }





         //研究資源
         var Res_resource = new List<res_resource>
         {
              new res_resource { guid = "1",  lang="tw" , sortIndex=1,title="研究人員" , pic="images/03-research/innovation/1-2.jpg",pic_alt="研究人員",content="研究主持人是由經過訓練的醫師、博士、具有博士學位的醫師或是具有相同能力的人員擔任。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource { guid = "1",  lang="en" , sortIndex=1,title="研究人員" , pic="images/03-research/innovation/1-2.jpg",pic_alt="研究人員",content="研究主持人是由經過訓練的醫師、博士、具有博士學位的醫師或是具有相同能力的人員擔任。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource { guid = "2",  lang="tw" , sortIndex=2,title="核心實驗室" , pic="images/03-research/innovation/1-3.jpg",pic_alt="核心實驗室",content="研究主持人是由經過訓練的醫師、博士、具有博士學位的醫師或是具有相同能力的人員擔任。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource { guid = "2",  lang="en" , sortIndex=2,title="核心實驗室" , pic="images/03-research/innovation/1-3.jpg",pic_alt="核心實驗室",content="研究主持人是由經過訓練的醫師、博士、具有博士學位的醫師或是具有相同能力的人員擔任。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource { guid = "3",  lang="tw" , sortIndex=3,title="研究中心" , pic="images/03-research/innovation/1-4.jpg",pic_alt="研究中心",content="研究主持人是由經過訓練的醫師、博士、具有博士學位的醫師或是具有相同能力的人員擔任。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
              new res_resource { guid = "3",  lang="en" , sortIndex=3,title="研究中心" , pic="images/03-research/innovation/1-4.jpg",pic_alt="研究中心",content="研究主持人是由經過訓練的醫師、博士、具有博士學位的醫師或是具有相同能力的人員擔任。", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },


       };
         if (context.res_results.ToList().Count == 0)
         {
             Res_resource.ForEach(s => context.res_resource.Add(s));
             context.SaveChanges();
         }


         //研究成果
         var Res_results = new List<res_results>
         {
             new res_results { guid = "1",  lang="tw" , sortIndex=1,title="研究計畫",content="<div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-paper.png\" alt=\"\"><div class=\"fz-36 fc-main\">5,222</div><div class=\"fz-18\">近五年累積院內件數為</div><div class=\"fz-18\">5,222 件</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-paper2.png\" alt=\"\"><div class=\"fz-36 fc-main\">5,188</div><div class=\"fz-18\">近五年累積院外件數為</div><div class=\"fz-18\">5,188 件</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-money.png\" alt=\"\"><div class=\"fz-36 fc-main\">6,828,519</div><div class=\"fz-18\">近五年累積院內經費數為</div><div class=\"fz-18\">6,828,519 千元</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-money2.png\" alt=\"\"><div class=\"fz-36 fc-main\">4,136,782</div><div class=\"fz-18\">近五年累積院外經費為</div><div class=\"fz-18\">4,136,782 千元</div></div></div>", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
             new res_results { guid = "1",  lang="en" , sortIndex=1,title="研究計畫",content="<div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-paper.png\" alt=\"\"><div class=\"fz-36 fc-main\">5,222</div><div class=\"fz-18\">近五年累積院內件數為</div><div class=\"fz-18\">5,222 件</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-paper2.png\" alt=\"\"><div class=\"fz-36 fc-main\">5,188</div><div class=\"fz-18\">近五年累積院外件數為</div><div class=\"fz-18\">5,188 件</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-money.png\" alt=\"\"><div class=\"fz-36 fc-main\">6,828,519</div><div class=\"fz-18\">近五年累積院內經費數為</div><div class=\"fz-18\">6,828,519 千元</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-money2.png\" alt=\"\"><div class=\"fz-36 fc-main\">4,136,782</div><div class=\"fz-18\">近五年累積院外經費為</div><div class=\"fz-18\">4,136,782 千元</div></div></div>", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

             new res_results { guid = "2",  lang="tw" , sortIndex=1,title="研究論文",content="<div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-1.png\" alt=\"\"><div class=\"fz-36 fc-main\">6,703</div><div class=\"fz-18\">近5年長庚體系</div><div class=\"fz-18\">林口院區論文篇數</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-2.png\" alt=\"\"><div class=\"fz-36 fc-main\">3,103</div><div class=\"fz-18\">近5年長庚體系</div><div class=\"fz-18\">高雄院區論文篇數</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-3.png\" alt=\"\"><div class=\"fz-36 fc-main\">1,020</div><div class=\"fz-18\">近5年長庚體系</div><div class=\"fz-18\">嘉義院區論文篇數</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-4.png\" alt=\"\"><div class=\"fz-36 fc-main\">1,137</div><div class=\"fz-18\">近5年長庚體系</div><div class=\"fz-18\">基隆院區論文篇數</div></div></div>", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },
             new res_results { guid = "2",  lang="en" , sortIndex=1,title="研究論文",content="<div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-1.png\" alt=\"\"><div class=\"fz-36 fc-main\">6,703</div><div class=\"fz-18\">近5年長庚體系</div><div class=\"fz-18\">林口院區論文篇數</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-2.png\" alt=\"\"><div class=\"fz-36 fc-main\">3,103</div><div class=\"fz-18\">近5年長庚體系</div><div class=\"fz-18\">高雄院區論文篇數</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-3.png\" alt=\"\"><div class=\"fz-36 fc-main\">1,020</div><div class=\"fz-18\">近5年長庚體系</div><div class=\"fz-18\">嘉義院區論文篇數</div></div></div><div class=\"wrap\"><div class=\"item\"><img src=\"images/03-research/icon-4.png\" alt=\"\"><div class=\"fz-36 fc-main\">1,137</div><div class=\"fz-18\">近5年長庚體系</div><div class=\"fz-18\">基隆院區論文篇數</div></div></div>", status="Y" , modifydate = DateTime.Now , create_date =  DateTime.Now  },

         };
         if (context.res_results.ToList().Count == 0)
         {
             Res_results.ForEach(s => context.res_results.Add(s));
             context.SaveChanges();
         }
         */

            //網站其他資料
            var Notes_data = new List<notes_data>
             {
                 new notes_data { guid = "1",   title = "組織架構", sub_title = "培育卓越的醫護人員、提供病患最佳的醫療服務為目的。", content = "<img class=\"tree\" src=\"/images/05-system/page1/1-1.png\" alt=\"\">", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                 new notes_data { guid = "1",   title = "組織架構", sub_title = "培育卓越的醫護人員、提供病患最佳的醫療服務為目的。", content = "<img class=\"tree\" src=\"/images/05-system/page1/1-1.png\" alt=\"\">", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="",  modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                 new notes_data { guid = "2",   title = "服務諮詢", sub_title = "", content = "<div class=\"fz-18\">長庚醫研部支持的研究來自各獨立研究主持人的個別型計畫或研究團隊的整合型計畫。<br>如需了解相關資訊,請洽各院區醫研部</div><ul class=\"ul-reset\"><li><img src=\"images/03-research/icon-equiment.png\" alt=\"\"><div class=\"fz-20\">儀器設備</div></li><li><img src=\"images/03-research/icon-advise.png\" alt=\"\"><div class=\"fz-20\">統計諮詢</div></li><li><img src=\"images/03-research/icon-train.png\" alt=\"\"><div class=\"fz-20\">教育訓練</div></li><li><img src=\"images/03-research/icon-edit.png\" alt=\"\"><div class=\"fz-20\">論文編修 </div></li></ul><div class=\"fz-17 fwb btns\"><a href=\"http://cghdpt1.cgmh.org.tw/intr/2s000/\" class=\"btn__round-transparent\">基隆醫研部</a><a href=\"https://www1.cgmh.org.tw/intr/intr2/c3s000/\" class=\"btn__round-transparent\">林口醫研部</a><a href=\"http://jiawww.cgmh.org.tw/intr/c01s60/\" class=\"btn__round-transparent\">嘉義醫研部</a><a href=\"https://www1.cgmh.org.tw/intr/intr4/c8s000/index-02.html\" class=\"btn__round-transparent\">高雄醫研部</a></div>", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                 new notes_data { guid = "2",   title = "服務諮詢", sub_title = "", content = "<div class=\"fz-18\">長庚醫研部支持的研究來自各獨立研究主持人的個別型計畫或研究團隊的整合型計畫。<br>如需了解相關資訊,請洽各院區醫研部</div><ul class=\"ul-reset\"><li><img src=\"images/03-research/icon-equiment.png\" alt=\"\"><div class=\"fz-20\">儀器設備</div></li><li><img src=\"images/03-research/icon-advise.png\" alt=\"\"><div class=\"fz-20\">統計諮詢</div></li><li><img src=\"images/03-research/icon-train.png\" alt=\"\"><div class=\"fz-20\">教育訓練</div></li><li><img src=\"images/03-research/icon-edit.png\" alt=\"\"><div class=\"fz-20\">論文編修 </div></li></ul><div class=\"fz-17 fwb btns\"><a href=\"http://cghdpt1.cgmh.org.tw/intr/2s000/\" class=\"btn__round-transparent\">基隆醫研部</a><a href=\"https://www1.cgmh.org.tw/intr/intr2/c3s000/\" class=\"btn__round-transparent\">林口醫研部</a><a href=\"http://jiawww.cgmh.org.tw/intr/c01s60/\" class=\"btn__round-transparent\">嘉義醫研部</a><a href=\"https://www1.cgmh.org.tw/intr/intr4/c8s000/index-02.html\" class=\"btn__round-transparent\">高雄醫研部</a></div>", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                 new notes_data { guid = "3",   title = "長庚社服", sub_title = "專業社工服務，醫護與病人間的橋樑伴你走向康健未來。", content = "<ul class=\"ul-reset\"><li><img src=\"images/04-welfare/page2/icon-1.svg\" alt=\"\"><div class=\"fz-18\">臨床個案服務</div></li><li><img src=\"images/04-welfare/page2/icon-2.svg\" alt=\"\"><div class=\"fz-18\">病人團體服務</div></li><li><img src=\"images/04-welfare/page2/icon-3.svg\" alt=\"\"><div class=\"fz-18\">社區服務</div></li><li><img class=\"fix4\" src=\"images/04-welfare/page2/icon-4.svg\" alt=\"\"><div class=\"fz-18\">急診服務</div></li><li><img class=\"fix5\" src=\"images/04-welfare/page2/icon-5.svg\" alt=\"\"><div class=\"fz-18\">志願服務</div></li><li><img class=\"fix6\" src=\"images/04-welfare/page2/icon-6.svg\" alt=\"\"><div class=\"fz-18\">出院準備</div></li></ul>", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                 new notes_data { guid = "3",   title = "長庚社服", sub_title = "專業社工服務，醫護與病人間的橋樑伴你走向康健未來。", content = "<ul class=\"ul-reset\"><li><img src=\"images/04-welfare/page2/icon-1.svg\" alt=\"\"><div class=\"fz-18\">臨床個案服務</div></li><li><img src=\"images/04-welfare/page2/icon-2.svg\" alt=\"\"><div class=\"fz-18\">病人團體服務</div></li><li><img src=\"images/04-welfare/page2/icon-3.svg\" alt=\"\"><div class=\"fz-18\">社區服務</div></li><li><img class=\"fix4\" src=\"images/04-welfare/page2/icon-4.svg\" alt=\"\"><div class=\"fz-18\">急診服務</div></li><li><img class=\"fix5\" src=\"images/04-welfare/page2/icon-5.svg\" alt=\"\"><div class=\"fz-18\">志願服務</div></li><li><img class=\"fix6\" src=\"images/04-welfare/page2/icon-6.svg\" alt=\"\"><div class=\"fz-18\">出院準備</div></li></ul>", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },


                 new notes_data { guid = "4",   title = "服務滿意度問卷調查", sub_title = "為提高醫療服務品質，及創造更好的醫療服務環境，本院提供門診、急診以及住院等三份滿意度調查問卷，<br>請您利用幾分鐘時間填寫調查問卷，以提供我們改進的方向。", content = "<ul class=\"ul-reset\"><li><a href=\"https://www.cgmh.org.tw/page/questionnaire/index.asp?quest_no=2010101516382500386\" target=\"_blank\"><img src=\"/images/02-service/online08-1.svg\" alt=\"\"><div class=\"text\"><div class=\"fz-20 fwb\">門診病人滿意度</div><div class=\"fz-17\">問卷調查填寫</div></div></a></li><li><a href=\"https://www.cgmh.org.tw/page/questionnaire/index.asp?quest_no=2010123013363505996\" target=\"_blank\"><img src=\"/images/02-service/online08-2.svg\" alt=\"\"><div class=\"text\"><div class=\"fz-20 fwb\">住院病人滿意度</div><div class=\"fz-17\">問卷調查填寫</div></div></a></li><li><a href=\"https://www.cgmh.org.tw/page/questionnaire/index.asp?quest_no=2010123013350905994\" target=\"_blank\"><img src=\"/images/02-service/online08-3.svg\" alt=\"\"><div class=\"text\"><div class=\"fz-20 fwb\">急診病人滿意度</div><div class=\"fz-17\">問卷調查填寫</div></div></a></li></ul>", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                 new notes_data { guid = "4",   title = "服務滿意度問卷調查", sub_title = "為提高醫療服務品質，及創造更好的醫療服務環境，本院提供門診、急診以及住院等三份滿意度調查問卷，<br>請您利用幾分鐘時間填寫調查問卷，以提供我們改進的方向。", content = "<ul class=\"ul-reset\"><li><a href=\"https://www.cgmh.org.tw/page/questionnaire/index.asp?quest_no=2010101516382500386\" target=\"_blank\"><img src=\"/images/02-service/online08-1.svg\" alt=\"\"><div class=\"text\"><div class=\"fz-20 fwb\">門診病人滿意度</div><div class=\"fz-17\">問卷調查填寫</div></div></a></li><li><a href=\"https://www.cgmh.org.tw/page/questionnaire/index.asp?quest_no=2010123013363505996\" target=\"_blank\"><img src=\"/images/02-service/online08-2.svg\" alt=\"\"><div class=\"text\"><div class=\"fz-20 fwb\">住院病人滿意度</div><div class=\"fz-17\">問卷調查填寫</div></div></a></li><li><a href=\"https://www.cgmh.org.tw/page/questionnaire/index.asp?quest_no=2010123013350905994\" target=\"_blank\"><img src=\"/images/02-service/online08-3.svg\" alt=\"\"><div class=\"text\"><div class=\"fz-20 fwb\">急診病人滿意度</div><div class=\"fz-17\">問卷調查填寫</div></div></a></li></ul>", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                 new notes_data { guid = "5",   title = "隱私權政策", sub_title = "歡迎您蒞臨「長庚醫療財團法人全球資訊網」，為了讓您能夠安全的使用本網站提供之各項服務與資訊，<br>依據「個人資料保護法」之相關規定與立法精神，特此向您說明本網站的「隱私權保護及資訊安全」政策<br>（以下簡稱本政策），請您詳閱下列內容，以保障您的權益：", content = "<div class=\"block\"><h4 class=\"fz-24 fc-main\">適用範圍</h4><ol><li>本政策適用於長庚醫療財團法人全球資訊網站（以下簡稱本網站）。「長庚醫療財團法人」是指長庚醫療財團法人及所屬各分院（以下簡稱長庚醫療體系）。</li><li>本政策適用於您使用本網站時，所涉及對個人資料之蒐集、運用與保護。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">不適用範圍</h4><p>本政策不適用於本網站以外的相關連結網站，也不適用於非本網站所委託或參與管理的人員。</p></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">資料之蒐集與運用 </h4><ol><li>本單純於本網站之瀏覽及檔案下載行為，本網站不會蒐集任何有關個人之身分資料。</li><li>於一般瀏覽時，伺服器會自行記錄相關行徑，包括您使用連線設備的IP位址、使用時間、使用的瀏覽器、瀏覽及點選資料紀錄等，此紀錄僅為內部應用，做為我們增進網站服務的參考依據，絕不對外公布。</li><li>為了提供您最佳的互動照護服務，本網站部分服務內容可能會請您提供相關個人資料，包括：網路掛號、線上轉診掛號系統、醫療諮詢、用藥諮詢、網站會員、意見信箱、滿意度調查、電子報訂閱等。</li><li>除非取得您的同意或為遵循相關法令規定、或為遵從相關機關合法命令外，本網站絕不會將您的個人資料揭露予第三人或使用於蒐集目的以外之其他用途。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">個人應盡義務及自我保護措施</h4><ol><li>當您點選連結至非本網站時，您即適用該連結網站的隱私權保護及資訊安全政策，務請依照中華民國相關法規之規範，尊重並維護第三人之所有相關法律權益。</li><li>如您因為使用本體系網站連結至其他非本體系網站之任何行為，有涉及侵害他人權益之行為或疑慮者，長庚醫療體系及本網站均已對您善盡提醒與提示注意之義務，長庚醫療體系絕不同意更不協助對於任何侵害第三人之行為，故因此所衍生之侵害他人權益行為，長庚醫療體系與本網站均不負有任何連帶法律責任。</li><li>於本網站中設置非本網站之連結網站均與長庚醫療體系間無任何直接或間接法律從屬或監督關係，亦無任何聯盟、結盟或相互保證之關係。故長庚醫療體系對其他連結網站之內容不負有保證或其他法律上與事實上責任與意義。提醒您注意並瞭解。</li><li>若您是與他人共享電腦或使用公共電腦，在完成個人化服務及線上服務等程序後，務必記得登出帳號、關閉瀏器視窗，以防止他人讀取使用您的資料。</li><li>所有網路民眾的行為應遵循國內、外法律規範，並且對於個人所屬帳號、密號所發生之情事負全部責任。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">Cookies 之運用</h4><ol><li>Cookie 是從網站傳送到瀏覽器，並保存在使用者電腦硬碟中的簡短資料。為提供個人化的服務，當您使用本體系網站服務時，本體系網站有時會在您的電腦上設定與存取 Cookie。</li><li>您可以透過設定您的個人電腦或上網設備，決定是否允許 Cookie 技術的使用，如果您選擇拒絕所有的 Cookies 時，可能會造成您使用本網站服務之不便或部份功能無法正常執行。</li><li>您可以在 IE 的「工具-網際網路選項」的「安全性」，或 Firefox 的「工具-選項」的「個人隱私」，或 Chrome 的「工具-選項」的「隱私權設定」中選擇修改瀏覽器對 Cookies 的接受程度（包括：接受所有Cookies、設定 Cookies 時得到通知、拒絕所有 Cookies 等）。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">資訊安全措施與資料保護</h4><ol><li>為了確保本網站安全及持續營運，本網站主機均設有入侵偵測防禦系統、防火牆、防毒系統等相關資訊安全設備及必要的安全防護，對本體系網站及您個人資料做嚴格保護。</li><li>網路安全保護措施<br>A. 裝設防火牆設備，限制特定通訊埠的連結，防止非法入侵，以避免網站遭到非法使用，保障使用者的權益。<br>B. 利用弱點偵測軟體，不定期針對網路系統弱點掃描，並予以補強修正。</li><li>任何未經授權而企圖破壞本網站機密性、完整性及可用性的行為均被禁止，且可能觸犯法律。</li><li>唯有經過授權的人員才能接觸您的個人資料及病歷相關資訊，相關處理人員皆簽有保密合約，如有違反保密義務者，將會受到相關的法律處分。</li><li>本網站中之互動照護服務與您相關之個人資料，本網站已確保資料在傳輸過程中不被第三者非法擷取或入侵。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">隱私權保護及資訊安全政策之修正</h4><p>本政策得因應需求適時進行修正，並刊登於網站。</p></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">隱私權保護及資訊安全政策之諮詢</h4><p>若您對本政策有任何疑問或意見，歡迎隨時與我們聯絡。</p></div>", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                 new notes_data { guid = "5",   title = "隱私權政策", sub_title = "歡迎您蒞臨「長庚醫療財團法人全球資訊網」，為了讓您能夠安全的使用本網站提供之各項服務與資訊，<br>依據「個人資料保護法」之相關規定與立法精神，特此向您說明本網站的「隱私權保護及資訊安全」政策<br>（以下簡稱本政策），請您詳閱下列內容，以保障您的權益：", content = "<div class=\"block\"><h4 class=\"fz-24 fc-main\">適用範圍</h4><ol><li>本政策適用於長庚醫療財團法人全球資訊網站（以下簡稱本網站）。「長庚醫療財團法人」是指長庚醫療財團法人及所屬各分院（以下簡稱長庚醫療體系）。</li><li>本政策適用於您使用本網站時，所涉及對個人資料之蒐集、運用與保護。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">不適用範圍</h4><p>本政策不適用於本網站以外的相關連結網站，也不適用於非本網站所委託或參與管理的人員。</p></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">資料之蒐集與運用 </h4><ol><li>本單純於本網站之瀏覽及檔案下載行為，本網站不會蒐集任何有關個人之身分資料。</li><li>於一般瀏覽時，伺服器會自行記錄相關行徑，包括您使用連線設備的IP位址、使用時間、使用的瀏覽器、瀏覽及點選資料紀錄等，此紀錄僅為內部應用，做為我們增進網站服務的參考依據，絕不對外公布。</li><li>為了提供您最佳的互動照護服務，本網站部分服務內容可能會請您提供相關個人資料，包括：網路掛號、線上轉診掛號系統、醫療諮詢、用藥諮詢、網站會員、意見信箱、滿意度調查、電子報訂閱等。</li><li>除非取得您的同意或為遵循相關法令規定、或為遵從相關機關合法命令外，本網站絕不會將您的個人資料揭露予第三人或使用於蒐集目的以外之其他用途。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">個人應盡義務及自我保護措施</h4><ol><li>當您點選連結至非本網站時，您即適用該連結網站的隱私權保護及資訊安全政策，務請依照中華民國相關法規之規範，尊重並維護第三人之所有相關法律權益。</li><li>如您因為使用本體系網站連結至其他非本體系網站之任何行為，有涉及侵害他人權益之行為或疑慮者，長庚醫療體系及本網站均已對您善盡提醒與提示注意之義務，長庚醫療體系絕不同意更不協助對於任何侵害第三人之行為，故因此所衍生之侵害他人權益行為，長庚醫療體系與本網站均不負有任何連帶法律責任。</li><li>於本網站中設置非本網站之連結網站均與長庚醫療體系間無任何直接或間接法律從屬或監督關係，亦無任何聯盟、結盟或相互保證之關係。故長庚醫療體系對其他連結網站之內容不負有保證或其他法律上與事實上責任與意義。提醒您注意並瞭解。</li><li>若您是與他人共享電腦或使用公共電腦，在完成個人化服務及線上服務等程序後，務必記得登出帳號、關閉瀏器視窗，以防止他人讀取使用您的資料。</li><li>所有網路民眾的行為應遵循國內、外法律規範，並且對於個人所屬帳號、密號所發生之情事負全部責任。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">Cookies 之運用</h4><ol><li>Cookie 是從網站傳送到瀏覽器，並保存在使用者電腦硬碟中的簡短資料。為提供個人化的服務，當您使用本體系網站服務時，本體系網站有時會在您的電腦上設定與存取 Cookie。</li><li>您可以透過設定您的個人電腦或上網設備，決定是否允許 Cookie 技術的使用，如果您選擇拒絕所有的 Cookies 時，可能會造成您使用本網站服務之不便或部份功能無法正常執行。</li><li>您可以在 IE 的「工具-網際網路選項」的「安全性」，或 Firefox 的「工具-選項」的「個人隱私」，或 Chrome 的「工具-選項」的「隱私權設定」中選擇修改瀏覽器對 Cookies 的接受程度（包括：接受所有Cookies、設定 Cookies 時得到通知、拒絕所有 Cookies 等）。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">資訊安全措施與資料保護</h4><ol><li>為了確保本網站安全及持續營運，本網站主機均設有入侵偵測防禦系統、防火牆、防毒系統等相關資訊安全設備及必要的安全防護，對本體系網站及您個人資料做嚴格保護。</li><li>網路安全保護措施<br>A. 裝設防火牆設備，限制特定通訊埠的連結，防止非法入侵，以避免網站遭到非法使用，保障使用者的權益。<br>B. 利用弱點偵測軟體，不定期針對網路系統弱點掃描，並予以補強修正。</li><li>任何未經授權而企圖破壞本網站機密性、完整性及可用性的行為均被禁止，且可能觸犯法律。</li><li>唯有經過授權的人員才能接觸您的個人資料及病歷相關資訊，相關處理人員皆簽有保密合約，如有違反保密義務者，將會受到相關的法律處分。</li><li>本網站中之互動照護服務與您相關之個人資料，本網站已確保資料在傳輸過程中不被第三者非法擷取或入侵。</li></ol></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">隱私權保護及資訊安全政策之修正</h4><p>本政策得因應需求適時進行修正，並刊登於網站。</p></div><div class=\"block\"><h4 class=\"fz-24 fc-main\">隱私權保護及資訊安全政策之諮詢</h4><p>若您對本政策有任何疑問或意見，歡迎隨時與我們聯絡。</p></div>", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },


                 new notes_data { guid = "6",   title = "本院遇2018年09月24日(週一)中秋節國定假日期間門診、檢查、檢驗均暫停服務。", sub_title = "#", content = "", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                 new notes_data { guid = "6",   title = "本院遇2018年09月24日(週一)中秋節國定假日期間門診、檢查、檢驗均暫停服務。", sub_title = "#", content = "", status = "Y",pic="",pic_alt="", seo_keywords ="",seo_description="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

             };
             if (context.notes_data.ToList().Count == 0)
             {
                 Notes_data.ForEach(s => context.notes_data.Add(s));
                 context.SaveChanges();
             }
          
            /*
           //人才招募資訊
           var Recruits = new List<recruits>
           {
               new recruits { guid = "1",  lang="tw" , category="D", sortIndex=1, pic="images/05-system/recruit/1.jpg" , pic_alt="醫師", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "醫師",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits { guid = "1",  lang="en" , category="D", sortIndex=1, pic="images/05-system/recruit/1.jpg" , pic_alt="醫師", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "醫師",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new recruits { guid = "2",  lang="tw" , category="N", sortIndex=2, pic="images/05-system/recruit/2.jpg" , pic_alt="護理人員", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "護理",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits { guid = "2",  lang="en" , category="N", sortIndex=2, pic="images/05-system/recruit/2.jpg" , pic_alt="護理人員", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "護理",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },


               new recruits { guid = "3",  lang="tw" , category="T", sortIndex=3, pic="images/05-system/recruit/3.jpg" , pic_alt="醫技人員", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "醫技",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits { guid = "3",  lang="en" , category="T", sortIndex=3, pic="images/05-system/recruit/3.jpg" , pic_alt="醫技人員", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "醫技",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },


               new recruits { guid = "4",  lang="tw" , category="A", sortIndex=4, pic="images/05-system/recruit/4.jpg" , pic_alt="行政人員", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "行政",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits { guid = "4",  lang="en" , category="A", sortIndex=4, pic="images/05-system/recruit/4.jpg" , pic_alt="行政人員", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "行政",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new recruits { guid = "5",  lang="tw" , category="O", sortIndex=4, pic="images/05-system/recruit/5.jpg" , pic_alt="其他人員", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "其他",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits { guid = "5",  lang="en" , category="O", sortIndex=4, pic="images/05-system/recruit/5.jpg" , pic_alt="其他人員", resume_url="#1" , admittance_url = "#1", top_notes="~竭誠歡迎您的加入~<br>與我們共同成為頂尖醫療團隊的一員! 掌握自己、邁向卓越、共創未來!",notes="本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。", title = "其他",status="Y", content="<p class=\"fz-17\">本體系基隆、台北、林口、桃園、雲林、嘉義、高雄、 鳳山院區，提供完整內、外、婦、兒、急重症、長期 照護、銀髮養生等急、慢性照護，提供護理人員全國最佳之工作環境、薪資及福利，並積極培養您成為各領域護理專家。</p>  <p class=\"fz-17\">本體系各院區間護理人員有完整跨院區調任作業，可滿足護理人員不同人　生階段，如：繼續進修、結婚、生子等之工作地點變更需要。<br>專科(含)以上護理科系畢業且具護理師（或護士）證書（應屆畢業生免），具臨床護理照護經驗者優先錄取。</p>", modifydate = DateTime.Now , create_date =  DateTime.Now  },


           };
           if (context.recruits.ToList().Count == 0)
           {
               Recruits.ForEach(s => context.recruits.Add(s));
               context.SaveChanges();
           }

           //人才招募資訊-福利
           var Recruits_data = new List<recruits_data>
           {
               new recruits_data { guid = "1",  lang="tw" , category="1", sortIndex=1, title = "薪資", content="<ul class=\"fz-17 ul-reset ul-circle ul-circle-color\"><li>依任用學歷核給底薪，並提供伙食津貼、交通津貼、地區津貼等，每月25,750元~28,250元。</li><li>績效獎金:培訓期間約5,000~6,000元，正式任用後依單位實際績效核發，每月約8,000~12,000元。</li><li>護理執照津貼：護理師執照1,200元/月，護士執照600元/月。</li><li class=\"fc-red\">夜班費：小夜450元/班，大夜600元/班(不須包班，即足額核發)。</li><li class=\"fc-red\">夜班績效:小夜及大夜班，每班再加發夜班績效200元。</li><li>特殊單位津貼：急護單位、開刀房、精神科病房等單位，每月1,000元~2,500元間。</li><li class=\"fc-red\">每年固定調薪：依個人工作考核結果調薪，近幾年平均調薪1.5%~3%。</li><li>年節獎金：端午、中秋節時核發半個月底薪外加勤勉獎金。</li><li class=\"fc-red\">年終獎金：每年核發年終獎金，近幾年平均發放4.0~4.5個月底薪，依個人年終考績核發；另視營運績效核發額外獎勵金。</li><li class=\"fc-red\">新進人員報到獎勵金10,000元(限基隆、桃園護理之家及雲林院區)。</li><li class=\"fc-red\">新進人員留任獎勵金37,000元。</li><li class=\"fc-red\">院區專業津貼：6000元/季(限基隆)。</li><li>經驗薪：納入底薪核算，依原任職醫院評鑑等級及工作年資，核發經驗薪300元~1,800元。</li>  </ul>",status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits_data { guid = "1",  lang="en" , category="1", sortIndex=1, title = "薪資", content="<ul class=\"fz-17 ul-reset ul-circle ul-circle-color\"><li>依任用學歷核給底薪，並提供伙食津貼、交通津貼、地區津貼等，每月25,750元~28,250元。</li><li>績效獎金:培訓期間約5,000~6,000元，正式任用後依單位實際績效核發，每月約8,000~12,000元。</li><li>護理執照津貼：護理師執照1,200元/月，護士執照600元/月。</li><li class=\"fc-red\">夜班費：小夜450元/班，大夜600元/班(不須包班，即足額核發)。</li><li class=\"fc-red\">夜班績效:小夜及大夜班，每班再加發夜班績效200元。</li><li>特殊單位津貼：急護單位、開刀房、精神科病房等單位，每月1,000元~2,500元間。</li><li class=\"fc-red\">每年固定調薪：依個人工作考核結果調薪，近幾年平均調薪1.5%~3%。</li><li>年節獎金：端午、中秋節時核發半個月底薪外加勤勉獎金。</li><li class=\"fc-red\">年終獎金：每年核發年終獎金，近幾年平均發放4.0~4.5個月底薪，依個人年終考績核發；另視營運績效核發額外獎勵金。</li><li class=\"fc-red\">新進人員報到獎勵金10,000元(限基隆、桃園護理之家及雲林院區)。</li><li class=\"fc-red\">新進人員留任獎勵金37,000元。</li><li class=\"fc-red\">院區專業津貼：6000元/季(限基隆)。</li><li>經驗薪：納入底薪核算，依原任職醫院評鑑等級及工作年資，核發經驗薪300元~1,800元。</li>  </ul>",status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits_data { guid = "2",  lang="tw" , category="1", sortIndex=2, title = "福利", content="<ul class=\"fz-17 ul-reset ul-circle ul-circle-color\"><li>依任用學歷核給底薪，並提供伙食津貼、交通津貼、地區津貼等，每月25,750元~28,250元。</li><li>績效獎金:培訓期間約5,000~6,000元，正式任用後依單位實際績效核發，每月約8,000~12,000元。</li><li>護理執照津貼：護理師執照1,200元/月，護士執照600元/月。</li><li class=\"fc-red\">夜班費：小夜450元/班，大夜600元/班(不須包班，即足額核發)。</li><li class=\"fc-red\">夜班績效:小夜及大夜班，每班再加發夜班績效200元。</li><li>特殊單位津貼：急護單位、開刀房、精神科病房等單位，每月1,000元~2,500元間。</li><li class=\"fc-red\">每年固定調薪：依個人工作考核結果調薪，近幾年平均調薪1.5%~3%。</li><li>年節獎金：端午、中秋節時核發半個月底薪外加勤勉獎金。</li><li class=\"fc-red\">年終獎金：每年核發年終獎金，近幾年平均發放4.0~4.5個月底薪，依個人年終考績核發；另視營運績效核發額外獎勵金。</li><li class=\"fc-red\">新進人員報到獎勵金10,000元(限基隆、桃園護理之家及雲林院區)。</li><li class=\"fc-red\">新進人員留任獎勵金37,000元。</li><li class=\"fc-red\">院區專業津貼：6000元/季(限基隆)。</li><li>經驗薪：納入底薪核算，依原任職醫院評鑑等級及工作年資，核發經驗薪300元~1,800元。</li>  </ul>",status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits_data { guid = "2",  lang="en" , category="1", sortIndex=2, title = "福利", content="<ul class=\"fz-17 ul-reset ul-circle ul-circle-color\"><li>依任用學歷核給底薪，並提供伙食津貼、交通津貼、地區津貼等，每月25,750元~28,250元。</li><li>績效獎金:培訓期間約5,000~6,000元，正式任用後依單位實際績效核發，每月約8,000~12,000元。</li><li>護理執照津貼：護理師執照1,200元/月，護士執照600元/月。</li><li class=\"fc-red\">夜班費：小夜450元/班，大夜600元/班(不須包班，即足額核發)。</li><li class=\"fc-red\">夜班績效:小夜及大夜班，每班再加發夜班績效200元。</li><li>特殊單位津貼：急護單位、開刀房、精神科病房等單位，每月1,000元~2,500元間。</li><li class=\"fc-red\">每年固定調薪：依個人工作考核結果調薪，近幾年平均調薪1.5%~3%。</li><li>年節獎金：端午、中秋節時核發半個月底薪外加勤勉獎金。</li><li class=\"fc-red\">年終獎金：每年核發年終獎金，近幾年平均發放4.0~4.5個月底薪，依個人年終考績核發；另視營運績效核發額外獎勵金。</li><li class=\"fc-red\">新進人員報到獎勵金10,000元(限基隆、桃園護理之家及雲林院區)。</li><li class=\"fc-red\">新進人員留任獎勵金37,000元。</li><li class=\"fc-red\">院區專業津貼：6000元/季(限基隆)。</li><li>經驗薪：納入底薪核算，依原任職醫院評鑑等級及工作年資，核發經驗薪300元~1,800元。</li>  </ul>",status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits_data { guid = "3",  lang="tw" , category="1", sortIndex=3, title = "培育計畫", content="<ul class=\"fz-17 ul-reset ul-circle ul-circle-color\"><li>依任用學歷核給底薪，並提供伙食津貼、交通津貼、地區津貼等，每月25,750元~28,250元。</li><li>績效獎金:培訓期間約5,000~6,000元，正式任用後依單位實際績效核發，每月約8,000~12,000元。</li><li>護理執照津貼：護理師執照1,200元/月，護士執照600元/月。</li><li class=\"fc-red\">夜班費：小夜450元/班，大夜600元/班(不須包班，即足額核發)。</li><li class=\"fc-red\">夜班績效:小夜及大夜班，每班再加發夜班績效200元。</li></ul>",status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new recruits_data { guid = "3",  lang="en" , category="1", sortIndex=3, title = "培育計畫", content="<ul class=\"fz-17 ul-reset ul-circle ul-circle-color\"><li>依任用學歷核給底薪，並提供伙食津貼、交通津貼、地區津貼等，每月25,750元~28,250元。</li><li>績效獎金:培訓期間約5,000~6,000元，正式任用後依單位實際績效核發，每月約8,000~12,000元。</li><li>護理執照津貼：護理師執照1,200元/月，護士執照600元/月。</li><li class=\"fc-red\">夜班費：小夜450元/班，大夜600元/班(不須包班，即足額核發)。</li><li class=\"fc-red\">夜班績效:小夜及大夜班，每班再加發夜班績效200元。</li></ul>",status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now  },
            };
           if (context.recruits_data.ToList().Count == 0)
           {
               Recruits_data.ForEach(s => context.recruits_data.Add(s));
               context.SaveChanges();
           }



           /*
           //關於長庚
           var Abouts = new List<abouts>
           {
               new abouts { guid = "1",  lang="tw" , category="1", sortIndex=1, title = "宗旨與理念",status="Y", content="<section class=\"about__idea layout__container-1400\"><div class=\"layout__textimg\"><div class=\"col\"><div class=\"col__img imgLiquidFill\"><img src=\"/images/05-system/page2/1.jpg\" alt=\"\"></div><div class=\"col__text\"><div class=\"fz-17\">長庚醫院創設於1976年，當時台灣醫療設施嚴重不足，平均每萬人僅有病床數19床， 與現代化國家每萬人有 40 床的水準，相距太遠。於是我們先後設立了台北、林口、基 隆、高雄、桃園、嘉義、雲林等大型醫院，每日診治病患 31,500 人次以上，擁有病床 數 9,000 床。是遠東地區規模最大、設備最完善、經營績效最佳的綜合醫院之一。為 了對幼兒提供更專業的醫療服務，長庚醫院於 1993 年及 1994 年分別於林口及高雄創 設大型的兒童醫學中心，擁有病床數 800 床。並且，為了使醫療資源作最有效的運用， 我們也於 2001 年初創設護理之家，並於 2003 年 12 月設立兼具急、慢性醫療之桃園 長庚醫院，發展亞急性、慢性醫療及長期照護，垂直整合成為完整之醫療體系，提供 民眾完整醫療照護。</div></div></div><div class=\"col\"><div class=\"col__img imgLiquidFill\"><img src=\"/images/05-system/page2/1-1.jpg\" alt=\"\"></div><div class=\"col__text\"><div class=\"fz-17\">與此同時，鑑於國內 65 歲以上的老年人口比例已超過 12.5%，為 因應高齡化社會的來臨，我們在 2005 年 1 月開放養生文化村，提供老年人口一個可 以繼續經營晚年生活的安養社區。另為發揚中國固有醫療文化，結合西醫現代化及科 學化的技巧和方法，我們率先於醫學中心級醫院設立中醫部門，並朝提供中西醫整合 性醫療之目標邁進。為提升醫療服務，已成立癌症中心，建構以重症為中心的專科特 色醫療，更投資數十億元，於林口長庚設立永慶尖端醫療園區，成立亞洲最大，國內 第一所質子放射治療中心，已於 104 年 11 月開始提供服務。</div></div></div></div><ul class=\"ideas ul-reset\"><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg fix-1\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 53 36\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><g><path d=\"M38.351,26.882c-2.733,3.625 -7.095,6.043 -11.929,6.043c-4.835,0 -9.197,-2.418 -11.93,-6.043c-3.206,-4.309 -8.304,-7.096 -14.031,-7.096l-0.263,0l0,2.628l0.21,0c4.94,0 9.354,2.418 12.034,6.097c3.206,4.256 8.303,6.989 13.98,6.989c5.675,0 10.773,-2.733 13.978,-6.989c2.733,-3.679 7.095,-6.097 12.035,-6.097l0.21,0l0,-2.628l-0.21,0c-5.781,0 -10.878,2.787 -14.084,7.096\" style=\"fill-rule:nonzero;\" /><path d=\"M35.145,16.266l0,3.521l-6.989,0l0,6.99l-1.735,0l-1.734,0l0,-5.255l0,-1.735l-1.734,0l-5.256,0l0,-3.521l6.99,0l0,-1.734l0,-5.255l3.469,0l0,6.989l6.989,0Zm-8.724,-10.51l-5.255,0l0,6.989l-6.989,0l0,10.51l6.989,0l0,7.042l10.458,0l0,-7.042l7.042,0l0,-10.51l-7.042,0l0,-6.989l-5.203,0Z\" style=\"fill-rule:nonzero;\" /><path d=\"M40.4,7.543c-3.206,-4.257 -8.303,-7.043 -13.979,-7.043c-5.676,0 -10.773,2.786 -13.979,6.99c-2.679,3.731 -7.094,6.148 -12.034,6.148l-0.211,0l0,2.628l0.263,0c5.729,0 10.827,-2.785 14.032,-7.094c2.733,-3.679 7.094,-6.044 11.929,-6.044c4.835,0 9.197,2.365 11.93,6.044c3.205,4.309 8.303,7.094 14.083,7.094l0.211,0l0,-2.628l-0.211,0c-4.939,0 -9.301,-2.417 -12.034,-6.095\" style=\"fill-rule:nonzero;\" /></g></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">宗旨</h4><div class=\"fz-18\">不以營利為目的，從事醫療事業，並秉持「取之於社會、用之於社會、止於至善、 永續經營」理念善盡社會責任，以促進社會公益福利。</div></div></div></li><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 511 511\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><path d=\"M506.103,431.815l-115.633,-161.278c-4.846,-6.76 -12.335,-10.635 -20.553,-10.635c-8.215,0 -15.708,3.879 -20.55,10.635l-45.836,63.93l-100.389,-137.146l0,-55.279l86.467,0c10.211,0 18.519,-8.312 18.519,-18.522l0,-66.078c0,-10.21 -8.308,-18.518 -18.519,-18.518l-97.898,0c-10.211,0 -18.519,8.308 -18.519,18.518l0,139.918l-167.976,231.843c-6.085,8.487 -6.92,20.012 -2.12,29.353c4.651,9.056 13.746,14.686 23.73,14.686l458.72,0c9.509,0 18.148,-5.33 22.554,-13.906c4.499,-8.753 3.715,-19.556 -1.997,-27.521Zm-302.961,-362.942l75.036,0l0,43.216l-75.036,0l0,-43.216Zm-171.15,374.42l156.237,-215.641l103.232,141.029l51.774,74.612l-311.243,0Zm347.695,0l-57.708,-83.162l47.942,-66.861l107.559,150.023l-97.793,0Z\" style=\"fill-rule:nonzero;\" /></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">任務</h4><div class=\"fz-18\">服務、教學、研究</div></div></div></li><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 512 512\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><path id=\"surface1\" d=\"M507.605,486.395l-60.293,-60.297c41.813,-46.91 64.688,-106.743 64.688,-170.098c0,-49.285 -13.867,-96.645 -40.207,-137.773l35.812,-35.813c3.676,-3.68 5.188,-8.996 3.993,-14.055c-1.196,-5.058 -4.922,-9.14 -9.856,-10.781l-35.484,-11.832l-11.828,-35.488c-1.649,-4.93 -5.727,-8.66 -10.785,-9.856c-5.059,-1.191 -10.375,0.317 -14.055,3.993l-35.813,35.816c-41.132,-26.34 -88.496,-40.211 -137.777,-40.211c-68.379,0 -132.668,26.629 -181.02,74.98c-48.351,48.352 -74.98,112.641 -74.98,181.02c0,63.355 22.875,123.188 64.691,170.098l-60.296,60.297c-5.86,5.855 -5.86,15.355 0,21.21c2.929,2.93 6.765,4.395 10.605,4.395c3.84,0 7.68,-1.465 10.605,-4.395l60.297,-60.296c46.911,41.82 106.743,64.691 170.098,64.691c63.355,0 123.188,-22.875 170.098,-64.691l60.297,60.296c2.929,2.93 6.765,4.395 10.605,4.395c3.84,0 7.68,-1.465 10.605,-4.395c5.86,-5.855 5.86,-15.351 0,-21.21Zm-73.96,-443.625l6.523,19.574c1.492,4.48 5.008,7.996 9.484,9.488l19.578,6.527l-33.085,33.086l-26.692,-8.898l-8.898,-26.692l33.09,-33.085Zm-112.309,213.23c0,36.023 -29.313,65.332 -65.336,65.332c-36.023,0 -65.332,-29.309 -65.332,-65.332c0,-36.023 29.309,-65.332 65.332,-65.332c12.59,0 24.352,3.586 34.336,9.781l-44.941,44.946c-5.86,5.859 -5.86,15.355 0,21.21c2.925,2.93 6.765,4.395 10.605,4.395c3.84,0 7.68,-1.465 10.605,-4.395l44.946,-44.941c6.195,9.984 9.785,21.746 9.785,34.336Zm-9.399,-77.148c-15.722,-11.43 -35.054,-18.184 -55.937,-18.184c-52.566,0 -95.332,42.766 -95.332,95.332c0,52.566 42.766,95.332 95.332,95.332c52.566,0 95.336,-42.766 95.336,-95.332c0,-20.883 -6.754,-40.211 -18.188,-55.938l35.864,-35.859c20.402,25.07 32.656,57.027 32.656,91.797c0,80.32 -65.348,145.668 -145.668,145.668c-80.32,0 -145.664,-65.348 -145.664,-145.668c0,-80.32 65.344,-145.668 145.664,-145.668c34.77,0 66.727,12.254 91.801,32.656l-35.864,35.864Zm-281.937,77.148c0,-124.617 101.383,-226 226,-226c41.297,0 81.059,11.02 116.031,32.012c-3.433,3.98 -4.551,9.5 -2.871,14.539l11.274,33.804l-11.34,11.34c-30.582,-25.793 -70.051,-41.363 -113.094,-41.363c-96.863,0 -175.664,78.805 -175.664,175.668c0,96.863 78.801,175.668 175.664,175.668c96.863,0 175.668,-78.805 175.668,-175.668c0,-43.043 -15.57,-82.512 -41.363,-113.094l11.34,-11.34l33.808,11.274c1.555,0.515 3.152,0.769 4.742,0.769c3.555,0 7.035,-1.277 9.789,-3.652c20.996,34.977 32.016,74.742 32.016,116.043c0,124.617 -101.383,226 -226,226c-124.617,0 -226,-101.383 -226,-226Z\" style=\"fill-rule:nonzero;\" /></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">目標</h4><div class=\"fz-18\">要做就做最好</div></div></div></li><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 512 512\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><g id=\"surface1\"><path d=\"M256,0c-88.004,0 -159.602,71.598 -159.602,159.602c0,50.648 23.879,97.91 64.266,128.003l0,13.329c-9.836,8.644 -16.062,21.304 -16.062,35.398c0,12.41 4.828,23.711 12.695,32.137c-7.867,8.426 -12.695,19.722 -12.695,32.133c0,21.14 13.988,39.074 33.199,45.027c6.179,37.594 38.883,66.371 78.199,66.371c39.316,0 72.023,-28.777 78.203,-66.371c19.207,-5.957 33.199,-23.887 33.199,-45.031c0,-12.407 -4.828,-23.707 -12.695,-32.133c7.867,-8.426 12.695,-19.723 12.695,-32.133c0,-14.094 -6.226,-26.754 -16.066,-35.402l0,-13.325c40.387,-30.093 64.266,-77.355 64.266,-128.003c0,-88.004 -71.598,-159.602 -159.602,-159.602Zm64.27,319.199c9.445,0 17.132,7.688 17.132,17.133c0,9.449 -7.687,17.137 -17.132,17.137l-128.536,0c-9.449,0 -17.136,-7.688 -17.136,-17.137c0,-9.445 7.687,-17.133 17.136,-17.133l128.536,0Zm-64.27,162.801c-21.938,0 -40.57,-14.414 -46.934,-34.266l93.868,0c-6.36,19.856 -24.996,34.266 -46.934,34.266Zm64.27,-64.266l-128.536,0c-9.449,0 -17.136,-7.687 -17.136,-17.132c0,-9.45 7.687,-17.133 17.136,-17.133l128.536,0c9.445,0 17.132,7.683 17.132,17.133c0,9.445 -7.687,17.132 -17.132,17.132Zm7.722,-150.355c-4.16,2.785 -6.656,7.461 -6.656,12.465l0,9.383c-0.356,-0.008 -0.711,-0.028 -1.066,-0.028l-17.137,0l0,-59.117l21.539,-21.539c5.859,-5.859 5.859,-15.356 0,-21.215c-5.856,-5.859 -15.356,-5.859 -21.211,0l-25.934,25.934c-2.812,2.812 -4.394,6.625 -4.394,10.605l0,65.332l-34.266,0l0,-65.332c0,-3.976 -1.582,-7.793 -4.394,-10.605l-25.934,-25.934c-5.855,-5.855 -15.351,-5.855 -21.211,0c-5.859,5.859 -5.859,15.356 0,21.215l21.539,21.539l0,59.121l-17.133,0c-0.359,0 -0.714,0.016 -1.07,0.024l0,-9.383c0,-5.004 -2.496,-9.68 -6.656,-12.465c-36.07,-24.149 -57.61,-64.438 -57.61,-107.777c0,-71.465 58.141,-129.602 129.602,-129.602c71.461,0 129.602,58.137 129.602,129.602c0,43.339 -21.539,83.628 -57.61,107.777Z\" style=\"fill-rule:nonzero;\" /><path d=\"M62.133,159.602c0,-8.286 -6.715,-15 -15,-15l-32.133,0c-8.285,0 -15,6.714 -15,15c0,8.281 6.715,15 15,15l32.133,0c8.285,0 15,-6.719 15,-15Z\" style=\"fill-rule:nonzero;\" /><path d=\"M497,144.602l-32.133,0c-8.281,0 -15,6.714 -15,15c0,8.281 6.719,15 15,15l32.133,0c8.285,0 15,-6.719 15,-15c0,-8.286 -6.715,-15 -15,-15Z\" style=\"fill-rule:nonzero;\" /><path d=\"M67.613,251.043l-27.828,16.066c-7.172,4.145 -9.629,13.317 -5.488,20.493c2.777,4.812 7.82,7.5 13.004,7.5c2.543,0 5.125,-0.645 7.484,-2.012l27.828,-16.063c7.176,-4.144 9.633,-13.316 5.492,-20.492c-4.14,-7.176 -13.316,-9.633 -20.492,-5.492Z\" style=\"fill-rule:nonzero;\" /><path d=\"M436.898,70.172c2.543,0 5.122,-0.649 7.485,-2.012l27.832,-16.07c7.172,-4.141 9.633,-13.313 5.488,-20.488c-4.144,-7.176 -13.316,-9.633 -20.488,-5.489l-27.832,16.067c-7.172,4.14 -9.633,13.316 -5.488,20.488c2.777,4.812 7.82,7.504 13.003,7.504Z\" style=\"fill-rule:nonzero;\" /><path d=\"M472.211,267.109l-27.828,-16.062c-7.176,-4.141 -16.352,-1.684 -20.492,5.492c-4.141,7.172 -1.68,16.348 5.492,20.488l27.832,16.063c2.359,1.363 4.937,2.012 7.484,2.012c5.184,0 10.227,-2.692 13.004,-7.504c4.145,-7.172 1.684,-16.348 -5.492,-20.489Z\" style=\"fill-rule:nonzero;\" /><path d=\"M82.617,42.176l-27.828,-16.063c-7.176,-4.144 -16.348,-1.683 -20.488,5.492c-4.145,7.176 -1.684,16.348 5.488,20.489l27.828,16.066c2.363,1.363 4.942,2.012 7.485,2.012c5.187,0 10.226,-2.692 13.007,-7.504c4.141,-7.176 1.68,-16.348 -5.492,-20.492Z\" style=\"fill-rule:nonzero;\" /></g></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">理念</h4><div class=\"fz-18\">取之社會，用之社會<br>人本濟世，病患優先<br>勤勞樸實，深耕生根</div></div></div></li><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 512 512\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><path id=\"surface1\" d=\"M460.339,340.679l35.027,-35.031c22.175,-22.172 22.179,-58.253 0.004,-80.437c-6.805,-6.801 -14.977,-11.578 -23.707,-14.226c7.14,-9.7 11.004,-21.375 11.004,-33.645c0,-15.191 -5.918,-29.476 -16.664,-40.219c-10.739,-10.742 -25.024,-16.66 -40.219,-16.66c-0.637,0 -1.274,0.012 -1.91,0.032c0.496,-15.184 -5.035,-30.535 -16.602,-42.106c-11.566,-11.562 -26.937,-17.101 -42.097,-16.597c0.019,-0.633 0.031,-1.274 0.031,-1.911c0,-15.195 -5.918,-29.476 -16.66,-40.218c-22.172,-22.172 -58.254,-22.176 -80.437,0l-12.11,12.109l-12.105,-12.105c-10.742,-10.746 -25.027,-16.664 -40.223,-16.664c-15.191,0 -29.472,5.918 -40.214,16.656c-11.571,11.57 -17.09,26.926 -16.586,42.121c-0.641,-0.024 -1.282,-0.047 -1.922,-0.047c-15.192,0 -29.477,5.914 -40.227,16.656c-11.566,11.574 -17.086,26.93 -16.582,42.121c-0.644,-0.023 -1.285,-0.047 -1.929,-0.047c-15.192,0 -29.477,5.918 -40.211,16.657c-10.746,10.742 -16.664,25.027 -16.664,40.222c0,12.266 3.859,23.941 11.004,33.637c-8.86,2.672 -16.981,7.507 -23.711,14.234c-22.121,22.129 -22.172,58.098 -0.149,80.281c0.055,0.059 0.11,0.113 0.164,0.172l35.079,35.07c0.046,0.051 0.097,0.102 0.148,0.152c2.105,2.11 4.355,4.02 6.715,5.75l-10.375,10.375c-10.09,10.09 -15.231,23.836 -14.469,38.707c0.727,14.278 6.871,27.879 17.293,38.301l59.332,59.336c11.008,11.004 25.461,16.508 39.918,16.508c14.457,0 28.914,-5.504 39.918,-16.508l27.578,-27.582c12.582,2.699 25.398,4.07 38.226,4.07c12.816,0 25.637,-1.363 38.215,-4.062l27.57,27.574c11.008,11.004 25.461,16.508 39.918,16.508c14.453,-0.004 28.914,-5.504 39.918,-16.508l59.336,-59.336c22.007,-22.008 24.507,-55.32 5.57,-74.261l-12.637,-12.637l6.234,-6.238c0.071,-0.067 0.137,-0.133 0.208,-0.204Zm-275.175,-299.311c4.945,-4.945 11.519,-7.668 18.511,-7.668c6.992,0 13.567,2.723 18.512,7.668l46.98,46.98c10.207,10.211 10.207,26.82 0,37.027c-4.945,4.946 -11.519,7.668 -18.508,7.668l-0.003,0c-6.993,-0.004 -13.563,-2.722 -18.508,-7.668c-0.008,-0.007 -0.016,-0.015 -0.02,-0.019l-46.953,-46.953c-0.039,-0.039 -0.074,-0.078 -0.117,-0.113c-10.101,-10.223 -10.066,-26.754 0.106,-36.922Zm-58.801,95.683c-10.137,-10.215 -10.113,-26.773 0.066,-36.953c4.95,-4.945 11.528,-7.668 18.52,-7.668c6.984,0 13.551,2.719 18.496,7.66c0.004,0 0.008,0.004 0.008,0.008c0.019,0.016 0.035,0.031 0.05,0.047l46.926,46.926c0.027,0.031 0.063,0.062 0.094,0.093l11.66,11.66c4.945,4.946 7.672,11.52 7.672,18.516c0,6.992 -2.723,13.566 -7.668,18.508c-4.945,4.945 -11.52,7.668 -18.512,7.668c-6.992,0 -13.566,-2.723 -18.508,-7.668c-0.062,-0.063 -0.125,-0.121 -0.187,-0.18l-11.512,-11.515c-0.015,-0.016 -0.031,-0.032 -0.047,-0.051l-46.988,-46.981l-0.004,-0.003c-0.023,-0.024 -0.047,-0.047 -0.066,-0.067Zm-58.652,21.773c4.941,-4.941 11.511,-7.664 18.504,-7.664c6.992,0 13.566,2.723 18.511,7.668l47.035,47.035c10.157,10.211 10.137,26.782 -0.047,36.973c-4.945,4.941 -11.523,7.664 -18.515,7.664c-6.992,0 -13.567,-2.723 -18.508,-7.668c-0.062,-0.059 -0.121,-0.121 -0.184,-0.18l-46.652,-46.652c-0.051,-0.051 -0.101,-0.105 -0.148,-0.152c-4.945,-4.945 -7.668,-11.516 -7.668,-18.508c-0.004,-6.992 2.723,-13.566 7.672,-18.516Zm-29.215,125.277c-0.059,-0.062 -0.117,-0.121 -0.176,-0.179c-10.187,-10.207 -10.179,-26.801 0.02,-37c4.945,-4.945 11.519,-7.668 18.511,-7.668c6.993,0 13.571,2.723 18.516,7.672l35.234,35.23c10.207,10.207 10.207,26.817 0.004,37.024c-4.945,4.945 -11.519,7.667 -18.508,7.667l-0.004,0c-1.285,0 -2.554,-0.093 -3.804,-0.273c-0.094,-0.02 -0.188,-0.031 -0.281,-0.047c-5.391,-0.84 -10.368,-3.332 -14.317,-7.234c-0.039,-0.043 -0.082,-0.082 -0.121,-0.125l-35.074,-35.067Zm178.453,149.934c-0.133,-0.036 -0.262,-0.051 -0.391,-0.082c-0.309,-0.075 -0.613,-0.145 -0.922,-0.2c-0.226,-0.039 -0.453,-0.066 -0.683,-0.097c-0.254,-0.032 -0.508,-0.063 -0.766,-0.082c-0.27,-0.024 -0.539,-0.035 -0.809,-0.039c-0.218,-0.008 -0.433,-0.012 -0.652,-0.008c-0.289,0.004 -0.574,0.019 -0.859,0.043c-0.207,0.015 -0.414,0.031 -0.622,0.054c-0.281,0.032 -0.558,0.075 -0.835,0.122c-0.219,0.035 -0.434,0.078 -0.653,0.125c-0.254,0.054 -0.504,0.113 -0.754,0.183c-0.238,0.063 -0.476,0.133 -0.715,0.207c-0.222,0.074 -0.437,0.153 -0.656,0.231c-0.254,0.097 -0.508,0.195 -0.754,0.304c-0.199,0.086 -0.398,0.18 -0.593,0.274c-0.25,0.121 -0.497,0.246 -0.739,0.383c-0.199,0.109 -0.394,0.226 -0.59,0.347c-0.218,0.137 -0.441,0.274 -0.656,0.422c-0.219,0.152 -0.433,0.313 -0.644,0.477c-0.176,0.133 -0.348,0.265 -0.52,0.406c-0.25,0.211 -0.488,0.43 -0.723,0.656c-0.089,0.086 -0.187,0.16 -0.277,0.25l-33.636,33.637c-10.043,10.043 -26.379,10.043 -36.422,0l-59.332,-59.332c-5.039,-5.039 -8.004,-11.488 -8.344,-18.164c-0.308,-6.082 1.649,-11.563 5.52,-15.43l21.195,-21.199c0.324,0.004 0.648,0.024 0.976,0.024l0.004,0c15.188,0 29.469,-5.918 40.215,-16.661c16.441,-16.445 20.68,-40.531 12.734,-60.929c10.672,-2.25 20.481,-7.523 28.379,-15.414c9.02,-9.027 14.371,-20.359 16.047,-32.117c4.586,1.176 9.348,1.789 14.203,1.789l0.004,0c15.188,0 29.469,-5.914 40.215,-16.656c10.742,-10.742 16.66,-25.028 16.66,-40.219c0,-4.848 -0.609,-9.601 -1.777,-14.18c12.109,-1.718 23.296,-7.273 32.097,-16.082c22.176,-22.171 22.176,-58.253 0.004,-80.433l-13.172,-13.172l12.11,-12.105c10.21,-10.207 26.82,-10.207 37.023,-0.004c4.945,4.945 7.668,11.519 7.668,18.512c0,6.992 -2.723,13.566 -7.668,18.511c-2.996,2.996 -4.496,6.926 -4.496,10.856c0,3.925 1.496,7.855 4.496,10.851c5.996,5.996 15.711,5.996 21.707,0c10.203,-10.203 26.812,-10.207 37.023,0c10.207,10.211 10.203,26.82 -0.004,37.027c-2.996,2.997 -4.496,6.926 -4.496,10.852c0,3.93 1.496,7.855 4.496,10.851c5.996,5.997 15.711,5.997 21.707,0.004c4.946,-4.945 11.524,-7.668 18.516,-7.671c6.992,0 13.566,2.722 18.508,7.667c4.949,4.95 7.671,11.524 7.671,18.516c0,6.898 -2.652,13.391 -7.472,18.309c-0.071,0.07 -0.141,0.136 -0.207,0.207l-22.012,22.015c-5.48,5.481 -6.015,14.188 -1.246,20.297l0.789,1.012c4.758,6.097 13.313,7.699 19.953,3.742c10.293,-6.133 23.406,-4.488 31.891,3.992c10.207,10.211 10.207,26.82 0,37.027l-35.231,35.227c-0.066,0.07 -0.136,0.14 -0.203,0.211l-75.621,75.621c-37.941,37.941 -93.753,52.898 -145.659,39.035Zm227.882,-52.571c6.73,6.735 4.129,21.153 -5.571,30.852l-59.335,59.332c-10.043,10.043 -26.379,10.043 -36.422,0l-16.328,-16.328c21.222,-9.059 40.66,-22.133 57.14,-38.613l47.875,-47.879l12.641,12.636Z\" style=\"fill-rule:nonzero;\" /></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">願景</h4><div class=\"fz-18\">成為人文、科技、團隊、學習、資訊的長庚</div></div></div></li></ul></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "1",  lang="en" , category="1", sortIndex=1,title = "宗旨與理念",status="Y", content="<section class=\"about__idea layout__container-1400\"><div class=\"layout__textimg\"><div class=\"col\"><div class=\"col__img imgLiquidFill\"><img src=\"/images/05-system/page2/1.jpg\" alt=\"\"></div><div class=\"col__text\"><div class=\"fz-17\">長庚醫院創設於1976年，當時台灣醫療設施嚴重不足，平均每萬人僅有病床數19床， 與現代化國家每萬人有 40 床的水準，相距太遠。於是我們先後設立了台北、林口、基 隆、高雄、桃園、嘉義、雲林等大型醫院，每日診治病患 31,500 人次以上，擁有病床 數 9,000 床。是遠東地區規模最大、設備最完善、經營績效最佳的綜合醫院之一。為 了對幼兒提供更專業的醫療服務，長庚醫院於 1993 年及 1994 年分別於林口及高雄創 設大型的兒童醫學中心，擁有病床數 800 床。並且，為了使醫療資源作最有效的運用， 我們也於 2001 年初創設護理之家，並於 2003 年 12 月設立兼具急、慢性醫療之桃園 長庚醫院，發展亞急性、慢性醫療及長期照護，垂直整合成為完整之醫療體系，提供 民眾完整醫療照護。</div></div></div><div class=\"col\"><div class=\"col__img imgLiquidFill\"><img src=\"/images/05-system/page2/1-1.jpg\" alt=\"\"></div><div class=\"col__text\"><div class=\"fz-17\">與此同時，鑑於國內 65 歲以上的老年人口比例已超過 12.5%，為 因應高齡化社會的來臨，我們在 2005 年 1 月開放養生文化村，提供老年人口一個可 以繼續經營晚年生活的安養社區。另為發揚中國固有醫療文化，結合西醫現代化及科 學化的技巧和方法，我們率先於醫學中心級醫院設立中醫部門，並朝提供中西醫整合 性醫療之目標邁進。為提升醫療服務，已成立癌症中心，建構以重症為中心的專科特 色醫療，更投資數十億元，於林口長庚設立永慶尖端醫療園區，成立亞洲最大，國內 第一所質子放射治療中心，已於 104 年 11 月開始提供服務。</div></div></div></div><ul class=\"ideas ul-reset\"><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg fix-1\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 53 36\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><g><path d=\"M38.351,26.882c-2.733,3.625 -7.095,6.043 -11.929,6.043c-4.835,0 -9.197,-2.418 -11.93,-6.043c-3.206,-4.309 -8.304,-7.096 -14.031,-7.096l-0.263,0l0,2.628l0.21,0c4.94,0 9.354,2.418 12.034,6.097c3.206,4.256 8.303,6.989 13.98,6.989c5.675,0 10.773,-2.733 13.978,-6.989c2.733,-3.679 7.095,-6.097 12.035,-6.097l0.21,0l0,-2.628l-0.21,0c-5.781,0 -10.878,2.787 -14.084,7.096\" style=\"fill-rule:nonzero;\" /><path d=\"M35.145,16.266l0,3.521l-6.989,0l0,6.99l-1.735,0l-1.734,0l0,-5.255l0,-1.735l-1.734,0l-5.256,0l0,-3.521l6.99,0l0,-1.734l0,-5.255l3.469,0l0,6.989l6.989,0Zm-8.724,-10.51l-5.255,0l0,6.989l-6.989,0l0,10.51l6.989,0l0,7.042l10.458,0l0,-7.042l7.042,0l0,-10.51l-7.042,0l0,-6.989l-5.203,0Z\" style=\"fill-rule:nonzero;\" /><path d=\"M40.4,7.543c-3.206,-4.257 -8.303,-7.043 -13.979,-7.043c-5.676,0 -10.773,2.786 -13.979,6.99c-2.679,3.731 -7.094,6.148 -12.034,6.148l-0.211,0l0,2.628l0.263,0c5.729,0 10.827,-2.785 14.032,-7.094c2.733,-3.679 7.094,-6.044 11.929,-6.044c4.835,0 9.197,2.365 11.93,6.044c3.205,4.309 8.303,7.094 14.083,7.094l0.211,0l0,-2.628l-0.211,0c-4.939,0 -9.301,-2.417 -12.034,-6.095\" style=\"fill-rule:nonzero;\" /></g></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">宗旨</h4><div class=\"fz-18\">不以營利為目的，從事醫療事業，並秉持「取之於社會、用之於社會、止於至善、 永續經營」理念善盡社會責任，以促進社會公益福利。</div></div></div></li><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 511 511\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><path d=\"M506.103,431.815l-115.633,-161.278c-4.846,-6.76 -12.335,-10.635 -20.553,-10.635c-8.215,0 -15.708,3.879 -20.55,10.635l-45.836,63.93l-100.389,-137.146l0,-55.279l86.467,0c10.211,0 18.519,-8.312 18.519,-18.522l0,-66.078c0,-10.21 -8.308,-18.518 -18.519,-18.518l-97.898,0c-10.211,0 -18.519,8.308 -18.519,18.518l0,139.918l-167.976,231.843c-6.085,8.487 -6.92,20.012 -2.12,29.353c4.651,9.056 13.746,14.686 23.73,14.686l458.72,0c9.509,0 18.148,-5.33 22.554,-13.906c4.499,-8.753 3.715,-19.556 -1.997,-27.521Zm-302.961,-362.942l75.036,0l0,43.216l-75.036,0l0,-43.216Zm-171.15,374.42l156.237,-215.641l103.232,141.029l51.774,74.612l-311.243,0Zm347.695,0l-57.708,-83.162l47.942,-66.861l107.559,150.023l-97.793,0Z\" style=\"fill-rule:nonzero;\" /></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">任務</h4><div class=\"fz-18\">服務、教學、研究</div></div></div></li><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 512 512\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><path id=\"surface1\" d=\"M507.605,486.395l-60.293,-60.297c41.813,-46.91 64.688,-106.743 64.688,-170.098c0,-49.285 -13.867,-96.645 -40.207,-137.773l35.812,-35.813c3.676,-3.68 5.188,-8.996 3.993,-14.055c-1.196,-5.058 -4.922,-9.14 -9.856,-10.781l-35.484,-11.832l-11.828,-35.488c-1.649,-4.93 -5.727,-8.66 -10.785,-9.856c-5.059,-1.191 -10.375,0.317 -14.055,3.993l-35.813,35.816c-41.132,-26.34 -88.496,-40.211 -137.777,-40.211c-68.379,0 -132.668,26.629 -181.02,74.98c-48.351,48.352 -74.98,112.641 -74.98,181.02c0,63.355 22.875,123.188 64.691,170.098l-60.296,60.297c-5.86,5.855 -5.86,15.355 0,21.21c2.929,2.93 6.765,4.395 10.605,4.395c3.84,0 7.68,-1.465 10.605,-4.395l60.297,-60.296c46.911,41.82 106.743,64.691 170.098,64.691c63.355,0 123.188,-22.875 170.098,-64.691l60.297,60.296c2.929,2.93 6.765,4.395 10.605,4.395c3.84,0 7.68,-1.465 10.605,-4.395c5.86,-5.855 5.86,-15.351 0,-21.21Zm-73.96,-443.625l6.523,19.574c1.492,4.48 5.008,7.996 9.484,9.488l19.578,6.527l-33.085,33.086l-26.692,-8.898l-8.898,-26.692l33.09,-33.085Zm-112.309,213.23c0,36.023 -29.313,65.332 -65.336,65.332c-36.023,0 -65.332,-29.309 -65.332,-65.332c0,-36.023 29.309,-65.332 65.332,-65.332c12.59,0 24.352,3.586 34.336,9.781l-44.941,44.946c-5.86,5.859 -5.86,15.355 0,21.21c2.925,2.93 6.765,4.395 10.605,4.395c3.84,0 7.68,-1.465 10.605,-4.395l44.946,-44.941c6.195,9.984 9.785,21.746 9.785,34.336Zm-9.399,-77.148c-15.722,-11.43 -35.054,-18.184 -55.937,-18.184c-52.566,0 -95.332,42.766 -95.332,95.332c0,52.566 42.766,95.332 95.332,95.332c52.566,0 95.336,-42.766 95.336,-95.332c0,-20.883 -6.754,-40.211 -18.188,-55.938l35.864,-35.859c20.402,25.07 32.656,57.027 32.656,91.797c0,80.32 -65.348,145.668 -145.668,145.668c-80.32,0 -145.664,-65.348 -145.664,-145.668c0,-80.32 65.344,-145.668 145.664,-145.668c34.77,0 66.727,12.254 91.801,32.656l-35.864,35.864Zm-281.937,77.148c0,-124.617 101.383,-226 226,-226c41.297,0 81.059,11.02 116.031,32.012c-3.433,3.98 -4.551,9.5 -2.871,14.539l11.274,33.804l-11.34,11.34c-30.582,-25.793 -70.051,-41.363 -113.094,-41.363c-96.863,0 -175.664,78.805 -175.664,175.668c0,96.863 78.801,175.668 175.664,175.668c96.863,0 175.668,-78.805 175.668,-175.668c0,-43.043 -15.57,-82.512 -41.363,-113.094l11.34,-11.34l33.808,11.274c1.555,0.515 3.152,0.769 4.742,0.769c3.555,0 7.035,-1.277 9.789,-3.652c20.996,34.977 32.016,74.742 32.016,116.043c0,124.617 -101.383,226 -226,226c-124.617,0 -226,-101.383 -226,-226Z\" style=\"fill-rule:nonzero;\" /></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">目標</h4><div class=\"fz-18\">要做就做最好</div></div></div></li><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 512 512\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><g id=\"surface1\"><path d=\"M256,0c-88.004,0 -159.602,71.598 -159.602,159.602c0,50.648 23.879,97.91 64.266,128.003l0,13.329c-9.836,8.644 -16.062,21.304 -16.062,35.398c0,12.41 4.828,23.711 12.695,32.137c-7.867,8.426 -12.695,19.722 -12.695,32.133c0,21.14 13.988,39.074 33.199,45.027c6.179,37.594 38.883,66.371 78.199,66.371c39.316,0 72.023,-28.777 78.203,-66.371c19.207,-5.957 33.199,-23.887 33.199,-45.031c0,-12.407 -4.828,-23.707 -12.695,-32.133c7.867,-8.426 12.695,-19.723 12.695,-32.133c0,-14.094 -6.226,-26.754 -16.066,-35.402l0,-13.325c40.387,-30.093 64.266,-77.355 64.266,-128.003c0,-88.004 -71.598,-159.602 -159.602,-159.602Zm64.27,319.199c9.445,0 17.132,7.688 17.132,17.133c0,9.449 -7.687,17.137 -17.132,17.137l-128.536,0c-9.449,0 -17.136,-7.688 -17.136,-17.137c0,-9.445 7.687,-17.133 17.136,-17.133l128.536,0Zm-64.27,162.801c-21.938,0 -40.57,-14.414 -46.934,-34.266l93.868,0c-6.36,19.856 -24.996,34.266 -46.934,34.266Zm64.27,-64.266l-128.536,0c-9.449,0 -17.136,-7.687 -17.136,-17.132c0,-9.45 7.687,-17.133 17.136,-17.133l128.536,0c9.445,0 17.132,7.683 17.132,17.133c0,9.445 -7.687,17.132 -17.132,17.132Zm7.722,-150.355c-4.16,2.785 -6.656,7.461 -6.656,12.465l0,9.383c-0.356,-0.008 -0.711,-0.028 -1.066,-0.028l-17.137,0l0,-59.117l21.539,-21.539c5.859,-5.859 5.859,-15.356 0,-21.215c-5.856,-5.859 -15.356,-5.859 -21.211,0l-25.934,25.934c-2.812,2.812 -4.394,6.625 -4.394,10.605l0,65.332l-34.266,0l0,-65.332c0,-3.976 -1.582,-7.793 -4.394,-10.605l-25.934,-25.934c-5.855,-5.855 -15.351,-5.855 -21.211,0c-5.859,5.859 -5.859,15.356 0,21.215l21.539,21.539l0,59.121l-17.133,0c-0.359,0 -0.714,0.016 -1.07,0.024l0,-9.383c0,-5.004 -2.496,-9.68 -6.656,-12.465c-36.07,-24.149 -57.61,-64.438 -57.61,-107.777c0,-71.465 58.141,-129.602 129.602,-129.602c71.461,0 129.602,58.137 129.602,129.602c0,43.339 -21.539,83.628 -57.61,107.777Z\" style=\"fill-rule:nonzero;\" /><path d=\"M62.133,159.602c0,-8.286 -6.715,-15 -15,-15l-32.133,0c-8.285,0 -15,6.714 -15,15c0,8.281 6.715,15 15,15l32.133,0c8.285,0 15,-6.719 15,-15Z\" style=\"fill-rule:nonzero;\" /><path d=\"M497,144.602l-32.133,0c-8.281,0 -15,6.714 -15,15c0,8.281 6.719,15 15,15l32.133,0c8.285,0 15,-6.719 15,-15c0,-8.286 -6.715,-15 -15,-15Z\" style=\"fill-rule:nonzero;\" /><path d=\"M67.613,251.043l-27.828,16.066c-7.172,4.145 -9.629,13.317 -5.488,20.493c2.777,4.812 7.82,7.5 13.004,7.5c2.543,0 5.125,-0.645 7.484,-2.012l27.828,-16.063c7.176,-4.144 9.633,-13.316 5.492,-20.492c-4.14,-7.176 -13.316,-9.633 -20.492,-5.492Z\" style=\"fill-rule:nonzero;\" /><path d=\"M436.898,70.172c2.543,0 5.122,-0.649 7.485,-2.012l27.832,-16.07c7.172,-4.141 9.633,-13.313 5.488,-20.488c-4.144,-7.176 -13.316,-9.633 -20.488,-5.489l-27.832,16.067c-7.172,4.14 -9.633,13.316 -5.488,20.488c2.777,4.812 7.82,7.504 13.003,7.504Z\" style=\"fill-rule:nonzero;\" /><path d=\"M472.211,267.109l-27.828,-16.062c-7.176,-4.141 -16.352,-1.684 -20.492,5.492c-4.141,7.172 -1.68,16.348 5.492,20.488l27.832,16.063c2.359,1.363 4.937,2.012 7.484,2.012c5.184,0 10.227,-2.692 13.004,-7.504c4.145,-7.172 1.684,-16.348 -5.492,-20.489Z\" style=\"fill-rule:nonzero;\" /><path d=\"M82.617,42.176l-27.828,-16.063c-7.176,-4.144 -16.348,-1.683 -20.488,5.492c-4.145,7.176 -1.684,16.348 5.488,20.489l27.828,16.066c2.363,1.363 4.942,2.012 7.485,2.012c5.187,0 10.226,-2.692 13.007,-7.504c4.141,-7.176 1.68,-16.348 -5.492,-20.492Z\" style=\"fill-rule:nonzero;\" /></g></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">理念</h4><div class=\"fz-18\">取之社會，用之社會<br>人本濟世，病患優先<br>勤勞樸實，深耕生根</div></div></div></li><li><div class=\"wrap\"><div class=\"img\"><div class=\"svg\"><svg width=\"100%\" height=\"100%\" viewBox=\"0 0 512 512\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xml:space=\"preserve\" xmlns:serif=\"http://www.serif.com/\" style=\"fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:1.41421;\"><path id=\"surface1\" d=\"M460.339,340.679l35.027,-35.031c22.175,-22.172 22.179,-58.253 0.004,-80.437c-6.805,-6.801 -14.977,-11.578 -23.707,-14.226c7.14,-9.7 11.004,-21.375 11.004,-33.645c0,-15.191 -5.918,-29.476 -16.664,-40.219c-10.739,-10.742 -25.024,-16.66 -40.219,-16.66c-0.637,0 -1.274,0.012 -1.91,0.032c0.496,-15.184 -5.035,-30.535 -16.602,-42.106c-11.566,-11.562 -26.937,-17.101 -42.097,-16.597c0.019,-0.633 0.031,-1.274 0.031,-1.911c0,-15.195 -5.918,-29.476 -16.66,-40.218c-22.172,-22.172 -58.254,-22.176 -80.437,0l-12.11,12.109l-12.105,-12.105c-10.742,-10.746 -25.027,-16.664 -40.223,-16.664c-15.191,0 -29.472,5.918 -40.214,16.656c-11.571,11.57 -17.09,26.926 -16.586,42.121c-0.641,-0.024 -1.282,-0.047 -1.922,-0.047c-15.192,0 -29.477,5.914 -40.227,16.656c-11.566,11.574 -17.086,26.93 -16.582,42.121c-0.644,-0.023 -1.285,-0.047 -1.929,-0.047c-15.192,0 -29.477,5.918 -40.211,16.657c-10.746,10.742 -16.664,25.027 -16.664,40.222c0,12.266 3.859,23.941 11.004,33.637c-8.86,2.672 -16.981,7.507 -23.711,14.234c-22.121,22.129 -22.172,58.098 -0.149,80.281c0.055,0.059 0.11,0.113 0.164,0.172l35.079,35.07c0.046,0.051 0.097,0.102 0.148,0.152c2.105,2.11 4.355,4.02 6.715,5.75l-10.375,10.375c-10.09,10.09 -15.231,23.836 -14.469,38.707c0.727,14.278 6.871,27.879 17.293,38.301l59.332,59.336c11.008,11.004 25.461,16.508 39.918,16.508c14.457,0 28.914,-5.504 39.918,-16.508l27.578,-27.582c12.582,2.699 25.398,4.07 38.226,4.07c12.816,0 25.637,-1.363 38.215,-4.062l27.57,27.574c11.008,11.004 25.461,16.508 39.918,16.508c14.453,-0.004 28.914,-5.504 39.918,-16.508l59.336,-59.336c22.007,-22.008 24.507,-55.32 5.57,-74.261l-12.637,-12.637l6.234,-6.238c0.071,-0.067 0.137,-0.133 0.208,-0.204Zm-275.175,-299.311c4.945,-4.945 11.519,-7.668 18.511,-7.668c6.992,0 13.567,2.723 18.512,7.668l46.98,46.98c10.207,10.211 10.207,26.82 0,37.027c-4.945,4.946 -11.519,7.668 -18.508,7.668l-0.003,0c-6.993,-0.004 -13.563,-2.722 -18.508,-7.668c-0.008,-0.007 -0.016,-0.015 -0.02,-0.019l-46.953,-46.953c-0.039,-0.039 -0.074,-0.078 -0.117,-0.113c-10.101,-10.223 -10.066,-26.754 0.106,-36.922Zm-58.801,95.683c-10.137,-10.215 -10.113,-26.773 0.066,-36.953c4.95,-4.945 11.528,-7.668 18.52,-7.668c6.984,0 13.551,2.719 18.496,7.66c0.004,0 0.008,0.004 0.008,0.008c0.019,0.016 0.035,0.031 0.05,0.047l46.926,46.926c0.027,0.031 0.063,0.062 0.094,0.093l11.66,11.66c4.945,4.946 7.672,11.52 7.672,18.516c0,6.992 -2.723,13.566 -7.668,18.508c-4.945,4.945 -11.52,7.668 -18.512,7.668c-6.992,0 -13.566,-2.723 -18.508,-7.668c-0.062,-0.063 -0.125,-0.121 -0.187,-0.18l-11.512,-11.515c-0.015,-0.016 -0.031,-0.032 -0.047,-0.051l-46.988,-46.981l-0.004,-0.003c-0.023,-0.024 -0.047,-0.047 -0.066,-0.067Zm-58.652,21.773c4.941,-4.941 11.511,-7.664 18.504,-7.664c6.992,0 13.566,2.723 18.511,7.668l47.035,47.035c10.157,10.211 10.137,26.782 -0.047,36.973c-4.945,4.941 -11.523,7.664 -18.515,7.664c-6.992,0 -13.567,-2.723 -18.508,-7.668c-0.062,-0.059 -0.121,-0.121 -0.184,-0.18l-46.652,-46.652c-0.051,-0.051 -0.101,-0.105 -0.148,-0.152c-4.945,-4.945 -7.668,-11.516 -7.668,-18.508c-0.004,-6.992 2.723,-13.566 7.672,-18.516Zm-29.215,125.277c-0.059,-0.062 -0.117,-0.121 -0.176,-0.179c-10.187,-10.207 -10.179,-26.801 0.02,-37c4.945,-4.945 11.519,-7.668 18.511,-7.668c6.993,0 13.571,2.723 18.516,7.672l35.234,35.23c10.207,10.207 10.207,26.817 0.004,37.024c-4.945,4.945 -11.519,7.667 -18.508,7.667l-0.004,0c-1.285,0 -2.554,-0.093 -3.804,-0.273c-0.094,-0.02 -0.188,-0.031 -0.281,-0.047c-5.391,-0.84 -10.368,-3.332 -14.317,-7.234c-0.039,-0.043 -0.082,-0.082 -0.121,-0.125l-35.074,-35.067Zm178.453,149.934c-0.133,-0.036 -0.262,-0.051 -0.391,-0.082c-0.309,-0.075 -0.613,-0.145 -0.922,-0.2c-0.226,-0.039 -0.453,-0.066 -0.683,-0.097c-0.254,-0.032 -0.508,-0.063 -0.766,-0.082c-0.27,-0.024 -0.539,-0.035 -0.809,-0.039c-0.218,-0.008 -0.433,-0.012 -0.652,-0.008c-0.289,0.004 -0.574,0.019 -0.859,0.043c-0.207,0.015 -0.414,0.031 -0.622,0.054c-0.281,0.032 -0.558,0.075 -0.835,0.122c-0.219,0.035 -0.434,0.078 -0.653,0.125c-0.254,0.054 -0.504,0.113 -0.754,0.183c-0.238,0.063 -0.476,0.133 -0.715,0.207c-0.222,0.074 -0.437,0.153 -0.656,0.231c-0.254,0.097 -0.508,0.195 -0.754,0.304c-0.199,0.086 -0.398,0.18 -0.593,0.274c-0.25,0.121 -0.497,0.246 -0.739,0.383c-0.199,0.109 -0.394,0.226 -0.59,0.347c-0.218,0.137 -0.441,0.274 -0.656,0.422c-0.219,0.152 -0.433,0.313 -0.644,0.477c-0.176,0.133 -0.348,0.265 -0.52,0.406c-0.25,0.211 -0.488,0.43 -0.723,0.656c-0.089,0.086 -0.187,0.16 -0.277,0.25l-33.636,33.637c-10.043,10.043 -26.379,10.043 -36.422,0l-59.332,-59.332c-5.039,-5.039 -8.004,-11.488 -8.344,-18.164c-0.308,-6.082 1.649,-11.563 5.52,-15.43l21.195,-21.199c0.324,0.004 0.648,0.024 0.976,0.024l0.004,0c15.188,0 29.469,-5.918 40.215,-16.661c16.441,-16.445 20.68,-40.531 12.734,-60.929c10.672,-2.25 20.481,-7.523 28.379,-15.414c9.02,-9.027 14.371,-20.359 16.047,-32.117c4.586,1.176 9.348,1.789 14.203,1.789l0.004,0c15.188,0 29.469,-5.914 40.215,-16.656c10.742,-10.742 16.66,-25.028 16.66,-40.219c0,-4.848 -0.609,-9.601 -1.777,-14.18c12.109,-1.718 23.296,-7.273 32.097,-16.082c22.176,-22.171 22.176,-58.253 0.004,-80.433l-13.172,-13.172l12.11,-12.105c10.21,-10.207 26.82,-10.207 37.023,-0.004c4.945,4.945 7.668,11.519 7.668,18.512c0,6.992 -2.723,13.566 -7.668,18.511c-2.996,2.996 -4.496,6.926 -4.496,10.856c0,3.925 1.496,7.855 4.496,10.851c5.996,5.996 15.711,5.996 21.707,0c10.203,-10.203 26.812,-10.207 37.023,0c10.207,10.211 10.203,26.82 -0.004,37.027c-2.996,2.997 -4.496,6.926 -4.496,10.852c0,3.93 1.496,7.855 4.496,10.851c5.996,5.997 15.711,5.997 21.707,0.004c4.946,-4.945 11.524,-7.668 18.516,-7.671c6.992,0 13.566,2.722 18.508,7.667c4.949,4.95 7.671,11.524 7.671,18.516c0,6.898 -2.652,13.391 -7.472,18.309c-0.071,0.07 -0.141,0.136 -0.207,0.207l-22.012,22.015c-5.48,5.481 -6.015,14.188 -1.246,20.297l0.789,1.012c4.758,6.097 13.313,7.699 19.953,3.742c10.293,-6.133 23.406,-4.488 31.891,3.992c10.207,10.211 10.207,26.82 0,37.027l-35.231,35.227c-0.066,0.07 -0.136,0.14 -0.203,0.211l-75.621,75.621c-37.941,37.941 -93.753,52.898 -145.659,39.035Zm227.882,-52.571c6.73,6.735 4.129,21.153 -5.571,30.852l-59.335,59.332c-10.043,10.043 -26.379,10.043 -36.422,0l-16.328,-16.328c21.222,-9.059 40.66,-22.133 57.14,-38.613l47.875,-47.879l12.641,12.636Z\" style=\"fill-rule:nonzero;\" /></svg></div></div><div class=\"text tac\"><h4 class=\"fz-20 fwb\">願景</h4><div class=\"fz-18\">成為人文、科技、團隊、學習、資訊的長庚</div></div></div></li></ul></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "2",  lang="tw" , category="1", sortIndex=2,title = "創辦人的話",status="Y", content="<section class=\"about__founder layout__container-1400 tac\"><h3 class=\"fz-36 fwb\">創辦人的話</h3><div class=\"fz-17\">我們必須與時俱進，永無止盡的謀求改善、再改善，以此奠定良好經營基礎，確保—「回饋社會」，<br>「利益眾生」的事業可以永續發展，充分冠測醫療服務之職志。</div><ul class=\"imgs ul-reset\"><li><div class=\"img imgLiquidFill\"><img src=\"/images/05-system/page2/1-2.jpg\" alt=\"\"></div></li><li><div class=\"img imgLiquidFill\"><img src=\"/images/05-system/page2/1-3.jpg\" alt=\"\"></div></li><li><div class=\"img imgLiquidFill\"><img src=\"/images/05-system/page2/1-4.jpg\" alt=\"\"></div></li><li><div class=\"img imgLiquidFill\"><img src=\"/images/05-system/page2/1-5.jpg\" alt=\"\"></div></li></ul><div class=\"decline\"></div></section><section class=\"about__speech layout__container-1400 pd__section-footer\"><article class=\"fz-17\"><h4 class=\"fz-28 fwb tac\">成立三十周年感言</h4><p>長庚紀念醫院於民國六十五年十二月開幕啟用，到今天已經屆滿三十年。中國的古聖先賢說：「三十而立」，強調一個有為之人應該在三十歲就能夠立足於社會，並有所成就。如果用這個觀點來衡量，長庚紀念醫院歷經三十年的發展到今天，在所有同仁持之以恆，持續不斷謀求改善之下，可以說是已經達到「三十而立」的成熟階段，無論是醫院規模，或者服務病患的質與量各方面，都已逐步提升至世界級的水準。身為此一醫療服務機構的創辦人，內心確實感到無比的欣慰，同時也由衷感謝醫院各級主管和全體同仁長期以來的努力奉獻。</p><p>回想三十年前，當時台灣整體醫療水準及相關設施均亟待加強，再加上醫療體制的不健全，國人生病就醫常遭遇重重困難，因此而造成生命和健康方面無可彌補的遺憾，甚至於釀成許多家庭的不幸；我的父親也是因為這樣而不幸辭世，令人至感悲痛。為了設法防止相同的悲劇一再重演，也基於回饋社會的使命感，我們在民國六十五年毅然投入以提升台灣醫療水準、維護國人生命健康為宗旨的醫療服務事業。</p><p>長庚紀念醫院經過全體同仁三十年來胼手胝足的辛勤耕耘，已經有了可觀的成就。目前在基隆、台北、林口、嘉義及高雄等地共設立了五個主要院區，擁有八千六百多張病床，每日門急診病患高達二萬六千人次。在此過程當中，為了配合社會發展趨勢所衍生的醫療照護需求，我們不斷擴充服務規模，除了設立守護兒童健康的「兒童醫院」、發揚中國傳統醫學的「中醫醫院」、專業照顧慢性病患、植物人及安寧照護的「桃園分院」與「護理之家」，以及提供銀髮族安享天年的「養生文化村」等一系列健康照護體系外，同時也正積極在雲嘉等醫療資源相對貧乏之地區，展開新建及擴建計畫，為當地居民就近提供良好的醫療服務。</p><p>除了醫療規模及服務項目的持續擴大之外，我們更認為，醫院既是因人而存在，所以我們從一開始即秉持「以人為本」、「病患優先」的經營理念，在醫院創辦初期即陸續締造許多先例，一一打破當時醫界普遍存在，而且沿襲已久的種種陋習，諸如取消住院保證金制度，一切以救治病患做為最優先考量，徹底排除因為經濟因素而延誤病情；設想一切可行辦法，大幅降低洗腎費用，助使病患可以依照實際需要增加洗腎次數，藉以維持良好體能狀態，能夠正常工作及生活，並且挽救其家庭所面臨的經濟危機；領導推動器官捐贈風氣，展現往生者遺愛人間，挽救寶貴生命的慈悲情懷；改革醫師薪給制度，並明令禁收紅包，以杜絕不良的醫界陋習等等。這些創舉，在在都產生了導引台灣醫界走向良性發展的明顯效果，對於增進病患福祉裨益良多，同時也是值得我們全體同仁感到光榮及驕傲的所在。</p><p>除了上述開創醫界善良風氣的創舉之外，另外一段令人印象深刻，並且充分突顯設立長庚紀念醫院重要意義所在的往事，也是頗為值得一提。民國六十七年，長庚紀念醫院林口醫學中心開幕前夕，當時的行政院院長蔣經國先生前來參觀，對於各項先進的軟硬體設施倍感震撼，也讓他感受到長庚紀念醫院的強大競爭壓力，所以回去以後不久，前後各撥出上百億資金，分別補助榮民總醫院和台大醫院，供其大舉擴充設備，之後持續好幾年，也都是動輒以數十億元補助各公立醫院，對其擴大醫療服務規模助益良多，進而造福眾多病患。每每思及此一往事，都令我們感覺非常欣慰。經由長庚紀念醫院的介入參與，不但促使政府大手筆撥出經費，強化公立醫院的設施，同時也刺激各私立醫院致力提升其醫療服務品質。在此一相互激勵以促發向上提升的發展趨勢之下，不論是對於台灣醫療水準的進步，以及國人醫療福祉的充實，在在都產生了深遠的影響。</p><p>我們在三十年前籌辦長庚紀念醫院時，也面臨到醫師來源的問題。當時國內教學醫院的規模很少擴充，住院醫師訓練養成以後，因為沒有主治醫師的缺額，往往就要被迫離開，離開以後不是自行開設診所就是出國深造，所以資深的醫師人才自然不多。我們為了要創辦一所具有國際先進水準的醫院，沒有好的醫師來配合，無論如何是辦不到的。雖然如此，但是我們堅決相信，天下儘管沒有容易的事，可是也沒有不可能的事，除了借重國內資深的醫師之外，更曾經多次赴美，延聘各個專科的權威醫師人才，反覆說明創辦長庚紀念醫院的抱負和願景，終能引發共鳴，許多醫師都願意放棄在美國的事業，舉家遷返台灣，加入長庚紀念醫院的服務陣容，共同實踐提升醫療水準的宗旨理念。今天長庚紀念醫院能有若干成就，他們的貢獻也是不可磨滅的。</p><p>與此同時，我們也持續不斷用心努力，制訂醫院各項管理規章制度，並且不斷謀求精化，使之能夠充分配合醫院提供優良服務之需要。發展至今，不但前來就醫的病患人數節節升高，醫學研究方面也屢創佳績，同時各項管理成效卓著，甚至有先進國家的醫院管理顧問公司前來向我們學習。綜觀這三十年來所創造的豐碩成果，堪謂是醫院全體同仁共同努力有以致之，也普遍受到廣大民眾的肯定。</p><p>雖然如此，可是我們並不因此而引為滿足，為了配合社會發展，以及新增的醫療照護需求，我們從來沒有停止向前邁進的腳步，持續將服務的範圍擴及於生命歷程的各個相關領域，年復一年，使事業成立宗旨愈益充分貫徹，對社會及人群作出更廣大的貢獻。其所依賴者，除了回饋社會的情懷和使命感之外，還有就是實事求是，以「止於至善」做為終極目標，為此而勤奮不休的合理化精神。</p><p>長庚紀念醫院從創辦到今天已經屆滿三十年，這當中，雖然也曾經遭遇過種種的波折和困難，但是在全體同仁齊心努力設法因應之下，總能順利克服，甚至從中開啟新的發展契機。我們也深信，今後還會有無數個三十年等著我們不斷去謀求開創及發展，而無論外在環境如何演變，我都希望我們醫院的全體同仁，能夠將長庚紀念醫院「利益眾生」的基本理念牢記在心，並且事事都從這個出發點來設想，謀求妥善的對待之道。</p><p>為了實踐「利益眾生」之理念，在醫院來講，基本上就是要善待病患。而所謂善待病患，除了妥善運用醫學的專業知識和技能，用心尋求有效醫治之道，助使病患脫離病痛，回復健康，同時也要將心比心，體會病患種種可能的擔心和憂慮，並且給予適當的說明和開導，使其明瞭可能的病情演變，以及應該如何面對。因為當一個人有病痛在身時，連帶也會影響其心情，甚至產生脆弱、無助的感覺。這個時候，若是我們善待他，適時提供必要的幫助，通常都會讓他心生感激，進而可能啟發其幫助他人、善待他人的心念和行動。這股良性的影響力量，也會像是水面產生的漣漪持續不斷向四周擴散。醫院同仁若是以這樣的體會來從事自己份內的工作，日積月累的結果，對於營造和諧社會將會產生可觀的貢獻。我們更深信，具有這種工作信念和實際作為的同仁，其本身也將在日常工作中感受到快樂與滿足，由此而獲致良好的工作回報。準此而觀，實踐「利益眾生」之理念的結果，不但是利人，同時也是利己的，值得我們全體醫院同仁繼續努力以赴。</p><p>值此長庚紀念醫院三十週年之際，我們對於過去努力所獲致的種種成就，固然都深感與有榮焉，但是我們也必須深刻體認，由於外在環境條件不斷在變動，隨時隨地都有可能產生新的挑戰，所以任何人都不應該滿足於現狀。我們必須與時俱進，永無止境的謀求改善、再改善，以此奠定良好經營基礎，確保此一「回饋社會」，「利益眾生」的事業可以永續發展，充分貫徹醫療服務之職志。</p></article><div class=\"founder fz-18 tar\"><span>創辦人</span><span><img src=\"/images/05-system/page2/1-6.png\" alt=\"\"></span></div></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "2",  lang="en" , category="1", sortIndex=2,title = "創辦人的話",status="Y", content="<section class=\"about__founder layout__container-1400 tac\"><h3 class=\"fz-36 fwb\">創辦人的話</h3><div class=\"fz-17\">我們必須與時俱進，永無止盡的謀求改善、再改善，以此奠定良好經營基礎，確保—「回饋社會」，<br>「利益眾生」的事業可以永續發展，充分冠測醫療服務之職志。</div><ul class=\"imgs ul-reset\"><li><div class=\"img imgLiquidFill\"><img src=\"/images/05-system/page2/1-2.jpg\" alt=\"\"></div></li><li><div class=\"img imgLiquidFill\"><img src=\"/images/05-system/page2/1-3.jpg\" alt=\"\"></div></li><li><div class=\"img imgLiquidFill\"><img src=\"/images/05-system/page2/1-4.jpg\" alt=\"\"></div></li><li><div class=\"img imgLiquidFill\"><img src=\"/images/05-system/page2/1-5.jpg\" alt=\"\"></div></li></ul><div class=\"decline\"></div></section><section class=\"about__speech layout__container-1400 pd__section-footer\"><article class=\"fz-17\"><h4 class=\"fz-28 fwb tac\">成立三十周年感言</h4><p>長庚紀念醫院於民國六十五年十二月開幕啟用，到今天已經屆滿三十年。中國的古聖先賢說：「三十而立」，強調一個有為之人應該在三十歲就能夠立足於社會，並有所成就。如果用這個觀點來衡量，長庚紀念醫院歷經三十年的發展到今天，在所有同仁持之以恆，持續不斷謀求改善之下，可以說是已經達到「三十而立」的成熟階段，無論是醫院規模，或者服務病患的質與量各方面，都已逐步提升至世界級的水準。身為此一醫療服務機構的創辦人，內心確實感到無比的欣慰，同時也由衷感謝醫院各級主管和全體同仁長期以來的努力奉獻。</p><p>回想三十年前，當時台灣整體醫療水準及相關設施均亟待加強，再加上醫療體制的不健全，國人生病就醫常遭遇重重困難，因此而造成生命和健康方面無可彌補的遺憾，甚至於釀成許多家庭的不幸；我的父親也是因為這樣而不幸辭世，令人至感悲痛。為了設法防止相同的悲劇一再重演，也基於回饋社會的使命感，我們在民國六十五年毅然投入以提升台灣醫療水準、維護國人生命健康為宗旨的醫療服務事業。</p><p>長庚紀念醫院經過全體同仁三十年來胼手胝足的辛勤耕耘，已經有了可觀的成就。目前在基隆、台北、林口、嘉義及高雄等地共設立了五個主要院區，擁有八千六百多張病床，每日門急診病患高達二萬六千人次。在此過程當中，為了配合社會發展趨勢所衍生的醫療照護需求，我們不斷擴充服務規模，除了設立守護兒童健康的「兒童醫院」、發揚中國傳統醫學的「中醫醫院」、專業照顧慢性病患、植物人及安寧照護的「桃園分院」與「護理之家」，以及提供銀髮族安享天年的「養生文化村」等一系列健康照護體系外，同時也正積極在雲嘉等醫療資源相對貧乏之地區，展開新建及擴建計畫，為當地居民就近提供良好的醫療服務。</p><p>除了醫療規模及服務項目的持續擴大之外，我們更認為，醫院既是因人而存在，所以我們從一開始即秉持「以人為本」、「病患優先」的經營理念，在醫院創辦初期即陸續締造許多先例，一一打破當時醫界普遍存在，而且沿襲已久的種種陋習，諸如取消住院保證金制度，一切以救治病患做為最優先考量，徹底排除因為經濟因素而延誤病情；設想一切可行辦法，大幅降低洗腎費用，助使病患可以依照實際需要增加洗腎次數，藉以維持良好體能狀態，能夠正常工作及生活，並且挽救其家庭所面臨的經濟危機；領導推動器官捐贈風氣，展現往生者遺愛人間，挽救寶貴生命的慈悲情懷；改革醫師薪給制度，並明令禁收紅包，以杜絕不良的醫界陋習等等。這些創舉，在在都產生了導引台灣醫界走向良性發展的明顯效果，對於增進病患福祉裨益良多，同時也是值得我們全體同仁感到光榮及驕傲的所在。</p><p>除了上述開創醫界善良風氣的創舉之外，另外一段令人印象深刻，並且充分突顯設立長庚紀念醫院重要意義所在的往事，也是頗為值得一提。民國六十七年，長庚紀念醫院林口醫學中心開幕前夕，當時的行政院院長蔣經國先生前來參觀，對於各項先進的軟硬體設施倍感震撼，也讓他感受到長庚紀念醫院的強大競爭壓力，所以回去以後不久，前後各撥出上百億資金，分別補助榮民總醫院和台大醫院，供其大舉擴充設備，之後持續好幾年，也都是動輒以數十億元補助各公立醫院，對其擴大醫療服務規模助益良多，進而造福眾多病患。每每思及此一往事，都令我們感覺非常欣慰。經由長庚紀念醫院的介入參與，不但促使政府大手筆撥出經費，強化公立醫院的設施，同時也刺激各私立醫院致力提升其醫療服務品質。在此一相互激勵以促發向上提升的發展趨勢之下，不論是對於台灣醫療水準的進步，以及國人醫療福祉的充實，在在都產生了深遠的影響。</p><p>我們在三十年前籌辦長庚紀念醫院時，也面臨到醫師來源的問題。當時國內教學醫院的規模很少擴充，住院醫師訓練養成以後，因為沒有主治醫師的缺額，往往就要被迫離開，離開以後不是自行開設診所就是出國深造，所以資深的醫師人才自然不多。我們為了要創辦一所具有國際先進水準的醫院，沒有好的醫師來配合，無論如何是辦不到的。雖然如此，但是我們堅決相信，天下儘管沒有容易的事，可是也沒有不可能的事，除了借重國內資深的醫師之外，更曾經多次赴美，延聘各個專科的權威醫師人才，反覆說明創辦長庚紀念醫院的抱負和願景，終能引發共鳴，許多醫師都願意放棄在美國的事業，舉家遷返台灣，加入長庚紀念醫院的服務陣容，共同實踐提升醫療水準的宗旨理念。今天長庚紀念醫院能有若干成就，他們的貢獻也是不可磨滅的。</p><p>與此同時，我們也持續不斷用心努力，制訂醫院各項管理規章制度，並且不斷謀求精化，使之能夠充分配合醫院提供優良服務之需要。發展至今，不但前來就醫的病患人數節節升高，醫學研究方面也屢創佳績，同時各項管理成效卓著，甚至有先進國家的醫院管理顧問公司前來向我們學習。綜觀這三十年來所創造的豐碩成果，堪謂是醫院全體同仁共同努力有以致之，也普遍受到廣大民眾的肯定。</p><p>雖然如此，可是我們並不因此而引為滿足，為了配合社會發展，以及新增的醫療照護需求，我們從來沒有停止向前邁進的腳步，持續將服務的範圍擴及於生命歷程的各個相關領域，年復一年，使事業成立宗旨愈益充分貫徹，對社會及人群作出更廣大的貢獻。其所依賴者，除了回饋社會的情懷和使命感之外，還有就是實事求是，以「止於至善」做為終極目標，為此而勤奮不休的合理化精神。</p><p>長庚紀念醫院從創辦到今天已經屆滿三十年，這當中，雖然也曾經遭遇過種種的波折和困難，但是在全體同仁齊心努力設法因應之下，總能順利克服，甚至從中開啟新的發展契機。我們也深信，今後還會有無數個三十年等著我們不斷去謀求開創及發展，而無論外在環境如何演變，我都希望我們醫院的全體同仁，能夠將長庚紀念醫院「利益眾生」的基本理念牢記在心，並且事事都從這個出發點來設想，謀求妥善的對待之道。</p><p>為了實踐「利益眾生」之理念，在醫院來講，基本上就是要善待病患。而所謂善待病患，除了妥善運用醫學的專業知識和技能，用心尋求有效醫治之道，助使病患脫離病痛，回復健康，同時也要將心比心，體會病患種種可能的擔心和憂慮，並且給予適當的說明和開導，使其明瞭可能的病情演變，以及應該如何面對。因為當一個人有病痛在身時，連帶也會影響其心情，甚至產生脆弱、無助的感覺。這個時候，若是我們善待他，適時提供必要的幫助，通常都會讓他心生感激，進而可能啟發其幫助他人、善待他人的心念和行動。這股良性的影響力量，也會像是水面產生的漣漪持續不斷向四周擴散。醫院同仁若是以這樣的體會來從事自己份內的工作，日積月累的結果，對於營造和諧社會將會產生可觀的貢獻。我們更深信，具有這種工作信念和實際作為的同仁，其本身也將在日常工作中感受到快樂與滿足，由此而獲致良好的工作回報。準此而觀，實踐「利益眾生」之理念的結果，不但是利人，同時也是利己的，值得我們全體醫院同仁繼續努力以赴。</p><p>值此長庚紀念醫院三十週年之際，我們對於過去努力所獲致的種種成就，固然都深感與有榮焉，但是我們也必須深刻體認，由於外在環境條件不斷在變動，隨時隨地都有可能產生新的挑戰，所以任何人都不應該滿足於現狀。我們必須與時俱進，永無止境的謀求改善、再改善，以此奠定良好經營基礎，確保此一「回饋社會」，「利益眾生」的事業可以永續發展，充分貫徹醫療服務之職志。</p></article><div class=\"founder fz-18 tar\"><span>創辦人</span><span><img src=\"/images/05-system/page2/1-6.png\" alt=\"\"></span></div></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "3",  lang="tw" , category="2", sortIndex=1,title = "病人安全政策",status="Y", content="<section class=\"about__secure layout__container-1400\"><img class=\"pic mb50\" src=\"images/05-system/page4/1.png\" alt=\"\"><div class=\"text text-bd mb100\"><h4 class=\"text-title fz-28 fwb\">就醫安全指南</h4><div class=\"wrap\"><ul class=\"fz-18 ul-reset ul-circle ul-circle-color\"><li>確認您所要看診的科別及主治醫師。</li><li>確認您是先已記下要告知醫師您身體的狀況。</li><li>確認醫師完全了解您的主訴。</li><li>確認您領取藥物時，能完全了解所服用藥物的作用及注意事項。</li></ul><ul class=\"fz-18 ul-reset ul-circle ul-circle-color\"><li>確認醫師完全了解您曾對何種藥物過敏反應。</li><li>確認您在沒有疑慮之情況下，接受醫療服務。</li><li>確認您從醫師、護理人員以及其他醫事人員，對您的健康提供充分的醫療資訊。</li><li>您有疑問時，勇於向醫院提出反映及建議。</li></ul></div></div></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "3",  lang="en" , category="2", sortIndex=1,title = "病人安全政策",status="Y", content="<section class=\"about__secure layout__container-1400\"><img class=\"pic mb50\" src=\"images/05-system/page4/1.png\" alt=\"\"><div class=\"text text-bd mb100\"><h4 class=\"text-title fz-28 fwb\">就醫安全指南</h4><div class=\"wrap\"><ul class=\"fz-18 ul-reset ul-circle ul-circle-color\"><li>確認您所要看診的科別及主治醫師。</li><li>確認您是先已記下要告知醫師您身體的狀況。</li><li>確認醫師完全了解您的主訴。</li><li>確認您領取藥物時，能完全了解所服用藥物的作用及注意事項。</li></ul><ul class=\"fz-18 ul-reset ul-circle ul-circle-color\"><li>確認醫師完全了解您曾對何種藥物過敏反應。</li><li>確認您在沒有疑慮之情況下，接受醫療服務。</li><li>確認您從醫師、護理人員以及其他醫事人員，對您的健康提供充分的醫療資訊。</li><li>您有疑問時，勇於向醫院提出反映及建議。</li></ul></div></div></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "4",  lang="tw" , category="2",  sortIndex=2,title = "病人安全目標",status="Y", content="<section class=\"about__secure about__secure-aim layout__container-full bg-grey pd100\"><div class=\"layout__container-1400 pd-lr-50 mb50\"><h3 class=\"fz-36 fwb tac\">病人安全目標</h3><ol class=\"ol-reset ol-circle\"><li><h4 class=\"fz-24 fwb\">提升用藥安全</h4><ol class=\"fz-17\"><li>落實正確給藥程序、查核</li><li>落實病人用藥過敏及不良反應史的登錄及運用</li><li>加強慢性病人用藥安全</li><li>提升病人及照護者安全用藥的能力</li><li>運用資訊提高用藥安全</li></ol></li><li><h4 class=\"fz-24 fwb\">落實感染控制</h4><ol class=\"fz-17\"><li>落實洗手遵從性及正確性</li><li>醫療照護相關感染重大事件應列為警訊事件處理</li><li>落實抗生素正確使用的教育及監測機制</li></ol></li><li><h4 class=\"fz-24 fwb\">提升手術安全</h4><ol class=\"fz-17\"><li>落實手術辨識流程</li><li>落實手術安全查核項目</li><li>提升麻醉照護功能，確保手術安全</li><li>落實手術儀器設備檢測作業</li><li>建立適當機制，檢討不必要之手術</li></ol></li><li><h4 class=\"fz-24 fwb\">預防病人跌倒及降低傷害程度</h4><ol class=\"fz-17\"><li>落實執行跌倒風險評估及防範措施</li><li>加強監測與通報病人跌倒</li><li>改善照護環境，以降低跌倒傷害程度</li></ol></li><li><h4 class=\"fz-24 fwb\">鼓勵異常事件通報</h4><ol class=\"fz-17\"><li>營造異常事件通報文化，並參與全國性病人安全通報系統</li><li>落實院內病人安全通報標準作業程</li><li>對重大異常事件進行根本原因分析</li><li>定期分析通報資料，採取適當預防及改善措施</li></ol></li><li><h4 class=\"fz-24 fwb\">提升醫療照護人員間溝通的有效性</h4><ol class=\"fz-17\"><li>落實交接班資訊傳遞之完整與及時性</li><li>實轉運病人之風險管理與標準作業程序</li><li>落實醫療照護人員間醫囑或訊息傳遞的正確性</li><li>檢驗、檢查、病理報告之危急值應及時通知與處理</li><li> 加強團隊溝通技能</li></ol></li><li><h4 class=\"fz-24 fwb\">鼓勵病人及其家屬參與病人安全工作</h4><ol class=\"fz-17\"><li>鼓勵醫療人員主動與病人及其家屬建立合作夥伴關係</li><li>擴大病人安全委員會參與層面</li><li>鼓勵民眾通報所關心的病人安全問題</li><li>主動提供病人醫療安全相關資訊</li></ol></li><li><h4 class=\"fz-24 fwb\">提升管路安全</h4><ol class=\"fz-17\"><li>加強管路使用之評估及照護品質</li><li> 加強監測及通報管路事件，採取預防及改善措施</li><li> 整合醫療團隊資源，提供跨專業管路照護</li></ol></li><li><h4 class=\"fz-24 fwb\">加強醫院火災預防與應變</h4><ol class=\"fz-17\"><li>確保建物與設施的防火性能</li><li>確保滅火及逃生設施的有效性</li><li>依照單位特性，擬定防火計畫</li><li>制定全院及各特殊單位之火警應變計畫</li><li>落實人員防火教育及火警應變訓練</li></ol></li></ol></div><div class=\"text layout__container-1400\"><h4 class=\"text-title fz-28 fwb\">病人權利與配合事項</h4><div class=\"fz-17\">病人與醫療團隊充分合作是疾病治療成功的重要因素之一，醫病雙方瞭解病人的權利與義務，有助於良好的醫病溝通增進雙方合作與互信。病人來本院就醫時，本院保障其下列十項基本權利，如有服務欠周之處，請病人不吝提出指正，督促本院持續改善進步。而為使疾病能夠正確診斷，順利使用最適當的方法治療，病人與家屬亦有責任，提供病人病情相關資訊供醫護人員參考，並配合執行診療，善用有限的醫療資源，共同維護醫療環境與全體人員健康。<br><br>期待所有病人能與醫院攜手打造健康和諧的醫療環境。</div></div></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "4",  lang="en" , category="2",  sortIndex=2,title = "病人安全目標",status="Y", content="<section class=\"about__secure about__secure-aim layout__container-full bg-grey pd100\"><div class=\"layout__container-1400 pd-lr-50 mb50\"><h3 class=\"fz-36 fwb tac\">病人安全目標</h3><ol class=\"ol-reset ol-circle\"><li><h4 class=\"fz-24 fwb\">提升用藥安全</h4><ol class=\"fz-17\"><li>落實正確給藥程序、查核</li><li>落實病人用藥過敏及不良反應史的登錄及運用</li><li>加強慢性病人用藥安全</li><li>提升病人及照護者安全用藥的能力</li><li>運用資訊提高用藥安全</li></ol></li><li><h4 class=\"fz-24 fwb\">落實感染控制</h4><ol class=\"fz-17\"><li>落實洗手遵從性及正確性</li><li>醫療照護相關感染重大事件應列為警訊事件處理</li><li>落實抗生素正確使用的教育及監測機制</li></ol></li><li><h4 class=\"fz-24 fwb\">提升手術安全</h4><ol class=\"fz-17\"><li>落實手術辨識流程</li><li>落實手術安全查核項目</li><li>提升麻醉照護功能，確保手術安全</li><li>落實手術儀器設備檢測作業</li><li>建立適當機制，檢討不必要之手術</li></ol></li><li><h4 class=\"fz-24 fwb\">預防病人跌倒及降低傷害程度</h4><ol class=\"fz-17\"><li>落實執行跌倒風險評估及防範措施</li><li>加強監測與通報病人跌倒</li><li>改善照護環境，以降低跌倒傷害程度</li></ol></li><li><h4 class=\"fz-24 fwb\">鼓勵異常事件通報</h4><ol class=\"fz-17\"><li>營造異常事件通報文化，並參與全國性病人安全通報系統</li><li>落實院內病人安全通報標準作業程</li><li>對重大異常事件進行根本原因分析</li><li>定期分析通報資料，採取適當預防及改善措施</li></ol></li><li><h4 class=\"fz-24 fwb\">提升醫療照護人員間溝通的有效性</h4><ol class=\"fz-17\"><li>落實交接班資訊傳遞之完整與及時性</li><li>實轉運病人之風險管理與標準作業程序</li><li>落實醫療照護人員間醫囑或訊息傳遞的正確性</li><li>檢驗、檢查、病理報告之危急值應及時通知與處理</li><li> 加強團隊溝通技能</li></ol></li><li><h4 class=\"fz-24 fwb\">鼓勵病人及其家屬參與病人安全工作</h4><ol class=\"fz-17\"><li>鼓勵醫療人員主動與病人及其家屬建立合作夥伴關係</li><li>擴大病人安全委員會參與層面</li><li>鼓勵民眾通報所關心的病人安全問題</li><li>主動提供病人醫療安全相關資訊</li></ol></li><li><h4 class=\"fz-24 fwb\">提升管路安全</h4><ol class=\"fz-17\"><li>加強管路使用之評估及照護品質</li><li> 加強監測及通報管路事件，採取預防及改善措施</li><li> 整合醫療團隊資源，提供跨專業管路照護</li></ol></li><li><h4 class=\"fz-24 fwb\">加強醫院火災預防與應變</h4><ol class=\"fz-17\"><li>確保建物與設施的防火性能</li><li>確保滅火及逃生設施的有效性</li><li>依照單位特性，擬定防火計畫</li><li>制定全院及各特殊單位之火警應變計畫</li><li>落實人員防火教育及火警應變訓練</li></ol></li></ol></div><div class=\"text layout__container-1400\"><h4 class=\"text-title fz-28 fwb\">病人權利與配合事項</h4><div class=\"fz-17\">病人與醫療團隊充分合作是疾病治療成功的重要因素之一，醫病雙方瞭解病人的權利與義務，有助於良好的醫病溝通增進雙方合作與互信。病人來本院就醫時，本院保障其下列十項基本權利，如有服務欠周之處，請病人不吝提出指正，督促本院持續改善進步。而為使疾病能夠正確診斷，順利使用最適當的方法治療，病人與家屬亦有責任，提供病人病情相關資訊供醫護人員參考，並配合執行診療，善用有限的醫療資源，共同維護醫療環境與全體人員健康。<br><br>期待所有病人能與醫院攜手打造健康和諧的醫療環境。</div></div></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "5",  lang="tw" , category="2",  sortIndex=3,title = "病人的權利",status="Y", content="<section class=\"about__secure pd100\"><div class=\"text text-bd layout__container-1400\"><h3 class=\"fz-36 fwb tac\">病人的權利</h3><ol class=\"ol-reset ol-circle mb50\"><li><h4 class=\"fz-24 fwb\">公平醫療</h4><div class=\"fz-17\">病人有權接受治療，不因種族、宗教、國籍、性別、年齡、疾病、性向、地理位置或社經地位的不同而受到歧視，每位病人皆能公平的接受適當的醫療服務。</div></li><li><h4 class=\"fz-24 fwb\">安全照護</h4><div class=\"fz-17\">病人有權在安全的醫療環境接受診療照護。</div></li><li><h4 class=\"fz-24 fwb\">告知說明</h4><div class=\"fz-17\">病人及主要照顧家屬有權詢問並得知負責治療醫師或其他負責醫療人員之名字，以及與病情相關之訊息。</div></li><li><h4 class=\"fz-24 fwb\">知情同意、拒絕權利、第二意見</h4><div class=\"fz-17\">病人及主要照顧之家屬有權參與診療照護過程之諮商與討論，並決定治療方式，包括拒絕治療及可尋求第二意見的權利。</div></li><li><h4 class=\"fz-24 fwb\">持續照護</h4><div class=\"fz-17\">病人有權要求醫護人員提供疼痛照護，疾病照護、用藥、飲食或生活等之衛教指導以及出院後居家照顧等相關資訊與醫療服務。</div></li><li><h4 class=\"fz-24 fwb\">安寧療護</h4><div class=\"fz-17\">依安寧緩和醫療條例，病人或家屬有權決定及改變是否在病危時施行心肺復甦術以及放棄或停止維持生命之治療。</div></li><li><h4 class=\"fz-24 fwb\">尊重隱私</h4><div class=\"fz-17\">病人之隱私、病情資料與紀錄均被本院尊重及依法妥善保管並保密。</div></li><li><h4 class=\"fz-24 fwb\">資訊提供</h4><div class=\"fz-17\">病人有權依法申請自己的病歷複製本、診斷證明書與醫療費用明細表。</div></li><li><h4 class=\"fz-24 fwb\">申訴服務</h4><div class=\"fz-17\">人對本院有任何抱怨或建議，有權向醫院提出申訴並得到回應。</div></li><li><h4 class=\"fz-24 fwb\">專業服務</h4><div class=\"fz-17\">本院員工均佩戴識別證，若未配戴識別證者，病人可以拒絕其所提供的服務。</div></li></ol><h3 class=\"fz-36 fwb tac\">病人配合事項</h3><ol class=\"ol-reset ol-circle mb50\"><li><div class=\"fz-17\">病人有權接受治療，不因種族、宗教、國籍、性別、年齡、疾病、性向、地理位置或社經地位的不同而受到歧視，每位病人皆能公平的接受適當的醫療服務。</div></li><li><div class=\"fz-17\">請病人和其家屬積極參與決定治療方針，並協調共同的意見，在決定簽署同意書、契約書或接受治療與否之前，<br>請充分了解其內容以及各種治療方法可能造成之結果。對於各項醫療處置若有疑問請向負責照護之醫護人員提出。</div></li><li><div class=\"fz-17\">請病人和其家屬配合醫師之醫囑進行治療、辦理出院或轉院，珍惜醫療資源，妥善利用醫院之各項設施。</div></li><li><div class=\"fz-17\">請配合醫院之就醫規定或作業流程，勿要求醫事人員提供不實的資料或診斷證明，遵守醫院門禁、感染管制措施，不在院內吸菸及嚼食檳榔等，<br>避免影響整體病人照護或他人權益。</div></li><li><div class=\"fz-17\">請您支付屬於自行負擔的醫療費用，若有困難，請洽本院社服部門或負責照護之醫護人員。</div></li><li><div class=\"fz-17\">本院提供個人物品保管場所，供病人及家屬放置個人財物，無承擔個人財物遺失或損壞的責任，請病人或家屬自行妥善管理隨身財物。</div></li></ol><div class=\"info fz-20 bg-grey tac\">對於本院之病人權利與責任有任何疑問或建議，歡迎向本院人員反應本院將竭誠為您處理！</div></div></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new abouts { guid = "5",  lang="en" , category="2",  sortIndex=3,title = "病人的權利",status="Y", content="<section class=\"about__secure pd100\"><div class=\"text text-bd layout__container-1400\"><h3 class=\"fz-36 fwb tac\">病人的權利</h3><ol class=\"ol-reset ol-circle mb50\"><li><h4 class=\"fz-24 fwb\">公平醫療</h4><div class=\"fz-17\">病人有權接受治療，不因種族、宗教、國籍、性別、年齡、疾病、性向、地理位置或社經地位的不同而受到歧視，每位病人皆能公平的接受適當的醫療服務。</div></li><li><h4 class=\"fz-24 fwb\">安全照護</h4><div class=\"fz-17\">病人有權在安全的醫療環境接受診療照護。</div></li><li><h4 class=\"fz-24 fwb\">告知說明</h4><div class=\"fz-17\">病人及主要照顧家屬有權詢問並得知負責治療醫師或其他負責醫療人員之名字，以及與病情相關之訊息。</div></li><li><h4 class=\"fz-24 fwb\">知情同意、拒絕權利、第二意見</h4><div class=\"fz-17\">病人及主要照顧之家屬有權參與診療照護過程之諮商與討論，並決定治療方式，包括拒絕治療及可尋求第二意見的權利。</div></li><li><h4 class=\"fz-24 fwb\">持續照護</h4><div class=\"fz-17\">病人有權要求醫護人員提供疼痛照護，疾病照護、用藥、飲食或生活等之衛教指導以及出院後居家照顧等相關資訊與醫療服務。</div></li><li><h4 class=\"fz-24 fwb\">安寧療護</h4><div class=\"fz-17\">依安寧緩和醫療條例，病人或家屬有權決定及改變是否在病危時施行心肺復甦術以及放棄或停止維持生命之治療。</div></li><li><h4 class=\"fz-24 fwb\">尊重隱私</h4><div class=\"fz-17\">病人之隱私、病情資料與紀錄均被本院尊重及依法妥善保管並保密。</div></li><li><h4 class=\"fz-24 fwb\">資訊提供</h4><div class=\"fz-17\">病人有權依法申請自己的病歷複製本、診斷證明書與醫療費用明細表。</div></li><li><h4 class=\"fz-24 fwb\">申訴服務</h4><div class=\"fz-17\">人對本院有任何抱怨或建議，有權向醫院提出申訴並得到回應。</div></li><li><h4 class=\"fz-24 fwb\">專業服務</h4><div class=\"fz-17\">本院員工均佩戴識別證，若未配戴識別證者，病人可以拒絕其所提供的服務。</div></li></ol><h3 class=\"fz-36 fwb tac\">病人配合事項</h3><ol class=\"ol-reset ol-circle mb50\"><li><div class=\"fz-17\">病人有權接受治療，不因種族、宗教、國籍、性別、年齡、疾病、性向、地理位置或社經地位的不同而受到歧視，每位病人皆能公平的接受適當的醫療服務。</div></li><li><div class=\"fz-17\">請病人和其家屬積極參與決定治療方針，並協調共同的意見，在決定簽署同意書、契約書或接受治療與否之前，<br>請充分了解其內容以及各種治療方法可能造成之結果。對於各項醫療處置若有疑問請向負責照護之醫護人員提出。</div></li><li><div class=\"fz-17\">請病人和其家屬配合醫師之醫囑進行治療、辦理出院或轉院，珍惜醫療資源，妥善利用醫院之各項設施。</div></li><li><div class=\"fz-17\">請配合醫院之就醫規定或作業流程，勿要求醫事人員提供不實的資料或診斷證明，遵守醫院門禁、感染管制措施，不在院內吸菸及嚼食檳榔等，<br>避免影響整體病人照護或他人權益。</div></li><li><div class=\"fz-17\">請您支付屬於自行負擔的醫療費用，若有困難，請洽本院社服部門或負責照護之醫護人員。</div></li><li><div class=\"fz-17\">本院提供個人物品保管場所，供病人及家屬放置個人財物，無承擔個人財物遺失或損壞的責任，請病人或家屬自行妥善管理隨身財物。</div></li></ol><div class=\"info fz-20 bg-grey tac\">對於本院之病人權利與責任有任何疑問或建議，歡迎向本院人員反應本院將竭誠為您處理！</div></div></section>", modifydate = DateTime.Now , create_date =  DateTime.Now  },
            };
           if (context.abouts.ToList().Count == 0)
           {
               Abouts.ForEach(s => context.abouts.Add(s));
               context.SaveChanges();
           }


           //大事記
           var Historys = new List<historys>
           {
               new historys { guid = "1",   year = "2018", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "1",   year = "2018", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new historys { guid = "2",   year = "2017", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "2",   year = "2017", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new historys { guid = "3",   year = "2016", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "3",   year = "2016", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new historys { guid = "4",   year = "2015", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "4",   year = "2015", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },


               new historys { guid = "5",   year = "2014", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "5",   year = "2014", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new historys { guid = "6",   year = "2013", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "6",   year = "2013", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new historys { guid = "7",   year = "2010", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "7",   year = "2010", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new historys { guid = "8",   year = "2007", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "8",   year = "2007", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },

               new historys { guid = "9",   year = "2005", lang = "tw",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },
               new historys { guid = "9",   year = "2005", lang = "en",status="Y",pic="Content/Upload/images/05-system/page2/2-1.jpg,Content/Upload/images/05-system/page2/2-2.jpg" ,pic_alt=""  , modifydate = DateTime.Now , create_date =  DateTime.Now  },


           };
           if (context.historys.ToList().Count == 0)
           {
               Historys.ForEach(s => context.historys.Add(s));
               context.SaveChanges();
           }

           //大事記(細項)
           var Historys_data = new List<historys_data>
           {
               new historys_data { guid = "1" , lang = "tw", day="01.01", category="1",title = "程文俊教授接任長庚決策委員會主任委員。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "1" , lang = "en", day="01.01", category="1",title = "程文俊教授接任長庚決策委員會主任委員。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "2" , lang = "tw", day="01.02", category="1",title = "李石增教授接任長庚決策委員會主任委員。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "2" , lang = "en", day="01.02", category="1",title = "李石增教授接任長庚決策委員會主任委員。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "3" , lang = "tw", day="03.28", category="1",title = "「高雄巿立鳳山醫院興建營運移轉案」舉行開 工動土典禮，由陳菊市長與王董事長親臨致詞 及動鏟。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "3" , lang = "en", day="03.28", category="1",title = "「高雄巿立鳳山醫院興建營運移轉案」舉行開 工動土典禮，由陳菊市長與王董事長親臨致詞 及動鏟。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },

               new historys_data { guid = "4" , lang = "tw", day="01.01", category="2",title = "程文俊教授接任長庚決策委員會主任委員。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "4" , lang = "en", day="01.01", category="2",title = "程文俊教授接任長庚決策委員會主任委員。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "5" , lang = "tw", day="01.02", category="2",title = "李石增教授接任長庚決策委員會主任委員。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "5" , lang = "en", day="01.02", category="2",title = "李石增教授接任長庚決策委員會主任委員。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "6" , lang = "tw", day="03.28", category="2",title = "「高雄巿立鳳山醫院興建營運移轉案」舉行開 工動土典禮，由陳菊市長與王董事長親臨致詞 及動鏟。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },
               new historys_data { guid = "6" , lang = "en", day="03.28", category="2",title = "「高雄巿立鳳山醫院興建營運移轉案」舉行開 工動土典禮，由陳菊市長與王董事長親臨致詞 及動鏟。" , sortIndex=1,status="Y", modifydate = DateTime.Now , create_date =  DateTime.Now },


           }; 
           if (context.historys_data.ToList().Count == 0)
           {
               Historys_data.ForEach(s => context.historys_data.Add(s));
               context.SaveChanges();
           }
           */
            /*
            //語系
            var Language = new List<language>
            {
                new language { guid = "001",   title = "繁體中文", codes = "tw",status="Y", date = DateTime.Now , lang = "tw" },
                new language { guid = "002",   title = "English", codes = "en",status="Y", date = DateTime.Now , lang = "en" },
            };
            if (context.language.ToList().Count == 0)
            {
                Language.ForEach(s => context.language.Add(s));
                context.SaveChanges();
            }

            //SMTP
            var Smtp_data = new List<smtp_data>
            {
                new smtp_data { guid = "b9732bfe-238d-4e4e-9114-8e6d30c34022",   host = "smtp.gmail.com", port = "587",smtp_auth="Y" , username="test@e-creative.tw" , password = "24252151", from_email="test@e-creative.tw", modifydate = DateTime.Now , create_date =  DateTime.Now },

            };
            if (context.smtp_data.ToList().Count == 0)
            {
                Smtp_data.ForEach(s => context.smtp_data.Add(s));
                context.SaveChanges();
            }
            */
            //網站基本資料
            var Web_data = new List<web_data>
            {
                new web_data { guid = "1",   title = "長庚醫療財團法人全球資訊網", url = "https://www.cgmh.org.tw/", phone="-" , fax="-" , servicemail = "service@youweb.tw", ext_num="9" , address="-", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",
                register_url = "https://register.cgmh.org.tw/",
                reference_url = "https://register.cgmh.org.tw/reference_02.htm",
                symptom_url = "https://register.cgmh.org.tw/reference_01.htm",
                sp_url = "https://www.cgmh.org.tw/page/cm.htm",
                International_url = "http://www.chang-gung.com/",
                health_url = "https://www1.cgmh.org.tw/healthpromotion/",
                medical_url = "https://www.cgmh.org.tw/cgmn/index.asp",
                care_url = "https://www.cgmh.org.tw/page/AdvanceCare/index.html"
                },
                new web_data { guid = "1",   title = "長庚醫療財團法人全球資訊網", url = "https://www.cgmh.org.tw/", phone="-" , fax="-" , servicemail = "service@youweb.tw", ext_num="9" , address="-", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",
                 register_url = "https://register.cgmh.org.tw/",
                reference_url = "https://register.cgmh.org.tw/reference_02.htm",
                symptom_url = "https://register.cgmh.org.tw/reference_01.htm",
                sp_url = "https://www.cgmh.org.tw/page/cm.htm",
                International_url = "http://www.chang-gung.com/",
                health_url = "https://www1.cgmh.org.tw/healthpromotion/",
                medical_url = "https://www.cgmh.org.tw/cgmn/index.asp",
                care_url = "https://www.cgmh.org.tw/page/AdvanceCare/index.html"
                },


            };
            if (context.web_data.ToList().Count == 0)
            {
                Web_data.ForEach(s => context.web_data.Add(s));
                context.SaveChanges();
            }


            /*
            var System_data = new List<system_data>
            {
                new system_data { guid = Guid.Parse("4795dabf-18de-490e-9bb2-d57b4d99c127"),   title = "長庚醫療財團法人全球資訊網 管理系統", login_title = "長庚醫療財團法人 管理登入",logo = "/Content/Siteadmin/styles/images/logo.png" , logo_alt="MINMAX" , design_by = "Minmax", modifydate = DateTime.Now , create_date =  DateTime.Now , background="/Content/siteadmin/styles/images/demo/example/01.jpg" , background_alt="" },

            };
            if (context.system_data.ToList().Count == 0)
            {
                System_data.ForEach(s => context.system_data.Add(s));
                context.SaveChanges();
            }


           

            //常見問題分類
            var Qas = new List<qas>
            {
                new qas { guid = "1",   title = "掛號常見問答", status = "Y" , sortIndex = 1 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new qas { guid = "1",   title = "掛號常見問答", status = "Y" , sortIndex = 1 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                new qas { guid = "2",   title = "門診常見問答", status = "Y" , sortIndex = 2 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new qas { guid = "2",   title = "門診常見問答", status = "Y" , sortIndex = 2 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                new qas { guid = "3",   title = "繳費常見問答", status = "Y" , sortIndex = 3 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new qas { guid = "3",   title = "繳費常見問答", status = "Y" , sortIndex = 3 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                new qas { guid = "4",   title = "領藥常見問答", status = "Y" , sortIndex = 4 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new qas { guid = "4",   title = "領藥常見問答", status = "Y" , sortIndex = 4 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                new qas { guid = "5",   title = "急診常見問答", status = "Y" , sortIndex = 5 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new qas { guid = "5",   title = "急診常見問答", status = "Y" , sortIndex = 5 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                new qas { guid = "6",   title = "檢查常見問答", status = "Y" , sortIndex = 6 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new qas { guid = "6",   title = "檢查常見問答", status = "Y" , sortIndex = 6 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },

                new qas { guid = "7",   title = "檢查常見問答", status = "Y" , sortIndex = 7 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new qas { guid = "7",   title = "檢查常見問答", status = "Y" , sortIndex = 7 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },


            };
            if (context.qas.ToList().Count == 0)
            {
                Qas.ForEach(s => context.qas.Add(s));
                context.SaveChanges();
            }


            //院區分類
            var Hospital_category = new List<hospital_category>
            {
                new hospital_category { guid = "1",   title = "院區", status = "Y" , sortIndex = 1 , modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new hospital_category { guid = "1",   title = "院區", status = "Y", sortIndex = 2, modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },
                new hospital_category { guid = "2",   title = "附設機構", status = "Y", sortIndex = 2, modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw" },
                new hospital_category { guid = "2",   title = "附設機構", status = "Y", sortIndex = 2, modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en" },


            };
            if (context.hospital_category.ToList().Count == 0)
            {
                Hospital_category.ForEach(s => context.hospital_category.Add(s));
                context.SaveChanges();
            }


            //院區
            var Hospitals = new List<hospitals>
            {
                new hospitals { guid = "1", title = "基隆長庚紀念醫院" , hospitalID = "2" ,sortIndex=1 ,view_type="api",pic="images/H2.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "" ,join_guid="2",join_title="基隆長庚紀念醫院暨情人湖院區" },
                new hospitals { guid = "1", title = "基隆長庚紀念醫院" , hospitalID = "2" ,sortIndex=1 ,view_type="api",pic="images/H2.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "" ,join_guid="2",join_title="基隆長庚紀念醫院暨情人湖院區"},

                new hospitals { guid = "2", title = "情人湖院區" , hospitalID = "E"  ,sortIndex=2 ,view_type="web",pic="images/HE.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "2", title = "情人湖院區" , hospitalID = "E"  ,sortIndex=2 ,view_type="web",pic="images/HE.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "3", title = "台北長庚紀念醫院" , hospitalID = "1"  ,sortIndex=3 ,view_type="api",pic="images/H1.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "3", title = "台北長庚紀念醫院" , hospitalID = "1"  ,sortIndex=3 ,view_type="api",pic="images/H1.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "4", title = "長庚診所" , hospitalID = "B"  ,sortIndex=4 ,view_type="web",pic="images/HB.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "4", title = "長庚診所" , hospitalID = "B"  ,sortIndex=4 ,view_type="web",pic="images/HB.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "5", title = "林口長庚紀念醫院" , hospitalID = "3"  ,sortIndex=5 ,view_type="api",pic="images/H3.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "5", title = "林口長庚紀念醫院" , hospitalID = "3"  ,sortIndex=5 ,view_type="api",pic="images/H3.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "6", title = "桃園長庚紀念醫院" , hospitalID = "5"  ,sortIndex=6 ,view_type="api",pic="images/H5.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "6", title = "桃園長庚紀念醫院" , hospitalID = "5"  ,sortIndex=6 ,view_type="api",pic="images/H5.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "7", title = "長青院區（護理之家）" , hospitalID = "N"  ,sortIndex=7 ,view_type="web",pic="images/HN.png",pic_alt="",category="2",website_type="url", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "7", title = "長青院區（護理之家）" , hospitalID = "N"  ,sortIndex=7 ,view_type="web",pic="images/HN.png",pic_alt="",category="2",website_type="url", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "8", title = "雲林長庚紀念醫院" , hospitalID = "M"  ,sortIndex=8 ,view_type="api",pic="images/HM.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "8", title = "雲林長庚紀念醫院" , hospitalID = "M"  ,sortIndex=8 ,view_type="api",pic="images/HM.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "9", title = "嘉義長庚紀念醫院" , hospitalID = "6"  ,sortIndex=9 ,view_type="api",pic="images/H6.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "9", title = "嘉義長庚紀念醫院" , hospitalID = "6"  ,sortIndex=9 ,view_type="api",pic="images/H6.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "10", title = "高雄長庚紀念醫院" , hospitalID = "8"  ,sortIndex=10 ,view_type="api",pic="images/H8.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "10", title = "高雄長庚紀念醫院" , hospitalID = "8"  ,sortIndex=10 ,view_type="api",pic="images/H8.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },

                new hospitals { guid = "11", title = "高雄市立鳳山醫院" , hospitalID = "T"  ,sortIndex=11 ,view_type="api",pic="images/HT.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "11", title = "高雄市立鳳山醫院" , hospitalID = "T"  ,sortIndex=11 ,view_type="api",pic="images/HT.png",pic_alt="",category="1",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },


                new hospitals { guid = "12", title = "高雄產後護理之家" , hospitalID = ""  ,sortIndex=12 ,view_type="web",pic="images/HN.png",pic_alt="",category="2",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="tw",seo_keywords="",seo_description = "",join_guid="",join_title="" },
                new hospitals { guid = "12", title = "高雄產後護理之家" , hospitalID = ""  ,sortIndex=12 ,view_type="web",pic="images/HN.png",pic_alt="",category="2",website_type="info", address = "", profile = "", status = "Y",bedQty="",doctorQty="",medicalQty="",tel="",fax="",website="", modifydate = DateTime.Now , create_date =  DateTime.Now , lang="en",seo_keywords="",seo_description = "",join_guid="",join_title="" },


            };
            if (context.hospitals.ToList().Count == 0)
            {
                Hospitals.ForEach(s => context.hospitals.Add(s));
                context.SaveChanges();
            }


            //台灣縣市區域
            var TaiwanCity = new List<taiwan_city>
            {
                new taiwan_city { city = "基隆市",district = "",zip = "",lang = "tw",sortIndex = 1,lay="1" },
                new taiwan_city { city = "基隆市",district = "",zip = "",lang = "en",sortIndex = 1,lay="1" },
                new taiwan_city { city = "台北市",district = "",zip = "",lang = "tw",sortIndex = 2,lay="1" },
                new taiwan_city { city = "台北市",district = "",zip = "",lang = "en",sortIndex = 2,lay="1" },
                new taiwan_city { city = "新北市",district = "",zip = "",lang = "tw",sortIndex = 3,lay="1" },
                new taiwan_city { city = "新北市",district = "",zip = "",lang = "en",sortIndex = 3,lay="1" },
                new taiwan_city { city = "桃園市",district = "",zip = "",lang = "tw",sortIndex = 4,lay="1" },
                new taiwan_city { city = "桃園市",district = "",zip = "",lang = "en",sortIndex = 4,lay="1" },
                new taiwan_city { city = "新竹市",district = "",zip = "",lang = "tw",sortIndex = 5,lay="1" },
                new taiwan_city { city = "新竹市",district = "",zip = "",lang = "en",sortIndex = 5,lay="1" },
                new taiwan_city { city = "新竹縣",district = "",zip = "",lang = "tw",sortIndex = 6,lay="1" },
                new taiwan_city { city = "新竹縣",district = "",zip = "",lang = "en",sortIndex = 6,lay="1" },
                new taiwan_city { city = "苗栗縣",district = "",zip = "",lang = "tw",sortIndex = 7,lay="1" },
                new taiwan_city { city = "苗栗縣",district = "",zip = "",lang = "en",sortIndex = 7,lay="1" },
                new taiwan_city { city = "台中市",district = "",zip = "",lang = "tw",sortIndex = 8,lay="1" },
                new taiwan_city { city = "台中市",district = "",zip = "",lang = "en",sortIndex = 8,lay="1" },
                new taiwan_city { city = "彰化縣",district = "",zip = "",lang = "tw",sortIndex = 9,lay="1" },
                new taiwan_city { city = "彰化縣",district = "",zip = "",lang = "en",sortIndex = 9,lay="1" },
                new taiwan_city { city = "南投縣",district = "",zip = "",lang = "tw",sortIndex = 10,lay="1" },
                new taiwan_city { city = "南投縣",district = "",zip = "",lang = "en",sortIndex = 10,lay="1" },
                new taiwan_city { city = "雲林縣",district = "",zip = "",lang = "tw",sortIndex = 11,lay="1" },
                new taiwan_city { city = "雲林縣",district = "",zip = "",lang = "en",sortIndex = 11,lay="1" },
                new taiwan_city { city = "嘉義市",district = "",zip = "",lang = "tw",sortIndex = 12,lay="1" },
                new taiwan_city { city = "嘉義市",district = "",zip = "",lang = "en",sortIndex = 12,lay="1" },
                new taiwan_city { city = "嘉義縣",district = "",zip = "",lang = "tw",sortIndex = 13,lay="1" },
                new taiwan_city { city = "嘉義縣",district = "",zip = "",lang = "en",sortIndex = 13,lay="1" },
                new taiwan_city { city = "台南市",district = "",zip = "",lang = "tw",sortIndex = 14,lay="1" },
                new taiwan_city { city = "台南市",district = "",zip = "",lang = "en",sortIndex = 14,lay="1" },
                new taiwan_city { city = "高雄市",district = "",zip = "",lang = "tw",sortIndex = 15,lay="1" },
                new taiwan_city { city = "高雄市",district = "",zip = "",lang = "en",sortIndex = 15,lay="1" },
                new taiwan_city { city = "屏東縣",district = "",zip = "",lang = "tw",sortIndex = 16,lay="1" },
                new taiwan_city { city = "屏東縣",district = "",zip = "",lang = "en",sortIndex = 16,lay="1" },
                new taiwan_city { city = "台東縣",district = "",zip = "",lang = "tw",sortIndex = 17,lay="1" },
                new taiwan_city { city = "台東縣",district = "",zip = "",lang = "en",sortIndex = 17,lay="1" },
                new taiwan_city { city = "花蓮縣",district = "",zip = "",lang = "tw",sortIndex = 18,lay="1" },
                new taiwan_city { city = "花蓮縣",district = "",zip = "",lang = "en",sortIndex = 18,lay="1" },
                new taiwan_city { city = "宜蘭縣",district = "",zip = "",lang = "tw",sortIndex = 19,lay="1" },
                new taiwan_city { city = "宜蘭縣",district = "",zip = "",lang = "en",sortIndex = 19,lay="1" },
                new taiwan_city { city = "澎湖縣",district = "",zip = "",lang = "tw",sortIndex = 20,lay="1" },
                new taiwan_city { city = "澎湖縣",district = "",zip = "",lang = "en",sortIndex = 20,lay="1" },
                new taiwan_city { city = "金門縣",district = "",zip = "",lang = "tw",sortIndex = 21,lay="1" },
                new taiwan_city { city = "金門縣",district = "",zip = "",lang = "en",sortIndex = 21,lay="1" },
                new taiwan_city { city = "連江縣",district = "",zip = "",lang = "tw",sortIndex = 22,lay="1" },
                new taiwan_city { city = "連江縣",district = "",zip = "",lang = "en",sortIndex = 22,lay="1" },
                new taiwan_city { city = "基隆市",district = "仁愛區",zip = "200",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "基隆市",district = "仁愛區",zip = "200",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "基隆市",district = "信義區",zip = "201",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "基隆市",district = "信義區",zip = "201",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "基隆市",district = "中正區",zip = "202",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "基隆市",district = "中正區",zip = "202",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "基隆市",district = "中山區",zip = "203",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "基隆市",district = "中山區",zip = "203",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "基隆市",district = "安樂區",zip = "204",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "基隆市",district = "安樂區",zip = "204",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "基隆市",district = "暖暖區",zip = "205",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "基隆市",district = "暖暖區",zip = "205",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "基隆市",district = "七堵區",zip = "206",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "基隆市",district = "七堵區",zip = "206",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台北市",district = "中正區",zip = "100",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "台北市",district = "中正區",zip = "100",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "台北市",district = "南港區",zip = "115",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "台北市",district = "南港區",zip = "115",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "台北市",district = "內湖區",zip = "114",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "台北市",district = "內湖區",zip = "114",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "台北市",district = "北投區",zip = "112",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "台北市",district = "北投區",zip = "112",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "台北市",district = "士林區",zip = "111",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "台北市",district = "士林區",zip = "111",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "台北市",district = "信義區",zip = "110",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "台北市",district = "信義區",zip = "110",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "台北市",district = "萬華區",zip = "108",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台北市",district = "萬華區",zip = "108",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台北市",district = "大安區",zip = "106",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "台北市",district = "大安區",zip = "106",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "台北市",district = "松山區",zip = "105",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "台北市",district = "松山區",zip = "105",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "台北市",district = "中山區",zip = "104",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "台北市",district = "中山區",zip = "104",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "台北市",district = "大同區",zip = "103",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "台北市",district = "大同區",zip = "103",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "台北市",district = "文山區",zip = "116",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "台北市",district = "文山區",zip = "116",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "新北市",district = "萬里區",zip = "207",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "新北市",district = "萬里區",zip = "207",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "新北市",district = "三峽區",zip = "237",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "新北市",district = "三峽區",zip = "237",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "新北市",district = "樹林區",zip = "238",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "新北市",district = "樹林區",zip = "238",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "新北市",district = "鶯歌區",zip = "239",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "新北市",district = "鶯歌區",zip = "239",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "新北市",district = "三重區",zip = "241",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "新北市",district = "三重區",zip = "241",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "新北市",district = "新莊區",zip = "242",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "新北市",district = "新莊區",zip = "242",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "新北市",district = "泰山區",zip = "243",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "新北市",district = "泰山區",zip = "243",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "新北市",district = "林口區",zip = "244",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "新北市",district = "林口區",zip = "244",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "新北市",district = "蘆洲區",zip = "247",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "新北市",district = "蘆洲區",zip = "247",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "新北市",district = "五股區",zip = "248",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "新北市",district = "五股區",zip = "248",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "新北市",district = "八里區",zip = "249",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "新北市",district = "八里區",zip = "249",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "新北市",district = "淡水區",zip = "251",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "新北市",district = "淡水區",zip = "251",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "新北市",district = "三芝區",zip = "252",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "新北市",district = "三芝區",zip = "252",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "新北市",district = "土城區",zip = "236",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "新北市",district = "土城區",zip = "236",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "新北市",district = "中和區",zip = "235",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "新北市",district = "中和區",zip = "235",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "新北市",district = "永和區",zip = "234",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "新北市",district = "永和區",zip = "234",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "新北市",district = "金山區",zip = "208",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "新北市",district = "金山區",zip = "208",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "新北市",district = "板橋區",zip = "220",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "新北市",district = "板橋區",zip = "220",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "新北市",district = "汐止區",zip = "221",lang = "tw",sortIndex = 19,lay="2" },
                new taiwan_city { city = "新北市",district = "汐止區",zip = "221",lang = "en",sortIndex = 19,lay="2" },
                new taiwan_city { city = "新北市",district = "深坑區",zip = "222",lang = "tw",sortIndex = 20,lay="2" },
                new taiwan_city { city = "新北市",district = "深坑區",zip = "222",lang = "en",sortIndex = 20,lay="2" },
                new taiwan_city { city = "新北市",district = "石碇區",zip = "223",lang = "tw",sortIndex = 21,lay="2" },
                new taiwan_city { city = "新北市",district = "石碇區",zip = "223",lang = "en",sortIndex = 21,lay="2" },
                new taiwan_city { city = "新北市",district = "瑞芳區",zip = "224",lang = "tw",sortIndex = 22,lay="2" },
                new taiwan_city { city = "新北市",district = "瑞芳區",zip = "224",lang = "en",sortIndex = 22,lay="2" },
                new taiwan_city { city = "新北市",district = "平溪區",zip = "226",lang = "tw",sortIndex = 23,lay="2" },
                new taiwan_city { city = "新北市",district = "平溪區",zip = "226",lang = "en",sortIndex = 23,lay="2" },
                new taiwan_city { city = "新北市",district = "雙溪區",zip = "227",lang = "tw",sortIndex = 24,lay="2" },
                new taiwan_city { city = "新北市",district = "雙溪區",zip = "227",lang = "en",sortIndex = 24,lay="2" },
                new taiwan_city { city = "新北市",district = "貢寮區",zip = "228",lang = "tw",sortIndex = 25,lay="2" },
                new taiwan_city { city = "新北市",district = "貢寮區",zip = "228",lang = "en",sortIndex = 25,lay="2" },
                new taiwan_city { city = "新北市",district = "新店區",zip = "231",lang = "tw",sortIndex = 26,lay="2" },
                new taiwan_city { city = "新北市",district = "新店區",zip = "231",lang = "en",sortIndex = 26,lay="2" },
                new taiwan_city { city = "新北市",district = "坪林區",zip = "232",lang = "tw",sortIndex = 27,lay="2" },
                new taiwan_city { city = "新北市",district = "坪林區",zip = "232",lang = "en",sortIndex = 27,lay="2" },
                new taiwan_city { city = "新北市",district = "烏來區",zip = "233",lang = "tw",sortIndex = 28,lay="2" },
                new taiwan_city { city = "新北市",district = "烏來區",zip = "233",lang = "en",sortIndex = 28,lay="2" },
                new taiwan_city { city = "新北市",district = "石門區",zip = "253",lang = "tw",sortIndex = 29,lay="2" },
                new taiwan_city { city = "新北市",district = "石門區",zip = "253",lang = "en",sortIndex = 29,lay="2" },
                new taiwan_city { city = "桃園市",district = "中壢區",zip = "320",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "桃園市",district = "中壢區",zip = "320",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "桃園市",district = "大園區",zip = "337",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "桃園市",district = "大園區",zip = "337",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "桃園市",district = "復興區",zip = "336",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "桃園市",district = "復興區",zip = "336",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "桃園市",district = "大溪區",zip = "335",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "桃園市",district = "大溪區",zip = "335",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "桃園市",district = "八德區",zip = "334",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "桃園市",district = "八德區",zip = "334",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "桃園市",district = "龜山區",zip = "333",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "桃園市",district = "龜山區",zip = "333",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "桃園市",district = "桃園區",zip = "330",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "桃園市",district = "桃園區",zip = "330",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "桃園市",district = "觀音區",zip = "328",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "桃園市",district = "觀音區",zip = "328",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "桃園市",district = "新屋區",zip = "327",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "桃園市",district = "新屋區",zip = "327",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "桃園市",district = "楊梅區",zip = "326",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "桃園市",district = "楊梅區",zip = "326",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "桃園市",district = "龍潭區",zip = "325",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "桃園市",district = "龍潭區",zip = "325",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "桃園市",district = "平鎮區",zip = "324",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "桃園市",district = "平鎮區",zip = "324",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "桃園市",district = "蘆竹區",zip = "338",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "桃園市",district = "蘆竹區",zip = "338",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "新竹市",district = "北區",zip = "300",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "新竹市",district = "北區",zip = "300",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "新竹市",district = "東區",zip = "300",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "新竹市",district = "東區",zip = "300",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "新竹市",district = "香山區",zip = "300",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "新竹市",district = "香山區",zip = "300",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "新竹縣",district = "竹北市",zip = "302",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "新竹縣",district = "竹北市",zip = "302",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "新竹縣",district = "北埔鄉",zip = "314",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "新竹縣",district = "北埔鄉",zip = "314",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "新竹縣",district = "尖石鄉",zip = "313",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "新竹縣",district = "尖石鄉",zip = "313",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "新竹縣",district = "橫山鄉",zip = "312",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "新竹縣",district = "橫山鄉",zip = "312",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "新竹縣",district = "五峰鄉",zip = "311",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "新竹縣",district = "五峰鄉",zip = "311",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "新竹縣",district = "竹東鎮",zip = "310",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "新竹縣",district = "竹東鎮",zip = "310",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "新竹縣",district = "寶山鄉",zip = "308",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "新竹縣",district = "寶山鄉",zip = "308",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "新竹縣",district = "芎林鄉",zip = "307",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "新竹縣",district = "芎林鄉",zip = "307",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "新竹縣",district = "關西鎮",zip = "306",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "新竹縣",district = "關西鎮",zip = "306",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "新竹縣",district = "新埔鎮",zip = "305",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "新竹縣",district = "新埔鎮",zip = "305",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "新竹縣",district = "新豐鄉",zip = "304",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "新竹縣",district = "新豐鄉",zip = "304",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "新竹縣",district = "湖口鄉",zip = "303",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "新竹縣",district = "湖口鄉",zip = "303",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "新竹縣",district = "峨眉鄉",zip = "315",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "新竹縣",district = "峨眉鄉",zip = "315",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "竹南鎮",zip = "350",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "竹南鎮",zip = "350",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "西湖鄉",zip = "368",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "西湖鄉",zip = "368",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "三義鄉",zip = "367",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "三義鄉",zip = "367",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "銅鑼鄉",zip = "366",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "銅鑼鄉",zip = "366",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "泰安鄉",zip = "365",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "泰安鄉",zip = "365",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "大湖鄉",zip = "364",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "大湖鄉",zip = "364",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "公館鄉",zip = "363",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "公館鄉",zip = "363",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "頭屋鄉",zip = "362",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "頭屋鄉",zip = "362",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "造橋鄉",zip = "361",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "造橋鄉",zip = "361",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "苗栗市",zip = "360",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "苗栗市",zip = "360",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "苑裡鎮",zip = "358",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "苑裡鎮",zip = "358",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "通霄鎮",zip = "357",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "通霄鎮",zip = "357",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "後龍鎮",zip = "356",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "後龍鎮",zip = "356",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "獅潭鄉",zip = "354",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "獅潭鄉",zip = "354",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "南庄鄉",zip = "353",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "南庄鄉",zip = "353",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "三灣鄉",zip = "352",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "三灣鄉",zip = "352",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "頭份鎮",zip = "351",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "頭份鎮",zip = "351",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "卓蘭鎮",zip = "369",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "苗栗縣",district = "卓蘭鎮",zip = "369",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "台中市",district = "中區",zip = "400",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "台中市",district = "中區",zip = "400",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "台中市",district = "和平區",zip = "424",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "台中市",district = "和平區",zip = "424",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "台中市",district = "新社區",zip = "426",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "台中市",district = "新社區",zip = "426",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "台中市",district = "潭子區",zip = "427",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "台中市",district = "潭子區",zip = "427",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "台中市",district = "大雅區",zip = "428",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "台中市",district = "大雅區",zip = "428",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "台中市",district = "神岡區",zip = "429",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "台中市",district = "神岡區",zip = "429",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "台中市",district = "大肚區",zip = "432",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台中市",district = "大肚區",zip = "432",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台中市",district = "沙鹿區",zip = "433",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "台中市",district = "沙鹿區",zip = "433",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "台中市",district = "龍井區",zip = "434",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "台中市",district = "龍井區",zip = "434",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "台中市",district = "梧棲區",zip = "435",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "台中市",district = "梧棲區",zip = "435",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "台中市",district = "清水區",zip = "436",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "台中市",district = "清水區",zip = "436",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "台中市",district = "大甲區",zip = "437",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "台中市",district = "大甲區",zip = "437",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "台中市",district = "外埔區",zip = "438",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "台中市",district = "外埔區",zip = "438",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "台中市",district = "東勢區",zip = "423",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "台中市",district = "東勢區",zip = "423",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "台中市",district = "石岡區",zip = "422",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "台中市",district = "石岡區",zip = "422",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "台中市",district = "后里區",zip = "421",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "台中市",district = "后里區",zip = "421",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "台中市",district = "東區",zip = "401",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "台中市",district = "東區",zip = "401",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "台中市",district = "南區",zip = "402",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "台中市",district = "南區",zip = "402",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "台中市",district = "西區",zip = "403",lang = "tw",sortIndex = 19,lay="2" },
                new taiwan_city { city = "台中市",district = "西區",zip = "403",lang = "en",sortIndex = 19,lay="2" },
                new taiwan_city { city = "台中市",district = "北區",zip = "404",lang = "tw",sortIndex = 20,lay="2" },
                new taiwan_city { city = "台中市",district = "北區",zip = "404",lang = "en",sortIndex = 20,lay="2" },
                new taiwan_city { city = "台中市",district = "北屯區",zip = "406",lang = "tw",sortIndex = 21,lay="2" },
                new taiwan_city { city = "台中市",district = "北屯區",zip = "406",lang = "en",sortIndex = 21,lay="2" },
                new taiwan_city { city = "台中市",district = "西屯區",zip = "407",lang = "tw",sortIndex = 22,lay="2" },
                new taiwan_city { city = "台中市",district = "西屯區",zip = "407",lang = "en",sortIndex = 22,lay="2" },
                new taiwan_city { city = "台中市",district = "南屯區",zip = "408",lang = "tw",sortIndex = 23,lay="2" },
                new taiwan_city { city = "台中市",district = "南屯區",zip = "408",lang = "en",sortIndex = 23,lay="2" },
                new taiwan_city { city = "台中市",district = "太平區",zip = "411",lang = "tw",sortIndex = 24,lay="2" },
                new taiwan_city { city = "台中市",district = "太平區",zip = "411",lang = "en",sortIndex = 24,lay="2" },
                new taiwan_city { city = "台中市",district = "大里區",zip = "412",lang = "tw",sortIndex = 25,lay="2" },
                new taiwan_city { city = "台中市",district = "大里區",zip = "412",lang = "en",sortIndex = 25,lay="2" },
                new taiwan_city { city = "台中市",district = "霧峰區",zip = "413",lang = "tw",sortIndex = 26,lay="2" },
                new taiwan_city { city = "台中市",district = "霧峰區",zip = "413",lang = "en",sortIndex = 26,lay="2" },
                new taiwan_city { city = "台中市",district = "烏日區",zip = "414",lang = "tw",sortIndex = 27,lay="2" },
                new taiwan_city { city = "台中市",district = "烏日區",zip = "414",lang = "en",sortIndex = 27,lay="2" },
                new taiwan_city { city = "台中市",district = "豐原區",zip = "420",lang = "tw",sortIndex = 28,lay="2" },
                new taiwan_city { city = "台中市",district = "豐原區",zip = "420",lang = "en",sortIndex = 28,lay="2" },
                new taiwan_city { city = "台中市",district = "大安區",zip = "439",lang = "tw",sortIndex = 29,lay="2" },
                new taiwan_city { city = "台中市",district = "大安區",zip = "439",lang = "en",sortIndex = 29,lay="2" },
                new taiwan_city { city = "彰化縣",district = "彰化市",zip = "500",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "彰化縣",district = "彰化市",zip = "500",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "彰化縣",district = "大村鄉",zip = "515",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "彰化縣",district = "大村鄉",zip = "515",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "彰化縣",district = "埔鹽鄉",zip = "516",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "彰化縣",district = "埔鹽鄉",zip = "516",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "彰化縣",district = "田中鎮",zip = "520",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "彰化縣",district = "田中鎮",zip = "520",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "彰化縣",district = "北斗鎮",zip = "521",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "彰化縣",district = "北斗鎮",zip = "521",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "彰化縣",district = "田尾鄉",zip = "522",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "彰化縣",district = "田尾鄉",zip = "522",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "彰化縣",district = "埤頭鄉",zip = "523",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "彰化縣",district = "埤頭鄉",zip = "523",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "彰化縣",district = "溪州鄉",zip = "524",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "彰化縣",district = "溪州鄉",zip = "524",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "彰化縣",district = "竹塘鄉",zip = "525",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "彰化縣",district = "竹塘鄉",zip = "525",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "彰化縣",district = "二林鎮",zip = "526",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "彰化縣",district = "二林鎮",zip = "526",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "彰化縣",district = "大城鄉",zip = "527",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "彰化縣",district = "大城鄉",zip = "527",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "彰化縣",district = "芳苑鄉",zip = "528",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "彰化縣",district = "芳苑鄉",zip = "528",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "彰化縣",district = "溪湖鎮",zip = "514",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "彰化縣",district = "溪湖鎮",zip = "514",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "彰化縣",district = "埔心鄉",zip = "513",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "彰化縣",district = "埔心鄉",zip = "513",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "彰化縣",district = "芬園鄉",zip = "502",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "彰化縣",district = "芬園鄉",zip = "502",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "彰化縣",district = "花壇鄉",zip = "503",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "彰化縣",district = "花壇鄉",zip = "503",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "彰化縣",district = "秀水鄉",zip = "504",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "彰化縣",district = "秀水鄉",zip = "504",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "彰化縣",district = "鹿港鎮",zip = "505",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "彰化縣",district = "鹿港鎮",zip = "505",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "彰化縣",district = "福興鄉",zip = "506",lang = "tw",sortIndex = 19,lay="2" },
                new taiwan_city { city = "彰化縣",district = "福興鄉",zip = "506",lang = "en",sortIndex = 19,lay="2" },
                new taiwan_city { city = "彰化縣",district = "線西鄉",zip = "507",lang = "tw",sortIndex = 20,lay="2" },
                new taiwan_city { city = "彰化縣",district = "線西鄉",zip = "507",lang = "en",sortIndex = 20,lay="2" },
                new taiwan_city { city = "彰化縣",district = "和美鎮",zip = "508",lang = "tw",sortIndex = 21,lay="2" },
                new taiwan_city { city = "彰化縣",district = "和美鎮",zip = "508",lang = "en",sortIndex = 21,lay="2" },
                new taiwan_city { city = "彰化縣",district = "伸港鄉",zip = "509",lang = "tw",sortIndex = 22,lay="2" },
                new taiwan_city { city = "彰化縣",district = "伸港鄉",zip = "509",lang = "en",sortIndex = 22,lay="2" },
                new taiwan_city { city = "彰化縣",district = "員林鎮",zip = "510",lang = "tw",sortIndex = 23,lay="2" },
                new taiwan_city { city = "彰化縣",district = "員林鎮",zip = "510",lang = "en",sortIndex = 23,lay="2" },
                new taiwan_city { city = "彰化縣",district = "社頭鄉",zip = "511",lang = "tw",sortIndex = 24,lay="2" },
                new taiwan_city { city = "彰化縣",district = "社頭鄉",zip = "511",lang = "en",sortIndex = 24,lay="2" },
                new taiwan_city { city = "彰化縣",district = "永靖鄉",zip = "512",lang = "tw",sortIndex = 25,lay="2" },
                new taiwan_city { city = "彰化縣",district = "永靖鄉",zip = "512",lang = "en",sortIndex = 25,lay="2" },
                new taiwan_city { city = "彰化縣",district = "二水鄉",zip = "530",lang = "tw",sortIndex = 26,lay="2" },
                new taiwan_city { city = "彰化縣",district = "二水鄉",zip = "530",lang = "en",sortIndex = 26,lay="2" },
                new taiwan_city { city = "南投縣",district = "南投市",zip = "540",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "南投縣",district = "南投市",zip = "540",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "南投縣",district = "竹山鎮",zip = "557",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "南投縣",district = "竹山鎮",zip = "557",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "南投縣",district = "信義鄉",zip = "556",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "南投縣",district = "信義鄉",zip = "556",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "南投縣",district = "魚池鄉",zip = "555",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "南投縣",district = "魚池鄉",zip = "555",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "南投縣",district = "水里鄉",zip = "553",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "南投縣",district = "水里鄉",zip = "553",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "南投縣",district = "集集鎮",zip = "552",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "南投縣",district = "集集鎮",zip = "552",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "南投縣",district = "名間鄉",zip = "551",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "南投縣",district = "名間鄉",zip = "551",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "南投縣",district = "仁愛鄉",zip = "546",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "南投縣",district = "仁愛鄉",zip = "546",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "南投縣",district = "埔里鎮",zip = "545",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "南投縣",district = "埔里鎮",zip = "545",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "南投縣",district = "國姓鄉",zip = "544",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "南投縣",district = "國姓鄉",zip = "544",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "南投縣",district = "草屯鎮",zip = "542",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "南投縣",district = "草屯鎮",zip = "542",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "南投縣",district = "中寮鄉",zip = "541",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "南投縣",district = "中寮鄉",zip = "541",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "南投縣",district = "鹿谷鄉",zip = "558",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "南投縣",district = "鹿谷鄉",zip = "558",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "雲林縣",district = "斗南鎮",zip = "630",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "雲林縣",district = "斗南鎮",zip = "630",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "雲林縣",district = "古坑鄉",zip = "646",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "雲林縣",district = "古坑鄉",zip = "646",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "雲林縣",district = "莿桐鄉",zip = "647",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "雲林縣",district = "莿桐鄉",zip = "647",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "雲林縣",district = "西螺鎮",zip = "648",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "雲林縣",district = "西螺鎮",zip = "648",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "雲林縣",district = "二崙鄉",zip = "649",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "雲林縣",district = "二崙鄉",zip = "649",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "雲林縣",district = "北港鎮",zip = "651",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "雲林縣",district = "北港鎮",zip = "651",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "雲林縣",district = "水林鄉",zip = "652",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "雲林縣",district = "水林鄉",zip = "652",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "雲林縣",district = "口湖鄉",zip = "653",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "雲林縣",district = "口湖鄉",zip = "653",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "雲林縣",district = "四湖鄉",zip = "654",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "雲林縣",district = "四湖鄉",zip = "654",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "雲林縣",district = "林內鄉",zip = "643",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "雲林縣",district = "林內鄉",zip = "643",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "雲林縣",district = "斗六市",zip = "640",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "雲林縣",district = "斗六市",zip = "640",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "雲林縣",district = "大埤鄉",zip = "631",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "雲林縣",district = "大埤鄉",zip = "631",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "雲林縣",district = "虎尾鎮",zip = "632",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "雲林縣",district = "虎尾鎮",zip = "632",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "雲林縣",district = "土庫鎮",zip = "633",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "雲林縣",district = "土庫鎮",zip = "633",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "雲林縣",district = "褒忠鄉",zip = "634",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "雲林縣",district = "褒忠鄉",zip = "634",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "雲林縣",district = "東勢鄉",zip = "635",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "雲林縣",district = "東勢鄉",zip = "635",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "雲林縣",district = "台西鄉",zip = "636",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "雲林縣",district = "台西鄉",zip = "636",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "雲林縣",district = "崙背鄉",zip = "637",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "雲林縣",district = "崙背鄉",zip = "637",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "雲林縣",district = "麥寮鄉",zip = "638",lang = "tw",sortIndex = 19,lay="2" },
                new taiwan_city { city = "雲林縣",district = "麥寮鄉",zip = "638",lang = "en",sortIndex = 19,lay="2" },
                new taiwan_city { city = "雲林縣",district = "元長鄉",zip = "655",lang = "tw",sortIndex = 20,lay="2" },
                new taiwan_city { city = "雲林縣",district = "元長鄉",zip = "655",lang = "en",sortIndex = 20,lay="2" },
                new taiwan_city { city = "嘉義市",district = "東區",zip = "600",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "嘉義市",district = "東區",zip = "600",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "嘉義市",district = "西區",zip = "600",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "嘉義市",district = "西區",zip = "600",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "番路鄉",zip = "602",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "番路鄉",zip = "602",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "義竹鄉",zip = "624",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "義竹鄉",zip = "624",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "溪口鄉",zip = "623",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "溪口鄉",zip = "623",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "大林鎮",zip = "622",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "大林鎮",zip = "622",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "民雄鄉",zip = "621",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "民雄鄉",zip = "621",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "新港鄉",zip = "616",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "新港鄉",zip = "616",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "六腳鄉",zip = "615",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "六腳鄉",zip = "615",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "東石鄉",zip = "614",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "東石鄉",zip = "614",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "朴子市",zip = "613",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "朴子市",zip = "613",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "太保市",zip = "612",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "太保市",zip = "612",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "鹿草鄉",zip = "611",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "鹿草鄉",zip = "611",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "水上鄉",zip = "608",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "水上鄉",zip = "608",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "大埔鄉",zip = "607",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "大埔鄉",zip = "607",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "中埔鄉",zip = "606",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "中埔鄉",zip = "606",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "阿里山鄉",zip = "605",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "阿里山鄉",zip = "605",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "竹崎鄉",zip = "604",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "竹崎鄉",zip = "604",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "梅山鄉",zip = "603",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "梅山鄉",zip = "603",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "布袋鎮",zip = "625",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "嘉義縣",district = "布袋鎮",zip = "625",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "台南市",district = "中西區",zip = "700",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "台南市",district = "中西區",zip = "700",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "台南市",district = "七股區",zip = "724",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "台南市",district = "七股區",zip = "724",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "台南市",district = "將軍區",zip = "725",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "台南市",district = "將軍區",zip = "725",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "台南市",district = "學甲區",zip = "726",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "台南市",district = "學甲區",zip = "726",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "台南市",district = "北門區",zip = "727",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "台南市",district = "北門區",zip = "727",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "台南市",district = "新營區",zip = "730",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "台南市",district = "新營區",zip = "730",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "台南市",district = "後壁區",zip = "731",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台南市",district = "後壁區",zip = "731",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台南市",district = "白河區",zip = "732",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "台南市",district = "白河區",zip = "732",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "台南市",district = "東山區",zip = "733",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "台南市",district = "東山區",zip = "733",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "台南市",district = "六甲區",zip = "734",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "台南市",district = "六甲區",zip = "734",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "台南市",district = "下營區",zip = "735",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "台南市",district = "下營區",zip = "735",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "台南市",district = "柳營區",zip = "736",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "台南市",district = "柳營區",zip = "736",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "台南市",district = "鹽水區",zip = "737",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "台南市",district = "鹽水區",zip = "737",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "台南市",district = "善化區",zip = "741",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "台南市",district = "善化區",zip = "741",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "台南市",district = "大內區",zip = "742",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "台南市",district = "大內區",zip = "742",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "台南市",district = "山上區",zip = "743",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "台南市",district = "山上區",zip = "743",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "台南市",district = "新市區",zip = "744",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "台南市",district = "新市區",zip = "744",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "台南市",district = "西港區",zip = "723",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "台南市",district = "西港區",zip = "723",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "台南市",district = "佳里區",zip = "722",lang = "tw",sortIndex = 19,lay="2" },
                new taiwan_city { city = "台南市",district = "佳里區",zip = "722",lang = "en",sortIndex = 19,lay="2" },
                new taiwan_city { city = "台南市",district = "麻豆區",zip = "721",lang = "tw",sortIndex = 20,lay="2" },
                new taiwan_city { city = "台南市",district = "麻豆區",zip = "721",lang = "en",sortIndex = 20,lay="2" },
                new taiwan_city { city = "台南市",district = "東區",zip = "701",lang = "tw",sortIndex = 21,lay="2" },
                new taiwan_city { city = "台南市",district = "東區",zip = "701",lang = "en",sortIndex = 21,lay="2" },
                new taiwan_city { city = "台南市",district = "南區",zip = "702",lang = "tw",sortIndex = 22,lay="2" },
                new taiwan_city { city = "台南市",district = "南區",zip = "702",lang = "en",sortIndex = 22,lay="2" },
                new taiwan_city { city = "台南市",district = "北區",zip = "704",lang = "tw",sortIndex = 23,lay="2" },
                new taiwan_city { city = "台南市",district = "北區",zip = "704",lang = "en",sortIndex = 23,lay="2" },
                new taiwan_city { city = "台南市",district = "安平區",zip = "708",lang = "tw",sortIndex = 24,lay="2" },
                new taiwan_city { city = "台南市",district = "安平區",zip = "708",lang = "en",sortIndex = 24,lay="2" },
                new taiwan_city { city = "台南市",district = "安南區",zip = "709",lang = "tw",sortIndex = 25,lay="2" },
                new taiwan_city { city = "台南市",district = "安南區",zip = "709",lang = "en",sortIndex = 25,lay="2" },
                new taiwan_city { city = "台南市",district = "永康區",zip = "710",lang = "tw",sortIndex = 26,lay="2" },
                new taiwan_city { city = "台南市",district = "永康區",zip = "710",lang = "en",sortIndex = 26,lay="2" },
                new taiwan_city { city = "台南市",district = "歸仁區",zip = "711",lang = "tw",sortIndex = 27,lay="2" },
                new taiwan_city { city = "台南市",district = "歸仁區",zip = "711",lang = "en",sortIndex = 27,lay="2" },
                new taiwan_city { city = "台南市",district = "新化區",zip = "712",lang = "tw",sortIndex = 28,lay="2" },
                new taiwan_city { city = "台南市",district = "新化區",zip = "712",lang = "en",sortIndex = 28,lay="2" },
                new taiwan_city { city = "台南市",district = "左鎮區",zip = "713",lang = "tw",sortIndex = 29,lay="2" },
                new taiwan_city { city = "台南市",district = "左鎮區",zip = "713",lang = "en",sortIndex = 29,lay="2" },
                new taiwan_city { city = "台南市",district = "玉井區",zip = "714",lang = "tw",sortIndex = 30,lay="2" },
                new taiwan_city { city = "台南市",district = "玉井區",zip = "714",lang = "en",sortIndex = 30,lay="2" },
                new taiwan_city { city = "台南市",district = "楠西區",zip = "715",lang = "tw",sortIndex = 31,lay="2" },
                new taiwan_city { city = "台南市",district = "楠西區",zip = "715",lang = "en",sortIndex = 31,lay="2" },
                new taiwan_city { city = "台南市",district = "南化區",zip = "716",lang = "tw",sortIndex = 32,lay="2" },
                new taiwan_city { city = "台南市",district = "南化區",zip = "716",lang = "en",sortIndex = 32,lay="2" },
                new taiwan_city { city = "台南市",district = "仁德區",zip = "717",lang = "tw",sortIndex = 33,lay="2" },
                new taiwan_city { city = "台南市",district = "仁德區",zip = "717",lang = "en",sortIndex = 33,lay="2" },
                new taiwan_city { city = "台南市",district = "關廟區",zip = "718",lang = "tw",sortIndex = 34,lay="2" },
                new taiwan_city { city = "台南市",district = "關廟區",zip = "718",lang = "en",sortIndex = 34,lay="2" },
                new taiwan_city { city = "台南市",district = "龍崎區",zip = "719",lang = "tw",sortIndex = 35,lay="2" },
                new taiwan_city { city = "台南市",district = "龍崎區",zip = "719",lang = "en",sortIndex = 35,lay="2" },
                new taiwan_city { city = "台南市",district = "官田區",zip = "720",lang = "tw",sortIndex = 36,lay="2" },
                new taiwan_city { city = "台南市",district = "官田區",zip = "720",lang = "en",sortIndex = 36,lay="2" },
                new taiwan_city { city = "台南市",district = "安定區",zip = "745",lang = "tw",sortIndex = 37,lay="2" },
                new taiwan_city { city = "台南市",district = "安定區",zip = "745",lang = "en",sortIndex = 37,lay="2" },
                new taiwan_city { city = "高雄市",district = "新興區",zip = "800",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "高雄市",district = "新興區",zip = "800",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "高雄市",district = "彌陀區",zip = "827",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "高雄市",district = "彌陀區",zip = "827",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "高雄市",district = "永安區",zip = "828",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "高雄市",district = "永安區",zip = "828",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "高雄市",district = "湖內區",zip = "829",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "高雄市",district = "湖內區",zip = "829",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "高雄市",district = "鳳山區",zip = "830",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "高雄市",district = "鳳山區",zip = "830",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "高雄市",district = "大寮區",zip = "831",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "高雄市",district = "大寮區",zip = "831",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "高雄市",district = "林園區",zip = "832",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "高雄市",district = "林園區",zip = "832",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "高雄市",district = "鳥松區",zip = "833",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "高雄市",district = "鳥松區",zip = "833",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "高雄市",district = "大樹區",zip = "840",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "高雄市",district = "大樹區",zip = "840",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "高雄市",district = "旗山區",zip = "842",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "高雄市",district = "旗山區",zip = "842",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "高雄市",district = "美濃區",zip = "843",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "高雄市",district = "美濃區",zip = "843",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "高雄市",district = "六龜區",zip = "844",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "高雄市",district = "六龜區",zip = "844",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "高雄市",district = "內門區",zip = "845",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "高雄市",district = "內門區",zip = "845",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "高雄市",district = "杉林區",zip = "846",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "高雄市",district = "杉林區",zip = "846",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "高雄市",district = "甲仙區",zip = "847",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "高雄市",district = "甲仙區",zip = "847",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "高雄市",district = "桃源區",zip = "848",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "高雄市",district = "桃源區",zip = "848",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "高雄市",district = "那瑪夏區",zip = "849",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "高雄市",district = "那瑪夏區",zip = "849",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "高雄市",district = "茂林區",zip = "851",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "高雄市",district = "茂林區",zip = "851",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "高雄市",district = "梓官區",zip = "826",lang = "tw",sortIndex = 19,lay="2" },
                new taiwan_city { city = "高雄市",district = "梓官區",zip = "826",lang = "en",sortIndex = 19,lay="2" },
                new taiwan_city { city = "高雄市",district = "橋頭區",zip = "825",lang = "tw",sortIndex = 20,lay="2" },
                new taiwan_city { city = "高雄市",district = "橋頭區",zip = "825",lang = "en",sortIndex = 20,lay="2" },
                new taiwan_city { city = "高雄市",district = "前金區",zip = "801",lang = "tw",sortIndex = 21,lay="2" },
                new taiwan_city { city = "高雄市",district = "前金區",zip = "801",lang = "en",sortIndex = 21,lay="2" },
                new taiwan_city { city = "高雄市",district = "苓雅區",zip = "802",lang = "tw",sortIndex = 22,lay="2" },
                new taiwan_city { city = "高雄市",district = "苓雅區",zip = "802",lang = "en",sortIndex = 22,lay="2" },
                new taiwan_city { city = "高雄市",district = "鹽埕區",zip = "803",lang = "tw",sortIndex = 23,lay="2" },
                new taiwan_city { city = "高雄市",district = "鹽埕區",zip = "803",lang = "en",sortIndex = 23,lay="2" },
                new taiwan_city { city = "高雄市",district = "鼓山區",zip = "804",lang = "tw",sortIndex = 24,lay="2" },
                new taiwan_city { city = "高雄市",district = "鼓山區",zip = "804",lang = "en",sortIndex = 24,lay="2" },
                new taiwan_city { city = "高雄市",district = "旗津區",zip = "805",lang = "tw",sortIndex = 25,lay="2" },
                new taiwan_city { city = "高雄市",district = "旗津區",zip = "805",lang = "en",sortIndex = 25,lay="2" },
                new taiwan_city { city = "高雄市",district = "前鎮區",zip = "806",lang = "tw",sortIndex = 26,lay="2" },
                new taiwan_city { city = "高雄市",district = "前鎮區",zip = "806",lang = "en",sortIndex = 26,lay="2" },
                new taiwan_city { city = "高雄市",district = "三民區",zip = "807",lang = "tw",sortIndex = 27,lay="2" },
                new taiwan_city { city = "高雄市",district = "三民區",zip = "807",lang = "en",sortIndex = 27,lay="2" },
                new taiwan_city { city = "高雄市",district = "楠梓區",zip = "811",lang = "tw",sortIndex = 28,lay="2" },
                new taiwan_city { city = "高雄市",district = "楠梓區",zip = "811",lang = "en",sortIndex = 28,lay="2" },
                new taiwan_city { city = "高雄市",district = "小港區",zip = "812",lang = "tw",sortIndex = 29,lay="2" },
                new taiwan_city { city = "高雄市",district = "小港區",zip = "812",lang = "en",sortIndex = 29,lay="2" },
                new taiwan_city { city = "高雄市",district = "左營區",zip = "813",lang = "tw",sortIndex = 30,lay="2" },
                new taiwan_city { city = "高雄市",district = "左營區",zip = "813",lang = "en",sortIndex = 30,lay="2" },
                new taiwan_city { city = "高雄市",district = "仁武區",zip = "814",lang = "tw",sortIndex = 31,lay="2" },
                new taiwan_city { city = "高雄市",district = "仁武區",zip = "814",lang = "en",sortIndex = 31,lay="2" },
                new taiwan_city { city = "高雄市",district = "大社區",zip = "815",lang = "tw",sortIndex = 32,lay="2" },
                new taiwan_city { city = "高雄市",district = "大社區",zip = "815",lang = "en",sortIndex = 32,lay="2" },
                new taiwan_city { city = "高雄市",district = "岡山區",zip = "820",lang = "tw",sortIndex = 33,lay="2" },
                new taiwan_city { city = "高雄市",district = "岡山區",zip = "820",lang = "en",sortIndex = 33,lay="2" },
                new taiwan_city { city = "高雄市",district = "路竹區",zip = "821",lang = "tw",sortIndex = 34,lay="2" },
                new taiwan_city { city = "高雄市",district = "路竹區",zip = "821",lang = "en",sortIndex = 34,lay="2" },
                new taiwan_city { city = "高雄市",district = "阿蓮區",zip = "822",lang = "tw",sortIndex = 35,lay="2" },
                new taiwan_city { city = "高雄市",district = "阿蓮區",zip = "822",lang = "en",sortIndex = 35,lay="2" },
                new taiwan_city { city = "高雄市",district = "田寮區",zip = "823",lang = "tw",sortIndex = 36,lay="2" },
                new taiwan_city { city = "高雄市",district = "田寮區",zip = "823",lang = "en",sortIndex = 36,lay="2" },
                new taiwan_city { city = "高雄市",district = "燕巢區",zip = "824",lang = "tw",sortIndex = 37,lay="2" },
                new taiwan_city { city = "高雄市",district = "燕巢區",zip = "824",lang = "en",sortIndex = 37,lay="2" },
                new taiwan_city { city = "高雄市",district = "茄萣區",zip = "852",lang = "tw",sortIndex = 38,lay="2" },
                new taiwan_city { city = "高雄市",district = "茄萣區",zip = "852",lang = "en",sortIndex = 38,lay="2" },
                new taiwan_city { city = "屏東縣",district = "屏東市",zip = "900",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "屏東縣",district = "屏東市",zip = "900",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "屏東縣",district = "新埤鄉",zip = "925",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "屏東縣",district = "新埤鄉",zip = "925",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "屏東縣",district = "南州鄉",zip = "926",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "屏東縣",district = "南州鄉",zip = "926",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "屏東縣",district = "林邊鄉",zip = "927",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "屏東縣",district = "林邊鄉",zip = "927",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "屏東縣",district = "東港鎮",zip = "928",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "屏東縣",district = "東港鎮",zip = "928",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "屏東縣",district = "琉球鄉",zip = "929",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "屏東縣",district = "琉球鄉",zip = "929",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "屏東縣",district = "佳冬鄉",zip = "931",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "屏東縣",district = "佳冬鄉",zip = "931",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "屏東縣",district = "新園鄉",zip = "932",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "屏東縣",district = "新園鄉",zip = "932",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "屏東縣",district = "枋寮鄉",zip = "940",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "屏東縣",district = "枋寮鄉",zip = "940",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "屏東縣",district = "枋山鄉",zip = "941",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "屏東縣",district = "枋山鄉",zip = "941",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "屏東縣",district = "春日鄉",zip = "942",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "屏東縣",district = "春日鄉",zip = "942",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "屏東縣",district = "獅子鄉",zip = "943",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "屏東縣",district = "獅子鄉",zip = "943",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "屏東縣",district = "車城鄉",zip = "944",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "屏東縣",district = "車城鄉",zip = "944",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "屏東縣",district = "牡丹鄉",zip = "945",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "屏東縣",district = "牡丹鄉",zip = "945",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "屏東縣",district = "恆春鎮",zip = "946",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "屏東縣",district = "恆春鎮",zip = "946",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "屏東縣",district = "崁頂鄉",zip = "924",lang = "tw",sortIndex = 16,lay="2" },
                new taiwan_city { city = "屏東縣",district = "崁頂鄉",zip = "924",lang = "en",sortIndex = 16,lay="2" },
                new taiwan_city { city = "屏東縣",district = "萬巒鄉",zip = "923",lang = "tw",sortIndex = 17,lay="2" },
                new taiwan_city { city = "屏東縣",district = "萬巒鄉",zip = "923",lang = "en",sortIndex = 17,lay="2" },
                new taiwan_city { city = "屏東縣",district = "來義鄉",zip = "922",lang = "tw",sortIndex = 18,lay="2" },
                new taiwan_city { city = "屏東縣",district = "來義鄉",zip = "922",lang = "en",sortIndex = 18,lay="2" },
                new taiwan_city { city = "屏東縣",district = "三地門鄉",zip = "901",lang = "tw",sortIndex = 19,lay="2" },
                new taiwan_city { city = "屏東縣",district = "三地門鄉",zip = "901",lang = "en",sortIndex = 19,lay="2" },
                new taiwan_city { city = "屏東縣",district = "霧台鄉",zip = "902",lang = "tw",sortIndex = 20,lay="2" },
                new taiwan_city { city = "屏東縣",district = "霧台鄉",zip = "902",lang = "en",sortIndex = 20,lay="2" },
                new taiwan_city { city = "屏東縣",district = "瑪家鄉",zip = "903",lang = "tw",sortIndex = 21,lay="2" },
                new taiwan_city { city = "屏東縣",district = "瑪家鄉",zip = "903",lang = "en",sortIndex = 21,lay="2" },
                new taiwan_city { city = "屏東縣",district = "九如鄉",zip = "904",lang = "tw",sortIndex = 22,lay="2" },
                new taiwan_city { city = "屏東縣",district = "九如鄉",zip = "904",lang = "en",sortIndex = 22,lay="2" },
                new taiwan_city { city = "屏東縣",district = "里港鄉",zip = "905",lang = "tw",sortIndex = 23,lay="2" },
                new taiwan_city { city = "屏東縣",district = "里港鄉",zip = "905",lang = "en",sortIndex = 23,lay="2" },
                new taiwan_city { city = "屏東縣",district = "高樹鄉",zip = "906",lang = "tw",sortIndex = 24,lay="2" },
                new taiwan_city { city = "屏東縣",district = "高樹鄉",zip = "906",lang = "en",sortIndex = 24,lay="2" },
                new taiwan_city { city = "屏東縣",district = "鹽埔鄉",zip = "907",lang = "tw",sortIndex = 25,lay="2" },
                new taiwan_city { city = "屏東縣",district = "鹽埔鄉",zip = "907",lang = "en",sortIndex = 25,lay="2" },
                new taiwan_city { city = "屏東縣",district = "長治鄉",zip = "908",lang = "tw",sortIndex = 26,lay="2" },
                new taiwan_city { city = "屏東縣",district = "長治鄉",zip = "908",lang = "en",sortIndex = 26,lay="2" },
                new taiwan_city { city = "屏東縣",district = "麟洛鄉",zip = "909",lang = "tw",sortIndex = 27,lay="2" },
                new taiwan_city { city = "屏東縣",district = "麟洛鄉",zip = "909",lang = "en",sortIndex = 27,lay="2" },
                new taiwan_city { city = "屏東縣",district = "竹田鄉",zip = "911",lang = "tw",sortIndex = 28,lay="2" },
                new taiwan_city { city = "屏東縣",district = "竹田鄉",zip = "911",lang = "en",sortIndex = 28,lay="2" },
                new taiwan_city { city = "屏東縣",district = "內埔鄉",zip = "912",lang = "tw",sortIndex = 29,lay="2" },
                new taiwan_city { city = "屏東縣",district = "內埔鄉",zip = "912",lang = "en",sortIndex = 29,lay="2" },
                new taiwan_city { city = "屏東縣",district = "萬丹鄉",zip = "913",lang = "tw",sortIndex = 30,lay="2" },
                new taiwan_city { city = "屏東縣",district = "萬丹鄉",zip = "913",lang = "en",sortIndex = 30,lay="2" },
                new taiwan_city { city = "屏東縣",district = "潮州鎮",zip = "920",lang = "tw",sortIndex = 31,lay="2" },
                new taiwan_city { city = "屏東縣",district = "潮州鎮",zip = "920",lang = "en",sortIndex = 31,lay="2" },
                new taiwan_city { city = "屏東縣",district = "泰武鄉",zip = "921",lang = "tw",sortIndex = 32,lay="2" },
                new taiwan_city { city = "屏東縣",district = "泰武鄉",zip = "921",lang = "en",sortIndex = 32,lay="2" },
                new taiwan_city { city = "屏東縣",district = "滿州鄉",zip = "947",lang = "tw",sortIndex = 33,lay="2" },
                new taiwan_city { city = "屏東縣",district = "滿州鄉",zip = "947",lang = "en",sortIndex = 33,lay="2" },
                new taiwan_city { city = "台東縣",district = "台東市",zip = "950",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "台東縣",district = "台東市",zip = "950",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "台東縣",district = "大武鄉",zip = "965",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "台東縣",district = "大武鄉",zip = "965",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "台東縣",district = "金峰鄉",zip = "964",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "台東縣",district = "金峰鄉",zip = "964",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "台東縣",district = "太麻里鄉",zip = "963",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "台東縣",district = "太麻里鄉",zip = "963",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "台東縣",district = "長濱鄉",zip = "962",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "台東縣",district = "長濱鄉",zip = "962",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "台東縣",district = "成功鎮",zip = "961",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "台東縣",district = "成功鎮",zip = "961",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "台東縣",district = "東河鄉",zip = "959",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台東縣",district = "東河鄉",zip = "959",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "台東縣",district = "池上鄉",zip = "958",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "台東縣",district = "池上鄉",zip = "958",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "台東縣",district = "海端鄉",zip = "957",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "台東縣",district = "海端鄉",zip = "957",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "台東縣",district = "關山鎮",zip = "956",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "台東縣",district = "關山鎮",zip = "956",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "台東縣",district = "鹿野鄉",zip = "955",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "台東縣",district = "鹿野鄉",zip = "955",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "台東縣",district = "卑南鄉",zip = "954",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "台東縣",district = "卑南鄉",zip = "954",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "台東縣",district = "延平鄉",zip = "953",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "台東縣",district = "延平鄉",zip = "953",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "台東縣",district = "綠島鄉",zip = "951",lang = "tw",sortIndex = 14,lay="2" },
                new taiwan_city { city = "台東縣",district = "綠島鄉",zip = "951",lang = "en",sortIndex = 14,lay="2" },
                new taiwan_city { city = "台東縣",district = "達仁鄉",zip = "966",lang = "tw",sortIndex = 15,lay="2" },
                new taiwan_city { city = "台東縣",district = "達仁鄉",zip = "966",lang = "en",sortIndex = 15,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "花蓮市",zip = "970",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "花蓮市",zip = "970",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "卓溪鄉",zip = "982",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "卓溪鄉",zip = "982",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "玉里鎮",zip = "981",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "玉里鎮",zip = "981",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "萬榮鄉",zip = "979",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "萬榮鄉",zip = "979",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "瑞穗鄉",zip = "978",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "瑞穗鄉",zip = "978",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "豐濱鄉",zip = "977",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "豐濱鄉",zip = "977",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "光復鄉",zip = "976",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "光復鄉",zip = "976",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "鳳林鎮",zip = "975",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "鳳林鎮",zip = "975",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "壽豐鄉",zip = "974",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "壽豐鄉",zip = "974",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "吉安鄉",zip = "973",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "吉安鄉",zip = "973",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "秀林鄉",zip = "972",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "秀林鄉",zip = "972",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "新城鄉",zip = "971",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "新城鄉",zip = "971",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "富里鄉",zip = "983",lang = "tw",sortIndex = 13,lay="2" },
                new taiwan_city { city = "花蓮縣",district = "富里鄉",zip = "983",lang = "en",sortIndex = 13,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "宜蘭市",zip = "260",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "宜蘭市",zip = "260",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "蘇澳鎮",zip = "270",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "蘇澳鎮",zip = "270",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "冬山鄉",zip = "269",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "冬山鄉",zip = "269",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "五結鄉",zip = "268",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "五結鄉",zip = "268",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "大同鄉",zip = "267",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "大同鄉",zip = "267",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "三星鄉",zip = "266",lang = "tw",sortIndex = 6,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "三星鄉",zip = "266",lang = "en",sortIndex = 6,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "羅東鎮",zip = "265",lang = "tw",sortIndex = 7,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "羅東鎮",zip = "265",lang = "en",sortIndex = 7,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "員山鄉",zip = "264",lang = "tw",sortIndex = 8,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "員山鄉",zip = "264",lang = "en",sortIndex = 8,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "壯圍鄉",zip = "263",lang = "tw",sortIndex = 9,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "壯圍鄉",zip = "263",lang = "en",sortIndex = 9,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "礁溪鄉",zip = "262",lang = "tw",sortIndex = 10,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "礁溪鄉",zip = "262",lang = "en",sortIndex = 10,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "頭城鎮",zip = "261",lang = "tw",sortIndex = 11,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "頭城鎮",zip = "261",lang = "en",sortIndex = 11,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "南澳鄉",zip = "272",lang = "tw",sortIndex = 12,lay="2" },
                new taiwan_city { city = "宜蘭縣",district = "南澳鄉",zip = "272",lang = "en",sortIndex = 12,lay="2" },
                new taiwan_city { city = "澎湖縣",district = "馬公市",zip = "880",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "澎湖縣",district = "馬公市",zip = "880",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "澎湖縣",district = "西嶼鄉",zip = "881",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "澎湖縣",district = "西嶼鄉",zip = "881",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "澎湖縣",district = "白沙鄉",zip = "884",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "澎湖縣",district = "白沙鄉",zip = "884",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "澎湖縣",district = "湖西鄉",zip = "885",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "澎湖縣",district = "湖西鄉",zip = "885",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "金門縣",district = "金沙鎮",zip = "890",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "金門縣",district = "金沙鎮",zip = "890",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "金門縣",district = "金湖鎮",zip = "891",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "金門縣",district = "金湖鎮",zip = "891",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "金門縣",district = "金寧鄉",zip = "892",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "金門縣",district = "金寧鄉",zip = "892",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "金門縣",district = "金城鎮",zip = "893",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "金門縣",district = "金城鎮",zip = "893",lang = "en",sortIndex = 4,lay="2" },
                new taiwan_city { city = "金門縣",district = "烈嶼鄉",zip = "894",lang = "tw",sortIndex = 5,lay="2" },
                new taiwan_city { city = "金門縣",district = "烈嶼鄉",zip = "894",lang = "en",sortIndex = 5,lay="2" },
                new taiwan_city { city = "連江縣",district = "南竿鄉",zip = "209",lang = "tw",sortIndex = 1,lay="2" },
                new taiwan_city { city = "連江縣",district = "南竿鄉",zip = "209",lang = "en",sortIndex = 1,lay="2" },
                new taiwan_city { city = "連江縣",district = "北竿鄉",zip = "210",lang = "tw",sortIndex = 2,lay="2" },
                new taiwan_city { city = "連江縣",district = "北竿鄉",zip = "210",lang = "en",sortIndex = 2,lay="2" },
                new taiwan_city { city = "連江縣",district = "莒光鄉",zip = "211",lang = "tw",sortIndex = 3,lay="2" },
                new taiwan_city { city = "連江縣",district = "莒光鄉",zip = "211",lang = "en",sortIndex = 3,lay="2" },
                new taiwan_city { city = "連江縣",district = "東引鄉",zip = "212",lang = "tw",sortIndex = 4,lay="2" },
                new taiwan_city { city = "連江縣",district = "東引鄉",zip = "212",lang = "en",sortIndex = 4,lay="2" }

            };
            if (context.taiwan_city.ToList().Count == 0)
            {
                TaiwanCity.ForEach(s => context.taiwan_city.Add(s));
                context.SaveChanges();
            }*/
        }
    }
}
