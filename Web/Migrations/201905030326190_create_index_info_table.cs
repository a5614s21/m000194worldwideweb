namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class create_index_info_table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.index_info",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        subject = c.String(),
                        notes = c.String(),
                        content = c.String(),
                        content2 = c.String(),
                        content3 = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        seo_keywords = c.String(),
                        seo_description = c.String(),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'頁面名稱' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'標語' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'subject'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'敘述' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'notes'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容(1)' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容(2)' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'content2'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容(3)' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'content3'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬類別' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO關鍵字' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'seo_keywords'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO敘述' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'seo_description'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'index_info', 'COLUMN', N'sortIndex'");

            CreateTable(
                "dbo.index_info_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        category = c.String(maxLength: 64),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'index_info_data', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'index_info_data', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'index_info_data', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'index_info_data', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建置日期' ,'SCHEMA', N'dbo','TABLE', N'index_info_data', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'所屬類別' ,'SCHEMA', N'dbo','TABLE', N'index_info_data', 'COLUMN', N'category'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'index_info_data', 'COLUMN', N'lang'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'index_info_data', 'COLUMN', N'sortIndex'");

        }
        
        public override void Down()
        {
            DropTable("dbo.index_info_data");
            DropTable("dbo.index_info");
        }
    }
}
