namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_link_list_to_recruits : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.recruits", "link_list", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'轉診作業連結' ,'SCHEMA', N'dbo','TABLE', N'recruits', 'COLUMN', N'link_list'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.recruits", "link_list");
        }
    }
}
