﻿$(document).ready(function () {

    //FB分享
    $("body").delegate(".fb_share", "click", function () {

        var thisURL = window.document.location.href;
        window.open('http://www.facebook.com/share.php?u='.concat(thisURL), 'pop', config = 'height=500,width=500');

    });
    //Google+分享
    $("body").delegate(".gp_share", "click", function () {

        var thisURL = window.document.location.href;
        window.open('https://plus.google.com/share?url='.concat(thisURL), 'pop', config = 'height=500,width=500');

    });
    //LINE分享
    $("body").delegate(".line_share", "click", function () {

        window.open('http://line.naver.jp/R/msg/text/?' + encodeURIComponent(document.title) + '/' + encodeURIComponent(location.href), 'pop', config = 'height=500,width=500');

    });
   //twitter分享
    $("body").delegate(".tt_share", "click", function () {

        var pageTitle = encodeURIComponent(document.title);
        var thisURL = window.document.location.href;
        window.open('http://twitter.com/home?status=' + pageTitle + "+".concat(thisURL), 'pop', config = 'height=500,width=500');

    });

    //重製驗證碼

    $('#captcha').click(function () {

        var src = $('#captcha').attr('src').split('?');

        $('#captcha').attr('src', src[0] + "?d=" + Date.now());

    });

});






function setLoadPlayer(view) {
    if (view == 'none') {
        $.unblockUI();
    }
    else {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}


var keyStr = "ABCDEFGHIJKLMNOP" +
    "QRSTUVWXYZabcdef" +
    "ghijklmnopqrstuv" +
    "wxyz0123456789+/" +
    "=";