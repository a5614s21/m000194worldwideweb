/***** AmCharts *****/
/* google analytics 年齡層*/
var chart = AmCharts.makeChart("analyticsAge", {
    "theme": "light",
    "type": "serial",
    "theme": "light",
    "dataLoader": {
       // "url": "/Content/Siteadmin/data/demo-analytics-age.json",
        "url": $('#siteadminArea').val() + "/analytics/age",
        "format": "json",
        "showErrors": true,
        "noStyles": true,
        "async": true,

    },

    "valueAxes": [{
        "gridColor": "#878787",
        "gridAlpha": 0.2,
        "dashLength": 0,
        "color": "#878787"
    }],
    "gridAboveGraphs": true,
    "startDuration": 1,
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "fillAlphas": 0.8,
        "lineAlpha": 0.2,
        "type": "column",
        "valueField": "visits",
        "color": "#878787"
    }],
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },
    "categoryField": "age",
    "categoryAxis": {
        "gridPosition": "start",
        "gridAlpha": 0,
        "tickPosition": "start",
        "tickLength": 20,
        "color": "#878787"
    },
    "export": {
        "enabled": false
    }
});
jQuery('.chart-input').off().on('input change', function() {
    var property = jQuery(this).data('property');
    var target = chart;
    chart.startDuration = 0;

    if (property == 'topRadius') {
        target = chart.graphs[0];
        if (this.value == 0) {
            this.value = undefined;
        }
    }

    target[property] = this.value;
    chart.validateNow();
});
/* google analytics 總覽*/
var analyticsAll = AmCharts.makeChart("analyticsAll", {
    "type": "serial",
    "dataLoader": {
        "url": "/Content/Siteadmin/data/demo-analytics-all.json",
        "format": "json",
        "showErrors": true,
        "noStyles": true,
        "async": true
    },
    "theme": "light",
    "dataDateFormat": "YYYY-MM-DD",
    "precision": 2,
    "valueAxes": [{
        "id": "v1",
        "title": "流量",
        "position": "left",
        "autoGridCount": false,
        "labelFunction": function(value) {
            return Math.round(value);
        }
    }, {
        "id": "v2",
        "title": "人數",
        "gridAlpha": 0,
        "position": "right",
        "autoGridCount": false
    }],
    "graphs": [{
        "id": "g3",
        "valueAxis": "v1",
        "lineColor": "#fff6f6",
        "fillColors": "#fff6f6",
        "fillAlphas": 1,
        "type": "column",
        "title": "總人次",
        "valueField": "sales2",
        "clustered": false,
        "columnWidth": 0.5,
        "legendValueText": "$[[value]]M",
        "balloonText": "[[title]]<br /><b style='font-size: 130%'>$[[value]]</b>"
    }, {
        "id": "g4",
        "valueAxis": "v1",
        "lineColor": "#ff5f74",
        "fillColors": "#ff5f74",
        "fillAlphas": 1,
        "type": "column",
        "title": "新訪客",
        "valueField": "sales1",
        "clustered": false,
        "columnWidth": 0.1,
        "legendValueText": "$[[value]]M",
        "balloonText": "[[title]]<br /><b style='font-size: 130%'>$[[value]]</b>"
    }, {
        "id": "g1",
        "valueAxis": "v2",
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 5,
        "hideBulletsCount": 50,
        "lineThickness": 2,
        "lineColor": "#ce93d8",
        "type": "smoothedLine",
        "title": "瀏覽量",
        "useLineColorForBulletBorder": true,
        "valueField": "market1",
        "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
    }, {
        "id": "g2",
        "valueAxis": "v2",
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "bulletSize": 5,
        "hideBulletsCount": 50,
        "lineThickness": 2,
        "lineColor": "#e1ede9",
        "type": "smoothedLine",
        "dashLength": 5,
        "title": "單次頁數",
        "useLineColorForBulletBorder": true,
        "valueField": "market2",
        "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
    }],
    "chartScrollbar": {
        "graph": "g1",
        "oppositeAxis": false,
        "offset": 30,
        "scrollbarHeight": 20,
        "backgroundAlpha": 0,
        "selectedBackgroundAlpha": 0.1,
        "selectedBackgroundColor": "#888888",
        "graphFillAlpha": 0,
        "graphLineAlpha": 0.5,
        "selectedGraphFillAlpha": 0,
        "selectedGraphLineAlpha": 1,
        "autoGridCount": true,
        "color": "#AAAAAA"
    },
    "chartCursor": {
        "pan": true,
        "valueLineEnabled": true,
        "valueLineBalloonEnabled": true,
        "cursorAlpha": 0,
        "valueLineAlpha": 0.2
    },
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "dashLength": 1,
        "minorGridEnabled": true
    },
    "legend": {
        "useGraphSettings": true,
        "position": "top"
    },
    "balloon": {
        "borderThickness": 1,
        "shadowAlpha": 0
    },
    "export": {
        "enabled": false
    }


});
/* 流量來源*/
var analyticsTraffic = AmCharts.makeChart("analyticsTraffic", {
    "type": "pie",
    "theme": "light",
    "dataLoader": {
        "url": $('#siteadminArea').val() + "/analytics/medium",
        "showCurtain": false
    },
    "titleField": "source",
    "valueField": "litres",
    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "innerRadius": "0%",
    "labelRadius": 5,
    "radius": "45%",
    "innerRadius": "0",
    "labelText": "",
});
/* 流量來源 - 1 organic*/
var traffic1 = AmCharts.makeChart("traffic_organic", {
    "type": "serial",
    "theme": "light",
    "dataLoader": {
        "url": $('#siteadminArea').val() + "/analytics/traffic/organic",
        "showCurtain": false
    },

    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "zeroGridAlpha": 0,

    }],
    "categoryAxis": {
        "dashLength": 0,
        "gridAlpha": 0,
        "axisAlpha": 0,
    },
    "balloon": {
        "shadowAlpha": 0,
    },
    "marginTop": 0,
    "marginRight": 0,
    "marginLeft": 0,
    "marginBottom": 0,
    "autoMargins": false,
    "startDuration": 1,
    "graphs": [{
        "id": "g1",
        "balloon": {
            "drop": false,
            "adjustBorderColor": false,
            "color": "#ffffff",
        },
        "fillAlphas": 0.2,
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "hideBulletsCount": 5,
        "lineThickness": 1,
        "valueField": "value",
    }],
    "chartCursor": {
        "valueLineEnabled": false,
        "categoryBalloonEnabled": false,
        "cursorColor": "#67b7dc",
        "pan": true,
    },

    "categoryField": "date",

    "export": {
        "enabled": false
    },

});
/* 流量來源 - 2 Direct */
var traffic2 = AmCharts.makeChart("traffic_WebPush", {
    "type": "serial",
    "theme": "light",
    "dataLoader": {
        "url": $('#siteadminArea').val() + "/analytics/traffic/WebPush",
        "showCurtain": false
    },

    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "zeroGridAlpha": 0,

    }],
    "categoryAxis": {
        "dashLength": 0,
        "gridAlpha": 0,
        "axisAlpha": 0,
    },
    "balloon": {
        "shadowAlpha": 0,
    },
    "marginTop": 0,
    "marginRight": 0,
    "marginLeft": 0,
    "marginBottom": 0,
    "autoMargins": false,
    "startDuration": 1,
    "graphs": [{
        "id": "g1",
        "balloon": {
            "drop": false,
            "adjustBorderColor": false,
            "color": "#ffffff",
        },
        "fillAlphas": 0.2,
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "hideBulletsCount": 5,
        "lineThickness": 1,
        "valueField": "value",
    }],
    "chartCursor": {
        "valueLineEnabled": false,
        "categoryBalloonEnabled": false,
        "cursorColor": "#67b7dc",
        "pan": true,
    },

    "categoryField": "date",

    "export": {
        "enabled": false
    },

});
/* 流量來源 - 3 Referral */
var traffic3 = AmCharts.makeChart("traffic_referral", {
    "type": "serial",
    "theme": "light",
    "dataLoader": {
        "url": $('#siteadminArea').val() + "/analytics/traffic/referral",
        "showCurtain": false
    },

    "valueAxes": [{
        "id": "v1",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "zeroGridAlpha": 0,

    }],
    "categoryAxis": {
        "dashLength": 0,
        "gridAlpha": 0,
        "axisAlpha": 0,
    },
    "balloon": {
        "shadowAlpha": 0,
    },
    "marginTop": 0,
    "marginRight": 0,
    "marginLeft": 0,
    "marginBottom": 0,
    "autoMargins": false,
    "startDuration": 1,
    "graphs": [{
        "id": "g1",
        "balloon": {
            "drop": false,
            "adjustBorderColor": false,
            "color": "#ffffff",
        },
        "fillAlphas": 0.2,
        "bullet": "round",
        "bulletBorderAlpha": 1,
        "bulletColor": "#FFFFFF",
        "hideBulletsCount": 5,
        "lineThickness": 1,
        "valueField": "value",
    }],
    "chartCursor": {
        "valueLineEnabled": false,
        "categoryBalloonEnabled": false,
        "cursorColor": "#67b7dc",
        "pan": true,
    },

    "categoryField": "date",

    "export": {
        "enabled": false
    },

});
