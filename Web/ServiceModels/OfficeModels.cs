﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Web;

namespace Web.ServiceModels
{
    /// <summary>
    /// 提供公告(最新消息、活動訊息)清單
    /// </summary>
    public partial class BulletinListModel
    {

        public string bulletinId { get; set; }

        public string startDate { get; set; }
        public string hospitalName { get; set; }
        public string bulletinTitle { get; set; }
        public Dictionary<string, string> PrevNextID { get; set; }
    }

    /// <summary>
    /// 提供公告(最新消息、活動訊息、學術活動資訊、職缺公告)內容
    /// </summary>
    public partial class BulletinModel
    {

        public string bulletinId { get; set; }
        public string startDate { get; set; }
        public string hospitalName { get; set; }
        public string bulletinTitle { get; set; }
        public string dept { get; set; }
        public string description { get; set; }
        public List<AttachedModel> attached { get; set; }

    }

    /// <summary>
    /// (附件)
    /// </summary>
    public partial class AttachedModel
    {
        public string attachedSq { get; set; }
        public string attachedName { get; set; }
        public string attachedUrl { get; set; }
    }

    /// <summary>
    /// 提供文章清單，包含各主頁之主視覺、首頁(聚焦點、新聞與觀點)、社會公益(圖文集)、研究教學(圖文集)、醫院訊息(聚焦點、新聞與觀點)
    /// </summary>
    public partial class GlobalArticleListModel
    {
        public string articleId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string photoUrl { get; set; }
        public Dictionary<string, string> PrevNextID { get; set; }

        public GlobalArticleModel Data { get; set; }
        public string photoUrl_s{ get; set; }
    }
    /// <summary>
    /// 提供文章內容，包含聚焦點、新聞與觀點、社會公益、研究教學
    /// </summary>
    public partial class GlobalArticleModel
    {
        public string articleId { get; set; }
        public string title { get; set; }
        public string startDate { get; set; }
        public string typeName { get; set; }
        public string dept { get; set; }
        public List<Paragraph> paragraph { get; set; }

    }
    /// <summary>
    /// 提供文章內容，包含聚焦點、新聞與觀點、社會公益、研究教學 (段落)
    /// </summary>
    public partial class Paragraph
    {
        public string sequence { get; set; }
        public string type { get; set; }
        public string paragraph { get; set; }
        public string photoUrl { get; set; }
        public string photoDesc { get; set; }
        public string url { get; set; }
        public string urlWord { get; set; }
        public string movieUrl { get; set; }
        public string movieDesc { get; set; }

    }



    /// <summary>
    /// 提供記者會資訊清單
    /// </summary>
    public partial class PressNewsListModel
    {
        public string pressId { get; set; }
        public string pressDate { get; set; }
        public string Title { get; set; }
        public Dictionary<string,string> PrevNextID { get; set; }
}
    /// <summary>
    /// 提供記者會資訊內容
    /// </summary>
    public partial class PressNewsModel
    {
        public string pressDate { get; set; }
        public string description { get; set; }
        public string Title { get; set; }
        public string author { get; set; }
    }

    /// <summary>
    /// 提供得獎記錄清單
    /// </summary>
    public partial class AwardListModel
    {
        public string awardId { get; set; }
        public string awardDate { get; set; }
        public string Title { get; set; }

        public Dictionary<string, string> PrevNextID { get; set; }
    }
    /// <summary>
    /// 提供得獎記錄內容
    /// </summary>
    public partial class AwardModel
    {
        public string awardDate { get; set; }      
        public string Title { get; set; }
        public string description { get; set; }

        public Dictionary<string, string> PrevNextID { get; set; }
    }

    /// <summary>
    /// 提供學術活動資訊清單
    /// </summary>
    public partial class AcademicEventsListModel
    {
        public string bulletinId { get; set; }
        public string startDate { get; set; }
        public string hospitalName { get; set; }
        public string bulletinTitle { get; set; }
        public Dictionary<string, string> PrevNextID { get; set; }
    }
    public partial class AcademicEventsModel
    {
        public string bulletinId { get; set; }
        public string startDate { get; set; }
        public string hospitalName { get; set; }
        public string dept { get; set; }
        public string bulletinTitle { get; set; }
        public string acsid { get; set; }
        public string eventRange { get; set; }
        public string eventType { get; set; }
        public string subject { get; set; }
        public string target { get; set; }
        public string eventDate { get; set; }
        public string eventTime { get; set; }
        public string place { get; set; }
        public string organizer { get; set; }
        public string coOrganizer { get; set; }
        public string presenter { get; set; }
        public string host { get; set; }
        public string contact { get; set; }
        public string tel { get; set; }
        public string description { get; set; }

        public List<AttachedModel> attached { get; set; }
    }

    /// <summary>
    /// 藥品異動訊息
    /// </summary>
    public partial class DrugChangeListModel
    {
        public string dataUrl { get; set; }
        public string yearMonth { get; set; }
    }

    /// <summary>
    /// 提供院區簡介
    /// </summary>
    public partial class BranchModel
    {
        public string hospitalName { get; set; }
        public string profile { get; set; }
        public List<SupervisorModel> supervisor { get; set; }
        public string bedQty { get; set; }
        public string doctorQty { get; set; }
        public string medicalQty { get; set; }
        public string tel { get; set; }
        public string fax { get; set; }
        public string address { get; set; }
        public string website { get; set; }
        public string webmaster { get; set; }
        public string updated { get; set; }
    }
    /// <summary>
    /// 院長級主管
    /// </summary>
    public partial class SupervisorModel
    {
        public string name { get; set; }
        public string jobTitle { get; set; }
        public string photoUrl { get; set; }
    }

    /// <summary>
    /// 提供醫療品質資訊公開清單
    /// </summary>
    public partial class MedicalQualityListModel
    {
        public string hospitalName { get; set; }
        public string title { get; set; }
        public string dataUrl { get; set; }
    }


    /// <summary>
    /// 品質績優事項清單
    /// </summary>
    public partial class ExcellentQualityListModel
    {
        public string year { get; set; }
        public string title { get; set; }
        public string dept { get; set; }
        public string award { get; set; }
    }

    /// <summary>
    /// 滿意度調查結果清單
    /// </summary>
    public partial class SatisfactionListModel
    {
        public string title { get; set; }
        public string description { get; set; }
    }
    /// <summary>
    /// 科部介紹清單
    /// </summary>
    public partial class DeptListModel
    {
        public string deptId { get; set; }
        public string deptName { get; set; }
        public string hasWeb { get; set; }
        public List<SubDeptModel> subDept { get; set; }
    }
    /// <summary>
    /// 次專科
    /// </summary>
    public partial class SubDeptModel
    {
        public string deptId { get; set; }
        public string deptName { get; set; }
        public string hasWeb { get; set; }
        public List<SubDeptModel> subDept { get; set; }
    }

    /// <summary>
    /// 次專科
    /// </summary>
    public partial class DoctorDeptSubDeptModel
    {
        public string deptId { get; set; }
        public string deptName { get; set; }
        public string hasDoctor { get; set; }

        public List<DoctorDeptSubDeptModel> subDept { get; set; }

    }

    /// <summary>
    /// 提供科部介紹資料
    /// </summary>
    public partial class DeptDataModel
    {
        public string deptId { get; set; }
        public string deptName { get; set; }
        public string deptIntro { get; set; }
        public List<MedicalTeamModel> medicalTeam { get; set; }
        public List<NursingTeamModel> nursingTeam { get; set; }
        public List<TechnicalTeamModel> technicalTeam { get; set; }
        public string services { get; set; }
        public string tel { get; set; }
        public string fax { get; set; }
        public string eMail { get; set; }
        public string location { get; set; }
        public List<LinksModel> links { get; set; }
        public string website { get; set; }
        public string registerParam { get; set; }
        public string webmaster { get; set; }
        public string updated { get; set; }

        public List<SubDeptModel> DeptList { get; set; }

    }

    /// <summary>
    /// 醫療團隊
    /// </summary>
    public partial class MedicalTeamModel
    {
        public string doctorId { get; set; }
        public string doctorName { get; set; }
        public string jobTitle { get; set; }
        public string specialty { get; set; }
    }

    /// <summary>
    /// 護理團隊
    /// </summary>
    public partial class NursingTeamModel
    {
        public string nurseName { get; set; }
        public string jobTitle { get; set; }
        public string specialty { get; set; }
    }

    /// <summary>
    /// 醫技團隊
    /// </summary>
    public partial class TechnicalTeamModel
    {
        public string technicalName { get; set; }
        public string jobTitle { get; set; }
        public string specialty { get; set; }
    }
    /// <summary>
    /// 友善連結
    /// </summary>
    public partial class LinksModel
    {
        public string linkName { get; set; }
        public string linkUrl { get; set; }
    }

    /// <summary>
    /// 提供查詢醫師結果清單
    /// </summary>
    public partial class SearchDoctorModel
    {
        public string hospitalID { get; set; }
        public string hospitalName { get; set; }
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string doctorId { get; set; }
        public string doctorName { get; set; }
        public string jobTitle { get; set; }
        public string specialty { get; set; }
    }


    /// <summary>
    /// 提供醫師介紹之部門清單
    /// </summary>
    public partial class DeptDoctorModel
    {
        public string doctorId { get; set; }
        public string doctorName { get; set; }
        public string jobTitle { get; set; }
        public string specialty { get; set; }
        public string deptGroupName { get; set; }
        public string deptGroupId { get; set; }
        public string deptID { get; set; }
        public string deptName { get; set; }
    }


    /// <summary>
    /// 提供醫師介紹之部門清單
    /// </summary>
    public partial class DoctorDeptListModel
    {
        public string deptGroupName { get; set; }
        public string deptId { get; set; }
        public string deptName { get; set; }

        public List<DoctorDeptSubDeptModel> subDept { get; set; }
    }



    /// <summary>
    /// 醫師資料
    /// </summary>
    public partial class DoctorModel
    {
        public string deptGroupId { get; set; }
        public string deptGroupName { get; set; }
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string doctorId { get; set; }
        public string doctorName { get; set; }
        public string language { get; set; }
        public string techingTitle { get; set; }
        public string specialty { get; set; }

        public List<DoctorJobTitleModel> jobTitle { get; set; }
        public List<DoctorEducationModel> education { get; set; }
        public List<DoctorExperienceModel> experience { get; set; }
        public List<DoctorCertificationModel> certification { get; set; }
        public List<DoctorResearchModel> research { get; set; }
        public List<DoctorRegisterModel> register { get; set; }

        public string webmaster { get; set; }

        public string updated { get; set; }

        public DeptDataModel DeptData { get; set; }

        public List<DeptDoctorModel> DeptDataList { get; set; }

        public string photoUrl { get; set; }

    }


    /// <summary>
    /// 醫師現職
    /// </summary>
    public partial class DoctorJobTitleModel
    {
        public string jobTitle { get; set; }
    }

    /// <summary>
    /// 醫師學歷
    /// </summary>
    public partial class DoctorEducationModel
    {
        public string education { get; set; }
    }

    /// <summary>
    /// 醫師經歷
    /// </summary>
    public partial class DoctorExperienceModel
    {
        public string experience { get; set; }
    }

    /// <summary>
    /// 醫師學會與認證
    /// </summary>
    public partial class DoctorCertificationModel
    {
        public string certification { get; set; }
    }

    /// <summary>
    /// 醫師論文及期刊發表
    /// </summary>
    public partial class DoctorResearchModel
    {
        public string research { get; set; }
        public string comefrom { get; set; }
        public string author { get; set; }
    }

    /// <summary>
    /// 醫師網路掛號
    /// </summary>
    public partial class DoctorRegisterModel
    {
        public string name { get; set; }
        public string param { get; set; }
        public string hospitalID { get; set; }
    }


    /// <summary>
    /// 提供社服活動清單
    /// </summary>
    public partial class SocialServiceListModel
    {
        public string activityDate { get; set; }
        public string activityTime { get; set; }
        public string hospitalName { get; set; }
        public string title { get; set; }
        public string place { get; set; }
        public string description { get; set; }
        public string contact { get; set; }
        public string memo { get; set; }
    }
    /// <summary>
    /// 會員登入檢查
    /// </summary>
    public partial class MemberLoginModel
    {
        public string isPass { get; set; }       
    }

    /// <summary>
    /// 網站會員資料
    /// </summary>
    public partial class MemberDataModel
    {
        public string account { get; set; }
        public string cName { get; set; }
        public string sex { get; set; }
        public string birthday { get; set; }
        public string tel { get; set; }
        public string address { get; set; }
        public List<ePaperModel> ePaper { get; set; }
    }
    /// <summary>
    /// 電子報
    /// </summary>
    public partial class ePaperModel
    {
        public string paperId { get; set; }
        public string paperName { get; set; }
        public string isOrder { get; set; }
    }

    /// <summary>
    /// 用藥諮詢清單
    /// </summary>
    public partial class DrugConsultListModel
    {
        public string postTime { get; set; }
        public string nickName { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public string replyName { get; set; }
        public string replyTime { get; set; }
        public string replyDescription { get; set; }
    }




    /// <summary>
    /// 提供職缺公告清單
    /// </summary>
    public partial class RecruitmentListModel
    {
        public string bulletinId { get; set; }
        public string startDate { get; set; }
        public string hospitalName { get; set; }
        public string bulletinTitle { get; set; }
    }

    /// <summary>
    /// 提供用藥指導單張清單
    /// </summary>
    public partial class DrugGuideListModel
    {
        public string medicNo { get; set; }
        public string title { get; set; }
        public string dataUrl { get; set; }
    }

    /// <summary>
    /// 長庚藥學學報
    /// </summary>
    public partial class DrugPaperListModel
    {
        public string volume { get; set; }
        public string dataUrl { get; set; }
    }

    /// <summary>
    /// 研討會列表
    /// </summary>
    public partial class SeminarListModel
    {
        public string TITLE { get; set; }
        public string TITLE_EN { get; set; }
        public string TITLEENGLISH { get; set; }
        public string TIME_HD_S { get; set; }
        public string TIME_UP_S { get; set; }
        public string TIME_UP_E { get; set; }
        public string HOSPTIAL { get; set; }
        public string HOSPTIAL_EN { get; set; }
        public string TIME_BM_S { get; set; }
        public string TIME_BM_E { get; set; }

        public string UNIT { get; set; }
    }

    /// <summary>
    /// 研討會內容
    /// </summary>
    public partial class SeminarModel
    {
        public string RMPE_CONF_SEMINARLISTID { get; set; }
        public string TITLE { get; set; }
        public string TITLE_EN { get; set; }
        public string ADDTIME { get; set; }
        public string INFO { get; set; }
        public string INFO_EN { get; set; }
        public string UPDATETIME { get; set; }
        public string ID { get; set; }
        public string TYPENAME { get; set; }
        public string PAIXU { get; set; }

    }


    /// <summary>
    /// 藥品查詢
    /// </summary>
    public partial class DrugQueryResultModel
    {
        public List<ChineseDrugModel> 中藥 { get; set; }
        public List<WesternDrugModel> 西藥 { get; set; }
    }


    /// <summary>
    /// 藥品查詢內容-中醫
    /// </summary>
    public partial class ChineseDrugModel
    {
        public string 藥品資訊 { get; set; }
        public string 圖示 { get; set; }
        public string 仿單 { get; set; }
        public string 藥品鑑別 { get; set; }
        public string 易混淆中藥材 { get; set; }
        public string 圖示錯誤訊息 { get; set; }     
    }

    /// <summary>
    /// 藥品查詢內容-西醫
    /// </summary>
    public partial class WesternDrugModel
    {
        public string 藥品資訊 { get; set; }
        public string 圖示 { get; set; }
        public string 仿單 { get; set; }
        public string 指導單張 { get; set; }
        public string 教學影片 { get; set; }
        public string 圖示錯誤訊息 { get; set; }
        public string 仿單錯誤訊息 { get; set; }
        public string 指導單張錯誤訊息 { get; set; }
        public string 教學影片錯誤訊息 { get; set; }
    }



    /// <summary>
    /// 衛教文章類別
    /// </summary>
    public partial class HealthEduListModel
    {
        public string 文章類別代號 { get; set; }
        public string 文章類別名稱 { get; set; }
        public string 文章編號 { get; set; }
        public string 文章名稱 { get; set; }
      
    }






    /// <summary>
    /// 取得衛教文章內容
    /// </summary>
    public partial class HealthEduContentModel
    {
        public string 文章名稱 { get; set; }
        public string 文章編號 { get; set; }
        public string 類別中文名稱 { get; set; }
        public string 閱讀次數 { get; set; }
        public string QRCODE掃描次數 { get; set; }
        public string 上檔年度 { get; set; }
        public string 建議看診科別 { get; set; }
        public string PDFPATH { get; set; }
        public string 閱讀網址 { get; set; }
        public string 下載網址 { get; set; }
        public string 列印網址 { get; set; }
        public string QRCODE圖片檔 { get; set; }
        public string QRCODE連結網址 { get; set; }
    }

    /// <summary>
    /// 取得衛教文章內容(搜尋)
    /// </summary>
    public partial class HealthEduContent4SearchModel
    {
        public string 文章編號 { get; set; }
        public string 文章類別名稱 { get; set; }
        public string 文章標題 { get; set; }
        public string 建議看診科別 { get; set; }
       
    }
    /// <summary>
    /// 會員註冊回傳
    /// </summary>
    public partial class SetMemberDataModel
    {
        public string activeCode { get; set; }


    }
}
