﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Web;

namespace Web.ServiceModels
{
    /// <summary>
    /// 醫院清單
    /// </summary>
    public partial class HospitalModel
    {

        public string Loc { get; set; }
        public string hospitaName { get; set; }
        public List<BranchModel> hospitaInfo { get; set; }
}

    /// <summary>
    /// 科別資料
    /// </summary>
    public partial class DeptModel
    {

        public string deptID { get; set; }

        public string deptName { get; set; }

        public string deptGroupID { get; set; }

        public string deptGroupName { get; set; }

    }

    /// <summary>
    /// 門診表看診週別
    /// </summary>
    public partial class SchedulePeriodModel
    {

        public string weekName { get; set; }

        public string startDate { get; set; }

        public string endDate { get; set; }


    }

    /// <summary>
    /// 門診資料
    /// </summary>
    public partial class OPDSchduleModel
    {

        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string weekDay { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string endDate { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorID { get; set; }
        public string subDoctorName { get; set; }
        public string isFull { get; set; }
        public string canReg { get; set; }
        public string memo { get; set; }
    }



    /// <summary>
    /// WS07 掛號查詢
    /// </summary>
    public partial class RegQueryModel
    {

        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorID { get; set; }
        public string subDoctorName { get; set; }
        public string regNumber { get; set; }
        public string memo { get; set; }
        public string calledNumber { get; set; }
        public string estiTime { get; set; }
        public string name { get; set; }
        public string kd { get; set; }


    }

    /// <summary>
    /// WS08 取消掛號
    /// </summary>
    public partial class DoRegCancelModel
    {

        public string processTime { get; set; }
        public string memo { get; set; }

    }


    /// <summary>
    /// WS11 病歷查詢
    /// </summary>
    public partial class PatQueryModel
    {

        public string patNumber { get; set; }
        public string idNumber { get; set; }
        public string idType { get; set; }
        public string name { get; set; }
        public string isFirst { get; set; }
        public string memo { get; set; }
      
    }

    /// <summary>
    /// WS05 復診掛號
    /// </summary>
    public partial class DoRegModel
    {
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorID { get; set; }
        public string subDoctorName { get; set; }
        public string regNumber { get; set; }
        public string estiTime { get; set; }
        public string name { get; set; }
        public string patNumber { get; set; }
        public string memo { get; set; }
    }

    /// <summary>
    /// WS06 初診掛號
    /// </summary>
    public partial class DoFirstRegModel
    {
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorID { get; set; }
        public string subDoctorName { get; set; }
        public string regNumber { get; set; }
        public string estiTime { get; set; }
        public string name { get; set; }
        public string patNumber { get; set; }
        public string memo { get; set; }
    }

    public partial class OPDProgressModel
    {
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdDate { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorName { get; set; }
        public string calledNumber { get; set; }
        public string waitCount { get; set; }
        public string regCount { get; set; }
        public string memo { get; set; }
    }

    public partial class OPDProgressPatientModel
    {
        public string deptID { get; set; }
        public string deptName { get; set; }
        public string opdTimeID { get; set; }
        public string roomID { get; set; }
        public string roomName { get; set; }
        public string roomLocation { get; set; }
        public string doctorID { get; set; }
        public string doctorName { get; set; }
        public string subDoctorName { get; set; }
        public string calledNumber { get; set; }
        public string waitCount { get; set; }
        public string regCount { get; set; }
        public string regNumber { get; set; }
        public string memo { get; set; }
    }



    public partial class PatContactInfoModel
    {
        public string patNumber { get; set; }
        public string email { get; set; }
        public string cell { get; set; }
        public string tel { get; set; }
        public string ofctel { get; set; }
        public string zip { get; set; }
        public string deptID { get; set; }
        public string city { get; set; }
        public string adr { get; set; }
    }


    public partial class SymptomQueryModel
    {

        public string loc { get; set; }
        public string absdat { get; set; }

        public string rgsdpt { get; set; }

        public string rgsdptnm { get; set; }
        public string shf { get; set; }
        public string drno { get; set; }
        public string drnonm { get; set; }
        public string opdvssqno { get; set; }


    }
    

    /// <summary>
    /// 取得院區重要訊息
    /// </summary>
    public partial class HospitalInfoModel
    {
        public string SEQ { get; set; }
        public string NM {
            get; set;
        }

    }

    /// <summary>
    /// 取得科別說明事項
    /// </summary>
    public partial class DeptInfoModel
    {
        public string SEQ { get; set; }
        public string CN { get; set; }//CN 科別 EX:胃腸肝膽科
        public string NM { get; set; }
    }


    /// <summary>
    /// 取得科別說明事項
    /// </summary>
    public partial class DPTModel
    {
        public string BIGDPTNO { get; set; }
        public string BIGDPT { get; set; }
    }

    /// <summary>
    /// 取得科別說明事項 - 細項
    /// </summary>
    public partial class DPTSubModel
    {
        public string DTLDPNO { get; set; }
        public string DTLDP { get; set; }
        public string XDESC { get; set; }

    }



    /// <summary>
    /// 查詢特殊處理科別資料
    /// </summary>
    public partial class DeptSpModel
    {
        public string loc { get; set; }
        public string dpt { get; set; }
        public string kd { get; set; }
        public string msg { get; set; }
    }



    /// <summary>
    /// 取得各類疾病相關就診科別參考之所有病症
    /// </summary>
    public partial class DoctorProfileModel
    {
        public string DSNO { get; set; }//症狀碼
        public string DSNM { get; set; }//症狀名
    }

    /// <summary>
    /// 取得各類疾病相關就診科別參考之某一病症之說明
    /// </summary>
    public partial class DoctorProfileSubModel
    {
        public string DSDNO { get; set; }//症狀細明碼
        public string DSDDESC { get; set; }//症狀明細描述

        public string DPTS { get; set; }//建議看診科別
    }

    /// <summary>
    /// 台灣縣市
    /// </summary>
    public partial class TaiwanModel
    {
        public string city { get; set; }
        public string en { get; set; }
        public string Weather { get; set; }
        public List<TaiwanAreaModel> area { get; set; }
    }



    public partial class TaiwanAreaModel
    {
        public string zip { get; set; }
        public string text { get; set; }
    }



    /// <summary>
    /// WS119 依據醫師代號查詢網路開診科別
    /// </summary>
    public partial class DoctorDepModel
    {
        public string LOC { get; set; }
        public string LOCNM { get; set; }
        public string DPID { get; set; }
        public string DPCNM { get; set; }
        public string DRNO { get; set; }
        public string rgsdpttcDRCNM { get; set; }
        public string hospitalName { get; set; }
    }

    /// <summary>
    /// 提供社服活動院區清單
    /// </summary>
    public partial class SocialServiceLocListModel
    {
        public string hospitalID { get; set; }
        public string hospitalName { get; set; }
    }
}
