"use strict";

$(document).ready(function () {
  var $window = $(window);
  var isSearchActive = false;
  var isMobile;
  var pageNow = $('main').attr('class').split(' ')[1];
  var menuItem;
  var btnScrollTop = $('.footer .arrow'); // page fadein when loaded

  $('.wp').addClass('active');

  if ($('.layout__urgent') && pageNow == 'index') {
    $('.layout__urgent').addClass('active');
  } //Detect if Edge broser


  if (/Edge\/\d./i.test(navigator.userAgent)) {
    // This is Microsoft Edge
    // window.alert('Microsoft Edge');
    $('main').addClass('edge');
  } //Detect page now and add active class to nav item


  menuPageNow(); // let mobile 100vh don't include browser top bar

  var vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', "".concat(vh, "px"));

  function menuPageNow() {
    if (pageNow) {
      menuItem = '.nav-' + pageNow;
      $(menuItem).addClass('active');
    }
  }

  checkWidth();
  $window.resize(checkWidth);
  slickControl();
  pluginInit();
  headerScrollToggle();

  function checkWidth() {
    var winWidth = $window.outerWidth();
    isMobile = winWidth <= 1024 ? true : false;
  }

  function pluginInit() {
    $('.imgLiquidFill').imgLiquid({
      fill: true,
      horizontalAlign: 'center',
      verticalAlign: 'center'
    });
  }

  function headerScrollToggle() {
    $(window).scroll(function () {
      if ($(window).scrollTop() > window.innerHeight / 2) {
        $(btnScrollTop).addClass('active');
      } else {
        $(btnScrollTop).removeClass('active');
      }
    });
  }

  function slickControl() {
    $('.slick-single').slick({
      infinite: false,
      lazyLoad: 'ondemand',
      draggable: false,
      swipe: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    });
    $('.slick-triple').slick({
      infinite: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      responsive: [{
        breakpoint: 1025,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 769,
        settings: {
          slidesToShow: 1
        }
      }]
    });
    $('.slick-four').slick({
      infinite: true,
      slidesToShow: 4,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      responsive: [{
        breakpoint: 1025,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 769,
        settings: {
          slidesToShow: 1
        }
      }]
    });
    $('.slick-block-switch').slick({
      infinite: false,
      arrows: false,
      draggable: false,
      swipe: false,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    });
    $('.slick-indexbg').slick({
      infinite: false,
      speed: 800,
      fade: true,
      autoplay: true,
      autoplaySpeed: 3000,
      dots: true,
      arrows: false,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      customPaging: function customPaging(slider, i) {
        var thumb = jQuery(slider.$slides[i]).data();
        return '<a>' + '0' + (i + 1) + '</a>';
      }
    });
    $('.slick-newslist').slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    });
    $('.slick-newsmain').slick({
      infinite: true,
      fade: true,
      dots: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    });
    $('.slick-hero').slick({
      infinite: true,
      fade: true,
      autoplay: true,
      autoplaySpeed: 5000,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      responsive: [{
        breakpoint: 1025,
        settings: {
          dots: true
        }
      }]
    });
    $('.slick-hero-dot').slick({
      infinite: true,
      fade: true,
      dots: true,
      autoplay: true,
      autoplaySpeed: 5000,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    }); //Slick To service-guide selection

    var url = window.location.href;
    var index = parseInt(url.split(/#/)[1]) || 0;
    var childNavSubclass = $('.slick-nav-subclass').children().length;
    $('.slick-nav-subclass').on('init', function () {
      // get index add class
      setTimeout(function () {
        $('.slick-nav-subclass .slick-slide').removeClass('slick-current');
        $('.slick-nav-subclass .slick-slide:eq(' + index + ')').addClass('slick-current'); // $('.slick-nav-subclass').slick('slickGoTo', index);
      }, 100);
    });
    $('.slick-target-subclass').on('init', function () {
      setTimeout(function () {
        $('.slick-target-subclass').slick('slickGoTo', index);
      }, 100);
    });
    $('.slick-nav-subclass').slick({
      infinite: false,
      slidesToShow: 6,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      variableWidth: true,
      asNavFor: '.slick-target-subclass',
      responsive: [{
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      }, {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }, {
        breakpoint: 541,
        settings: {
          variableWidth: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    });
    $('.slick-target-subclass').slick({
      infinite: false,
      arrows: false,
      draggable: false,
      swipe: false,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    });
    $('.slick-innovation-result').slick({
      infinite: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      responsive: [{
        breakpoint: 1025,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }, {
        breakpoint: 541,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    });
    $('.slick-nav-subclass .slick-slide').on('click', function (event) {
      $('.slick-nav-subclass .slick-slide').removeClass('slick-current');
      $(this).addClass('slick-current');
      $('.slick-target-subclass').slick('slickGoTo', $(this).data('slickIndex'));
    }); //----calender-----

    $('.slick-calender').slick({
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      // variableWidth: true,
      asNavFor: '.slick-calender-for'
    });
    $('.slick-calender-for').slick({
      infinite: false,
      arrows: false,
      draggable: false,
      swipe: false,
      // fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 1000,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    }); //----system-----

    $('.slick-system-area').slick({
      infinite: false,
      arrows: false,
      draggable: false,
      swipe: false,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    });
    $('.slick-area-contact').slick({
      infinite: false,
      arrows: false,
      draggable: false,
      swipe: false,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 500,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)'
    });
    var slickYear = $('.slick-year > li').length >= 7 ? 7 : $('.slick-year > li').length - 1;
    var slickYearLast = $('.slick-year > li').length - 1; //-----year slide----

    $('.slick-year').slick({
      infinite: true,
      centerMode: true,
      centerPadding: 0,
      initialSlide: slickYearLast,
      slidesToShow: slickYear,
      slidesToScroll: 1,
      arrows: false,
      speed: 600,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      asNavFor: '.slick-year-for',
      responsive: [{
        breakpoint: 1025,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5
        }
      }, {
        breakpoint: 769,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      }, {
        breakpoint: 541,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true
        }
      }]
    });
    $('.slick-year .slick-slide').on('click', function (event) {
      $(this).siblings().removeClass('slick-now');
      $(this).siblings().removeClass('slick-current');
      $(this).addClass('slick-current');
      $('.slick-year-for').slick('slickGoTo', $(this).data('slickIndex'));
    });
    $('.slick-year-for').slick({
      infinite: true,
      draggable: false,
      swipe: false,
      initialSlide: slickYearLast,
      slidesToShow: 1,
      slidesToScroll: 1,
      speed: 800,
      cssEase: 'cubic-bezier(0.25, 0.46, 0.45, 0.94)',
      asNavFor: '.slick-year',
      responsive: [{
        breakpoint: 541,
        settings: {
          arrows: false
        }
      }]
    });
  } // Click/hover  event toggle


  $('.js-menu-toggle').hover(function () {
    if (!isMobile) {
      $('.js-menu-toggle').removeClass('active');
      $(this).toggleClass('active');
    }
  }, function () {
    if (!isMobile) {
      $('.js-menu-toggle').removeClass('active');
    }
  });
  $('.js-menu-toggle').click(function () {
    if (isMobile) {
      // $('.js-menu-toggle').removeClass('active');
      $(this).toggleClass('active');
    }
  });
  $('.js-menu-toggle ~ .submenu').hover(function () {
    if (!isMobile) {
      $(this).siblings().toggleClass('active');
    }
  }, function () {
    if (!isMobile) {
      $(this).siblings().removeClass('active');
    }
  });
  $('.js-menu-close').click(function () {
    $(this).parent().prev('.js-menu-toggle').removeClass('active');
  });
  $('.js-urgent-close').click(function () {
    $('.layout__urgent').fadeOut(600);
  });
  $('.js-lang').click(function () {
    $(this).toggleClass('active');
  });
  $('.js-lang a').click(function (e) {
    if (!$('.js-lang').hasClass('active')) {
      e.preventDefault();
    }
  }); // $('.js-btn-search').click(function(e) {
  //   if (!isSearchActive) {
  //     e.preventDefault();
  //     $(this)
  //       .parent()
  //       .addClass('active');
  //     isSearchActive = true;
  //   }
  // });

  $('.layout__imggrid .img').click(function () {
    if (isMobile) {
      $(this).toggleClass('active');
    }
  });
  $('.menu__wrap-search').hover(function (e) {
    $(this).addClass('active');
  }, function (e) {
    $(this).removeClass('active');
  });
  $('.js-burgerlist').click(function () {
    $(this).toggleClass('active');
    $('.header__menu').toggleClass('active');
  });
  $('.js-language').click(function () {
    $(this).toggleClass('active');
  });
  var qa = $('.layout__grid-qa > li');
  var qaHeightOriginal = [];
  var qaHeadHeight = [];
  qa.each(function () {
    var totalHeight = $(this).outerHeight() + 20;
    var headHeight = $(this).find('.qa__head').outerHeight();
    qaHeightOriginal.push(totalHeight);
    qaHeadHeight.push(headHeight);
    $(this).css('maxHeight', headHeight);
    $(this).css('minHeight', headHeight);
  });
  $('.js-qa-toggle').each(function (i) {
    $(this).click(function () {
      var target = $(this).parents('li');
      target.toggleClass('active');

      if (target.hasClass('active')) {
        target.css('maxHeight', 'none');
        target.css('minHeight', qaHeightOriginal[i]);
      } else {
        target.css('maxHeight', qaHeadHeight[i]);
        target.css('minHeight', qaHeadHeight[i]);
      }
    });
  });
  $('.js-calender-activity').click(function () {
    $('.js-calender-info').addClass('active');
  });
  $('.submenu__title').click(function () {
    $(this).toggleClass('active');
  });
  $('.slick-block-remote .btn__rec').click(function () {
    var slideno = $(this).data('slide');
    $(this).toggleClass('active');
    $(this).siblings().removeClass('active');
    $('.slick-block-switch').slick('slickGoTo', slideno);
  });
  $('.area__intro .btn__rec-main').click(function () {
    var slideno = $(this).data('slide');
    $(this).toggleClass('active');
    $(this).siblings().removeClass('active');
    $('.slick-system-area').slick('slickGoTo', slideno);
  });
  $('.area__contact .btn__rec-main').click(function () {
    var slideno = $(this).data('slide');
    $(this).toggleClass('active');
    $(this).siblings().removeClass('active');
    $('.slick-area-contact').slick('slickGoTo', slideno);
  }); // $('.header__menu').scroll(function(e) {
  //   e.preventDefault();
  //   e.stopPropagation();
  // });
  // $(window).scroll(function(e) {
  //   if ($('.header__menu').hasClass('active')) {
  //     e.preventDefault();
  //     e.stopPropagation();
  //     return false;
  //   }
  // });
  //fontsize change

  var s = 59.5;
  var m = 62.5;
  var l = 66.5;
  $('.menu__fontchange li').click(function () {
    var val = $(this).data('size');
    $(this).addClass('active');
    $(this).siblings().removeClass('active');

    switch (val) {
      case 's':
        $('html').css({
          'font-size': s + '%'
        });
        break;

      case 'm':
        $('html').css({
          'font-size': m + '%'
        });
        break;

      case 'l':
        $('html').css({
          'font-size': l + '%'
        });
        break;

      default:
        $('html').css({
          'font-size': m + '%'
        });
        break;
    }
  }); //lightbox toggle

  $('.js-lightbox a').click(function () {
    if ($(this).attr('href') === undefined) {
      var imgSrc = $(this).find('img').attr('src');
      $('.layout__lightbox').find('img').attr('src', imgSrc);
      $('.layout__lightbox').fadeIn(500);
    }
  });
  $('.layout__lightbox .btn-close, .layout__lightbox .overlay').click(function () {
    $('.layout__lightbox').fadeOut(500);
  }); //Remove active class when lose focus

  $(document.body).click(function (e) {
    var target = e.target;

    if (!$(target).is('.js-lang') && !$(target).parents().is('.js-lang')) {
      $('.js-lang').removeClass('active');
    }
  }); //Sidenav arrow

  $(document).ready(function () {
    setTimeout(function(){
      $(".apo-talk-box").fadeOut(300)
    },10000);
  });
   
  $('.apo-icon').hover(function () {
    $('.apo-talk-box').fadeIn(300)
    },function () {
      setTimeout(function(){
        $(".apo-talk-box").fadeOut(300)
      },10000);
  });

  $('.js-btn-scrollTop').click(function () {
    $('html,body').animate({
      scrollTop: 0
    }, 'slow');
  });

  $('.js-btn-nav-arrow').click(function (e) {
    e.preventDefault();
    $('.sidenav').toggleClass('active');
  });

  function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE '); // If Internet Explorer
    // if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    //   $('select').css('background', '#fff');
    // }
    // else {
    // }

    return false;
  }
});