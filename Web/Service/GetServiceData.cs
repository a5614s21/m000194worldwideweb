﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.CgmhAppService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Web.Models;
using System.Text.RegularExpressions;
using System.IO;
using Web.ServiceModels;
using Web.Service;
using Web.OfficialWebSite;
using System.Configuration;
using System.Web.Caching;

namespace Web.Service
{
    public class GetServiceData : Controller
    {
        public RMS_WS AppServices = new RMS_WS();//宣告api             
        public v2 OfficeServices = new v2();//官網API
        public string comId = "24241872";
        protected System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;//更改較少資訊Cache使用
        protected int CacheSaveHour = int.Parse(ConfigurationManager.ConnectionStrings["CacheSaveHour"].ConnectionString);//Cache 儲存(小時)

        /// <summary>
        /// 回傳authKey
        /// </summary>
        /// <returns></returns>
        public string GetAuthKey()
        {
            JObject authKeyResponse = JObject.Parse(AppServices.WS100("IPL", "IPLcgmh"));
            string authKey = authKeyResponse["resultList"].ToString();
            return authKey;
        }

        /// <summary>
        /// WS01 醫院清單
        /// </summary>
        /// <returns></returns>
        public dynamic GetHospital(string language)
        {
            JObject Response = JObject.Parse(AppServices.WS01("zh-TW", GetAuthKey()));
            if(Response["isSuccess"].ToString() == "Y")
            {
                List<HospitalModel> resultList =  JsonConvert.DeserializeObject<List<HospitalModel>>(Response["resultList"].ToString());

                if(resultList.Count > 0)
                {
                    GetOfficeServiceData OfficeAppData = new GetOfficeServiceData();//官網API

                    int i = 0;
                    foreach(HospitalModel item in resultList)
                    {
                        Dictionary<string, string> callApiData = new Dictionary<string, string>();
                        callApiData.Add("language", language);
                        callApiData.Add("hospitalID", item.Loc);
                        var Models = OfficeAppData.GetBranch(callApiData);
                        if (Models != null && Models.GetType().Name != "JObject")
                        {
                            resultList[i].hospitaInfo = Models;                           
                        }
                        i++;
                    }

                }


                return resultList;
            }
            else
            {
                return Response;
            }            
        }

        /// <summary>
        /// WS02 取得各院科別
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="deptGrouptID"></param>
        /// <returns></returns>
        public dynamic GetDept(string hospitalID , string deptGrouptID , string types = "WS02_2")
        {
            JObject Response = new JObject();

            if(types == "WS02_2")
            {
                Response = JObject.Parse(AppServices.WS02_2(hospitalID, "", deptGrouptID, GetAuthKey()));
            }
            else
            {
                Response = JObject.Parse(AppServices.WS02(hospitalID, "", deptGrouptID, GetAuthKey()));
            }        


            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DeptModel> resultList = JsonConvert.DeserializeObject<List<DeptModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS03 取得門診表看診週別
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        public dynamic GetSchedulePeriod(string hospitalID)
        {
            JObject Response = JObject.Parse(AppServices.WS03(hospitalID, GetAuthKey()));
            if (Response["isSuccess"].ToString() == "Y")
            {
                List<SchedulePeriodModel> resultList = JsonConvert.DeserializeObject<List<SchedulePeriodModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS04 查詢門診資料
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="opdBeginDate"></param>
        /// <param name="opdEndDate"></param>
        /// <param name="doctorID"></param>
        /// <param name="deptID"></param>
        /// <returns></returns>
        public dynamic GetOPDSchdule(string hospitalID , string opdBeginDate, string opdEndDate, string doctorID, string deptID)
        {
             //JObject Response = JObject.Parse(AppServices.WS04(hospitalID ,"", opdBeginDate , opdEndDate , doctorID , deptID, GetAuthKey()));
             JObject Response = JObject.Parse(AppServices.WS04_2(hospitalID, "", opdBeginDate, opdEndDate, doctorID, deptID, GetAuthKey()));
            if (Response["isSuccess"].ToString() == "Y")
            {
                List<OPDSchduleModel> resultList = JsonConvert.DeserializeObject<List<OPDSchduleModel>>(Response["resultList"].ToString());                
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        /// <summary>
        /// 掛號查詢
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetRegQuery(Dictionary<string, string> data)
        {
            JObject Response = JObject.Parse(
                AppServices.WS07(
                    data["hospitalID"].ToString(), 
                    "", 
                    data["isFirst"].ToString(),
                    data["patNumber"].ToString(), 
                    data["idNumber"].ToString(),
                    data["idType"].ToString(), 
                    data["birthday"].ToString(), 
                    GetAuthKey())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<RegQueryModel> resultList = JsonConvert.DeserializeObject<List<RegQueryModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS08 取消掛號
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDoRegCancel(Dictionary<string, string> data)
        {
            JObject Response = JObject.Parse(
                AppServices.WS08(
                    data["hospitalID"].ToString(), 
                    "", 
                    data["patNumber"].ToString(),
                    data["idNumber"].ToString(), 
                    data["idType"].ToString(), 
                    data["birthday"].ToString(),
                    data["opdDate"].ToString(),
                    data["deptID"].ToString(),
                    data["opdTimeID"].ToString(),
                    data["doctorID"].ToString(),
                    data["roomID"].ToString(),               
                    GetAuthKey(),
                    data["ip"].ToString())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DoRegCancelModel> resultList = JsonConvert.DeserializeObject<List<DoRegCancelModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS09 取得診間看診進度
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetOPDProgress(Dictionary<string, string> data)
        {
            JObject Response = JObject.Parse(
                AppServices.WS09(
                    data["hospitalID"].ToString(),
                    "",                   
                    data["deptID"].ToString(),
                    data["opdTimeID"].ToString(),
                    GetAuthKey())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<OPDProgressModel> resultList = JsonConvert.DeserializeObject<List<OPDProgressModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// WS10 取得診間看診進度依病歷號
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetOPDProgress_Patient(Dictionary<string, string> data)
        {
            JObject Response = JObject.Parse(
                AppServices.WS10(
                    data["hospitalID"].ToString(),
                    "",
                    data["patNumber"].ToString(),
                    data["idNumber"].ToString(),
                    data["idType"].ToString(),
                    data["birthday"].ToString(),
                    GetAuthKey())
                    );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<OPDProgressPatientModel> resultList = JsonConvert.DeserializeObject<List<OPDProgressPatientModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }



        /// <summary>
        /// WS11 病歷查詢
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="patNumber"></param>
        /// <param name="idNumber"></param>
        /// <param name="idType"></param>
        /// <param name="birthday"></param>
        /// <returns></returns>
        public dynamic PatQuery(string hospitalID , string patNumber , string idNumber, string idType, string birthday)
        {
            JObject Response = JObject.Parse(AppServices.WS11(hospitalID, "",patNumber , idNumber , idType , birthday, GetAuthKey()));
            if (Response["isSuccess"].ToString() == "Y")
            {
                List<PatQueryModel> resultList = JsonConvert.DeserializeObject<List<PatQueryModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// WS05 復診掛號
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public dynamic DoReg(FormCollection form)
        {        
            JObject Response = JObject.Parse(AppServices.WS05(
                form["hospitalID"].ToString(),
                "",
                form["patNumber"].ToString(),
                form["idNumber"].ToString(),
                form["idType"].ToString(),
                form["birthday"].ToString().Replace("-", ""),//birthday
                "Y",
                form["opdDate"].ToString(),
                form["deptID"].ToString(),
                form["opdTimeID"].ToString(),
                form["doctorID"].ToString(),
                form["roomID"].ToString(),
                form["subDoctorID"].ToString(),
                GetAuthKey(),               
                 FunctionService.GetIP()
                ));
        

            return Response;
        }

        /// <summary>
        /// WS06 初診掛號
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public dynamic DoFirstReg(FormCollection form)
        {
          
            JObject Response = JObject.Parse(AppServices.WS06(
                form["hospitalID"].ToString(),
                "",
                "",
                form["idNumber"].ToString(),
                form["idType"].ToString(),
                form["birthday"].ToString().Replace("-",""),//birthday
                "Y",
                form["opdDate"].ToString(),
                form["deptID"].ToString(),
                form["opdTimeID"].ToString(),
                form["doctorID"].ToString(),
                form["roomID"].ToString(),
                form["subDoctorID"].ToString(),
                GetAuthKey(),
                form["name"].ToString(),
                "",
                "",
                "",
                "",
                form["telephone"].ToString(),
                form["cellphone"].ToString(),
                "",
                "",
                form["address"].ToString(),
                 FunctionService.GetIP()
                ));
           
            return Response;

        }


        /// <summary>
        /// WS34Q 依病歷號查詢出病患的聯繫資訊
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public dynamic getPatContactInfo(FormCollection form)
        {
            JObject Response = JObject.Parse(AppServices.WS34Q(
                "",
                "",
                form["patNumber"].ToString(),
                form["idNumber"].ToString(),
                form["idType"].ToString(),
                form["birthday"].ToString(),
                 GetAuthKey()
                ));
            if (Response["isSuccess"].ToString() == "Y")
            {
                List<PatContactInfoModel> resultList = JsonConvert.DeserializeObject<List<PatContactInfoModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }


        }


        /// <summary>
        /// 三個月未到診紀錄
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic getSymptomQuery(Dictionary<string, string> data)
        {

            JObject Response = JObject.Parse(
               AppServices.WS89(
                   data["hospitalID"].ToString(),                   
                   data["chtnoID"].ToString(),            
                   GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<SymptomQueryModel> resultList = JsonConvert.DeserializeObject<List<SymptomQueryModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        /// <summary>
        /// 取得院區重要訊息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetHospitalInfo(string locID)
        {

            var json = (
                   AppServices.WS97(
                   locID,
                   GetAuthKey())
                   );

            //json = Regex.Replace(json, @"<(.|\n)*?>", "");           
            JObject Response = JObject.Parse(json);

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<HospitalInfoModel> resultList = JsonConvert.DeserializeObject<List<HospitalInfoModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// 取得科別說明事項
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDeptInfo(string locID , string dptID)
        {

            var json = (
               AppServices.WS98(
                   locID,
                   dptID,
                   GetAuthKey())
                   );


           // json = Regex.Replace(json, @"<(.|\n)*?>", "");
            JObject Response = JObject.Parse(json);

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DeptInfoModel> resultList = JsonConvert.DeserializeObject<List<DeptInfoModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// 取得各科門診看診病症所有科別(我要看哪一科)
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        public dynamic GetDPT(string hospitalID)
        {

            JObject Response = JObject.Parse(AppServices.WS76(
                   hospitalID,
                   "",
                   "",
                   GetAuthKey()));

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DPTModel> resultList = JsonConvert.DeserializeObject<List<DPTModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        /// <summary>
        /// 取得各科門診看診病症所有科別(我要看哪一科)-細項
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="bigDptNo"></param>
        /// <returns></returns>
        public dynamic GetSubDPT(string hospitalID , string bigDptNo)
        {

            JObject Response = JObject.Parse(AppServices.WS77(
                   hospitalID,
                   "",
                   bigDptNo,
                   GetAuthKey()));

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DPTSubModel> resultList = JsonConvert.DeserializeObject<List<DPTSubModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }


        /// <summary>
        /// 查詢特殊處理科別資料
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        public dynamic GetDeptSp(string locID , string dptID)
        {

            JObject Response = JObject.Parse(
               AppServices.WS92(
                   locID,
                   dptID,
                   GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DeptSpModel> resultList = JsonConvert.DeserializeObject<List<DeptSpModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }
        /// <summary>
        /// 取得各類疾病相關就診科別參考之所有病症
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <returns></returns>
        public dynamic GetDoctorProfile(string hospitalID)
        {

            JObject Response = JObject.Parse(
               AppServices.WS78(
                   hospitalID,
                   "",
                   "",
                   GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DoctorProfileModel> resultList = JsonConvert.DeserializeObject<List<DoctorProfileModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// 取得各類疾病相關就診科別參考之某一病症之說明
        /// </summary>
        /// <param name="hospitalID"></param>
        /// <param name="dsNo"></param>
        /// <returns></returns>
        public dynamic GetDoctorSubProfile(string hospitalID , string dsNo)
        {

            JObject Response = JObject.Parse(
               AppServices.WS79(
                   hospitalID,
                   "",
                   dsNo,
                   GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<DoctorProfileSubModel> resultList = JsonConvert.DeserializeObject<List<DoctorProfileSubModel>>(Response["resultList"].ToString());
                return resultList;
            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// WS119 依據醫師代號查詢網路開診科別
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDoctorDep(Dictionary<string, string> data , List<hospitals> Hospitals)
        {

            string CacheName = "GetDoctorDep" + GetCacheName(data);

            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {

                    JObject Response = JObject.Parse(
                 AppServices.WS119(
                     data["hospitalID"].ToString(),
                     data["language"].ToString(),
                     data["drid"].ToString(),
                     GetAuthKey())
                     );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DoctorDepModel> resultList = JsonConvert.DeserializeObject<List<DoctorDepModel>>(Response["resultList"].ToString());
                        //  
                        //取醫院名
                        if(resultList.Count > 0)
                        {
                            int i = 0;
                            foreach(DoctorDepModel item in resultList)
                            {
                                var hospital = Hospitals.Find(m => m.hospitalID == item.LOC);
                                if(hospital != null)
                                {
                                    resultList[i].hospitalName = hospital.title;
                                }                              

                                i++;
                            }
                        }
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);

                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 組合cache名稱
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetCacheName(Dictionary<string, string> data)
        {
            string re = "";
            foreach (KeyValuePair<string, string> item in data)
            {
                re += item.Value;
            }
            return re;

        }
    }
}