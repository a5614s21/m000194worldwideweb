﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;

namespace Web.Service
{
    public class TablesService : Controller
    {


        // GET: TablesService


        /// <summary>
        /// 取得列表資料提供給DataTable
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> getListData(string tables , NameValueCollection requests , string urlRoot)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> list = new Dictionary<String, Object>();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            int skip = int.Parse(requests["iDisplayStart"].ToString());
            int take = int.Parse(requests["iDisplayLength"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if(requests["sSearch"].ToString() != "")
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["sSearch"].ToString());
                sSearch = sSearch[0];
            }

            string orderByKey = requests["mDataProp_" + requests["iSortCol_0"]].ToString();
            string orderByType = requests["sSortDir_0"].ToString();
            string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            if(tables == "historys_data" && orderByKey == "category")
            {
                orderBy = "category desc ,day " + orderByType.ToUpper();
            }

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {

                //滿意度調查結果細項
                case "quality_result_data":
                    if (model != null)
                    {
                        var data = model.Repository<quality_result_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;


                //滿意度調查結果
                case "quality_result":
                    if (model != null)
                    {
                        var data = model.Repository<quality_result>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //特色醫療中心
                case "cm_list":
                    if (model != null)
                    {
                        var data = model.Repository<cm_list>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //服務概況細項
                case "about_overview_data":
                    if (model != null)
                    {
                        var data = model.Repository<about_overview_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //附屬網站&友善連結
                case "subsidiary_web":
                    if (model != null)
                    {
                        var data = model.Repository<subsidiary_web>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }
                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //員工專區
                case "staff_area":
                    if (model != null)
                    {
                        var data = model.Repository<staff_area>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //職缺院區資訊
                case "recruits_hospitals":
                    if (model != null)
                    {
                        var data = model.Repository<recruits_hospitals>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //服務概況
                case "about_overview":
                    if (model != null)
                    {
                        var data = model.Repository<about_overview>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;


                //各入口頁面資訊管理-其他內容
                case "index_info_data":
                    if (model != null)
                    {
                        var data = model.Repository<index_info_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //各入口頁面資訊管理
                case "index_info":
                    if (model != null)
                    {
                        var data = model.Repository<index_info>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;



                //醫療諮詢
                case "hospitals_advisory":
                    if (model != null)
                    {
                        var data = model.Repository<hospitals_advisory>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category == category);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //社會服務
                case "social_service":
                    if (model != null)
                    {
                        var data = model.Repository<social_service>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;


                //公益活動
                case "public_welfare":
                    if (model != null)
                    {
                        var data = model.Repository<public_welfare>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //申請與查詢
                case "app_inquiry":
                    if (model != null)
                    {
                        var data = model.Repository<app_inquiry>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category == category);
                            }
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //各院區醫教業務洽詢單位
                case "edu_service":
                    if (model != null)
                    {
                        var data = model.Repository<edu_service>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //教學資源單位
                case "edu_resource_data":
                    if (model != null)
                    {
                        var data = model.Repository<edu_resource_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //教學資源
                case "edu_resource":
                    if (model != null)
                    {
                        var data = model.Repository<edu_resource>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //教育培訓
                case "edu_train":
                    if (model != null)
                    {
                        var data = model.Repository<edu_train>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //研究資源相關連結
                case "res_resource_data":
                    if (model != null)
                    {
                        var data = model.Repository<res_resource_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //研究資源
                case "res_resource":
                    if (model != null)
                    {
                        var data = model.Repository<res_resource>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //研究成果
                case "res_results":
                    if (model != null)
                    {
                        var data = model.Repository<res_results>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;






                //人才招募-薪資與福利
                case "recruits_data":
                    if (model != null)
                    {
                        var data = model.Repository<recruits_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //人才招募
                case "recruits":
                    if (model != null)
                    {
                        var data = model.Repository<recruits>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //大事記-月日
                case "historys_data":
                    if (model != null)
                    {
                        var data = model.Repository<historys_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //大事記-年
                case "historys":
                    if (model != null)
                    {
                        var data = model.Repository<historys>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.year.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //關於長庚
                case "abouts":
                    if (model != null)
                    {
                        var data = model.Repository<abouts>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.content.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //常見問題-細項
                case "qas_data":
                    if (model != null)
                    {
                        var data = model.Repository<qas_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.content.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //常見問題
                case "qas":
                    if (model != null)
                    {
                        var data = model.Repository<qas>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.content.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //院區資訊
                case "hospitals":
                    if (model != null)
                    {
                        var data = model.Repository<hospitals>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.profile.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                            if (sSearch["category"] != null && sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }

                        }




                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;


                //院區分類
                case "hospital_category":
                    if (model != null)
                    {
                        var data = model.Repository<hospital_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }
                        

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //就醫指南(2)-細項
                case "medical_other_data":
                    if (model != null)
                    {
                        var data = model.Repository<medical_other_data>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.url.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //就醫指南(2)
                case "medical_other":
                    if (model != null)
                    {
                        var data = model.Repository<medical_other>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.content.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;
                //就醫指南(1)
                case "medical_guide":
                    if (model != null)
                    {
                        var data = model.Repository<medical_guide>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m=>m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.content.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }

                        if (requests["category"] != null && requests["category"].ToString() != "")
                        {
                            string category = requests["category"].ToString();
                            models = models.Where(m => m.category.Contains(category));
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;


                //聯絡我們
                case "contacts":
                    if (model != null)
                    {
                        var data = model.Repository<contacts>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                        }



                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;



           


                //最新消息分類
                case "news_category":
                    if (model != null)
                    {
                        var data = model.Repository<news_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }


                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //最新消息
                case "news":
                    if (model != null)
                    {
                        var data = model.Repository<news>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }


                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //群組管理
                case "roles":
                    if (model != null)
                    {
                        var data = model.Repository<roles>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }


                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //使用者
                case "user":
                    if (model != null)
                    {
                        var data = model.Repository<user>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m=>m.username != "sysadmin");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.username.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.role_guid.Contains(category));
                            }
                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;


                //防火牆
                case "firewalls":
                    if (model != null)
                    {
                        var data = model.Repository<firewalls>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }


                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

                //系統日誌
                case "system_log":
                    if (model != null)
                    {
                        var data = model.Repository<system_log>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.ip.Contains(keywords) || m.notes.Contains(keywords));
                            }
                            /*if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }*/
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;


                //資源回收桶
                case "ashcan":
                    if (model != null)
                    {
                        var data = model.Repository<ashcan>();

                        var models = data.ReadsWhere(m => m.from_guid != "");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                           


                        }


                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);

                    }
                    break;

            }



            list.Add("data", dataTableListData(listData, tables , urlRoot , requests));
            list.Add("iTotalDisplayRecords", iTotalDisplayRecords);
         

            return list;
        }


        /// <summary>
        /// 調整DataTable資料輸出
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static List<Object> dataTableListData(dynamic list, string tables, string urlRoot, NameValueCollection requests)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            Model DB = new Model();

            List<Object> re = new List<Object>();
        
            Dictionary<String, Object> dataTableRow = dataTableTitle(tables);//資料欄位

            string json = JsonConvert.SerializeObject(list, Formatting.Indented);//轉型(model to Json String)

            var Json1 = Newtonsoft.Json.Linq.JArray.Parse(json);//第一層列表轉json陣列
            string picurlRoot = urlRoot;
            if (urlRoot == "/")
            {
                urlRoot = "";
             
            }


            //取得選單一些基本資訊

            IQueryable<system_menu> system_menu_search = DB.system_menu.Where(m => m.tables == tables);
            if (requests["category"] != null && requests["category"].ToString() != "")
            {
                string category = requests["category"].ToString();
                string act_path = "list/" + category;
                system_menu_search = system_menu_search.Where(m => m.act_path == act_path);
            }

            var system_menu = system_menu_search.FirstOrDefault();

            if(system_menu == null)
            {
                system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();
            }

            int i = 0;
            foreach(var dataList in Json1)
            {

                Dictionary<String, Object> rowData = new Dictionary<string, object>();

                string thisGuid = dataList["guid"].ToString();


                rowData.Add("DT_RowId", "row_"+ thisGuid);//欄位ID

                foreach (KeyValuePair<string, object> dataItem in dataTableRow)
                {

                  

                    switch (dataItem.Key.ToString())
                    {
                        #region 鍵值
                        case "guid":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/checkbox.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$title}", tableTitleKey(tables , dataList));
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion

                        #region 圖片
                        case "pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);                             

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}","");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion
                        #region 圖片
                        case "icon":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "style=\"background-color:#333;\"");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion

                        #region 圖片
                        case "list_pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion
                        #region 內容摘要
                        case "info":

                            if (thisGuid != null && thisGuid != "")
                            {
                                if(tables != "shareholders")
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/info.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);





                                    readText = readText.Replace("{$value}", dataList["title"].ToString());

                                    //置頂
                                    string sticky = "";
                                    if (dataList["sticky"] != null && dataList["sticky"].ToString() != "")
                                    {
                                        if (dataList["sticky"].ToString() == "Y")
                                        {
                                            sticky = "<span class=\"badge badge-pill badge-warning ml-2\">置頂</span>";
                                        }

                                    }
                                    readText = readText.Replace("{$sticky}", sticky);

                                    //今日異動
                                    string modifydate = "";
                                    if (dataList["modifydate"] != null && dataList["modifydate"].ToString() != "")
                                    {
                                        string date = DateTime.Parse(dataList["modifydate"].ToString()).ToString("yyyy-MM-dd");
                                        if (date == DateTime.Now.ToString("yyyy-MM-dd"))
                                        {
                                            modifydate = "<span class=\"badge badge-pill badge-info ml-2\">今日異動</span>";
                                        }

                                    }
                                    readText = readText.Replace("{$modifydate}", modifydate);


                                    //簡述
                                    string content = "";

                                    if (dataList["content"] != null && dataList["content"].ToString() != "")
                                    {

                                        content = Regex.Replace(dataList["content"].ToString(), "<.*?>", String.Empty);
                                        content = content.Replace(System.Environment.NewLine, "");
                                        if (content.Length > 50)
                                        {
                                            content = content.Substring(0, 50) + "...";
                                        }
                                        content = "<p class=\"m-0 p-0 text-pre-line\"><small>" + content + "</small></p>";


                                    }
                                    readText = readText.Replace("{$content}", content);

                                    string notes = "";//

                                    readText = readText.Replace("{$notes}", notes);


                                    //連結
                                    string addId = "";
                                    //dataTableCategory
                                    if (context.Session["dataTableCategory"] != null)
                                    {
                                        addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                    }
                                    readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {

                                    string readText = "<div>" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                               
                            }
                            break;
                        #endregion


                        #region 分類或其他上層對應
                        case "category":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                              if (tables == "news")
                                {
                                    string categoryID = dataList["category"].ToString();
                                    var category = DB.news_category.Where(m => m.guid == categoryID).FirstOrDefault();
                                    readText = category.title;
                                }
                                if (tables == "hospitals")
                                {
                                    string categoryID = dataList["category"].ToString();
                                    var category = DB.hospital_category.Where(m => m.guid == categoryID).FirstOrDefault();
                                    readText = category.title;
                                }
                                if (tables == "historys_data")
                                {
                                    string categoryID = dataList["category"].ToString();
                                    var category = DB.historys.Where(m => m.guid == categoryID).FirstOrDefault();
                                    readText = category.year + "." + dataList["day"].ToString();
                                }
                                if (tables == "app_inquiry")
                                {
                                    string categoryID = dataList["category"].ToString();
                                    var category = DB.app_inquiry_category.Where(m => m.guid == categoryID).FirstOrDefault();
                                    readText = category.title;
                                }
                                if (tables == "hospitals_advisory")
                                {
                                    string categoryID = dataList["category"].ToString();
                                    var category = DB.hospitals.Where(m => m.guid == categoryID).FirstOrDefault();
                                    readText = category.title;
                                }
                                if (tables == "quality_result")
                                {
                                    string categoryID = dataList["category"].ToString();
                                    var category = DB.hospitals.Where(m => m.guid == categoryID).FirstOrDefault();
                                    readText = category.title;
                                }
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion





                        #region 複選分類
                        case "pluralCategory":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                if (tables == "user")
                                {
                                    string[] categoryArr = dataList["role_guid"].ToString().Split(',');
                                    for(int s=0;s< categoryArr.Length;s++)
                                    {
                                        string categoryID = categoryArr[s].ToString();
                                        var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                        if(category != null)
                                        {
                                            readText += category.title + "<br>";
                                        }
                                       
                                    }
                                  
                                   
                                }
                               
                                readText = "<div>" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion

                        #region 審核者
                        case "user":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                               
                                    if (dataList["user_guid"] != null && dataList["user_guid"].ToString() != "")
                                    {
                                        string categoryID = dataList["user_guid"].ToString();
                                        var category = DB.user.Where(m => m.guid == categoryID).FirstOrDefault();
                                        if (category != null)
                                        {
                                            readText = category.name;
                                        }
                                        else
                                        {
                                            readText = "查無審核者!";
                                        }

                                    }
                                    else
                                    {
                                        readText = "未指定";

                                    }

                                
                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion

                        #region 瀏覽次數
                        case "view":

                          /*  if (thisGuid != null && thisGuid != "")
                            {
                                int record_log_qty = DB.record_log.Where(m => m.from == tables).Where(m => m.from_guid == thisGuid).Where(m => m.types == "view").Count();
                                string readText = "<div class=\"text-center\">"+ record_log_qty.ToString() + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }*/
                            break;
                        #endregion



                        #region 日期
                        case "date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;
                        #endregion

                        #region 發布日期
                        case "startdate":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if(dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;
                        #endregion

                        #region 發布日期2
                        case "create_date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;
                        #endregion

                        #region 狀態
                        case "status":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "啟用";
                                string status = dataList[dataItem.Key.ToString()].ToString();

                                if(status == "N")
                                {
                                     style = "secondary";
                                     statusSubject = "停用";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);
                                readText = readText.Replace("{$status}", status);
                                readText = readText.Replace("{$guid}", thisGuid);
                                if(tables == "user" || tables == "project_users")
                                {
                                    readText = readText.Replace("{$title}", dataList["name"].ToString());
                                }
                                else if (tables == "historys" )
                                {
                                    readText = readText.Replace("{$title}", dataList["year"].ToString());
                                }
                                else
                                {
                                    readText = readText.Replace("{$title}", dataList["title"].ToString());
                                }
                              

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;
                        #endregion


                        #region 討論區狀態
                        case "forum_status":

                            if (dataList["status"] != null && dataList["status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "待分派";
                                string status = dataList["status"].ToString();

                                if (status == "disable")
                                {
                                    style = "secondary";
                                    statusSubject = "停用";
                                }
                                if (status == "stagnate")
                                {
                                    style = "dark";
                                    statusSubject = "後續評估";
                                }
                                if (status == "success")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }
                                if (status == "warning")
                                {
                                    style = "warning";
                                    statusSubject = "處理中";
                                }
                                if (status == "end")
                                {
                                    style = "dark";
                                    statusSubject = "已解決";
                                }
                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);
                           

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;
                        #endregion

                        #region 回覆狀態
                        case "re_status":

                            if (dataList["re_status"] != null && dataList["re_status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "尚未回覆";
                                string status = dataList["re_status"].ToString();

                           
                                if (status == "Y")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }
                               
                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);


                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "");
                            }
                            break;
                        #endregion

                        #region 角色
                        case "role":

                            if (dataList["role"] != null && dataList["role"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "primary";
                                string statusSubject = "回覆者";
                                string status = dataList["role"].ToString();


                                if (status == "Tongren")
                                {
                                    style = "secondary";
                                    statusSubject = "同仁";
                                }
                                if (status == "Asker")
                                {
                                    style = "danger";
                                    statusSubject = "提問者";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);


                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;
                        #endregion

                        #region 屬性
                        case "type":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string readText = "";
                                if (tables == "holiday")
                                {
                                    string type = "休假";
                                    if (dataList[dataItem.Key.ToString()].ToString() == "duty")
                                    {
                                        type = "補上班";
                                    }
                                    readText = type;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);

                            }

                            break;
                        #endregion

                        #region 排序
                        case "sortIndex":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/sortIndex.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$guid}", thisGuid);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion

                        #region 成員數
                        case "user_qty":
                            if (dataList["guid"] != null)
                            {
                                string role_guid = dataList["guid"].ToString();
                                var data = DB.user.Where(m => m.role_guid.Contains(role_guid)).Where(m=>m.username != "sysadmin").Count();
                                string readText = "<div>"+ data + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion


                        #region 來源資料表名稱
                        case "tables":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string fromTable = dataList[dataItem.Key.ToString()].ToString();
                                var data = DB.system_menu.Where(m => m.tables == fromTable).Select(a => new { title = a.title }).FirstOrDefault();
                                rowData.Add(dataItem.Key.ToString(), data.title);
                            }
                            break;
                        #endregion

                        #region 動作
                        case "action":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/action.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$guid}", thisGuid);

                               //是否修改
                               if(system_menu.can_edit == "Y")
                                {
                                    readText = readText.Replace("{$editHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$editHide}", "style=\"display:none;\"");
                                }

                                //是否可刪除
                                dynamic permissions = context.Session["permissions"];//取得權限紀錄
                                if (system_menu.can_del == "Y" && permissions.ContainsKey(system_menu.guid) &&  permissions[system_menu.guid] == "F")
                                {
                                    readText = readText.Replace("{$delHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$delHide}", "style=\"display:none;\"");
                                }

                                string addId = "";
                                //dataTableCategory
                                if (context.Session["dataTableCategory"] != null)
                                {
                                    addId = "?c="+ context.Session["dataTableCategory"].ToString();
                                }

                                //連結
                                readText = readText.Replace("{$editUrl}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);
                               
                                if(system_menu.index_view_url != null && system_menu.index_view_url.ToString() != "")
                                {                                     
                                     readText = readText.Replace("{$index_view_url}", "<a href=\""+urlRoot+ system_menu.index_view_url.ToString() + thisGuid + "\" target=\"_blank\" class=\"btn btn-outline-default btn-sm\" role=\"button\" aria-pressed=\"true\" title=\"瀏覽前端\"><i class=\"icon-eye3\"></i><span class=\"text-hide\">瀏覽</span></a>");
                                }
                                else
                                {
                                    readText = readText.Replace("{$index_view_url}", "");
                                }

                                readText = readText.Replace("{$title}",tableTitleKey(tables , dataList));

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;
                        #endregion


                        #region 按鈕
                        case "button":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/button.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                switch(tables)
                                {
                                    case "medical_other":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/medical_other_data/list/" + thisGuid);
                                        break;
                                    case "qas":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/qas_data/list/" + thisGuid);
                                        break;
                                    case "historys":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/historys_data/list/" + thisGuid);
                                        break;
                                    case "res_resource":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/res_resource_data/list/" + thisGuid);
                                        break;

                                    case "hospitals":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/hospitals_advisory/list/" + thisGuid);
                                        break;

                                  case "recruits":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/recruits_data/list/" + thisGuid);
                                        break;
                                    case "about_overview":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/about_overview_data/list/" + thisGuid);
                                        break;
                                    case "edu_resource":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/edu_resource_data/list/" + thisGuid);
                                        break;
                                    case "index_info":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/index_info_data/list/" + thisGuid);
                                        break;
                                    case "quality_result":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/quality_result_data/list/" + thisGuid);
                                        break;
                                }


                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion

                        #region 按鈕2
                        case "button2":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/button.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                switch (tables)
                                {
                                   
                                    case "recruits":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/recruits_hospitals/list/" + thisGuid);
                                        break;
                                }


                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                        #endregion

                        default:
                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string readText = "<div >" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                    }

                }

                re.Add(rowData);

            }



            return re;

        }


        /// <summary>
        /// 是否使用語系
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string useLang(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //滿意度調查結果細項
                case "quality_result_data":
                    re = qualityResultDataRepository.useLang();
                    break;
                //滿意度調查結果
                case "quality_result":
                    re = qualityResultRepository.useLang();
                    break;
                //特色醫療中心
                case "cm_list":
                    re = cmListRepository.useLang();
                    break;
                //服務概況細項
                case "about_overview_data":
                    re = aboutOverviewDataRepository.useLang();
                    break;
                //附屬網站&友善連結
                case "subsidiary_web":
                    re = subsidiaryWebRepository.useLang();
                    break;

                //員工專區
                case "staff_area":
                    re = staffAreaRepository.useLang();
                    break;
                //職缺院區資訊
                case "recruits_hospitals":
                    re = recruitsHospitalsDataRepository.useLang();
                    break;
                //服務概況
                case "about_overview":
                    re = aboutOverviewRepository.useLang();
                    break;

                //各入口頁面資訊管理-其他內容
                case "index_info_data":
                    re = indexInfoDataRepository.useLang();
                    break;
                //各入口頁面資訊管理
                case "index_info":
                    re = indexInfoRepository.useLang();
                    break;

                //醫療諮詢
                case "hospitals_advisory":
                    re = hospitalsAdvisoryRepository.useLang();
                    break;

                //社會服務
                case "social_service":
                    re = socialServiceRepository.useLang();
                    break;


                //公益活動
                case "public_welfare":
                    re = publicWelfareRepository.useLang();
                    break;
                //申請與查詢
                case "app_inquiry":
                    re = appInquiryRepository.useLang();
                    break;

                //各院區醫教業務洽詢單位
                case "edu_service":
                    re = eduServiceRepository.useLang();
                    break;

                //教學資源單位
                case "edu_resource_data":
                    re = eduResourceDataRepository.useLang();
                    break;
                //教學資源
                case "edu_resource":
                    re = eduResourceRepository.useLang();
                    break;

                //教育培訓
                case "edu_train":
                    re = eduTrainRepository.useLang();
                    break;

                //研究資源相關連結
                case "res_resource_data":
                    re = resResourceDataRepository.useLang();
                    break;
                //研究資源
                case "res_resource":
                    re = resResourceRepository.useLang();
                    break;

                //研究成果
                case "res_results":
                    re = resResultsRepository.useLang();
                    break;



                //人才招募-薪資與福利
                case "recruits_data":
                    re = recruitsDataRepository.useLang();
                    break;
                //人才招募
                case "recruits":
                    re = recruitsRepository.useLang();
                    break;


                //大事記-月日
                case "historys_data":
                    re = historysDataRepository.useLang();
                    break;
                //大事記-年
                case "historys":
                    re = historysRepository.useLang();
                    break;
                //關於長庚
                case "abouts":
                    re = aboutsRepository.useLang();
                    break;

                //常見問題-細項
                case "qas_data":
                    re = qasDataRepository.useLang();
                    break;
                //常見問題
                case "qas":
                    re = qasRepository.useLang();
                    break;


                //院區資訊
                case "hospitals":
                    re = hospitalsRepository.useLang();
                    break;
                //院區分類
                case "hospital_category":
                    re = hospitalCategoryRepository.useLang();
                    break;
                //其他資訊
                case "notes_data":
                    re =notesDataRepository.useLang();
                    break;

                //就醫指南(2)-細項
                case "medical_other_data":
                    re = medicalOtherDataRepository.useLang();
                    break;
                //就醫指南(2)
                case "medical_other":
                    re = medicalOtherRepository.useLang();
                    break;
                //就醫指南(1)
                case "medical_guide":
                    re = medicalGuideRepository.useLang();
                    break;

                //系統日誌
                case "system_log":
                    re = systemLogRepository.useLang();
                    break;

                //防火牆
                case "firewalls":
                    re = firewallsRepository.useLang();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.useLang();
                    break;


                //最新消息分類
                case "news_category":
                    re = newsCategoryRepository.useLang();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.useLang();
                    break;
                //使用者
                case "user":
                    re = userRepository.useLang();
                    break;


                //網站基本資料
                case "web_data":
                    re = webDataRepository.useLang();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.useLang();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.useLang();
                    break;

            }


            return re;
        }


        /// <summary>
        /// 取得DataTable欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic dataTableTitle(string tables)
        {
          
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //滿意度調查結果細項
                case "quality_result_data":
                    re = qualityResultDataRepository.dataTableTitle();
                    break;
                //滿意度調查結果
                case "quality_result":
                    re = qualityResultRepository.dataTableTitle();
                    break;
                //特色醫療中心
                case "cm_list":
                    re = cmListRepository.dataTableTitle();
                    break;
                //服務概況細項
                case "about_overview_data":
                    re = aboutOverviewDataRepository.dataTableTitle();
                    break;
                //附屬網站&友善連結
                case "subsidiary_web":
                    re = subsidiaryWebRepository.dataTableTitle();
                    break;

                //員工專區
                case "staff_area":
                    re = staffAreaRepository.dataTableTitle();
                    break;
                //職缺院區資訊
                case "recruits_hospitals":
                    re = recruitsHospitalsDataRepository.dataTableTitle();
                    break;
                //服務概況
                case "about_overview":
                    re = aboutOverviewRepository.dataTableTitle();
                    break;
                //各入口頁面資訊管理-其他內容
                case "index_info_data":
                    re = indexInfoDataRepository.dataTableTitle();
                    break;
                //各入口頁面資訊管理
                case "index_info":
                    re = indexInfoRepository.dataTableTitle();
                    break;
                //醫療諮詢
                case "hospitals_advisory":
                    re = hospitalsAdvisoryRepository.dataTableTitle();
                    break;
                //社會服務
                case "social_service":
                    re = socialServiceRepository.dataTableTitle();
                    break;
                //公益活動
                case "public_welfare":
                    re = publicWelfareRepository.dataTableTitle();
                    break;
                //申請與查詢
                case "app_inquiry":
                    re = appInquiryRepository.dataTableTitle();
                    break;

                //各院區醫教業務洽詢單位
                case "edu_service":
                    re = eduServiceRepository.dataTableTitle();
                    break;

                //教學資源單位
                case "edu_resource_data":
                    re = eduResourceDataRepository.dataTableTitle();
                    break;
                //教學資源
                case "edu_resource":
                    re = eduResourceRepository.dataTableTitle();
                    break;

                //教育培訓
                case "edu_train":
                    re = eduTrainRepository.dataTableTitle();
                    break;

                //研究資源相關連結
                case "res_resource_data":
                    re = resResourceDataRepository.dataTableTitle();
                    break;
                //研究資源
                case "res_resource":
                    re = resResourceRepository.dataTableTitle();
                    break;

                //研究成果
                case "res_results":
                    re = resResultsRepository.dataTableTitle();
                    break;


                //人才招募-薪資與福利
                case "recruits_data":
                    re = recruitsDataRepository.dataTableTitle();
                    break;
                //人才招募
                case "recruits":
                    re = recruitsRepository.dataTableTitle();
                    break;

                //大事記-月日
                case "historys_data":
                    re = historysDataRepository.dataTableTitle();
                    break;
                //大事記-年
                case "historys":
                    re = historysRepository.dataTableTitle();
                    break;
                //關於長庚
                case "abouts":
                    re = aboutsRepository.dataTableTitle();
                    break;
                //常見問題-細項
                case "qas_data":
                    re = qasDataRepository.dataTableTitle();
                    break;
                //常見問題
                case "qas":
                    re = qasRepository.dataTableTitle();
                    break;
                //院區資訊
                case "hospitals":
                    re = hospitalsRepository.dataTableTitle();
                    break;
                //院區分類
                case "hospital_category":
                    re = hospitalCategoryRepository.dataTableTitle();
                    break;
         

                //就醫指南(2)-細項
                case "medical_other_data":
                    re = medicalOtherDataRepository.dataTableTitle();
                    break;
                //就醫指南(2)
                case "medical_other":
                    re = medicalOtherRepository.dataTableTitle();
                    break;
                //就醫指南(1)
                case "medical_guide":
                    re = medicalGuideRepository.dataTableTitle();
                    break;

                //系統日誌
                case "system_log":
                    re = systemLogRepository.dataTableTitle();
                    break;
                //防火牆
                case "firewalls":
                    re = firewallsRepository.dataTableTitle();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.dataTableTitle();
                    break;


                //最新消息分類
                case "news_category":
                    re = newsCategoryRepository.dataTableTitle();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.dataTableTitle();
                    break;
               
                //使用者
                case "user":
                    re = userRepository.dataTableTitle();
                    break;

                
                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.dataTableTitle();
                    break;


            }


            return re;
        }

        /// <summary>
        /// 取得列表說明
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic listMessage(string tables)
        {
            dynamic re = null;
            switch (tables)
            {

                //防火牆
                case "firewalls":
                    re = firewallsRepository.listMessage();
                    break;

                default:
                    re = "";
                    break;
            }


            return re;
        }

        /// <summary>
        /// 欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getColData(string tables , dynamic data)
        {       
            dynamic re = null;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //滿意度調查結果細項
                case "quality_result_data":
                    re = qualityResultDataRepository.colFrom();
                    break;
                //滿意度調查結果
                case "quality_result":
                    re = qualityResultRepository.colFrom();
                    break;
                //特色醫療中心
                case "cm_list":
                    re = cmListRepository.colFrom();
                    break;
                //服務概況細項
                case "about_overview_data":
                    re = aboutOverviewDataRepository.colFrom();
                    break;
                //附屬網站&友善連結
                case "subsidiary_web":
                    re = subsidiaryWebRepository.colFrom();
                    break;

                //員工專區
                case "staff_area":
                    re = staffAreaRepository.colFrom();
                    break;
                //職缺院區資訊
                case "recruits_hospitals":
                    re = recruitsHospitalsDataRepository.colFrom();
                    break;
                //服務概況
                case "about_overview":
                    re = aboutOverviewRepository.colFrom();
                    break;
                //各入口頁面資訊管理-其他內容
                case "index_info_data":
                    re = indexInfoDataRepository.colFrom();
                    break;
                //各入口頁面資訊管理
                case "index_info":
                    re = indexInfoRepository.colFrom(data);
                    break;
                //醫療諮詢
                case "hospitals_advisory":
                    re = hospitalsAdvisoryRepository.colFrom();
                    break;
                //社會服務
                case "social_service":
                    re = socialServiceRepository.colFrom();
                    break;
                //公益活動
                case "public_welfare":
                    re = publicWelfareRepository.colFrom();
                    break;
                //申請與查詢
                case "app_inquiry":
                    re = appInquiryRepository.colFrom();
                    break;

                //各院區醫教業務洽詢單位
                case "edu_service":
                    re = eduServiceRepository.colFrom();
                    break;

                //教學資源單位
                case "edu_resource_data":
                    re = eduResourceDataRepository.colFrom();
                    break;
                //教學資源
                case "edu_resource":
                    re = eduResourceRepository.colFrom();
                    break;

                //教育培訓
                case "edu_train":
                    re = eduTrainRepository.colFrom();
                    break;

                //研究資源相關連結
                case "res_resource_data":
                    re = resResourceDataRepository.colFrom();
                    break;
                //研究資源
                case "res_resource":
                    re = resResourceRepository.colFrom();
                    break;

                //研究成果
                case "res_results":
                    re = resResultsRepository.colFrom();
                    break;


                //人才招募-薪資與福利
                case "recruits_data":
                    re = recruitsDataRepository.colFrom();
                    break;
                //人才招募
                case "recruits":
                    re = recruitsRepository.colFrom();
                    break;

                //大事記-月日
                case "historys_data":
                    re = historysDataRepository.colFrom();
                    break;
                //大事記-年
                case "historys":
                    re = historysRepository.colFrom();
                    break;
                //關於長庚
                case "abouts":
                    re = aboutsRepository.colFrom();
                    break;
                //常見問題-細項
                case "qas_data":
                    re = qasDataRepository.colFrom();
                    break;
                //常見問題
                case "qas":
                    re = qasRepository.colFrom();
                    break;
                //院區資訊
                case "hospitals":
                    re = hospitalsRepository.colFrom();
                    break;
                //院區分類
                case "hospital_category":
                    re = hospitalCategoryRepository.colFrom();
                    break;
                //其他資訊
                case "notes_data":
                    re = notesDataRepository.colFrom(data);
                    break;

                //就醫指南(2)-細項
                case "medical_other_data":
                    re = medicalOtherDataRepository.colFrom();
                    break;
                //就醫指南(2)
                case "medical_other":
                    re = medicalOtherRepository.colFrom();
                    break;
                //就醫指南(1)
                case "medical_guide":
                    re = medicalGuideRepository.colFrom();
                    break;
                //系統日誌
                case "system_log":
                    re = systemLogRepository.colFrom();
                    break;
                //防火牆
                case "firewalls":
                    re = firewallsRepository.colFrom();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.colFrom();
                    break;


                //最新消息分類
                case "news_category":
                    re = newsCategoryRepository.colFrom();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.colFrom();
                    break;
                //使用者
                case "user":
                    re = userRepository.colFrom();
                    break;

            
                //網站基本資料
                case "web_data":
                    re = webDataRepository.colFrom();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.colFrom();
                    break;


                //系統參數
                case "system_data":
                    re = systemDataRepository.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.colFrom();
                    break;

            }


            return re;
        }

        /// <summary>
        /// 取得資料庫資料
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>

        public static string getPresetData(string tables , string guid)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            string re = "";

            Dictionary<String, Object> reValData = new Dictionary<String, Object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {

                //滿意度調查結果細項
                case "quality_result_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<quality_result_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //滿意度調查結果
                case "quality_result":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<quality_result>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //特色醫療中心
                case "cm_list":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<cm_list>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //服務概況細項
                case "about_overview_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<about_overview_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //附屬網站&友善連結
                case "subsidiary_web":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<subsidiary_web>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //員工專區
                case "staff_area":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<staff_area>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //職缺院區資訊
                case "recruits_hospitals":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<recruits_hospitals>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //服務概況
                case "about_overview":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<about_overview>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //各入口頁面資訊管理-其他內容
                case "index_info_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<index_info_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //各入口頁面資訊管理
                case "index_info":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<index_info>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //醫療諮詢
                case "hospitals_advisory":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<hospitals_advisory>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //社會服務
                case "social_service":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<social_service>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //公益活動
                case "public_welfare":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<public_welfare>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //申請與查詢
                case "app_inquiry":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<app_inquiry>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //各院區醫教業務洽詢單位
                case "edu_service":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<edu_service>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //教學資源單位
                case "edu_resource_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<edu_resource_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //教學資源
                case "edu_resource":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<edu_resource>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //教育培訓
                case "edu_train":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<edu_train>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //研究資源相關連結
                case "res_resource_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<res_resource_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //研究資源
                case "res_resource":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<res_resource>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //研究成果
                case "res_results":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<res_results>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;


                //人才招募-薪資與福利
                case "recruits_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<recruits_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //人才招募
                case "recruits":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<recruits>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //大事記-月日
                case "historys_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<historys_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //大事記-年
                case "historys":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<historys>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //關於長庚
                case "abouts":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<abouts>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //常見問題-細項
                case "qas_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<qas_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //常見問題
                case "qas":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<qas>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //院區資訊
                case "hospitals":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<hospitals>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //院區分類
                case "hospital_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<hospital_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //其他資訊
                case "notes_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<notes_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //就醫指南(2)-細項
                case "medical_other_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<medical_other_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //就醫指南(2)
                case "medical_other":
                 
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<medical_other>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //就醫指南(1)
                case "medical_guide":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<medical_guide>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統日誌
                case "system_log":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //防火牆
                case "firewalls":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<firewalls>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //聯絡我們
                case "contacts":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<contacts>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);

                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);

                    }
                    break;

               

                //最新消息
                case "news":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<news>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);

                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);

                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (guid != null && guid != "")
                    {                     

                        var data = model.Repository<news_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);                    
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //群組管理
                case "roles":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<roles>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);

                    }

                    break;

                //使用者
                case "user":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<user>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);

                    }

                    break;


                //網站基本資料
                case "web_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<web_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);

                    }
                    break;


                //系統參數
                case "system_data":
               
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_data>();
                        Guid newGuid = Guid.Parse(guid);
                        var tempData = data.ReadsWhere(m => m.guid == newGuid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);

                    }
                    break;




                //SMTP資料
                case "smtp_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<smtp_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);

                    }
                    break;

        
            }




            return re;
        }


        /// <summary>
        /// 取得分類
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getCategory(string tables , dynamic data = null)
        {
            Model DB = new Model();
            dynamic re = null;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            switch (tables)
            {
                //申請與查詢
                case "app_inquiry_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.app_inquiry_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();

                    }
                    break;

                //院區分類
                case "hospital_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.hospital_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();

                    }
                    break;
                //院區
                case "hospitals":
                    if (tables != null && tables != "")
                    {
                        re = DB.hospitals.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();

                    }
                    break;
                //最新消息分類
                case "news_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.news_category.Where(m => m.status == "Y").Where(m=>m.lang == defLang).ToList();

                    }
                    break;
                //群組
                case "roles":
                    if (tables != null && tables != "")
                    {
                        re = DB.roles.Where(m => m.status == "Y").ToList();

                    }
                    break;

                //使用者
                case "user":
                    if (tables != null && tables != "")
                    {
                        re = DB.user.Where(m => m.status == "Y").ToList();
                    }
                    break;
          
                    

            }


            return re;

        }

        /// <summary>
        /// 回傳標題
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getDataTitle(string tables , string guid)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                case "index_info_data":
                    if (guid != "")
                    {
                        var data = DB.index_info.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = " [ " + data.title + " ] ";
                        }
                    }
                    break;

                case "res_resource_data":
                    if (guid != "")
                    {
                        var data = DB.res_resource.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = " [ " + data.title + " ] ";
                        }
                    }
                    break;

                case "edu_resource_data":
                    if (guid != "")
                    {
                        var data = DB.edu_resource.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = " [ " + data.title + " ] ";
                        }
                    }
                    break;

                case "medical_other_data":
                    if (guid != "")
                    {
                        var data = DB.medical_other.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = " [ " + data.title + " ] ";
                        }
                    }
                    break;
                case "qas_data":
                    if (guid != "")
                    {
                        var data = DB.qas.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = " [ " + data.title + " ] ";
                        }
                    }
                    break;

                case "hospitals_advisory":
                    if (guid != "")
                    {
                        var data = DB.hospitals.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = " [ " + data.title + " ] ";
                        }
                    }
                    break;
                case "historys_data":
                    if (guid != "")
                    {
                        var data = DB.historys.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = " [ " + data.year + " ] ";
                        }
                    }
                    break;
                case "about_overview_data":
                    if (guid != "")
                    {
                        var data = DB.about_overview.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = " [ " + data.title + " ] ";
                        }
                    }
                    break;
                case "quality_result_data":
                    if (guid != "")
                    {
                        var data = DB.quality_result.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {

                            var subData = DB.hospitals.Where(m=>m.guid == data.category).FirstOrDefault();

                            if (subData != null)
                            {
                                re = " [ " + subData.title + " ] ";
                            }
                        }
                    }
                    break;
            }
           
           
            
            return re;
         }

        /// <summary>
        /// 複選選單選取判斷用
        /// </summary>
        /// <param name="tabels"></param>
        /// <param name="user_guid"></param>
        /// <param name="forum_guid"></param>
        /// <returns></returns>
        public static string selectMultipleSelected(string tabels, string user_guid, string forum_guid)
        {
            Model DB = new Model();

            string selected = "";
            /*
            switch(tabels)
            {
                case "forum_message":

                    int data = DB.forum_message.Where(m=>m.user_guid == user_guid).Where(m=>m.forum_guid == forum_guid).Select(a => new { guid = a.guid }).Count();
                    if(data > 0)
                    {
                        selected = " selected";
                    }
                    break;
            }
            */


            return selected;

        }

        

        /// <summary>
        /// 新增 修改
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="form"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public static string saveData(string tables , string form ,string Field,string guid , string actType, Dictionary<String, Object> FormObj = null)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            dynamic Model = null;//目前Model
            dynamic FromData = null;//表單資訊
            string lang = "";


          

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //滿意度調查結果細項
                case "quality_result_data":
                    FromData = JsonConvert.DeserializeObject<quality_result_data>(form);
                    Model = model.Repository<quality_result_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.quality_result_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //滿意度調查結果
                case "quality_result":
                    FromData = JsonConvert.DeserializeObject<quality_result>(form);
                    Model = model.Repository<quality_result>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.quality_result.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //特色醫療中心
                case "cm_list":
                    FromData = JsonConvert.DeserializeObject<cm_list>(form);
                    Model = model.Repository<cm_list>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.cm_list.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //服務概況細項
                case "about_overview_data":
                    FromData = JsonConvert.DeserializeObject<about_overview_data>(form);
                    Model = model.Repository<about_overview_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.about_overview_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //附屬網站&友善連結
                case "subsidiary_web":
                    FromData = JsonConvert.DeserializeObject<subsidiary_web>(form);
                    Model = model.Repository<subsidiary_web>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.subsidiary_web.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //員工專區
                case "staff_area":
                    FromData = JsonConvert.DeserializeObject<staff_area>(form);
                    Model = model.Repository<staff_area>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.staff_area.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //職缺院區資訊
                case "recruits_hospitals":
                    FromData = JsonConvert.DeserializeObject<recruits_hospitals>(form);
                    Model = model.Repository<recruits_hospitals>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.recruits_hospitals.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //服務概況
                case "about_overview":
                    FromData = JsonConvert.DeserializeObject<about_overview>(form);
                    Model = model.Repository<about_overview>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.about_overview.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //各入口頁面資訊管理-其他內容
                case "index_info_data":
                    FromData = JsonConvert.DeserializeObject<index_info_data>(form);
                    Model = model.Repository<index_info_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.index_info_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //各入口頁面資訊管理
                case "index_info":
                    FromData = JsonConvert.DeserializeObject<index_info>(form);
                    Model = model.Repository<index_info>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.index_info.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //醫療諮詢
                case "hospitals_advisory":
                    FromData = JsonConvert.DeserializeObject<hospitals_advisory>(form);
                    Model = model.Repository<hospitals_advisory>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.hospitals_advisory.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //社會服務
                case "social_service":
                    FromData = JsonConvert.DeserializeObject<social_service>(form);
                    Model = model.Repository<social_service>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.social_service.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //公益活動
                case "public_welfare":
                    FromData = JsonConvert.DeserializeObject<public_welfare>(form);
                    Model = model.Repository<public_welfare>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.public_welfare.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //申請與查詢
                case "app_inquiry":
                    FromData = JsonConvert.DeserializeObject<app_inquiry>(form);
                    Model = model.Repository<app_inquiry>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.app_inquiry.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //各院區醫教業務洽詢單位
                case "edu_service":
                    FromData = JsonConvert.DeserializeObject<edu_service>(form);
                    Model = model.Repository<edu_service>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.edu_service.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //教學資源單位
                case "edu_resource_data":
                    FromData = JsonConvert.DeserializeObject<edu_resource_data>(form);
                    Model = model.Repository<edu_resource_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.edu_resource_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //教學資源
                case "edu_resource":
                    FromData = JsonConvert.DeserializeObject<edu_resource>(form);
                    Model = model.Repository<edu_resource>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.edu_resource.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //教育培訓
                case "edu_train":
                    FromData = JsonConvert.DeserializeObject<edu_train>(form);
                    Model = model.Repository<edu_train>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.edu_train.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //研究資源相關連結
                case "res_resource_data":
                    FromData = JsonConvert.DeserializeObject<res_resource_data>(form);
                    Model = model.Repository<res_resource_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.res_resource_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //研究資源
                case "res_resource":
                    FromData = JsonConvert.DeserializeObject<res_resource>(form);
                    Model = model.Repository<res_resource>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.res_resource.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //研究成果
                case "res_results":
                    FromData = JsonConvert.DeserializeObject<res_results>(form);
                    Model = model.Repository<res_results>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.res_results.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;


                //人才招募-薪資與福利
                case "recruits_data":
                    FromData = JsonConvert.DeserializeObject<recruits_data>(form);
                    Model = model.Repository<recruits_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.recruits_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //人才招募
                case "recruits":
                    FromData = JsonConvert.DeserializeObject<recruits>(form);
                    Model = model.Repository<recruits>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.recruits.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //大事記-月日
                case "historys_data":
                    FromData = JsonConvert.DeserializeObject<historys_data>(form);
                    Model = model.Repository<historys_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.historys_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //大事記-年
                case "historys":
                    FromData = JsonConvert.DeserializeObject<historys>(form);
                    Model = model.Repository<historys>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.historys.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //關於長庚
                case "abouts":
                    FromData = JsonConvert.DeserializeObject<abouts>(form);
                    Model = model.Repository<abouts>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.abouts.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //常見問題-細項
                case "qas_data":
                    FromData = JsonConvert.DeserializeObject<qas_data>(form);
                    Model = model.Repository<qas_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.qas_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //常見問題
                case "qas":
                    FromData = JsonConvert.DeserializeObject<qas>(form);
                    Model = model.Repository<qas>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.qas.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //院區資訊
                case "hospitals":
                    FromData = JsonConvert.DeserializeObject<hospitals>(form);
                    Model = model.Repository<hospitals>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.hospitals.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //院區分類
                case "hospital_category":
                    FromData = JsonConvert.DeserializeObject<hospital_category>(form);
                    Model = model.Repository<hospital_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.hospital_category.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //其他資訊
                case "notes_data":
                    FromData = JsonConvert.DeserializeObject<notes_data>(form);
                    Model = model.Repository<notes_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.notes_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //就醫指南(2)-細項
                case "medical_other_data":
                    FromData = JsonConvert.DeserializeObject<medical_other_data>(form);
                    Model = model.Repository<medical_other_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.medical_other_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //就醫指南(2)
                case "medical_other":
                    FromData = JsonConvert.DeserializeObject<medical_other>(form);
                    Model = model.Repository<medical_other>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.medical_other.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //就醫指南(1)
                case "medical_guide":
                    FromData = JsonConvert.DeserializeObject<medical_guide>(form);
                    Model = model.Repository<medical_guide>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.medical_guide.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //系統日誌
                case "system_log":

                    FromData = JsonConvert.DeserializeObject<system_log>(form);
                    Model = model.Repository<system_log>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.system_log.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //防火牆
                case "firewalls":

                    FromData = JsonConvert.DeserializeObject<firewalls>(form);
                    Model = model.Repository<firewalls>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.firewalls.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //最新消息
                case "news":
                    FromData = JsonConvert.DeserializeObject<news>(form);
                    Model = model.Repository<news>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;
                //最新消息分類
                case "news_category":


                    FromData = JsonConvert.DeserializeObject<news_category>(form);
                    Model = model.Repository<news_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news_category.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                       
                    }

                 
                    
                    break;
                //群組管理
                case "roles":

                    FromData = JsonConvert.DeserializeObject<roles>(form);
                    Model = model.Repository<roles>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

              
                //網站基本資料
                case "web_data":
                    FromData = JsonConvert.DeserializeObject<web_data>(form);
                    Model = model.Repository<web_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.web_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;


                //SMTP資料
                case "smtp_data":
                    FromData = JsonConvert.DeserializeObject<smtp_data>(form);
                    Model = model.Repository<smtp_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.smtp_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //資源回收桶
                case "ashcan":

                    FromData = JsonConvert.DeserializeObject<ashcan>(form);
                    Model = model.Repository<ashcan>();
                    if (Field != "")
                    {                     
                        Guid NewGuid = Guid.Parse(FromData.guid.ToString());
                        Model = DB.ashcan.Where(m => m.guid == NewGuid).FirstOrDefault();
                    }

                    break;
                //使用者
                case "user":

                    FromData = JsonConvert.DeserializeObject<user>(form);
                    Model = model.Repository<user>();

                    if (FromData.password != null && FromData.password != "")
                    {
                        FromData.password = FunctionService.md5(FromData.password);
                    }
                    //無輸入密碼進入修改情況下
                    if (FromData.password == "" && FromData.guid != "")
                    {
                        FromData.password = FormObj["defPassword"].ToString();
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.user.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;


                //系統參數
                case "system_data":


                    FromData = JsonConvert.DeserializeObject<system_data>(form);
                    Model = model.Repository<system_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Guid newGuid = Guid.Parse(guid);
                        Model = DB.system_data.Where(m => m.guid == newGuid).FirstOrDefault();
                    }

                  
                    break;

            }


             FromData.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            

            if (actType == "add")
            {              
           
                FromData.guid = guid;
                FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));               
                Model.Create(FromData);
                Model.SaveChanges();
               
             }
            else
            {
                if (Field == "")
                {

                     Model.Update(FromData);
                     Model.SaveChanges();


                    //Model = FromData;
                    //DB.SaveChanges();

                    //回覆信件
                  /*  if(tables == "contacts" && FromData.status == "Y" && FromData.is_send == "N")
                    {
                      
                        web_data webData = DB.web_data.Where(m => m.lang == "tw").FirstOrDefault();//取得網站基本資訊
                        List<string> MailList = new List<string>();
                        MailList = webData.contact_email.Split(',').ToList();

                        var jss = new JavaScriptSerializer();
                        var dict = jss.Deserialize<Dictionary<string, string>>(form);

                        NameValueCollection nvc = null;
                        if (dict != null)
                        {
                            nvc = new NameValueCollection(dict.Count);
                            foreach (var k in dict)
                            {
                                nvc.Add(k.Key, k.Value);
                            }
                            if(FromData.category == "contacts")
                            {
                                nvc.Add("subTitle", "主旨");
                            }
                            else
                            {
                                nvc.Add("subTitle", "建案");
                            }
                            FunctionService.sendMail(MailList, "3", "tw", FunctionService.getWebUrl(), nvc);
                            string sendGuid = FromData.guid;
                            Model = DB.contacts.Where(m => m.guid == sendGuid).FirstOrDefault();
                            Model.is_send = "Y";
                            Model.re_date = DateTime.Now;
                            DB.SaveChanges();

                        }                      
                    }
                    */
                }
                else
                {                 
                    //單一欄位修改
                    switch(Field)
                    {
                        case "status":
                            Model.status = FromData.status;
                            break;
                        case "sortIndex":
                            Model.sortIndex = FromData.sortIndex;
                            break;
                        case "logindate":
                            Model.logindate = FromData.logindate;
                            break;

                  

                    }
              
                    DB.SaveChanges();
                }
             
            }

            if(FormObj != null)
            {
                

                

                #region 權限
                if (FormObj.ContainsKey("permissions") && FormObj["permissions"] != null && FormObj["permissions"].ToString() != "")
                {


                    NameValueCollection permissions = FunctionService.reSubmitFormDataJson(FormObj["permissions"].ToString());

                    role_permissions role_permissions_add = new role_permissions();

                    var delData = DB.role_permissions.Where(m => m.role_guid == guid).ToList();

                    foreach (var item in delData)
                    {
                        DB.role_permissions.Remove((role_permissions)item);
                    }

                    foreach (string key in permissions)
                    {
                        //Guid newGuid = Guid.NewGuid();
                        role_permissions_add = new role_permissions();
                        role_permissions_add.guid = Guid.NewGuid();
                        role_permissions_add.role_guid = guid;
                        role_permissions_add.permissions_guid = key.ToString();
                        role_permissions_add.permissions_status = permissions[key].ToString();
                        DB.role_permissions.Add(role_permissions_add);
                        DB.SaveChanges();
                    }
                   
                }
                #endregion

                

            }




            return guid;

        }

        /// <summary>
        /// 取得預設排序
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy(string tables)
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          switch (tables)
            {
                //滿意度調查結果細項
                case "quality_result_data":
                    re = qualityResultDataRepository.defaultOrderBy();
                    break;
                //滿意度調查結果
                case "quality_result":
                    re = qualityResultRepository.defaultOrderBy();
                    break;
                //特色醫療中心
                case "cm_list":
                    re = cmListRepository.defaultOrderBy();
                    break;

                //服務概況  
                case "about_overview_data":
                    re = aboutOverviewDataRepository.defaultOrderBy();
                    break;

                //附屬網站&友善連結
                case "subsidiary_web":
                    re = subsidiaryWebRepository.defaultOrderBy();
                    break;

                //員工專區
                case "staff_area":
                    re = staffAreaRepository.defaultOrderBy();
                    break;
                //職缺院區資訊
                case "recruits_hospitals":
                    re = recruitsHospitalsDataRepository.defaultOrderBy();
                    break;
                //服務概況
                case "about_overview":
                    re = aboutOverviewRepository.defaultOrderBy();
                    break;

                //各入口頁面資訊管理-其他內容
                case "index_info_data":
                    re = indexInfoDataRepository.defaultOrderBy();
                    break;
                //各入口頁面資訊管理
                case "index_info":
                    re = indexInfoRepository.defaultOrderBy();
                    break;
                //醫療諮詢
                case "hospitals_advisory":
                    re = hospitalsAdvisoryRepository.defaultOrderBy();
                    break;
                //社會服務
                case "social_service":
                    re = socialServiceRepository.defaultOrderBy();
                    break;
                //公益活動
                case "public_welfare":
                    re = publicWelfareRepository.defaultOrderBy();
                    break;
                //申請與查詢
                case "app_inquiry":
                    re = appInquiryRepository.defaultOrderBy();
                    break;

                //各院區醫教業務洽詢單位
                case "edu_service":
                    re = eduServiceRepository.defaultOrderBy();
                    break;

                //教學資源單位
                case "edu_resource_data":
                    re = eduResourceDataRepository.defaultOrderBy();
                    break;
                //教學資源
                case "edu_resource":
                    re = eduResourceRepository.defaultOrderBy();
                    break;

                //教育培訓
                case "edu_train":
                    re = eduTrainRepository.defaultOrderBy();
                    break;

                //研究資源相關連結
                case "res_resource_data":
                    re = resResourceDataRepository.defaultOrderBy();
                    break;
                //研究資源
                case "res_resource":
                    re = resResourceRepository.defaultOrderBy();
                    break;

                //研究成果
                case "res_results":
                    re = resResultsRepository.defaultOrderBy();
                    break;

                //人才招募-薪資與福利
                case "recruits_data":
                    re = recruitsDataRepository.defaultOrderBy();
                    break;
                //人才招募
                case "recruits":
                    re = recruitsRepository.defaultOrderBy();
                    break;

                //大事記-月日
                case "historys_data":
                    re = historysDataRepository.defaultOrderBy();
                    break;
                //大事記-年
                case "historys":
                    re = historysRepository.defaultOrderBy();
                    break;
                //關於長庚
                case "abouts":
                    re = aboutsRepository.defaultOrderBy();
                    break;
                //常見問題-細項
                case "qas_data":
                    re = qasDataRepository.defaultOrderBy();
                    break;
                //常見問題
                case "qas":
                    re = qasRepository.defaultOrderBy();
                    break;
                //院區資訊
                case "hospitals":
                    re = hospitalsRepository.defaultOrderBy();
                    break;
                //院區分類
                case "hospital_category":
                    re = hospitalCategoryRepository.defaultOrderBy();
                    break;
           

                //就醫指南(2)-細項
                case "medical_other_data":
                    re.Clear();
                    re = medicalOtherDataRepository.defaultOrderBy();
                    break;
                //就醫指南(2)
                case "medical_other":
                    re.Clear();
                    re = medicalOtherRepository.defaultOrderBy();
                    break;
                //就醫指南(1)
                case "medical_guide":
                    re.Clear();
                    re = medicalGuideRepository.defaultOrderBy();
                    break;
                //系統日誌
                case "system_log":
                    re.Clear();
                    re = systemLogRepository.defaultOrderBy();
                    break;
                //防火牆
                case "firewalls":
                    re.Clear();
                    re = firewallsRepository.defaultOrderBy();
                    break;
                //醫院訊息分類  
                case "news_category":
                    re.Clear();
                    re = newsCategoryRepository.defaultOrderBy();
                    break;
                //醫院訊息  
                case "news":
                    re.Clear();
                    re = newsRepository.defaultOrderBy();
                    break;
            }
            return re;

        }

        /// <summary>
        /// 格式化排序欄位
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="orderByKey"></param>
        /// <returns></returns>
        public static string formatOrderByKey(string tables , string orderByKey)
        {
            string re = "";
            switch(orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                 case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;
                case "action":
                    re = "guid";
                    break;
                case "user":
                    re = "user_guid";
                    break;
                case "category":
                    re = "category";
                    if(tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;

            }
            return re;
        }

        /// <summary>
        /// 回傳資料表的標題或姓名
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string tableTitleKey(string tables , dynamic dataList)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
               
                case "user":
                    re = dataList["name"];
                    break;

               
                default:
                    re = dataList["title"];
                    break;

            }
            return re;
        }

      


    }
}