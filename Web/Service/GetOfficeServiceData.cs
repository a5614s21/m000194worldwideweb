﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.OfficialWebSite;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Web.ServiceModels;
using System.Text.RegularExpressions;
using System.IO;
using Web.CgmhAppService;
using Web.Service;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.Web.Caching;
using System.Configuration;

namespace Web.Service
{
    public class GetOfficeServiceData : Controller
    {
        public RMS_WS AppServices = new RMS_WS();//宣告api           
        public v2 OfficeServices = new v2();//官網API
        public string comId = "24241872";
        private static string strkey = "mWq032xjrz";//衛教文章用
        protected System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;//更改較少資訊Cache使用
        protected int CacheSaveHour = int.Parse(ConfigurationManager.ConnectionStrings["CacheSaveHour"].ConnectionString);//Cache 儲存(小時)
        /// <summary>
        /// 回傳authKey
        /// </summary>
        /// <returns></returns>
        public string GetAuthKey()
        {
            JObject authKeyResponse = JObject.Parse(AppServices.WS100("IPL", "IPLcgmh"));
            string authKey = authKeyResponse["resultList"].ToString();
            return authKey;
        }

        /// <summary>
        /// 組合cache名稱
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetCacheName(Dictionary<string, string> data)
        {
            string re = "";
            foreach (KeyValuePair<string, string> item in data)
            {
                if(!string.IsNullOrEmpty(item.Value))
                {
                    re += item.Key + item.Value;
                }
                
            }
            return re;

        }


        /// <summary>
        /// 提供首頁最新消息、活動訊息清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetHomePageBulletinList(Dictionary<string, string> data)
        {
            string CacheName = "GetHomePageBulletinList" + GetCacheName(data);

            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
                    OfficeServices.GetHomePageBulletinList(
                   data["language"].ToString(),
                   data["newsType"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<BulletinListModel> resultList = JsonConvert.DeserializeObject<List<BulletinListModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);

                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供公告(最新消息、活動訊息)清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetBulletinList(Dictionary<string, string> data)
        {
            string CacheName = "GetBulletinList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetBulletinList(
                   data["language"].ToString(),
                   data["newsType"].ToString(),
                   data["keyword"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<BulletinListModel> resultList = JsonConvert.DeserializeObject<List<BulletinListModel>>(Response["resultList"].ToString());
                        //取上下筆
                        if (data.ContainsKey("ID"))
                        {
                            Dictionary<string, string> PrevNextIDModels = new Dictionary<string, string>();

                            PrevNextIDModels.Add("PrevID", "");
                            PrevNextIDModels.Add("NextID", "");

                            if (resultList != null && resultList.GetType().Name != "JObject")
                            {
                                string ID = data["ID"].ToString();
                                int nowIndex = resultList.FindIndex(m => m.bulletinId == ID);


                                if ((nowIndex - 1) >= 0 && resultList[nowIndex - 1] != null)
                                {
                                    PrevNextIDModels.Remove("PrevID");
                                    PrevNextIDModels.Add("PrevID", resultList[nowIndex - 1].bulletinId);
                                }

                                if ((nowIndex + 1) < resultList.Count && resultList[nowIndex + 1] != null)
                                {
                                    PrevNextIDModels.Remove("NextID");
                                    PrevNextIDModels.Add("NextID", resultList[nowIndex + 1].bulletinId);
                                }

                                resultList[0].PrevNextID = PrevNextIDModels;
                            }
                        }
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// 提供公告(最新消息、活動訊息、學術活動資訊、職缺公告)內容
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetBulletin(Dictionary<string, string> data)
        {
            string CacheName = "GetBulletinList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {

                    JObject Response = JObject.Parse(
               OfficeServices.GetBulletin(
                   data["language"].ToString(),
                   data["bulletinId"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<BulletinModel> resultList = JsonConvert.DeserializeObject<List<BulletinModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供文章清單，包含各主頁之主視覺、首頁(聚焦點、新聞與觀點)、社會公益(圖文集)、研究教學(圖文集)、醫院訊息(聚焦點、新聞與觀點)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetGlobalArticleList(Dictionary<string, string> data, string addDefult = null)
        {
            string CacheName = "GetGlobalArticleList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
                    OfficeServices.GetGlobalArticleList(
                       data["language"].ToString(),
                       data["articleType"].ToString(),
                       data["keyword"].ToString(),
                       comId,
                       GetAuthKey())
                       );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        if (data["articleType"].ToString() == "S03")
                        {
                            var test = Response;
                        }

                        List<GlobalArticleListModel> resultList = JsonConvert.DeserializeObject<List<GlobalArticleListModel>>(Response["resultList"].ToString());
                        //取上下筆
                        if (data.ContainsKey("articleId"))
                        {
                            string ID = data["articleId"].ToString();
                            int nowIndex = resultList.FindIndex(m => m.articleId == ID);
                            Dictionary<string, string> PrevNextIDModels = new Dictionary<string, string>();

                            PrevNextIDModels.Add("PrevID", "");
                            PrevNextIDModels.Add("NextID", "");

                            if ((nowIndex - 1) >= 0 && resultList[nowIndex - 1] != null)
                            {
                                PrevNextIDModels.Remove("PrevID");
                                PrevNextIDModels.Add("PrevID", resultList[nowIndex - 1].articleId);
                            }

                            if ((nowIndex + 1) < resultList.Count && resultList[nowIndex + 1] != null)
                            {
                                PrevNextIDModels.Remove("NextID");
                                PrevNextIDModels.Add("NextID", resultList[nowIndex + 1].articleId);
                            }

                            resultList[0].PrevNextID = PrevNextIDModels;
                        }

                        //取內容
                        if (addDefult != null)
                        {
                            int i = 0;
                            if (resultList != null && resultList.Count > 0)
                            {
                                foreach (GlobalArticleListModel item in resultList)
                                {
                                    data.Remove("articleId");
                                    data.Add("articleId", item.articleId);
                                    var subData = GetGlobalArticle(data);

                                    if (subData != null && subData.GetType().Name != "JObject")
                                    {
                                        resultList[i].Data = subData[0];
                                    }


                                    i++;
                                }
                            }
                        }

                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);

                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }


                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }

            }
            catch
            {
                return null;
            }



        }

        /// <summary>
        /// 提供文章內容，包含聚焦點、新聞與觀點、社會公益、研究教學
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetGlobalArticle(Dictionary<string, string> data)
        {

            string CacheName = "GetGlobalArticle" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetGlobalArticle(
                   data["language"].ToString(),
                   data["articleId"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<GlobalArticleModel> resultList = JsonConvert.DeserializeObject<List<GlobalArticleModel>>(Response["resultList"].ToString());
                        if (resultList != null)
                        {
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return Response;
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供記者會資訊清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetPressNewsList(Dictionary<string, string> data)
        {
            string CacheName = "GetPressNewsList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetPressNewsList(
                   data["language"].ToString(),
                   data["keyword"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<PressNewsListModel> resultList = JsonConvert.DeserializeObject<List<PressNewsListModel>>(Response["resultList"].ToString());

                        //取上下筆
                        if (data.ContainsKey("ID"))
                        {
                            string ID = data["ID"].ToString();
                            int nowIndex = resultList.FindIndex(m => m.pressId == ID);
                            Dictionary<string, string> PrevNextIDModels = new Dictionary<string, string>();

                            PrevNextIDModels.Add("PrevID", "");
                            PrevNextIDModels.Add("NextID", "");

                            if ((nowIndex - 1) >= 0 && resultList[nowIndex - 1] != null)
                            {
                                PrevNextIDModels.Remove("PrevID");
                                PrevNextIDModels.Add("PrevID", resultList[nowIndex - 1].pressId);
                            }

                            if ((nowIndex + 1) < resultList.Count && resultList[nowIndex + 1] != null)
                            {
                                PrevNextIDModels.Remove("NextID");
                                PrevNextIDModels.Add("NextID", resultList[nowIndex + 1].pressId);
                            }

                            resultList[0].PrevNextID = PrevNextIDModels;
                        }
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供記者會資訊內容
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetPressNews(Dictionary<string, string> data)
        {
            string CacheName = "GetPressNews" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetPressNews(
                   data["language"].ToString(),
                   data["pressID"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<PressNewsModel> resultList = JsonConvert.DeserializeObject<List<PressNewsModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 學術活動資訊清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetAcademicEventsList(Dictionary<string, string> data)
        {

            string CacheName = "GetAcademicEventsList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetAcademicEventsList(
                   data["language"].ToString(),
                   data["keyword"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<AcademicEventsListModel> resultList = JsonConvert.DeserializeObject<List<AcademicEventsListModel>>(Response["resultList"].ToString());

                        //取上下筆
                        if (data.ContainsKey("ID"))
                        {
                            string ID = data["ID"].ToString();
                            int nowIndex = resultList.FindIndex(m => m.bulletinId == ID);
                            Dictionary<string, string> PrevNextIDModels = new Dictionary<string, string>();

                            PrevNextIDModels.Add("PrevID", "");
                            PrevNextIDModels.Add("NextID", "");

                            if ((nowIndex - 1) >= 0 && resultList[nowIndex - 1] != null)
                            {
                                PrevNextIDModels.Remove("PrevID");
                                PrevNextIDModels.Add("PrevID", resultList[nowIndex - 1].bulletinId);
                            }

                            if ((nowIndex + 1) < resultList.Count && resultList[nowIndex + 1] != null)
                            {
                                PrevNextIDModels.Remove("NextID");
                                PrevNextIDModels.Add("NextID", resultList[nowIndex + 1].bulletinId);
                            }

                            resultList[0].PrevNextID = PrevNextIDModels;
                        }
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }

                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 學術活動資訊內容
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetAcademicEvents(Dictionary<string, string> data)
        {
            string CacheName = "GetAcademicEvents" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {

                    JObject Response = JObject.Parse(
               OfficeServices.GetAcademicEvents(
                   data["language"].ToString(),
                   data["bulletinId"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<AcademicEventsModel> resultList = JsonConvert.DeserializeObject<List<AcademicEventsModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 得獎紀錄
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetAwardList(Dictionary<string, string> data)
        {
            string CacheName = "GetAwardList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {

                    JObject Response = JObject.Parse(
                       OfficeServices.GetAwardList(
                           data["language"].ToString(),
                           data["keyword"].ToString(),
                           comId,
                           GetAuthKey())
                           );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<AwardListModel> resultList = JsonConvert.DeserializeObject<List<AwardListModel>>(Response["resultList"].ToString());

                        //取上下筆
                        if (data.ContainsKey("ID"))
                        {
                            string ID = data["ID"].ToString();
                            int nowIndex = resultList.FindIndex(m => m.awardId == ID);
                            Dictionary<string, string> PrevNextIDModels = new Dictionary<string, string>();

                            PrevNextIDModels.Add("PrevID", "");
                            PrevNextIDModels.Add("NextID", "");

                            if ((nowIndex - 1) >= 0 && resultList[nowIndex - 1] != null)
                            {
                                PrevNextIDModels.Remove("PrevID");
                                PrevNextIDModels.Add("PrevID", resultList[nowIndex - 1].awardId);
                            }

                            if ((nowIndex + 1) < resultList.Count && resultList[nowIndex + 1] != null)
                            {
                                PrevNextIDModels.Remove("NextID");
                                PrevNextIDModels.Add("NextID", resultList[nowIndex + 1].awardId);
                            }

                            resultList[0].PrevNextID = PrevNextIDModels;
                        }
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 得獎紀錄內容
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetAward(Dictionary<string, string> data)
        {
            string CacheName = "GetAward" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
                       OfficeServices.GetAward(
                           data["language"].ToString(),
                           data["awardId"].ToString(),
                           comId,
                           GetAuthKey())
                           );

                if (Response["isSuccess"].ToString() == "Y")
                {
                    List<AwardModel> resultList = JsonConvert.DeserializeObject<List<AwardModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                }
                else
                {
                    return Response;
                }
            }
            else
            {
                    return cacheContainer.Get(CacheName);//自cache取出
            }
          }
          catch
          {
                return null;
          }
        }
    



        /// <summary>
        /// 藥品異動訊息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDrugChangeList(Dictionary<string, string> data)
        {
            string CacheName = "GetDrugChangeList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {

                    JObject Response = JObject.Parse(
               OfficeServices.GetDrugChangeList(
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DrugChangeListModel> resultList = JsonConvert.DeserializeObject<List<DrugChangeListModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }

        }


        /// <summary>
        /// 醫療品質資訊公開清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetMedicalQualityList(Dictionary<string, string> data)
        {
            string CacheName = "GetMedicalQualityList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetMedicalQualityList(
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<MedicalQualityListModel> resultList = JsonConvert.DeserializeObject<List<MedicalQualityListModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 提供品質績優事項清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetExcellentQualityList(Dictionary<string, string> data)
        {
            string CacheName = "GetExcellentQualityList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetExcellentQualityList(
                   data["hospitalID"].ToString(),
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<ExcellentQualityListModel> resultList = JsonConvert.DeserializeObject<List<ExcellentQualityListModel>>(Response["resultList"].ToString());
                        if (resultList != null)
                        {
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return Response;
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 提供滿意度調查結果清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetSatisfactionList(Dictionary<string, string> data)
        {
            string CacheName = "GetSatisfactionList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetSatisfactionList(
                   data["hospitalID"].ToString(),
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<SatisfactionListModel> resultList = JsonConvert.DeserializeObject<List<SatisfactionListModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 科部介紹清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDeptList(Dictionary<string, string> data)
        {
            string CacheName = "GetDeptList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetDeptList(
                   data["hospitalID"].ToString(),
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DeptListModel> resultList = JsonConvert.DeserializeObject<List<DeptListModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供科部介紹資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDept(Dictionary<string, string> data)
        {
            string CacheName = "GetDept" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetDept(
                   data["language"].ToString(),
                   data["deptId"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DeptDataModel> resultList = JsonConvert.DeserializeObject<List<DeptDataModel>>(Response["resultList"].ToString());

                        //選單用列表
                        if (data.ContainsKey("topDeptId"))
                        {
                            var Models = GetDeptList(data);
                            if (Models != null && Models.GetType().Name != "JObject")
                            {
                                List<DeptListModel> DeptList = Models;

                                DeptList = DeptList.FindAll(m => m.deptId == data["topDeptId"].ToString());


                                if (DeptList[0].subDept != null && DeptList[0].subDept.Count > 0)
                                {
                                    resultList[0].DeptList = DeptList[0].subDept;
                                }


                            }
                        }
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }




        /// <summary>
        /// 提供查詢醫師結果清單(hospitalID、deptId、keyword三個至少傳入一個非空白)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetSearchDoctor(Dictionary<string, string> data)
        {
            string CacheName = "GetSearchDoctor" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetSearchDoctor(
                   data["language"].ToString(),
                   data.ContainsKey("hospitalID") ? data["hospitalID"].ToString() : "",
                   data.ContainsKey("deptId") ? data["deptId"].ToString() : "",
                   data.ContainsKey("keyword") ? data["keyword"].ToString() : "",
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<SearchDoctorModel> resultList = JsonConvert.DeserializeObject<List<SearchDoctorModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }





        /// <summary>
        /// 提供醫師介紹之部門清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDoctorDeptList(Dictionary<string, string> data)
        {
            string CacheName = "GetDoctorDeptList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetDoctorDeptList(
                   data["hospitalID"].ToString(),
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DoctorDeptListModel> resultList = JsonConvert.DeserializeObject<List<DoctorDeptListModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 提供醫師介紹之部門清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDeptDoctor(Dictionary<string, string> data)
        {
            string CacheName = "GetDeptDoctor" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetDeptDoctor(
                   data["language"].ToString(),
                   data["deptId"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DeptDoctorModel> resultList = JsonConvert.DeserializeObject<List<DeptDoctorModel>>(Response["resultList"].ToString());
                        if (resultList.Count > 0)
                        {
                            int i = 0;
                            foreach (DeptDoctorModel item in resultList)
                            {
                                resultList[i].deptGroupId = Response["deptGroupId"].ToString();
                                resultList[i].deptGroupName = Response["deptGroupName"].ToString();
                                resultList[i].deptID = Response["deptID"].ToString();
                                resultList[i].deptGroupId = Response["deptName"].ToString();

                                i++;
                            }
                        }
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 提供醫師個人資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDoctor(Dictionary<string, string> data)
        {
            string CacheName = "GetDoctor" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetDoctor(
                   data["language"].ToString(),
                   data["doctorId"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DoctorModel> resultList = JsonConvert.DeserializeObject<List<DoctorModel>>(Response["resultList"].ToString());

                        //選單用列表
                        if (resultList[0].deptID != null && resultList[0].deptID != "")
                        {
                            data.Add("deptId", resultList[0].deptID);
                            var Models = GetDeptDoctor(data);
                            if (Models != null && Models.GetType().Name != "JObject")
                            {
                                resultList[0].DeptDataList = Models;
                            }
                        }
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供院區簡介
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetBranch(Dictionary<string, string> data)
        {
            string CacheName = "GetBranch" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetBranch(
                   data["hospitalID"].ToString(),
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<BranchModel> resultList = JsonConvert.DeserializeObject<List<BranchModel>>(Response["resultList"].ToString());
                        cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                        return resultList;
                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }


        /// <summary>
        /// 提供社服活動清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetSocialServiceList(Dictionary<string, string> data)
        {
            string CacheName = "GetSocialServiceList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetSocialServiceList(
                   data["language"].ToString(),
                   data["hospitalID"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<SocialServiceListModel> resultList = JsonConvert.DeserializeObject<List<SocialServiceListModel>>(Response["resultList"].ToString());

                        if (resultList != null)
                        {
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return Response;
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 網站會員登入檢查
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic CheckMemberLogin(Dictionary<string, string> data)
        {

            JObject Response = JObject.Parse(
               OfficeServices.CheckMemberLogin(
                   data["account"].ToString(),
                   data["password"].ToString(),
                   comId,
                   GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<MemberLoginModel> resultList = JsonConvert.DeserializeObject<List<MemberLoginModel>>(Response["resultList"].ToString());
                if (resultList != null)
                {
                    return resultList;
                }
                else
                {
                    return Response;
                }

            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// 網站會員登入檢查
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetMemberData(Dictionary<string, string> data)
        {

            JObject Response = JObject.Parse(
               OfficeServices.GetMemberData(
                   data["account"].ToString(),
                   comId,
                   GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<MemberDataModel> resultList = JsonConvert.DeserializeObject<List<MemberDataModel>>(Response["resultList"].ToString());
                if (resultList != null)
                {
                    return resultList;
                }
                else
                {
                    return Response;
                }

            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// 提供用藥諮詢清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDrugConsultList(Dictionary<string, string> data)
        {

            string CacheName = "GetDrugConsultList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetDrugConsultList(
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DrugConsultListModel> resultList = JsonConvert.DeserializeObject<List<DrugConsultListModel>>(Response["resultList"].ToString());
                        if (resultList != null)
                        {
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return Response;
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供職缺公告清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetRecruitmentList(Dictionary<string, string> data)
        {

            string CacheName = "GetRecruitmentList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetRecruitmentList(
                   data["language"].ToString(),
                   data["orgnid"].ToString(),
                   data["hospitalID"].ToString(),
                   data["category"].ToString(),
                   data["bulletinType"].ToString(),
                   data["keyword"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<RecruitmentListModel> resultList = JsonConvert.DeserializeObject<List<RecruitmentListModel>>(Response["resultList"].ToString());
                        if (resultList != null)
                        {
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return Response;
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供用藥指導單張清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDrugGuideList(Dictionary<string, string> data)
        {

            string CacheName = "GetDrugGuideList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetDrugGuideList(
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DrugGuideListModel> resultList = JsonConvert.DeserializeObject<List<DrugGuideListModel>>(Response["resultList"].ToString());
                        if (resultList != null)
                        {
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return Response;
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 提供長庚藥學學報清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetDrugPaperList(Dictionary<string, string> data)
        {

            string CacheName = "GetDrugPaperList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetDrugPaperList(
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<DrugPaperListModel> resultList = JsonConvert.DeserializeObject<List<DrugPaperListModel>>(Response["resultList"].ToString());
                        if (resultList != null)
                        {
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return Response;
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 設定網站會員資料(含新會員)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic SetMemberData(Dictionary<string, string> data)
        {

            JObject Response = JObject.Parse(
               OfficeServices.SetMemberData(
                    data["saveType"].ToString(),
                    data["account"].ToString(),
                    data["password"].ToString(),
                    data["cName"].ToString(),
                    data["sex"].ToString(),
                    data["birthday"].ToString(),
                    data["tel"].ToString(),
                    data["address"].ToString(),
                    data["ePaper"].ToString(),
                    comId,
                    GetAuthKey())
                   );

            if (Response["isSuccess"].ToString() == "Y")
            {
                List<SetMemberDataModel> resultList = JsonConvert.DeserializeObject<List<SetMemberDataModel>>(Response["resultList"].ToString());
                if (resultList != null)
                {
                    return resultList;
                }
                else
                {
                    return Response;
                }

            }
            else
            {
                return Response;
            }
        }

        /// <summary>
        /// 設定網站會員資料(含新會員)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic SetMemberDataActive(Dictionary<string, string> data)
        {
            JObject Response = JObject.Parse(
                OfficeServices.SetMemberData(
                    data["saveType"].ToString(),
                    data["account"].ToString(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    comId,
                    GetAuthKey())
                );

            if(Response["isSuccess"].ToString() == "Y")
            {
                List<SetMemberDataModel> resultList = JsonConvert.DeserializeObject<List<SetMemberDataModel>>(Response["resultList"].ToString());
                if(resultList != null)
                {
                    return resultList;
                }
                else
                {
                    return Response;
                }
            }
            else
            {
                return Response;
            }
        }
        //


        /// <summary>
        /// 網站會員啟用
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic SetMemberActive(Dictionary<string, string> data)
        {
            JObject Response = JObject.Parse(
               OfficeServices.SetMemberActive(
                    data["activeCode"].ToString(),                  
                    comId,
                    GetAuthKey())
                   );

            return Response;

        }





        /// <summary>
        /// 儲存用藥諮詢問題資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic SetDrugConsult(Dictionary<string, string> data)
        {
            JObject Response = JObject.Parse(
               OfficeServices.SetDrugConsult(
                    data["language"].ToString(),
                    data["nickName"].ToString(),
                    data["postTime"].ToString(),
                    data["sex"].ToString(),
                    data["age"].ToString(),
                    data["profession"].ToString(),
                    data["eMail"].ToString(),
                    data["description"].ToString(),
                    comId,
                    GetAuthKey())
                   );

            return Response;

        }

        /// <summary>
        /// 發送意見信箱
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic SendOpinionMail(Dictionary<string, string> data)
        {
            JObject Response = JObject.Parse(
               OfficeServices.SendOpinionMail(
                    data["cName"].ToString(),
                    data["tel"].ToString(),
                    data["eMail"].ToString(),
                    data["hospitalID"].ToString(),
                    data["matter"].ToString(),
                    data["description"].ToString(),
                    data["remoteIP"].ToString(),
                    comId,
                    GetAuthKey())
                   );

            return Response;

        }

        /// <summary>
        /// 提供社服活動院區清單
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetSocialServiceLocList(Dictionary<string, string> data)
        {
            string CacheName = "GetSocialServiceLocList" + GetCacheName(data);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    JObject Response = JObject.Parse(
               OfficeServices.GetSocialServiceLocList(
                   data["language"].ToString(),
                   comId,
                   GetAuthKey())
                   );

                    if (Response["isSuccess"].ToString() == "Y")
                    {
                        List<SocialServiceLocListModel> resultList = JsonConvert.DeserializeObject<List<SocialServiceLocListModel>>(Response["resultList"].ToString());
                        if (resultList != null)
                        {
                            cacheContainer.Insert(CacheName, resultList, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            return resultList;
                        }
                        else
                        {
                            return Response;
                        }

                    }
                    else
                    {
                        return Response;
                    }
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }



        #region 其他API(非 Office文件)


        /// <summary>
        /// 國際研討會API
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetSeminarList(NameValueCollection postParams)
        {
            string CacheName = "GetSeminarList" + postParams["rows"].ToString() + postParams["page"].ToString() + postParams["TIME_HD_S_Year"].ToString();
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    dynamic data = null;

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://app.cgmh.org.tw/api/cgmerc/API_yth/SEMINARLIST/List.ashx");
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";

                    byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());

                    using (Stream reqStream = request.GetRequestStream())
                    {
                        reqStream.Write(byteArray, 0, byteArray.Length);
                    }
                    string responseStr = "";
                    //發出Request
                    using (WebResponse response = request.GetResponse())
                    {
                        using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            responseStr = sr.ReadToEnd();

                            JObject Response = JObject.Parse(responseStr);
                            if (int.Parse(Response["total"].ToString()) > 0)
                            {
                                List<SeminarListModel> resultList = JsonConvert.DeserializeObject<List<SeminarListModel>>(Response["rows"].ToString());
                                if (resultList != null && resultList.GetType().Name != "JObject")
                                {
                                    data = resultList;
                                }
                            }

                        }//end using  
                    }
                    cacheContainer.Insert(CacheName, data, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                    return data;
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 國際研討會API(內容)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public dynamic GetSeminar(NameValueCollection postParams)
        {

            string CacheName = "GetSeminar" + postParams["TITLEENGLISH"].ToString();
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    dynamic data = null;

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://app.cgmh.org.tw/api/cgmerc/API_yth/EDITOR/Load.ashx?modify=0&TITLEENGLISH=" + postParams["TITLEENGLISH"]);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";

                    byte[] byteArray = Encoding.UTF8.GetBytes(postParams.ToString());

                    using (Stream reqStream = request.GetRequestStream())
                    {
                        reqStream.Write(byteArray, 0, byteArray.Length);
                    }
                    string responseStr = "";
                    //發出Request
                    using (WebResponse response = request.GetResponse())
                    {
                        using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            responseStr = sr.ReadToEnd();
                            if (!string.IsNullOrEmpty(responseStr))
                            {
                                SeminarModel resultList = JsonConvert.DeserializeObject<SeminarModel>(responseStr);
                                data = resultList;
                            }


                        }//end using  
                    }
                    cacheContainer.Insert(CacheName, data, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                    return data;
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// 衛教文章 & 藥品綜合查詢 產出 eKey
        /// </summary>
        /// <returns></returns>
        public string BuildKey()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>()
            {
                {"times", DateTime.Now.ToString("yyyyMMddHHmmss")},
                {"keys", strkey},
            };
            string json = JsonConvert.SerializeObject(dictionary, Newtonsoft.Json.Formatting.None);
            byte[] bytes = Encoding.UTF8.GetBytes(json);
            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// 藥品綜合查詢
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public dynamic GetDrugQueryResult(Dictionary<string, string> postParams)
        {
            string CacheName = "GetDrugQueryResult" + GetCacheName(postParams);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    dynamic data = null;
                    string eKey = BuildKey();

                    postParams.Add("eKey", BuildKey());

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://webapp.cgmh.org.tw/stor/Service/GetDrugQueryResult.ashx");
                    request.Method = "POST";
                    request.ContentType = "application/json";

                    using (var reqStream = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = JsonConvert.SerializeObject(postParams, Newtonsoft.Json.Formatting.None);
                        reqStream.Write(json);
                        reqStream.Flush();
                        reqStream.Close();
                    }
                    string responseStr = "";
                    //發出Request
                    using (WebResponse response = request.GetResponse())
                    {
                        using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            responseStr = sr.ReadToEnd();

                            JObject ResponseJson = JObject.Parse(responseStr);
                            if (!string.IsNullOrEmpty(responseStr) && bool.Parse(ResponseJson["Status"].ToString()))
                            {
                                data = new Dictionary<string, string>();

                                data.Add("中藥", ResponseJson["Value"]["中藥"].ToString());
                                data.Add("西藥", ResponseJson["Value"]["西藥"].ToString());

                            }
                            else
                            {
                                data = ResponseJson;
                            }


                        }//end using  
                    }
                    cacheContainer.Insert(CacheName, data, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                    return data;
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// WS02 取得衛教文章類別與清單
        /// </summary>
        /// <param name="postParams"></param>
        /// <returns></returns>
        public dynamic GetHealthEduList(Dictionary<string, string> postParams)
        {
            string CacheName = "GetHealthEduList" + GetCacheName(postParams);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    dynamic data = null;
                    string eKey = BuildKey();

                    postParams.Add("eKey", BuildKey());

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://webapp.cgmh.org.tw/healthedu/Service/GetHealthEduList.ashx");
                    request.Method = "POST";
                    request.ContentType = "application/json";

                    using (var reqStream = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = JsonConvert.SerializeObject(postParams, Newtonsoft.Json.Formatting.None);
                        reqStream.Write(json);
                        reqStream.Flush();
                        reqStream.Close();
                    }
                    string responseStr = "";
                    //發出Request
                    using (WebResponse response = request.GetResponse())
                    {
                        using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            responseStr = sr.ReadToEnd();

                            JObject ResponseJson = JObject.Parse(responseStr);
                            if (!string.IsNullOrEmpty(responseStr) && bool.Parse(ResponseJson["Status"].ToString()))
                            {
                                List<HealthEduListModel> resultList = JsonConvert.DeserializeObject<List<HealthEduListModel>>(ResponseJson["Objects"].ToString());
                                data = resultList;
                            }
                            else
                            {
                                data = ResponseJson;
                            }


                        }//end using  
                    }
                    cacheContainer.Insert(CacheName, data, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                    return data;
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// WS02 取得衛教文章內容
        /// </summary>
        /// <param name="postParams"></param>
        /// <returns></returns>
        public dynamic GetHealthEduContent(Dictionary<string, string> postParams)
        {
            string CacheName = "GetHealthEduContent" + GetCacheName(postParams);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    dynamic data = null;
                    string eKey = BuildKey();

                    postParams.Add("eKey", BuildKey());

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://webapp.cgmh.org.tw/healthedu/Service/GetHealthEduContent.ashx");
                    request.Method = "POST";
                    request.ContentType = "application/json";

                    using (var reqStream = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = JsonConvert.SerializeObject(postParams, Newtonsoft.Json.Formatting.None);
                        reqStream.Write(json);
                        reqStream.Flush();
                        reqStream.Close();
                    }
                    string responseStr = "";
                    //發出Request
                    using (WebResponse response = request.GetResponse())
                    {
                        using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            responseStr = sr.ReadToEnd();

                            JObject ResponseJson = JObject.Parse(responseStr);
                            if (!string.IsNullOrEmpty(responseStr) && bool.Parse(ResponseJson["Status"].ToString()))
                            {
                                List<HealthEduContentModel> resultList = JsonConvert.DeserializeObject<List<HealthEduContentModel>>(ResponseJson["Objects"].ToString());
                                data = resultList;
                            }
                            else
                            {
                                data = ResponseJson;
                            }


                        }//end using  
                    }
                    cacheContainer.Insert(CacheName, data, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                    return data;
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }

        }


        /// <summary>
        /// WS02 取得衛教搜尋結果
        /// </summary>
        /// <param name="postParams"></param>
        /// <returns></returns>
        public dynamic GetHealthEduContent4Search(Dictionary<string, string> postParams)
        {
            string CacheName = "GetHealthEduContent4Search" + GetCacheName(postParams);
            try
            {
                if (cacheContainer.Get(CacheName) == null)
                {
                    dynamic data = null;
                    string eKey = BuildKey();

                    postParams.Add("eKey", BuildKey());

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("https://webapp.cgmh.org.tw/healthedu/Service/GetHealthEduContent4Search.ashx");
                    request.Method = "POST";
                    request.ContentType = "application/json";

                    using (var reqStream = new StreamWriter(request.GetRequestStream()))
                    {
                        string json = JsonConvert.SerializeObject(postParams, Newtonsoft.Json.Formatting.None);
                        reqStream.Write(json);
                        reqStream.Flush();
                        reqStream.Close();
                    }
                    string responseStr = "";
                    //發出Request
                    using (WebResponse response = request.GetResponse())
                    {
                        using (StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                        {
                            responseStr = sr.ReadToEnd();

                            JObject ResponseJson = JObject.Parse(responseStr);
                            if (!string.IsNullOrEmpty(responseStr) && bool.Parse(ResponseJson["Status"].ToString()))
                            {
                                List<HealthEduContent4SearchModel> resultList = JsonConvert.DeserializeObject<List<HealthEduContent4SearchModel>>(ResponseJson["Objects"].ToString());
                                data = resultList;
                            }
                            else
                            {
                                data = ResponseJson;
                            }


                        }//end using  
                    }
                    cacheContainer.Insert(CacheName, data, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);
                    return data;
                }
                else
                {
                    return cacheContainer.Get(CacheName);//自cache取出
                }
            }
            catch
            {
                return null;
            }


        }
        #endregion


      
    }

}