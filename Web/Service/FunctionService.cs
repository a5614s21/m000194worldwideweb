﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.Collections;
using System.Net.Mail;
using System.Runtime.Caching;
using System.Web.Caching;
using System.Text.RegularExpressions;
using Web.ServiceModels;
using Web.Controllers;
using System.IO;

namespace Web.Service
{
    public class FunctionService : Controller
    {
        // GET: FunctionService
            
             /// <summary>
             /// 儲存LOG
             /// </summary>
             /// <param name="ex"></param>
             /// <param name="files"></param>
            public static void saveLog(dynamic ex , string files)
            {
                string lines = "";
                string fileName = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Log/" + files;
                if (System.IO.File.Exists(fileName))
                {
                    StreamReader str = new StreamReader(fileName);
                    str.ReadLine();
                    lines = str.ReadToEnd();
                    str.Close();
                }

                lines += DateTime.Now.ToString() + "：Message => " + ex.Message + "，";
                lines += $"Main exception occurs {ex}.";
                lines += "\r\n";
                System.IO.File.WriteAllText(fileName, lines);
            }



            /// <summary>
            /// 秒數轉時分秒
            /// </summary>
            /// <param name="sec"></param>
            /// <returns></returns>
        public static string secToTime(string sec)
        {
            TimeSpan t = TimeSpan.FromSeconds(double.Parse(sec));

          return string.Format("{0:D2}:{1:D2}:{2:D2}",
                            t.Hours,
                            t.Minutes,
                            t.Seconds,
                            t.Milliseconds);


        }
        public static string getWebUrl()
        {
            string projeftNo = ConfigurationManager.ConnectionStrings["projeftNo"].ConnectionString;//專案編號
            System.Web.HttpContext context = System.Web.HttpContext.Current;

            string httpPath = "http";
            string port = "14574";
            if (context.Request.IsSecureConnection)
            {
                 httpPath = "https";
                port = "44307";
            }

            string webURL = httpPath+"://" + context.Request.ServerVariables["Server_Name"];

            if (context.Request.ServerVariables["Server_Name"] == "localhost")
            {             
                webURL = webURL + ":" + port;
            }
            if (context.Request.ServerVariables["Server_Name"].IndexOf("iis.youweb.tw") != -1)
            {
                webURL = httpPath+"://iis.youweb.tw/projects/public/"+ projeftNo + "/test";
            }
            if (context.Request.ServerVariables["Server_Name"].IndexOf("webapp.cgmh.org.tw") != -1)
            {
                webURL = httpPath + "://webapp.cgmh.org.tw/test";
            }
            return webURL;
        }


        public static bool HaveApiData(string Api , Dictionary<string, string> callApiData)
        {
            switch(Api)
            {
                case "GetSatisfactionList":


                    break;

            }

            return false;
        }


        /// <summary>
        /// 回傳判斷是否有組合之
        /// </summary>
        /// <param name="hospitals"></param>
        /// <returns></returns>
        public static List<hospitals> CheckHospitalsJoin(List<hospitals> hospitals )
        {
            List<string> isJoinID = new List<string>();
            if (hospitals != null && hospitals.Count > 0)
            {
                foreach (hospitals item in hospitals.FindAll(m => !string.IsNullOrEmpty(m.join_guid)).OrderBy(m => m.sortIndex).ToList())
                {
                    isJoinID.Add(item.join_guid);
                }
            }


            return hospitals.Where(m => !isJoinID.Contains(m.guid)).OrderBy(m => m.sortIndex).ToList();
        }


        /// <summary>
        /// 無限欄位JSON拆解
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static Dictionary<string, List<string>> InfinityJsonFormat(string content)
        {
            Dictionary<string, string> re = new Dictionary<string, string>();

        

            Dictionary<string, List<string>> tempVal = new Dictionary<string, List<string>>();

            if(!string.IsNullOrEmpty(content))
            {
                List<SiteadminController.infinityLayoutModel> jsonData = JsonConvert.DeserializeObject<List<SiteadminController.infinityLayoutModel>>(content);

                foreach (SiteadminController.infinityLayoutModel valItem in jsonData)
                {
                    tempVal.Add(valItem.key, valItem.val);
                }
            }
           
            
            return tempVal;
        }

        /// <summary>
        /// 格式化圖片路徑
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string FormatImagePath(string content)
        {
            return content.Replace("../", "")
                   .Replace("src=\"images", "src=\""+ getWebUrl() + "/images")
                   .Replace("src=\"/images", "src=\"" + getWebUrl() + "/images")
                   .Replace("src=\"Content/Upload/images", "src=\"" + getWebUrl() + "/Content/Upload/images")
                   .Replace("href=\"/tw", "href=\"" + getWebUrl() + "/tw")
                   .Replace("href=\"/en", "href=\"" + getWebUrl() + "/en")
                   ;
        }


        /// <summary>
        /// 回傳首頁相關新聞連結
        /// </summary>
        /// <param name="path"></param>
        /// <param name="Data"></param>
        /// <returns></returns>
        public static List<string> IndexNewsURL(string path , GlobalArticleModel Data, GlobalArticleListModel TopData = null)
        {
            List<string> reUrl = new List<string>();
            if(Data != null)
            {
                if (Data.paragraph != null && Data.paragraph.Count > 0)
                {
                    switch (Data.paragraph[0].type)
                    {
                        //文字
                        case "1":
                            reUrl.Add(path + "/" + Data.articleId);
                            reUrl.Add("_self");
                            break;
                        //圖及說明
                        case "2":
                            reUrl.Add(path + "/" + Data.articleId);
                            reUrl.Add("_self");
                            break;
                        //網址
                        case "3":
                            reUrl.Add(Data.paragraph[0].url);
                            reUrl.Add("_black");
                            break;
                        //影片
                        case "4":
                            reUrl.Add(Data.paragraph[0].movieUrl);
                            reUrl.Add("_black");
                            break;
                        //預設
                        default:
                            reUrl.Add(path + "/" + Data.articleId);
                            reUrl.Add("_self");
                            break;

                    }
                }
                else
                {
                    reUrl.Add(path + "/" + Data.articleId);
                    reUrl.Add("_self");
                }
            }
            else
            {
                if(TopData != null && TopData.articleId != null)
                {
                    reUrl.Add(path + "/" + TopData.articleId);
                    reUrl.Add("_self");
                }
                else
                {
                    reUrl.Add("javascript:viod(0);");
                    reUrl.Add("_self");
                }
              
            }
            
      

            return reUrl;

        }

        /// <summary>
        /// 縣市區域
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public static List<taiwan_city> TaiwanCitys(string city )
        {
            Model DB = new Model();
            List<taiwan_city> re = new List<taiwan_city>();

            if(city == "")
            {
                re = DB.taiwan_city.Where(m => m.lang == "tw").Where(m => m.lay == "1").ToList();
            }
            else
            {
                re = DB.taiwan_city.Where(m => m.lang == "tw").Where(m => m.city == city).Where(m => m.lay == "2").ToList();
            }


            return re;
        }


        /// <summary>
            /// 多圖處理
            /// </summary>
            /// <param name="pic"></param>
            /// <param name="pic_alt"></param>
            /// <returns></returns>
        public static Dictionary<String, Object> PluralImage(string pic , string pic_alt)
        {
            Dictionary<String, Object> reData = new Dictionary<String, Object>();
            List<string> pics = pic.Split(',').ToList();

            List<string> pics_alt = new List<string>();
 
            List<string> pics_alt_temp = new List<string>();

            if(pic_alt != null && pic_alt != "")
            {
                pics_alt_temp = pic_alt.Split('§').ToList();
            }
            int i = 0;
            foreach(var item in pics)
            {
                if (pic_alt != null && pic_alt != "" && pics_alt_temp[i] != null)
                {
                    pics_alt.Add(pics_alt_temp[i]);
                }
                else
                {
                    pics_alt.Add("　");
                }

              i++;
            }

            reData.Add("pic", pics);
            reData.Add("pic_alt", pics_alt);

            return reData;
        }


        /// <summary>
        /// SEO資料格式化
        /// </summary>
        /// <param name="web_Data"></param>
        /// <param name="title"></param>
        /// <param name="seo_keywords"></param>
        /// <param name="seo_description"></param>
        /// <returns></returns>
        public static Dictionary<String, String> SEO(Models.web_data web_Data , string title , string seo_keywords , string seo_description )
        {
            Dictionary<String, String> reData = new Dictionary<String, String>();

            reData.Add("title", web_Data.title);
            reData.Add("seo_keywords", web_Data.seo_keywords);
            reData.Add("seo_description", web_Data.seo_description);

            if(title != "")
            {
                reData["title"] = title + "|" + reData["title"].ToString();
            }

            if(seo_keywords != "")
            {
                reData["seo_keywords"] = seo_keywords;
            }

            if (seo_description != "")
            {
                reData["seo_description"] = seo_description;
            }

            return reData;
        }

        /// <summary>
        /// 取得語系黨
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static NameValueCollection getLangData(string lang)
        {
            try
            {
                if (lang != "" && lang.Length > 2)
                {
                    lang = "tw";
                }
                NameValueCollection langData = new NameValueCollection();
                if (lang != "")
                {
                    lang = "_" + lang;
                }
                ResourceManager rm = new ResourceManager("Web.App_GlobalResources.Resource" + lang, Assembly.GetExecutingAssembly());
                ResourceSet resourceSet = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
                foreach (DictionaryEntry entry in resourceSet)
                {
                    langData.Add(entry.Key.ToString(), entry.Value.ToString());
                }

                return langData;
            }
            catch
            {
                return null;
            }
         

        }

        /// <summary>
        /// 格式化檔案更新日期
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        public static string FileModifyTime(string files)
        {
            string re = System.DateTime.Parse(System.IO.File.GetLastWriteTime(files).ToString()).ToString("yyyyMMddHHmmss");
            return re;
        }

        /// <summary>
        /// GUID樣式
        /// </summary>
        /// <returns></returns>
        public static string getGuid()
        {
            string GuidType = ConfigurationManager.ConnectionStrings["GuidType"].ConnectionString;

            if(GuidType == "System")
            {
                return Guid.NewGuid().ToString();
            }
            else
            {
                return DateTime.Now.ToString("yyMMddHHmmssff");
            }

        }

        /// <summary>
        /// 表單JSON轉換
        /// </summary>
        /// <param name="jsonText"></param>
        /// <returns></returns>
        public static NameValueCollection reSubmitFormDataJson(string jsonText)
        {

            NameValueCollection reData = new NameValueCollection();


            dynamic dynObj = JsonConvert.DeserializeObject(jsonText);
            foreach (var item in dynObj)
            {
                if ((string)item.name != "")
                {
                    if (reData[(string)item.name] == null)
                    {
                        string val = item.value;
                        reData.Add((string)item.name, val);
                    }
                    else
                    {
                        string val = reData[(string)item.name] + "," + item.value;
                        reData.Remove((string)item.name);
                        reData.Add((string)item.name, val);
                    }

                }

            }


            return reData;

        }


        
        /// <summary>
        /// md5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string md5(string str)
        {
            string reStr = "";


            byte[] Original = Encoding.Default.GetBytes(str); //將字串來源轉為Byte[] 
            MD5 s1 = MD5.Create(); //使用MD5 
            byte[] Change = s1.ComputeHash(Original);//進行加密 
            reStr = Convert.ToBase64String(Change);//將加密後的字串從byte[]轉回string


            return reStr;
        }

        /// <summary>
        /// 後台登入判斷
        /// </summary>
        /// <param name="sysUsername"></param>
        /// <param name="adID"></param>
        /// <returns></returns>
        public static bool systemUserCheck()
        {
            Model DB = new Model();

            bool re = false;
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie
            string username = "";


            if (context.Session["sysUsername"] != null || context.Request.Cookies["sysLogin"] != null)
            {
                if (adminCathType == "Session")
                {
                    if (context.Session["sysUsername"] != null && context.Session["sysUsername"].ToString() != "")
                    {
                        re = true;
                        username = System.Web.HttpContext.Current.Session["sysUsername"].ToString();
                    }
                }
                else
                {

                    if (context.Request.Cookies["sysLogin"] != null)
                    {
                        re = true;
                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');
                        username = context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", ""));
                    }
                }

                if (username != "")
                {
                    //更新登入日期
                    var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").FirstOrDefault();
                    saveData.logindate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    DB.SaveChanges();

                    getAdminPermissions(username);//權限
                }

            }


            //IP判斷
            if (re == true && username != "sysadmin")
            {
                /*
                SOGOEntities DB = new SOGOEntities();
                var ipLock = DB.sogo_ip_lock.Where(m => m.status == "Y").ToList();

                if (ipLock.Count > 0)
                {
                    string ip = GetIP();
                    var ipLock2 = DB.sogo_ip_lock.Where(m => m.status == "Y").Where(m => m.ip == ip).ToList();
                    if (ipLock2.Count > 0)
                    {
                        re = true;
                    }
                    else
                    {
                        re = false;
                    }

                }*/

            }



            return re;

        }

        /// <summary>
        /// 回傳登入者帳號
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, String> ReUserData()
        {

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie
          

               Dictionary<String, String> userData = new Dictionary<string, string>();
           
                if (adminCathType == "Session")
                {
                    if (System.Web.HttpContext.Current.Session["sysUsername"] != null && System.Web.HttpContext.Current.Session["sysUsername"].ToString() != "")
                    {

                    userData.Add("username", System.Web.HttpContext.Current.Session["sysUsername"].ToString());
                    userData.Add("guid", System.Web.HttpContext.Current.Session["sysUserGuid"].ToString());
                    }
                }
                else
                {

                    if (context.Request.Cookies["sysLogin"] != null)
                    {                       
                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');


                    userData.Add("username", context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", "").ToString()));
                    userData.Add("guid", context.Server.HtmlEncode(tempAccount[1].Replace("sysUserGuid=", "").ToString()));
                    }
                }

            

            return userData;
        }
      
        /// <summary>
        /// 角色權限
        /// </summary>
        /// <param name="role_guid"></param>
        /// <returns></returns>
        public static NameValueCollection role_permissions(string role_guid)
        {
            Model DB = new Model();
            string site = ConfigurationManager.ConnectionStrings["site"].ConnectionString;
            var data = DB.role_permissions.Where(m => m.role_guid == role_guid).ToList();

            NameValueCollection re = new NameValueCollection();
            foreach (var item in data)
            {
                re.Add(item.permissions_guid , item.permissions_status);
            }
                return re;
        }
        
     
        /// <summary>
        /// 設定權限群組
        /// </summary>
        /// <param name="username"></param>
        public static void getAdminPermissions(string username) {

            Model DB = new Model();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
           

            Dictionary<String, Object> permissions = new Dictionary<String, Object>();
            List<String> permissionsTop = new List<String>();

            //非系統權限
            if (username != "sysadmin")
            {
                //取得權限
                var tempUser = DB.user.Where(m => m.username == username).Select(a => new { role_guid = a.role_guid }).FirstOrDefault();
                if (tempUser != null)
                {

                    string role_guid = tempUser.role_guid;
                    var roleData = DB.roles.Where(m => m.guid == role_guid.ToString()).FirstOrDefault();
                    var perData = DB.role_permissions.Where(m => m.role_guid == role_guid.ToString()).ToList();

                    if (perData.Count > 0)
                    {
                        foreach (var item in perData)
                        {
                            string systemMenuGuid = item.permissions_guid;

                            var tempData = DB.system_menu.Where(m => m.guid == systemMenuGuid).Select(a => new { category = a.category , tables = a.tables }).FirstOrDefault();
                            if (tempData != null)
                            {
                                if(!string.IsNullOrEmpty(getPrevTable(tempData.tables)))
                                {
                                        string tables = tempData.tables;                                  
                                        var tempData2 = DB.system_menu.Where(m => m.tables == tables).Select(a => new { guid = a.guid }).FirstOrDefault();

                                    
                                        if (tempData2 != null && !string.IsNullOrEmpty(tempData2.guid))
                                        {
                                            string permissions_guid2 = tempData2.guid;

                                            var perData2 = DB.role_permissions.Where(m => m.role_guid == role_guid.ToString()).Where(m => m.permissions_guid == permissions_guid2).FirstOrDefault();

                                            permissions.Add(item.permissions_guid, perData2.permissions_status);
                                        }
                                        else
                                        {
                                            permissions.Add(item.permissions_guid, item.permissions_status);
                                        }
                                
                                  
                                }
                                else
                                {
                                    if(!permissions.ContainsKey(item.permissions_guid))
                                    {
                                        permissions.Add(item.permissions_guid, item.permissions_status);
                                    }
                                   
                                }
                            

                                


                                if (permissionsTop.IndexOf(tempData.category) == -1)
                                {
                                    permissionsTop.Add(tempData.category);
                                }
                            }

                        }

                 

                    }


                    var menuData = DB.system_menu.Where(m => m.category != "0").Select(a => new { category = a.category, guid = a.guid, tables = a.tables }).ToList();

                    if (menuData.Count > 0)
                    {
                        foreach (var item in menuData)
                        {
                            if (!string.IsNullOrEmpty(getPrevTable(item.tables)) && !permissions.ContainsKey(item.guid))
                            {
                                permissions.Add(item.guid, "F");
                                if (permissionsTop.IndexOf(item.category) == -1)
                                {
                                    permissionsTop.Add(item.category);
                                }
                            }

                        }
                     
                    }


                    context.Session.Remove("permissions");
                    context.Session.Remove("permissionsTop");

                    context.Session.Add("permissions", permissions);
                    context.Session.Add("permissionsTop", permissionsTop);

                }
            }
            else
            {
                var menuData = DB.system_menu.Where(m => m.category != "0").Select(a => new { category = a.category , guid=a.guid }).ToList();

                if (menuData.Count > 0)
                {
                    foreach (var item in menuData)
                    {

                            permissions.Add(item.guid, "F");
                           if (permissionsTop.IndexOf(item.category) == -1)
                            {
                                permissionsTop.Add(item.category);
                            }

                    }

                    context.Session.Remove("permissions");
                    context.Session.Remove("permissionsTop");

                    context.Session.Add("permissions", permissions);
                    context.Session.Add("permissionsTop", permissionsTop);
                }



            }



        }


        public static string getPrevTable(string reTables)
        {      
            if (reTables == "cases_city_history")
            {
                reTables = "cases_city";
            }
            else if (reTables == "progress_info")
            {
                reTables = "progress_cases";
            }
            else if(reTables == "medical_other_data")
            {
                reTables = "medical_other";
            }
            else if (reTables == "qas_data")
            {
                reTables = "qas";
            }
            else if (reTables == "historys_data")
            {
                reTables = "historys";
            }
            else if (reTables == "recruits_data")
            {
                reTables = "recruits";
            }
            else if (reTables == "res_resource_data")
            {
                reTables = "res_resource";
            }
            else if (reTables == "res_resource_data")
            {
                reTables = "res_resource";
            }
            else if (reTables == "recruits_hospitals")
            {
                reTables = "edu_resource";
            }
           else if (reTables == "edu_resource_data")
            {
                reTables = "edu_resource";
            }

            else if (reTables == "hospitals_advisory")
            {
                reTables = "hospitals";
            }
            else if (reTables == "index_info_data")
            {
                reTables = "index_info";
            }
            else if (reTables == "about_overview_data")
            {
                reTables = "about_overview";
            }
            else if (reTables == "quality_result_data")
            {
                reTables = "quality_result";
            }
            else
            {
                reTables = "";

            }

            return reTables;
        }

        /// <summary>
        /// 取得頁碼
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="Allpage"></param>
        /// <param name="page"></param>
        /// <param name="inPage"></param>
        /// <param name="totalNum"></param>
        /// <param name="addpach"></param>
        /// <returns></returns>
        public static string getPageNum(string URL, int Allpage, int page, int totalNum, string addpach)
        {

            //addpach 附屬參數

            string PageList = "";

            int countPage = Allpage;
            int startPage = 1;
            int pageSec = Convert.ToInt16(Math.Ceiling((double)page / 5));

            if (Allpage > 1)
            {

                if (Allpage > 5)
                {
                    startPage = pageSec * 5 - 4;
                    if (startPage < Allpage)
                    {
                        countPage = startPage + 4;
                    }

                    if (page >= Allpage)
                    {
                        countPage = Allpage;
                    }
                }


                //上一頁
                if (Allpage > 1)
                {
                    if (page == 1)
                    {
                       // PageList += "<li class=\"prev\"><a class=\"icon-chevron-left\" href=\"javascript:void(0)\"></a></li>";
                    }
                    else
                    {
                      
                         PageList += "<li><a href=\"" + URL + "?page=" + (page - 1).ToString() + addpach + "\"><i class=\"icon-angle-left\"></i></a></li>";
                         
                    }
                }

                for (int p = startPage; p <= countPage; p++)
                {

                    if (p == page)
                    {

                        PageList += "<li><a  class=\"active\" href=\"javascript:void(0)\">" + p.ToString() + "</a></li>";

                    }
                    else
                    {
                        PageList += "<li><a href=\"" + URL + "?page=" + p.ToString() + addpach + "\">" + p.ToString() + "</a></li>";
                    }

                }


                if ((Allpage - ((pageSec - 1) * 5)) >= 5)
                {
                    PageList += " <li><a class=\"page-more\" href=\"javascript:;\">...</a></li><li><a href=\"" + URL + "?page=" + Allpage.ToString() + addpach + "\">" + Allpage.ToString() + "</a></li>";
                }

                //下一頁
                if (Allpage > 1)
                {
                    if (Allpage > page)
                    {
                        PageList += "<li><a href=\"" + URL + "?page=" + (page + 1).ToString() + addpach + "\"><i class=\"icon-angle-right\"></i></a></li>";
                    }
                    else
                    {
                      //  PageList += "<li class=\"next\"><a href=\"#\"><i class=\"icon-right-open-mini\"></i></a></li>";
                      //  PageList += "<li class=\"next_all hidden\"><a href=\"#\"><i class=\"icon-angle-double-right\"></i></a></li>";

                    }
                }


            }



            return PageList;

        }


        /// <summary>
        /// 發送信件
        /// </summary>
        /// <param name="MailList"></param>
        /// <param name="content"></param>
        /// <param name="defLang"></param>
        public static void sendMail(List<string> MailList , string contentID, string defLang , string webURL, NameValueCollection form)
        {
            Model DB = new Model();

            web_data web_Data = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊
            Guid sysGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");

            system_data system_data = DB.system_data.Where(model => model.guid == sysGuid).FirstOrDefault();//取得網站基本資訊
            mail_contents mail_contents = DB.mail_contents.Where(m => m.guid == contentID).Where(model => model.lang == defLang).FirstOrDefault();

            string title = mail_contents.title;
            string content = mail_contents.content;

            content = content.Replace("{[title]}", title);
            content = content.Replace("{[websiteName]}", web_Data.title);
            content = content.Replace("{[sysName]}", "管理者");
            content = content.Replace("{[websiteUrl]}", webURL);
            content = content.Replace("{[logo]}", webURL + "/" + system_data.logo);
            content = content.Replace("{[date]}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            foreach (var key in form.AllKeys)
            {
                content = content.Replace("{[" + key + "]}", form[key].ToString());
            }


            var smtpData = DB.smtp_data.Where(m => m.guid == "b9732bfe-238d-4e4e-9114-8e6d30c34022").FirstOrDefault();

      

            string re = "";
                    MailMessage msg = new MailMessage();
                    //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
                    msg.To.Add(string.Join(",", MailList.ToArray()));
                    msg.From = new MailAddress(smtpData.from_email.ToString(), web_Data.title, System.Text.Encoding.UTF8);
                    //郵件標題 
                    msg.Subject = title.ToString();
                    //郵件標題編碼  
                    msg.SubjectEncoding = System.Text.Encoding.UTF8;
                    //郵件內容
                    msg.Body = content;
                    msg.IsBodyHtml = true;
                    msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼 
                    msg.Priority = MailPriority.Normal;//郵件優先級 
                                                       //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port 

                    SmtpClient MySmtp = new SmtpClient(smtpData.host.ToString(), int.Parse(smtpData.port.ToString()));
                    //設定你的帳號密碼
                    MySmtp.Credentials = new System.Net.NetworkCredential(smtpData.username.ToString(), smtpData.password.ToString());
                    //Gmial 的 smtp 使用 SSL
                    if (smtpData.smtp_auth.ToString() == "Y")
                    {
                        MySmtp.EnableSsl = true;
                    }
                    else
                    {
                        MySmtp.EnableSsl = false;

                    }
                    try
                    {
                        MySmtp.Send(msg);
                    }
                    catch (SmtpFailedRecipientsException ex)
                    {
                        Console.WriteLine("Exception caught in RetryIfBusy(): {0}", ex.ToString());
                    }

          

        }
        /// <summary>
        /// 下層選單選取
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static string PrevActivePath(string tables, string guid, string lang)
        {
            string re = "";
            Model DB = new Model();


            switch(tables)
            {
                case "medical_other":
                    if(guid != "")
                    {
                        var data = DB.medical_other.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = tables + "/list/" + data.category;
                        }

                    }
                 

                    break;
                case "qas":

                    if (guid != "")
                    {
                        var data = DB.qas.Where(m => m.guid == guid).FirstOrDefault();
                        if (data != null)
                        {
                            re = tables + "/list/" + data.category;
                        }

                    }

                    break;
            }

            return re;
        }

        /// <summary>
        /// 移除HTML TAG
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string RemoveHtmlTag(string str)
        {
         
            str = Regex.Replace(str, "<[^>]*>", "", RegexOptions.IgnoreCase);
            //以防有不正常結尾-->EX:"文字 <a styl"
            str = Regex.Replace(str, "<[^>]*", "", RegexOptions.IgnoreCase);

            return str;
        }

        /// <summary>
        /// 格式化搜尋連結
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string FormatSearchUrl(full_search item , string urlRoot , string Lang)
        {

            string url = "javascript:void(0);";

            if (item.url.IndexOf("http") == -1)
            {
                url = urlRoot + Lang + "/" + item.url + "/" + item.guid;
            }

            if(item.tables == "notes_data")
            {
                switch(item.guid)
                {
                    case "1":
                        url = urlRoot + Lang + "/Systems/Area";
                        break;
                    case "2":
                        url = urlRoot + Lang + "/Research/Education#service";
                        break;
                    case "3":
                        url = urlRoot + Lang + "/SocialService/Service#1";
                        break;
                    case "4":
                        url = urlRoot + Lang + "/Services/Questionnaire";
                        break;
                    case "5":
                        url = urlRoot + Lang + "/Home/Privacy";
                        break;
                }
            }

            if (item.tables == "abouts")
            {
               
                switch (item.category)
                {
                    case "1":
                        url = urlRoot + Lang + "/Systems/About";
                        break;
                    case "2":
                        url = urlRoot + Lang + "/Systems/Security";
                        break;
              
                   
                }
            }


            return url;
        }

        /// <summary>
        /// 日期轉換
        /// </summary>
        /// <param name="week"></param>
        /// <param name="Lang"></param>
        /// <returns></returns>
        public static string WeekToString(int week , string Lang)
        {
            string re = "";
            if(Lang == "tw")
            {
                switch(week)
                {
                    case 0:
                        re = "日";
                        break;
                    case 1:
                        re = "一";
                        break;
                    case 2:
                        re = "二";
                        break;
                    case 3:
                        re = "三";
                        break;
                    case 4:
                        re = "四";
                        break;
                    case 5:
                        re = "五";
                        break;
                    case 6:
                        re = "六";
                        break;
                }
            }
            else
            {
                switch (week)
                {
                    case 0:
                        re = "Sun";
                        break;
                    case 1:
                        re = "Mon";
                        break;
                    case 2:
                        re = "Tue";
                        break;
                    case 3:
                        re = "Wed";
                        break;
                    case 4:
                        re = "Thur";
                        break;
                    case 5:
                        re = "Fri";
                        break;
                    case 6:
                        re = "Sat";
                        break;
                }
            }

            return re;

        }


        /// <summary>
        /// 發問問題列表參數格式化
        /// </summary>
        public class forumData
        {
            public string guid { get; set; } // or whatever
            public string title { get; set; }  // or whatever

            public string forum_categoryguid { get; set; }  // or whatever
            public string forum_subcategoryguid { get; set; }  // or whatever
            public string ad_id { get; set; }  // or whatever

            public string status { get; set; }  // or whatever

             public DateTime create_date { get; set; }  // or whatever

            public string permission { get; set; }  // or whatever

            public string look { get; set; }  // or whatever

            public string thumbsup { get; set; }  // or whatever

            public string ad_name { get; set; }  // or whatever
            public string ad_mail { get; set; }  // or whatever
            public string comment { get; set; }  // or whatever

            public string ad_imgurl { get; set; }  // or whatever
            public string ad_department { get; set; }  // or whatever

            public string ad_ext_num { get; set; }  // or whatever
            public string top_guid { get; set; }  // or whatever
        }

     

        /// <summary>
        /// 取得真實IP
        /// </summary>
        /// <returns></returns>
        public static string GetIP()
        {
            string ip;
            string trueIP = string.Empty;

            //先取得是否有經過代理伺服器
            ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ip))
            {
                //將取得的 IP 字串存入陣列
                string[] ipRange = ip.Split(',');

                //比對陣列中的每個 IP
                for (int i = 0; i < ipRange.Length; i++)
                {
                    //剔除內部 IP 及不合法的 IP 後，取出第一個合法 IP
                    if (ipRange[i].Trim().Substring(0, 3) != "10." &&
                        ipRange[i].Trim().Substring(0, 7) != "192.168" &&
                        ipRange[i].Trim().Substring(0, 7) != "172.16." &&
                        CheckIP(ipRange[i].Trim()))
                    {
                        trueIP = ipRange[i].Trim();
                        break;
                    }
                }

            }
            else
            {
                //沒經過代理伺服器，直接使用 ServerVariables["REMOTE_ADDR"]
                //並經過 CheckIP( ) 的驗證
                trueIP = CheckIP(System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]) ?
                     System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] : "";
            }

            return trueIP;
        }

        public static bool CheckIP(string strPattern)
        {
            // 繼承自：System.Text.RegularExpressions
            // regular: ^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$
            Regex regex = new Regex("^\\d{1,3}[\\.]\\d{1,3}[\\.]\\d{1,3}[\\.]\\d{1,3}$");
            Match m = regex.Match(strPattern);

            return m.Success;
        }

        /// <summary>
        /// 產生亂碼
        /// </summary>
        public class RandomPassword
        {
            // Define default min and max password lengths.
            private static int DEFAULT_MIN_PASSWORD_LENGTH = 8;
            private static int DEFAULT_MAX_PASSWORD_LENGTH = 10;

            // Define supported password characters divided into groups.
            // You can add (or remove) characters to (from) these groups.
            private static string PASSWORD_CHARS_LCASE = "abcdefgijkmnpqrstwxyz";
            private static string PASSWORD_CHARS_UCASE = "abcdefgijkmnpqrstwxyz";
            private static string PASSWORD_CHARS_NUMERIC = "23456789";
            // private static string PASSWORD_CHARS_SPECIAL = "*$-+?_&=!%{}/";

            /// <summary>
            /// Generates a random password.
            /// </summary>
            /// <returns>
            /// Randomly generated password.
            /// </returns>
            /// <remarks>
            /// The length of the generated password will be determined at
            /// random. It will be no shorter than the minimum default and
            /// no longer than maximum default.
            /// </remarks>
            public static string Generate()
            {
                return Generate(DEFAULT_MIN_PASSWORD_LENGTH,
                                DEFAULT_MAX_PASSWORD_LENGTH);
            }

            /// <summary>
            /// Generates a random password of the exact length.
            /// </summary>
            /// <param name="length">
            /// Exact password length.
            /// </param>
            /// <returns>
            /// Randomly generated password.
            /// </returns>
            public static string Generate(int length)
            {
                return Generate(length, length);
            }

            /// <summary>
            /// Generates a random password.
            /// </summary>
            /// <param name="minLength">
            /// Minimum password length.
            /// </param>
            /// <param name="maxLength">
            /// Maximum password length.
            /// </param>
            /// <returns>
            /// Randomly generated password.
            /// </returns>
            /// <remarks>
            /// The length of the generated password will be determined at
            /// random and it will fall with the range determined by the
            /// function parameters.
            /// </remarks>
            public static string Generate(int minLength,
                                          int maxLength)
            {
                // Make sure that input parameters are valid.
                if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                    return null;

                // Create a local array containing supported password characters
                // grouped by types. You can remove character groups from this
                // array, but doing so will weaken the password strength.
                char[][] charGroups = new char[][]
        {
            PASSWORD_CHARS_LCASE.ToCharArray(),
            PASSWORD_CHARS_UCASE.ToCharArray(),
            PASSWORD_CHARS_NUMERIC.ToCharArray(),
            //PASSWORD_CHARS_SPECIAL.ToCharArray()
        };

                // Use this array to track the number of unused characters in each
                // character group.
                int[] charsLeftInGroup = new int[charGroups.Length];

                // Initially, all characters in each group are not used.
                for (int i = 0; i < charsLeftInGroup.Length; i++)
                    charsLeftInGroup[i] = charGroups[i].Length;

                // Use this array to track (iterate through) unused character groups.
                int[] leftGroupsOrder = new int[charGroups.Length];

                // Initially, all character groups are not used.
                for (int i = 0; i < leftGroupsOrder.Length; i++)
                    leftGroupsOrder[i] = i;

                // Because we cannot use the default randomizer, which is based on the
                // current time (it will produce the same "random" number within a
                // second), we will use a random number generator to seed the
                // randomizer.

                // Use a 4-byte array to fill it with random bytes and convert it then
                // to an integer value.
                byte[] randomBytes = new byte[4];

                // Generate 4 random bytes.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                rng.GetBytes(randomBytes);

                // Convert 4 bytes into a 32-bit integer value.
                int seed = BitConverter.ToInt32(randomBytes, 0);

                // Now, this is real randomization.
                Random random = new Random(seed);

                // This array will hold password characters.
                char[] password = null;

                // Allocate appropriate memory for the password.
                if (minLength < maxLength)
                    password = new char[random.Next(minLength, maxLength + 1)];
                else
                    password = new char[minLength];

                // Index of the next character to be added to password.
                int nextCharIdx;

                // Index of the next character group to be processed.
                int nextGroupIdx;

                // Index which will be used to track not processed character groups.
                int nextLeftGroupsOrderIdx;

                // Index of the last non-processed character in a group.
                int lastCharIdx;

                // Index of the last non-processed group.
                int lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

                // Generate password characters one at a time.
                for (int i = 0; i < password.Length; i++)
                {
                    // If only one character group remained unprocessed, process it;
                    // otherwise, pick a random character group from the unprocessed
                    // group list. To allow a special character to appear in the
                    // first position, increment the second parameter of the Next
                    // function call by one, i.e. lastLeftGroupsOrderIdx + 1.
                    if (lastLeftGroupsOrderIdx == 0)
                        nextLeftGroupsOrderIdx = 0;
                    else
                        nextLeftGroupsOrderIdx = random.Next(0,
                                                             lastLeftGroupsOrderIdx);

                    // Get the actual index of the character group, from which we will
                    // pick the next character.
                    nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];

                    // Get the index of the last unprocessed characters in this group.
                    lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;

                    // If only one unprocessed character is left, pick it; otherwise,
                    // get a random character from the unused character list.
                    if (lastCharIdx == 0)
                        nextCharIdx = 0;
                    else
                        nextCharIdx = random.Next(0, lastCharIdx + 1);

                    // Add this character to the password.
                    password[i] = charGroups[nextGroupIdx][nextCharIdx];

                    // If we processed the last character in this group, start over.
                    if (lastCharIdx == 0)
                        charsLeftInGroup[nextGroupIdx] =
                                                  charGroups[nextGroupIdx].Length;
                    // There are more unprocessed characters left.
                    else
                    {
                        // Swap processed character with the last unprocessed character
                        // so that we don't pick it until we process all characters in
                        // this group.
                        if (lastCharIdx != nextCharIdx)
                        {
                            char temp = charGroups[nextGroupIdx][lastCharIdx];
                            charGroups[nextGroupIdx][lastCharIdx] =
                                        charGroups[nextGroupIdx][nextCharIdx];
                            charGroups[nextGroupIdx][nextCharIdx] = temp;
                        }
                        // Decrement the number of unprocessed characters in
                        // this group.
                        charsLeftInGroup[nextGroupIdx]--;
                    }

                    // If we processed the last group, start all over.
                    if (lastLeftGroupsOrderIdx == 0)
                        lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                    // There are more unprocessed groups left.
                    else
                    {
                        // Swap processed group with the last unprocessed group
                        // so that we don't pick it until we process all groups.
                        if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                        {
                            int temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                            leftGroupsOrder[lastLeftGroupsOrderIdx] =
                                        leftGroupsOrder[nextLeftGroupsOrderIdx];
                            leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                        }
                        // Decrement the number of unprocessed groups.
                        lastLeftGroupsOrderIdx--;
                    }
                }

                // Convert password characters into a string and return the result.
                return new string(password);
            }
        }

        /// <summary>
        /// Illustrates the use of the RandomPassword class.
        /// </summary>
        public class RandomPasswordTest
        {
            /// <summary>
            /// The main entry point for the application.
            /// </summary>
            [STAThread]
            static void Main(string[] args)
            {
                // Print 100 randomly generated passwords (8-to-10 char long).
                for (int i = 0; i < 100; i++)
                    Console.WriteLine(RandomPassword.Generate(8, 10));
            }
        }


        public static string ReplaceRN(string data)
        {
            string result = "";
            string[] Array = Regex.Split(data, "rn");
            var num = 0;
            foreach(var item in Array)
            {
                var last = item.Substring(item.Length - 1, 1);
                var first = "";
                if((num + 1) >= Array.Length)
                {
                    first = " ";
                }
                else
                {
                    first = Array[num + 1].Substring(0, 1);
                }
                

                if((num + 1) < Array.Length)
                {
                    if (last != " " && first != " ")
                    {
                        result += item + "<br>";
                    }
                    else
                    {
                        result += item + "rn";
                    }
                }
                else
                {
                    result += item + "<br>";
                }
                num++;
            }

            return result;
        }


    }



}