﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository
{
    public class indexInfoRepository : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
        public static Dictionary<String, Object> colFrom(dynamic data = null)
        {

            string guid = "";
            if (data != null && data.ContainsKey("tw"))
            {
                guid = data["tw"]["guid"];
            }
          

            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '頁面名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("subject", "[{'subject': '標語','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("notes", "[{'subject': '敘述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            if (guid == "1")
            {
                main.Add("content", "[{'subject': '醫療團隊','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
                main.Add("content2", "[{'subject': '就醫指南','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
                main.Add("content3", "[{'subject': '線上服務','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            }
            if (guid == "2")
            {
                main.Add("content", "[{'subject': '創新研究','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
                main.Add("content2", "[{'subject': '醫學教育','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
                main.Add("content3", "[{'subject': '會議與申請','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            }
            if (guid == "3")
            {
                main.Add("content", "[{'subject': '關於⾧庚','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
                main.Add("content2", "[{'subject': '醫療體系','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
                main.Add("content3", "[{'subject': '人員招募','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            }
             main.Add("seo_keywords", "[{'subject': 'SEO關鍵字','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("seo_description", "[{'subject': 'SEO敘述','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

      
            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();
          
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;

        }


        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");          
            re.Add("info", "內容摘要");
            re.Add("button", "詳細內容");
            //re.Add("sortIndex", "排序");
            re.Add("create_date", "日期");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "create_date");
            re.Add("orderByType", "asc");

            return re;
        }


    }
}