﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository
{
    public class webDataRepository : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("title", "[{'subject': '網站名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("url", "[{'subject': '網站網址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("name", "[{'subject': '聯絡人姓名','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("phone", "[{'subject': '電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("ext_num", "[{'subject': '分機','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            main.Add("fax", "[{'subject': '傳真','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("servicemail", "[{'subject': '客服信箱','type': 'email','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("address", "[{'subject': '地址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("register_url", "[{'subject': '即時掛號連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("reference_url", "[{'subject': '看哪一科連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("symptom_url", "[{'subject': '病症參考連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("sp_url", "[{'subject': '特色醫療中心連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("International_url", "[{'subject': '國際醫療連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("health_url", "[{'subject': '健康促進連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("medical_url", "[{'subject': '長庚醫訊連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("care_url", "[{'subject': '預立醫療照護連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");

            media.Add("csr_url", "[{'subject': '永續經營連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");

            media.Add("refs_url", "[{'subject': '轉診作業連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("webwork_url", "[{'subject': '健康存摺上傳連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("fidrug_url", "[{'subject': '藥品引進資訊連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");
            media.Add("material_url", "[{'subject': '材料資訊連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");

            media.Add("department_url", "[{'subject': '社服處官網','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");

            media.Add("donate1_url", "[{'subject': '補助與捐贈連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");

            media.Add("en_url", "[{'subject': '英文版網址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");

            media.Add("share_url", "[{'subject': '醫病共享決策連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請輸入包含https://或http://之完整網址</small>','useLang':'N'}]");



            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();

         
            #endregion






            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

        

            return fromData;

        }


       


    
    }
}