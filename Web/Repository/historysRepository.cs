﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository
{
    public class historysRepository : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("year", "[{'subject': '西元年','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");        
           // main.Add("content", "[{'subject': '內容','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
           // main.Add("seo_keywords", "[{'subject': 'SEO關鍵字','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("seo_description", "[{'subject': 'SEO敘述','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            media.Add("pic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高345 x 335 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");


            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();
          
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;

        }


        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");          
            re.Add("year", "西元年");
            re.Add("button", "詳細內容");
           // re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "year");
            re.Add("orderByType", "desc");

            return re;
        }


    }
}