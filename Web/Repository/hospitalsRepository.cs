﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository
{
    public class hospitalsRepository : Controller
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '院區名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("hospitalID", "[{'subject': 'API hospitalID','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");
            main.Add("category", "[{'subject': '所屬分類','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'hospital_category','useLang':'N'}]");


            main.Add("address", "[{'subject': '地址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("tel", "[{'subject': '電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("fax", "[{'subject': '傳真','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("bedQty", "[{'subject': '病床總床數','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("doctorQty", "[{'subject': '專科醫師人數','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("medicalQty", "[{'subject': '醫事人員數','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("profile", "[{'subject': '院區簡介','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("website", "[{'subject': '院區官網網址','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

        
        
            main.Add("join_guid", "[{'subject': '合併院區','type': 'select','default': '','class': 'col-lg-10','required': '','notes': '','inherit':'hospitals','useLang':'N'}]");
            main.Add("join_title", "[{'subject': '合併顯示標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");


            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("pic", "[{'subject': '列表圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高201 x 201 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            // media.Add("video", "[{'subject': '影片上傳','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','filetype': 'video/mp4','multiple': 'N','useLang':'Y'}]");
            media.Add("traffic_url", "[{'subject': '交通資訊連結','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            media.Add("map_code", "[{'subject': 'GoogleMap嵌入碼','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");
            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion



            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("view_type", "[{'subject': '資料取得來源','type': 'radio','defaultVal': 'api','classVal': 'col-lg-10','required': '','notes': '','data':'API取得/後台取得','Val':'api/web','useLang':'N'}]");
            other.Add("website_type", "[{'subject': '詳細資訊顯示方式','type': 'radio','defaultVal': 'info','classVal': 'col-lg-10','required': '','notes': '','data':'內頁/超連結','Val':'info/url','useLang':'N'}]");

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': '排序','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;

        }


        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("pic", "圖片");
            re.Add("info", "內容摘要");
            re.Add("category", "分類");
           // re.Add("button", "醫療諮詢資訊");
            re.Add("modifydate", "異動日期");
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }


    }
}