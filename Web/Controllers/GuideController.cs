﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;

namespace Web.Controllers
{
    public class GuideController : BaseController
    {
        // GET: Guide
        public ActionResult Index()
        {
            ViewBag.Action = "Guide";
            Title = ViewBag.ResLang["就醫指南"]+"|";

            List<medical_guide> Models = DB.medical_guide.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();
            if (Models.Count > 0)
            {
                Data = Models;
            }

            List<medical_other> medical_other = DB.medical_other.Where(m => m.lang == defLang).Where(m => m.title != "").Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.Data2 = null;
            if (medical_other.Count > 0)
            {
                ViewBag.Data2 = medical_other;
            }
           
            List<qas> Qas = DB.qas.Where(m => m.lang == defLang).Where(m => m.title != "").Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            ViewBag.Qas = null;
            if (Qas.Count > 0)
            {
                ViewBag.Qas = Qas;
            }


            return View();
        }

        public ActionResult Info()
        {
            ViewBag.Action = "Guide";

            ViewBag.pathTitle = "";

            if (ID != "")
            {
                switch(ID)
                {
                    case "1":
                        Title = ViewBag.ResLang["門診"] + "|" + ViewBag.ResLang["就醫指南"] + "|";
                        ViewBag.pathTitle = ViewBag.ResLang["門診"];
                        break;
                    case "2":
                        Title = ViewBag.ResLang["急診"] + "|" + ViewBag.ResLang["就醫指南"] + "|";
                        ViewBag.pathTitle = ViewBag.ResLang["急診"];
                        break;
                    case "3":
                        Title = ViewBag.ResLang["住院"] + "|" + ViewBag.ResLang["就醫指南"] + "|";
                        ViewBag.pathTitle = ViewBag.ResLang["住院"];
                        break;
                
                    case "4":
                        Title = ViewBag.ResLang["轉診就醫流程"] + "|" + ViewBag.ResLang["就醫指南"] + "|";
                        ViewBag.pathTitle = ViewBag.ResLang["轉診就醫流程"];
                        break;
                    case "5":
                        Title = ViewBag.ResLang["文件申請"] + "|" + ViewBag.ResLang["就醫指南"] + "|";
                        ViewBag.pathTitle = ViewBag.ResLang["文件申請"];
                        break;
                }


                List<medical_guide> Models = DB.medical_guide.Where(m => m.category == ID).Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m=>m.sortIndex).ToList();
                if(Models.Count > 0)
                {
                    Data = Models;
                }
              

                return View();
            }        
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }

}

        public ActionResult List()
        {
            ViewBag.Action = "Guide";
            ViewBag.pathTitle = "";
            ViewBag.SubData = null;

            if (ID != "")
            {
                switch (ID)
                {
                    case "1":
                        Title = ViewBag.ResLang["檢查與檢驗"] + "|" + ViewBag.ResLang["就醫指南"] + "|";
                        ViewBag.pathTitle = ViewBag.ResLang["檢查與檢驗"];
                        break;
                    case "2":
                        Title = ViewBag.ResLang["收費標準"] + "|" + ViewBag.ResLang["就醫指南"] + "|";
                        ViewBag.pathTitle = ViewBag.ResLang["收費標準"];
                        break;                   
                }

                List<medical_other> Models = DB.medical_other.Where(m => m.category == ID).Where(m => m.lang == defLang).Where(m => m.title != "").Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
                List<medical_other_data> SubModels = DB.medical_other_data.Where(m => m.lang == defLang).Where(m => m.title != "").Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();

                if (Models.Count > 0)
                {
                    Data = Models;
                    ViewBag.SubData = SubModels;
                }


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }

        }

        public ActionResult Qa(int? page)
        {
            ViewBag.Action = "Guide";

            Title = ViewBag.ResLang["常見問題"] + "|" + ViewBag.ResLang["就醫指南"] + "|";
            ViewBag.pathTitle = ViewBag.ResLang["常見問題"];

            List<qas> Qas = DB.qas.Where(m => m.lang == defLang).Where(m => m.title != "").Where(m => m.status == "Y").OrderBy(m=>m.sortIndex).ToList();

            Categorys = Qas;

            ViewBag.pageView = "";
         

            int pageWidth = 10;//預設每頁長度
         

            if (ID == "")
            {   
                try
                {
                    ID = Qas.FirstOrDefault().guid.ToString();
                }
                catch
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }        
            }

            try
            {
                Title = Qas.Find(m => m.guid == ID).title + "|" + ViewBag.ResLang["就醫指南"] + "|";
            }
            catch {}


            var pageNumeber = page ?? 1;

            List<qas_data> QasData = DB.qas_data.Where(m=>m.category == ID).Where(m => m.lang == defLang).Where(m => m.title != "").Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            if(QasData != null && QasData.Count > 0)
            {
                Data = QasData.ToPagedList(pageNumeber, pageWidth);
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)QasData.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/Guide/Qa/" + ID, Allpage, pageNumeber, QasData.Count, "");//頁碼            
            }

            ViewBag.page = pageNumeber;
            ViewBag.pageWidth = pageWidth;

            return View();
        }
    }
}