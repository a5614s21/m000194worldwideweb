﻿using System;
using System.Web.Mvc;
namespace Web.Controllers
{
    public class OldPathController : BaseController
    {
        public ActionResult Situation_A()
        {
            return RedirectPermanent(Url.Content("~/"));
        }
        public ActionResult Situation_B()
        {
            try
            {
                if (RouteData.Values["path"].ToString() == "healthedu" && RouteData.Values["uri"].ToString() == "Default.aspx")
                {
                    return RedirectPermanent(Url.Content("~/"));
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/"));
                }
            }
            catch
            {
                return RedirectPermanent(Url.Content("~/"));
            }
         
        }

        public ActionResult Situation_C()
        {
            try
            {
                if (RouteData.Values["path1"].ToString() == "healthedu" && 
                    (RouteData.Values["path2"].ToString() == "healthedu" || RouteData.Values["path2"].ToString() == "view") && 
                    RouteData.Values["uri"].ToString() == "health_cntt.aspx")
                {
                    var Artno = Request.QueryString["artno"];
                    var Sources = Request.QueryString["s"] == null ? string.Empty : Convert.ToString(Request.QueryString["s"]);
                    if (!string.IsNullOrEmpty(Artno) && !string.IsNullOrEmpty(Sources))
                    {                      
                        return RedirectPermanent("https://webapp.cgmh.org.tw/healthedu/view/health_cntt.aspx?artno=" + Artno + "&s=" + Sources);
                    }
                    else if (!string.IsNullOrEmpty(Artno) && string.IsNullOrEmpty(Sources))
                    {
                        return RedirectPermanent("https://webapp.cgmh.org.tw/healthedu/view/health_cntt.aspx?artno=" + Artno);
                    }
                    else
                    {
                        return RedirectPermanent("https://webapp.cgmh.org.tw/healthedu/");
                    }                    
                   
                }
                else if (RouteData.Values["path1"].ToString() == "healthedu" &&
                   ( RouteData.Values["path2"].ToString() == "view") &&
                    RouteData.Values["uri"].ToString() == "edu_detail.aspx")
                {


                    string Artno = Request.QueryString["artno"];

                    if (!string.IsNullOrEmpty(Artno))
                    {
                        return RedirectPermanent("https://webapp.cgmh.org.tw/healthedu/view/edu_detail.aspx?artno=" + Artno);
                    }
                    else
                    {
                        return RedirectPermanent("https://webapp.cgmh.org.tw/healthedu/");
                    }

                }
                else
                {
                    return RedirectPermanent(Url.Content("~/"));
                }
            }
            catch
            {
                return RedirectPermanent(Url.Content("~/"));
            }

        }

    }
}