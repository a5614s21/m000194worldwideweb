﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.UI;
using Web.Models;
using Web.Service;
using Web.ServiceModels;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        protected string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系
        protected string defApiLang = ConfigurationManager.ConnectionStrings["defaultLanguageApi"].ConnectionString;//預設語系
        protected Model DB = new Model();
        protected web_data webData = new web_data();
        protected NameValueCollection langData = new NameValueCollection();
        protected System.DateTime todayFirst = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        protected System.DateTime todayLast = System.DateTime.Parse(System.DateTime.Now.ToString("yyyy-MM-dd") + " 00:00:00");
        protected string webURL = "";
        protected string ID = "";
        protected string Category = "";
        protected string Guid = "";
        protected string Action = "";
        protected string SearchKeyword = "";
        protected string SearchCode = "";
        protected string SubID = "";
        protected string LastID = "";
        protected GetOfficeServiceData OfficeAppData = new GetOfficeServiceData();//官網API
        protected GetServiceData AppData = new GetServiceData();//掛號API
       // protected List<HospitalModel> Hospitals = new List<HospitalModel>();//醫院清單
        protected List<hospitals> Hospitals = new List<hospitals>();//醫院清單
        protected System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;//更改較少資訊Cache使用
        protected int CacheSaveHour = int.Parse(ConfigurationManager.ConnectionStrings["CacheSaveHour"].ConnectionString);//Cache 儲存(小時)

        public string UseWebType = ConfigurationManager.ConnectionStrings["UseWebType"].ConnectionString;//前後台切換 User(前台) Master(後台)

        /// <summary>
        /// 紀錄取得的資料庫資料
        /// </summary>
        protected dynamic Data
        {
            set
            {
                ViewBag.Data = value;
            }
        }

        protected dynamic Categorys
        {
            set
            {
                ViewBag.Categorys = value;
            }
        }

        /// <summary>
        /// Banner
        /// </summary>
        protected string Banner
        {
            set
            {
                ViewBag.Banner = value;
            }
        }

        /// <summary>
        /// 各頁顯示標題用
        /// </summary>
        protected string PathTitle
        {
            set
            {
                ViewBag.PathTitle = value;
            }
        }

        /// <summary>
        /// SEO 標題
        /// </summary>
        protected string Title
        {
            set
            {
                ViewBag.Title = value + webData.title;
            }
        }

        /// <summary>
        /// SEO Description
        /// </summary>
        protected string Description
        {
            set
            {
                if (value == null || value == "")
                {
                    value = webData.seo_description;
                }
                ViewBag.Description = value;
            }
        }

        /// <summary>
        /// SEO Keywords
        /// </summary>
        protected string Keywords
        {
            set
            {
                if (value == null || value == "")
                {
                    value = webData.seo_keywords;
                }
                ViewBag.Keywords = value;
            }
        }
        

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // string pwd = FunctionService.         
            if (UseWebType == "Admin")
            {
                filterContext.Result = new RedirectResult(Url.Content("~/siteadmin"));
                return;
            }

            webURL = FunctionService.getWebUrl();

                if (RouteData.Values["lang"] != null)
                {
                    defLang = RouteData.Values["lang"].ToString();
                }

                if (defLang == "en")
                {
                    defApiLang = "en-US";
                }

                ViewBag.BodyClass = "";
                ViewBag.BodyID = "";
                ViewBag.Lang = defLang;
                ViewBag.defApiLang = defApiLang;

                langData = FunctionService.getLangData(defLang);//取得語系檔案
                ViewBag.ResLang = langData;//取得語系檔案
                ViewBag.Action = "";
                ViewBag.ActionDefult = "";
                ViewBag.prevID = "";//上層ID
                ViewBag.nextID = "";//下層ID

                ViewBag.FileRoot = Url.Content("~/Content/");

                //搜尋用關鍵字
                ViewBag.SearchCode = "";
                if (Request["bcode"] != null && Request["bcode"].ToString() != "")
                {
                    SearchCode = Request["bcode"].ToString();
                    ViewBag.SearchCode = Request["bcode"].ToString();
                }
                ViewBag.SearchKeyword = "";
                if (Request["keyword"] != null && Request["keyword"].ToString() != "")
                {
                    SearchKeyword = Request["keyword"].ToString();
                    ViewBag.SearchKeyword = Request["keyword"].ToString();
                }

                webData = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊
                if (webData != null)
                {
                    ViewBag.webData = webData;
                    ViewBag.SEO = FunctionService.SEO(webData, "", "", "");
                    ViewBag.FileRoot = Url.Content("~/Content/");
                }

                //傳入鍵值
                if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                {
                    ID = RouteData.Values["id"].ToString();
                    ViewBag.ID = RouteData.Values["id"].ToString();
                }
                if (RouteData.Values["guid"] != null && RouteData.Values["guid"].ToString() != "")
                {
                    Guid = RouteData.Values["guid"].ToString();
                    ViewBag.Guid = RouteData.Values["guid"].ToString();
                }
                if (RouteData.Values["subid"] != null && RouteData.Values["subid"].ToString() != "")
                {
                    SubID = RouteData.Values["subid"].ToString();
                    ViewBag.SubID = RouteData.Values["subid"].ToString();
                }
                if (RouteData.Values["lastid"] != null && RouteData.Values["lastid"].ToString() != "")
                {
                    LastID = RouteData.Values["lastid"].ToString();
                    ViewBag.LastID = RouteData.Values["lastid"].ToString();
                }
                if (RouteData.Values["action"] != null && RouteData.Values["action"].ToString() != "")
                {
                    Action = RouteData.Values["action"].ToString();
                    ViewBag.Action = RouteData.Values["action"].ToString();
                    ViewBag.ActionDefult = RouteData.Values["action"].ToString();
                }

                //醫院資料處理(包含Cache)
                if (cacheContainer.Get("HospitalsList") == null)
                {
                    Hospitals = DB.hospitals.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();//醫院清單
                    cacheContainer.Insert("HospitalsList", Hospitals, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);//記錄至cache(3小時)
                }
                else
                {
                    Hospitals = (List<hospitals>)cacheContainer.Get("HospitalsList");//自cache取出
                }
                ViewBag.Hospitals = Hospitals;
                // ViewBag.Hospitals = AppData.GetHospital(defApiLang);//醫院清單
                // Hospitals = DB.hospitals.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();//醫院清單
                //  ViewBag.Hospitals = Hospitals;


                //人才招募(包含Cache)
                ViewBag.Recruit = null;

                if (cacheContainer.Get("RecruitList") == null)
                {
                    List<recruits> recruits = DB.recruits.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();
                    if (recruits != null && recruits.Count > 0)
                    {
                        cacheContainer.Insert("RecruitList", recruits, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);//記錄至cache(3小時)
                        ViewBag.Recruit = recruits;
                    }
                }
                else
                {
                    ViewBag.Recruit = (List<recruits>)cacheContainer.Get("RecruitList");//自cache取出
                }

                //ViewBag.Recruit = DB.recruits.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();

                //申請與查詢(包含Cache)
                ViewBag.AppInquiryCategory = null;

                if (cacheContainer.Get("AppInquiryCategory") == null)
                {
                    List<app_inquiry_category> app_inquiry_category = DB.app_inquiry_category.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();
                    if (app_inquiry_category != null && app_inquiry_category.Count > 0)
                    {
                        cacheContainer.Insert("AppInquiryCategory", app_inquiry_category, null, DateTime.Now.AddHours(CacheSaveHour), TimeSpan.Zero, CacheItemPriority.Normal, null);//記錄至cache(3小時)
                        ViewBag.AppInquiryCategory = app_inquiry_category;
                    }
                }
                else
                {
                    ViewBag.AppInquiryCategory = (List<app_inquiry_category>)cacheContainer.Get("AppInquiryCategory");//自cache取出
                }

                //ViewBag.AppInquiryCategory = DB.app_inquiry_category.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();

                // 社服 Header
                ViewBag.SocialServiceTop = DB.social_service.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").OrderBy(m => m.sortIndex).ToList().Skip(0).Take(10);

                // 公益 Header
                ViewBag.PublicWelfareTop = DB.public_welfare.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").OrderBy(m => m.sortIndex).ToList().Skip(0).Take(10);

                // 頁底相關連結
                ViewBag.SubWebUrl = DB.subsidiary_web.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").OrderBy(m => m.sortIndex).ToList();

                //首頁上板公告
                ViewBag.Announcement = DB.notes_data.Where(m => m.lang == defLang && m.status == "Y").Where(m => m.guid == "6").FirstOrDefault();

                //研究教學-主視覺
                ViewBag.SB1 = null;
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleType", "SB1");
                callApiData.Add("keyword", SearchKeyword);
                var Models = OfficeAppData.GetGlobalArticleList(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    ViewBag.SB1 = Models;
                }



                callApiData.Clear();

                //社會公益-主視覺
                #region 社會公益-主視覺 (SC1)

                ViewBag.SC1 = null;

                callApiData.Clear();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleType", "SC1");
                callApiData.Add("keyword", "");
                Models = OfficeAppData.GetGlobalArticleList(callApiData, "Y");
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    ViewBag.SC1 = Models;
                }


            

            #endregion

        }
    }
}