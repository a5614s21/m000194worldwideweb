﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.ServiceModels;
using PagedList;
using Web.Service;
using System.Web.UI;
using System.Text;
using System.Net;
using System.IO;
using System.Collections.Specialized;

namespace Web.Controllers
{
    public class ResearchController : BaseController
    {

     
        // GET: Research
        public ActionResult Index()
        {
            Title = ViewBag.ResLang["研究教學"] + "|";
            PathTitle = ViewBag.ResLang["研究教學"];
            Data = null;

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "SB2");
            callApiData.Add("keyword", SearchKeyword);      

            //研究教學-圖文集
            var Models = OfficeAppData.GetGlobalArticleList(callApiData);

            ViewBag.SB2 = null;
            if (Models != null && Models.GetType().Name != "JObject")
            {
                ViewBag.SB2 = Models;
            }

            //頁面資訊
            Models = DB.index_info.Where(m => m.lang == defLang).Where(m => m.guid == "2").FirstOrDefault();
            Data = Models;
            Description = Models.seo_description;
            Keywords = Models.seo_keywords;

            return View();
        }

        /// <summary>
        /// 學術活動資訊
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult AcademicEventsList(int? page)
        {
            Title = ViewBag.ResLang["學術活動資訊"] + "|";
            PathTitle = ViewBag.ResLang["學術活動資訊"];
            Data = null;
            Banner = Url.Content("~/images/03-research/activity/hero.jpg");
            ViewBag.pageView = "";
            int pageWidth = 10;//預設每頁長度
            ViewBag.Action = "Activity";

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);
            callApiData.Add("keyword", "");

            List<AcademicEventsListModel> Models = OfficeAppData.GetAcademicEventsList(callApiData);

            if (Models != null && Models.GetType().Name != "JObject")
            {
                var pageNumeber = page ?? 1;
                Data = Models.ToPagedList(pageNumeber, pageWidth);
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/Research/AcademicEventsList", Allpage, pageNumeber, Models.Count, "");//頁碼
            }
            return View();
        }

        /// <summary>
        /// 學術活動資訊 (詳細內容)
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult AcademicEvents()
        {
            PathTitle = ViewBag.ResLang["學術活動資訊"];
            Data = null;
            ViewBag.Action = "Activity";
            if (ID != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("bulletinId", ID);

                List<AcademicEventsModel> Models = OfficeAppData.GetAcademicEvents(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    Title = Models[0].subject + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("keyword", "");
                    callApiData.Add("ID", ID);
                    List<AcademicEventsListModel> ModelList = OfficeAppData.GetAcademicEventsList(callApiData);
                    ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                    ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 創新研究
        /// </summary>
        /// <returns></returns>
        public ActionResult Innovation()
        {
            Title = ViewBag.ResLang["創新研究"] + "|";
            PathTitle = ViewBag.ResLang["創新研究"];
            Data = DB.notes_data.Where(m => m.lang == defLang).Where(m => m.guid == "2").FirstOrDefault();

            ViewBag.ResResults = DB.res_results.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();
            ViewBag.ResResource = DB.res_resource.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();
            ViewBag.ResResourceData = DB.res_resource_data.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();

            return View();
        }

        /// <summary>
        /// 醫學教育
        /// </summary>
        /// <returns></returns>
        public ActionResult Education()
        {
            Title = ViewBag.ResLang["醫學教育"] + "|";
            PathTitle = ViewBag.ResLang["醫學教育"];
            Data = null;

            ViewBag.EduTrain = DB.edu_train.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();
            ViewBag.EduResource = DB.edu_resource.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();
            ViewBag.EduResourceData = DB.edu_resource_data.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();
            ViewBag.EduService = DB.edu_service.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();

            return View();
        }

        /// <summary>
        /// 研討會資訊
        /// </summary>
        /// <returns></returns>
        public ActionResult Activity(int? page)
        {
            Title = ViewBag.ResLang["研討會資訊"] + "|";
            PathTitle = ViewBag.ResLang["研討會資訊"];
            ViewBag.Action = "Activity";
            Data = null;
            int pageWidth = 10;
            var pageNumeber = page ?? 1;

            NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
            postParams.Add("rows", pageWidth.ToString());
            postParams.Add("page", pageNumeber.ToString());
            postParams.Add("TIME_HD_S_Year", DateTime.Now.ToString("yyyy"));
            postParams.Add("TIME_HD_S_Month", "");
            postParams.Add("IsFont", "");
            postParams.Add("TITLE", "");
            var Models = OfficeAppData.GetSeminarList(postParams);//API

            if (Models != null && Models.GetType().Name != "JObject")
            {
                List<SeminarListModel> ModelsData = Models;
                Data = ModelsData.ToPagedList(pageNumeber, pageWidth);
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/Research/Activity", Allpage, pageNumeber, Models.Count, "");//頁碼
            }

            return View();
        }

        /// <summary>
        /// 研討會資訊-內容
        /// </summary>
        /// <returns></returns>
        public ActionResult ActivityInfo()
        {
            Title = ViewBag.ResLang["研討會資訊"] + "|";
            PathTitle = ViewBag.ResLang["研討會資訊"];
            Data = null;
            ViewBag.Action = "Activity";
            if (ID != "")
            {
                NameValueCollection postParams = System.Web.HttpUtility.ParseQueryString(string.Empty);
                postParams.Add("modify", "0");
                postParams.Add("TITLEENGLISH", ID);

                var Models = OfficeAppData.GetSeminar(postParams);//API

                if (Models != null)
                {
                    Data = Models;

                    if (defLang == "tw")
                    {
                        Title = Models.TITLE + "|";
                    }
                    else
                    {
                        Title = Models.TITLE_EN + "|";
                    }

                    //取得列表對應用資料
                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang + "/Research/Activity"));
                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Research/Activity"));
            }
        }


        /// <summary>
        /// 申請與查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Search()
        {
            Title = ViewBag.ResLang["申請與查詢"] + "|";
            PathTitle = ViewBag.ResLang["申請與查詢"];
            Data = null;

            ViewBag.AppInquiry = DB.app_inquiry.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();

            return View();
        }



        /// <summary>
        /// 新聞與觀點
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ArticleList(int? page)
        {

            Title = ViewBag.ResLang["新聞與觀點"] + "|";
            PathTitle = ViewBag.ResLang["新聞與觀點"];
            Data = null;
      
            ViewBag.pageView = "";
            int pageWidth = 12;//預設每頁長度
            if (ID != "")
            {
                

                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleType", ID);
                callApiData.Add("keyword", SearchKeyword);

                List<GlobalArticleListModel> Models = OfficeAppData.GetGlobalArticleList(callApiData);

                if (Models != null && Models.GetType().Name != "JObject")
                {
                    var pageNumeber = page ?? 1;
                    Data = Models.ToPagedList(pageNumeber, pageWidth);
                    int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/News/ArticleList/" + ID, Allpage, pageNumeber, Models.Count, "");//頁碼                   
                }
                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));

            }

        }

        /// <summary>
        /// 新聞與觀點 (詳細內容)
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ArticleInfo()
        {

            PathTitle = ViewBag.ResLang["新聞與觀點"];
            Data = null;

            if (ID != "" && Guid != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleId", Guid);

                List<GlobalArticleModel> Models = OfficeAppData.GetGlobalArticle(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {               

                    Title = Models[0].title + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("articleType", ID);
                    callApiData.Add("keyword", SearchKeyword);
                    callApiData.Add("articleId", Guid);
                    var ModelList = OfficeAppData.GetGlobalArticleList(callApiData);
                    if (ModelList != null && ModelList.GetType().Name != "JObject")
                    {
                        ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                        ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }



        /// <summary>
        /// 藥品異動訊息
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult DrugPaperList()
        {
            Title = ViewBag.ResLang["長庚藥學學報"] + "|";
            PathTitle = ViewBag.ResLang["長庚藥學學報"];
            Data = null;
            ViewBag.YearLen = 0;
            ViewBag.LastYear = 0;
            ViewBag.FirstYear = 0;

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);

            var Models = OfficeAppData.GetDrugPaperList(callApiData);
            if (Models != null && Models.GetType().Name != "JObject")
            {

                List<DrugPaperListModel> DrugChangeList = Models;

                List<string> lastYear = DrugChangeList[0].volume.ToString().Split('年').ToList();
                List<string> firstYear = DrugChangeList[DrugChangeList.Count - 1].volume.ToString().Split('年').ToList();

                ViewBag.LastYear = int.Parse(lastYear[0].ToString());
                ViewBag.FirstYear = int.Parse(firstYear[0].ToString());

                int yearLen = int.Parse(lastYear[0].ToString()) - int.Parse(firstYear[0].ToString());
                ViewBag.YearLen = yearLen;
                Data = Models;


                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }

        }
    }
}