﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Service;
using Web.ServiceModels;

namespace Web.Controllers
{
    public class SystemsController : BaseController
    {
        // GET: Systems
        public ActionResult Index()
        {
            Title = ViewBag.ResLang["長庚體系"] + "|";
            PathTitle = ViewBag.ResLang["長庚體系"];

            Dictionary<string, string> callApiData = new Dictionary<string, string>();

            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "SE1");
            callApiData.Add("keyword", SearchKeyword);

            //長庚體系-主視覺
            var Models = OfficeAppData.GetGlobalArticleList(callApiData);

            ViewBag.SE1 = null;
            if (Models != null && Models.GetType().Name != "JObject")
            {
                ViewBag.SE1 = Models;
            }



            callApiData.Clear();
            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "S02");
            callApiData.Add("keyword", SearchKeyword);

            //長庚體系-圖文
            Models = OfficeAppData.GetGlobalArticleList(callApiData);

            ViewBag.SE2 = null;
            if (Models != null && Models.GetType().Name != "JObject")
            {
                ViewBag.SE2 = Models;
            }




            //頁面資訊
            Models = DB.index_info.Where(m => m.lang == defLang).Where(m => m.guid == "3").FirstOrDefault();
            Data = Models;
            Description = Models.seo_description;
            Keywords = Models.seo_keywords;



            return View();
        }
        /// <summary>
        /// 醫療體系
        /// </summary>
        /// <returns></returns>
        public ActionResult Area()
        {
            Title = ViewBag.ResLang["醫療體系"] + "|";
            PathTitle = ViewBag.ResLang["醫療體系"];
            Data = null;

            Data = DB.notes_data.Where(m => m.lang == defLang).Where(m => m.guid == "1").FirstOrDefault();
            List<hospitals> hospitalsList = DB.hospitals.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();

            List<string> isJoinID = new List<string>();
            if(hospitalsList != null && hospitalsList.Count > 0)
            {
                foreach (hospitals item in hospitalsList.FindAll(m => !string.IsNullOrEmpty(m.join_guid)).OrderBy(m => m.sortIndex).ToList())
                {
                    isJoinID.Add(item.join_guid);
                }
            }
       

           ViewBag.hospitalsList = hospitalsList;
           ViewBag.isJoinID = isJoinID;

            return View();
        }

        /// <summary>
        /// 院區簡介
        /// </summary>
        /// <returns></returns>
        public ActionResult AreaInfo()
        {
            Title = ViewBag.ResLang["院區簡介"] + "|";
            PathTitle = ViewBag.ResLang["院區簡介"];
            Data = null;
            ViewBag.ActionDefult = "Area";

            ViewBag.hospitalInfo = null;
            ViewBag.MapCode = null;

            if (ID != null && ID != "")
            {
                List<hospitals> hospitalsList = DB.hospitals.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();
                ViewBag.hospitalsList = hospitalsList;

                hospitals hospitalsData = hospitalsList.Find(m => m.guid == ID);
                Data = hospitalsData;

                if(!string.IsNullOrEmpty(hospitalsData.map_code))
                {
                    ViewBag.MapCode = hospitalsData.map_code.Replace("width=\"600\"", "width=\"100%\"").Replace("height=\"450\"", "height=\"550\"");
                }
             

                Title = hospitalsData.title + "|";
                PathTitle = hospitalsData.title;

                if (hospitalsData.view_type == "api")
                {
                    Dictionary<string, string> callApiData = new Dictionary<string, string>();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("hospitalID", hospitalsData.hospitalID);
                    var temp = OfficeAppData.GetBranch(callApiData);
                    if (temp != null)
                    {
                        ViewBag.hospitalInfo = temp[0];
                    }
                }
                else
                {
                    ViewBag.hospitalInfo = hospitalsData;
                }

                ViewBag.JoinHospital = null;
                ViewBag.hospitalTitle = "";
                if (!string.IsNullOrEmpty(hospitalsData.join_guid))
                {
                    if(!string.IsNullOrEmpty(hospitalsData.join_title))
                    { ViewBag.hospitalTitle = hospitalsData.join_title; }
                    else { ViewBag.hospitalTitle = hospitalsData.title; }


                    ViewBag.JoinHospital = hospitalsList.Find(m => m.guid == hospitalsData.join_guid);
                }




            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang)+ "/Systems/Area");
            }




            return View();
        }

        /// <summary>
        /// 關於長庚
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            Title = ViewBag.ResLang["關於長庚"] + "|";
            PathTitle = ViewBag.ResLang["關於長庚"];
            Data = null;

            var Models = DB.abouts.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m=>m.category == "1").ToList();
            if(Models != null && Models.Count > 0)
            {
                Data = Models;
            }

            return View();
        }

        /// <summary>
        /// 病人安全
        /// </summary>
        /// <returns></returns>
        public ActionResult Security()
        {
            Title = ViewBag.ResLang["病人安全"] + "|";
            PathTitle = ViewBag.ResLang["病人安全"];
            Data = null;
            ViewBag.ActionDefult = "About";
            var Models = DB.abouts.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.category == "2").ToList();
            if (Models != null && Models.Count > 0)
            {
                Data = Models;
            }

            return View();
        }

        /// <summary>
        /// 大事記
        /// </summary>
        /// <returns></returns>
        public ActionResult Historys()
        {
            Title = ViewBag.ResLang["大事記"] + "|";
            PathTitle = ViewBag.ResLang["大事記"];
            Data = null;
            ViewBag.ActionDefult = "About";

            ViewBag.SubData = null;
            var Models = DB.historys.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderByDescending(m=>m.year).ToList();
            if (Models != null && Models.Count > 0)
            {
                Data = Models;

                var SubModels = DB.historys_data.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").ToList();
                if (SubModels != null && SubModels.Count > 0)
                {
                    ViewBag.SubData = SubModels;
                }
            }

            return View();
        }

        /// <summary>
        /// 人員招募
        /// </summary>
        /// <returns></returns>
        public ActionResult Recruit()
        {
            Title = ViewBag.ResLang["人員招募"] + "|";
            PathTitle = ViewBag.ResLang["人員招募"];
            Data = null;


            return View();
        }


        /// <summary>
        /// 人員招募(各列表)
        /// </summary>
        /// <returns></returns>
      
        public ActionResult RecruitInfo(int? page , string orgnid = "Z", string hospitalID = "Z", string bulletinType = "A", string category = null)
        {
            Title = ViewBag.ResLang["人員招募"] + "|";
            PathTitle = ViewBag.ResLang["人員招募"];
            Data = null;
            ViewBag.RecruitList = null;

            ViewBag.RecruitData = null;

            ViewBag.RecruitHospitalData = null;

            ViewBag.ActionDefult = "Recruit";


            int pageWidth = 10;//預設每頁長度
            if (ID != "")
            {


                List<recruits> recruits = ViewBag.Recruit;
                var Models = recruits.Find(m => m.guid == ID);
                Data = Models;

                Title = Models.title + ViewBag.ResLang["人員招募"] + "|";

                if(category == null)
                {
                    category = Models.category;
                }

                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("orgnid", orgnid);
                callApiData.Add("hospitalID", hospitalID);
                callApiData.Add("category", category);
                callApiData.Add("bulletinType", bulletinType);
                callApiData.Add("keyword", SearchKeyword);

                var ListModels = OfficeAppData.GetRecruitmentList(callApiData);
                if (ListModels != null && ListModels.GetType().Name != "JObject")
                {
                    var pageNumeber = page ?? 1;
                    List<RecruitmentListModel> temp = ListModels;
                    ViewBag.RecruitList = temp.ToPagedList(pageNumeber, pageWidth);
                    int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)ListModels.Count / pageWidth));//總頁數
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/Systems/RecruitInfo/" + ID , Allpage, pageNumeber, ListModels.Count, "#list");//頁碼                
                }


                ViewData["orgnid"] = orgnid;
                ViewData["hospitalID"] = hospitalID;
                ViewData["bulletinType"] = bulletinType;
                ViewData["category"] = category;


                List<recruits_data> recruits_data = DB.recruits_data.Where(m => m.category == ID).Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").OrderBy(m=>m.sortIndex).ToList();
                if(recruits_data != null && recruits_data.Count > 0)
                {
                    ViewBag.RecruitData = recruits_data;

                }


                //院區資訊

                List<recruits_hospitals> recruits_hospitals = DB.recruits_hospitals.Where(m => m.category == ID).Where(m => m.status == "Y").Where(m => m.lang == defLang).Where(m => m.title != "").OrderBy(m => m.sortIndex).ToList();
                if (recruits_hospitals != null && recruits_hospitals.Count > 0)
                {
                    ViewBag.RecruitHospitalData = recruits_hospitals;

                }

                return View();

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Systems/Recruit"));

            }
            
        }


        /// <summary>
        /// 人員招募(詳細內容)
        /// </summary>
        /// <returns></returns>
        public ActionResult RecruitContent()
        {
            Title = ViewBag.ResLang["人員招募"] + "|";
            PathTitle = ViewBag.ResLang["人員招募"];
            string keys = Guid;
            ViewBag.TopData = null;
            ViewBag.ActionDefult = "Recruit";

            if (ID != "" && Guid != "")
            {

                List<recruits> recruits = ViewBag.Recruit;
                var ModelsTop = recruits.Find(m => m.guid == ID);
                ViewBag.TopData = ModelsTop;


                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("bulletinId", Guid);

                List<BulletinModel> Models = OfficeAppData.GetBulletin(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    Title = Models[0].bulletinTitle + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("newsType", ID);
                    callApiData.Add("keyword", SearchKeyword);
                    callApiData.Add("ID", Guid);
                    var ModelList = OfficeAppData.GetBulletinList(callApiData);
                    if (ModelList != null && ModelList.GetType().Name != "JObject")
                    {
                        ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                        ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang + "/Systems/Recruit"));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Systems/Recruit"));
            }

        }

        /// <summary>
        /// 服務概況
        /// </summary>
        /// <returns></returns>
        public ActionResult Overview()
        {
            Title = ViewBag.ResLang["服務概況"] + "|";
            PathTitle = ViewBag.ResLang["服務概況"];
            Data = null;
            ViewBag.ActionDefult = "About";
            ViewBag.subData = null;
            ViewBag.ReportData = null;
            Dictionary<int, string> ReportData = new Dictionary<int, string>();

            var Models = DB.about_overview.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.sortIndex).ToList();
            if (Models != null && Models.Count > 0)
            {
                Data = Models;
                var subData = DB.about_overview_data.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.title).ToList();


               
                    int i = 1;
                    foreach(about_overview item in ViewBag.Data)
                    {                           
                         
                            var temp = FunctionService.InfinityJsonFormat(item.content);//取得json 數量
                            int qty = temp["X"].ToList().Count();
                            string labels = "";
                            List<string> numData = new List<string>();
                            foreach (about_overview_data subItem in subData.FindAll(m => m.category == item.guid).OrderBy(m => m.title).ToList())
                            {
                              
                                labels = labels + "'"+subItem.title + "',";//年分
                                List<string> numConcent = subItem.content.Split(',').ToList();//各年分資料
                                for (int s=0;s<qty;s++)
                                {
                                    if(numData.Count >= (s + 1) )
                                    {
                                        numData[s] = numData[s] + numConcent[s].ToString() + ",";
                                    }
                                    else
                                    {
                                        numData.Add(numConcent[s].ToString() + ",");
                                    }
                                }
                            
                            }

                            //SubReportData.Add("labels", labels);
                           // SubReportData.Add("data", numData);
                          

                            string viewData = "var data"+i+" = {" +
                                              "labels: [" + labels.Substring(0, labels.Length - 1) + "]," +
                                              "datasets: [";

                                        for (int s = 0; s < qty; s++)
                                        {
                                         viewData = viewData + "{" +
                                                                "label: '"+ temp["X"][s].ToString() + "'," +
                                                                "data: [" + numData[s].Substring(0, numData[s].Length - 1) + "]," +
                                                                "borderColor: '" + temp["Color"][s].ToString() + "'," +
                                                                "backgroundColor: '" + temp["Color"][s].ToString() + "',},";
                                         }
                                       viewData = viewData + "]}" + "\r\n";


                    ReportData.Add(i, viewData);

                    i++;
                    
                    }
                ViewBag.ReportData = ReportData;
            }

           

          return View();
        }



        /// <summary>
        /// 科部介紹
        /// </summary>
        /// <returns></returns>
        public ActionResult Branch()
        {
            Title = ViewBag.ResLang["科部介紹"] + "|";
            PathTitle = ViewBag.ResLang["科部介紹"];
            Data = null;
            ViewBag.ActionDefult = "Area";
            ViewBag.ThisHospital = "";
            ViewBag.ThisHospitalIID = "";
            ViewBag.ThisHospitalGuid = "";
            if (ID != null && ID != "")
            {
                try
                {
                    ViewBag.ThisHospitalIID = ID;

                    Dictionary<string, string> callApiData = new Dictionary<string, string>();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("hospitalID", ID);

                    var Models = OfficeAppData.GetDeptList(callApiData);
                    if (Models != null && Models.GetType().Name != "JObject")
                    {
                        Data = Models;
                        //目前醫院名稱
                        var tmp = Hospitals.Find(m => m.hospitalID == ID);
                        if (tmp != null)
                        {
                            ViewBag.ThisHospitalGuid = tmp.guid;
                            ViewBag.ThisHospital = tmp.title;
                            if (!string.IsNullOrEmpty(tmp.join_title))
                            {
                                ViewBag.ThisHospital = tmp.join_title;
                            }
                        }
                        Title = ViewBag.ThisHospital + "|" + ViewBag.ResLang["科部介紹"] + "|";
                    }
                }
                catch
                {
                    return RedirectPermanent(Url.Content("~/" + defLang + "/Systems/Area#area__intro"));
                }
              
            }

            return View();
        }


        /// <summary>
        /// 科部介紹
        /// </summary>
        /// <returns></returns>
        public ActionResult BranchInfo()
        {
            Title = ViewBag.ResLang["科部介紹"] + "|";
            PathTitle = ViewBag.ResLang["科部介紹"];
            Data = null;

            ViewBag.ThisHospital = "";
            ViewBag.ThisHospitalIID = "";
            ViewBag.ActionDefult = "Area";

            ViewBag.TopDep = null;

            if (ID != null && ID != "" && Guid != null && Guid != "")
            {
                ViewBag.ThisHospitalIID = ID;



                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);            
                callApiData.Add("hospitalID", ID);

                callApiData.Add("topDeptId", Guid);
              
                if (!string.IsNullOrEmpty(LastID))
                {
                    callApiData.Add("deptId", LastID);
                }
                else if (!string.IsNullOrEmpty(SubID))
                {
                    callApiData.Add("deptId", SubID);
                }
                else
                {
                    callApiData.Add("deptId", Guid);
                }
             

                var Models = OfficeAppData.GetDept(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    Data = Models[0];
                    Title = Models[0].deptName + "|" + ViewBag.ResLang["科部介紹"] + "|";
                    //目前醫院名稱
                    var tmp = Hospitals.Find(m => m.hospitalID == ID);
                    if (tmp != null)
                    {
                        ViewBag.ThisHospital = tmp.title;
                        if (!string.IsNullOrEmpty(tmp.join_title))
                        {
                            ViewBag.ThisHospital = tmp.join_title;
                        }
                    }
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang + "/Systems/Branch/"+ ID));
                }

              



                Dictionary<string, string> callApiData2 = new Dictionary<string, string>();
                callApiData2.Add("language", defApiLang);
                callApiData2.Add("hospitalID", ID);

                var Models2 = OfficeAppData.GetDeptList(callApiData2);
                if (Models2 != null && Models2.GetType().Name != "JObject")
                {
                    ViewBag.Dept = Models2;
                }

                if (!string.IsNullOrEmpty(SubID))
                {
                    //取得團隊資料
                    callApiData2.Clear();
                    callApiData2.Add("language", defApiLang);
                    callApiData2.Add("deptId", SubID.ToString());

                    Models2 = OfficeAppData.GetDeptDoctor(callApiData2);
                    if (Models2 != null && Models2.GetType().Name != "JObject")
                    {
                        ViewBag.DoctorData = Models2[0];
                    }
                }
                ViewBag.DeptId = SubID.ToString();


                if (!string.IsNullOrEmpty(Guid))
                {
                    callApiData.Remove("deptId");
                    callApiData.Add("deptId", Guid);

                    var Models_top = OfficeAppData.GetDept(callApiData);
                    if (Models_top != null && Models_top.GetType().Name != "JObject")
                    {
                        ViewBag.TopDep = Models_top;
                    }
                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
            return View();
        }



    }
}