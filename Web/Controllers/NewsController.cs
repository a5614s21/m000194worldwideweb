﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.ServiceModels;
using PagedList;
using Web.Service;
using System.Web.UI;

namespace Web.Controllers
{
    public class NewsController : BaseController
    {


        // GET: News
        /// <summary>
        /// 最新消息 & 活動訊息
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult List(int? page)
        {
            Title = ViewBag.ResLang["最新消息"] +"|";
            PathTitle = ViewBag.ResLang["最新消息"];
            Data = null;
            Banner = Url.Content("~/images/05-system/news/hero.jpg");
            ViewBag.pageView = "";

          

            int pageWidth = 10;//預設每頁長度
            if (ID != "")
            {
                if(ID == "B")
                {
                    Title = ViewBag.ResLang["活動訊息"] + "|";
                    PathTitle = ViewBag.ResLang["活動訊息"];
                }

                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("newsType", ID);
                callApiData.Add("keyword", SearchKeyword);

                List<BulletinListModel> Models = OfficeAppData.GetBulletinList(callApiData);

                if (Models != null && Models.GetType().Name != "JObject")
                {
                    var pageNumeber = page ?? 1;
                    Data = Models.OrderByDescending(m => m.startDate).ToPagedList(pageNumeber , pageWidth);
                    int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/News/List/"+ ID, Allpage, pageNumeber, Models.Count, "");//頁碼                    
                }
                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 最新消息 & 活動訊息 (詳細內容)
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Info()
        {
        
            PathTitle = ViewBag.ResLang["最新消息"];
            Data = null;        

            if (ID != "" && Guid != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("bulletinId", Guid);

                List<BulletinModel> Models = OfficeAppData.GetBulletin(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    if (ID == "B")
                    {
                        PathTitle = ViewBag.ResLang["活動訊息"];
                    }

                    Title = Models[0].bulletinTitle + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("newsType", ID);
                    callApiData.Add("keyword", SearchKeyword);
                    callApiData.Add("ID", Guid);
                    var ModelList = OfficeAppData.GetBulletinList(callApiData);
                    if (ModelList != null && ModelList.GetType().Name != "JObject")
                    {
                        ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                        ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 聚焦點  新聞與觀點
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ArticleList(int? page)
        {

            Title = ViewBag.ResLang["聚焦點"] + "|";
            PathTitle = ViewBag.ResLang["聚焦點"];
            Data = null;
            Banner = Url.Content("~/images/05-system/news/hero.jpg");
            ViewBag.pageView = "";
            int pageWidth = 12;//預設每頁長度
            if (ID != "")
            {
                if (ID == "A1")
                {
                    Title = ViewBag.ResLang["新聞與觀點"] + "|";
                    PathTitle = ViewBag.ResLang["新聞與觀點"];
                }

                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleType", ID);
                callApiData.Add("keyword", SearchKeyword);

                List<GlobalArticleListModel> Models = OfficeAppData.GetGlobalArticleList(callApiData);

                if (Models != null && Models.GetType().Name != "JObject")
                {
                    var pageNumeber = page ?? 1;
                    Data = Models.ToPagedList(pageNumeber, pageWidth);
                    int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/News/ArticleList/"+ ID, Allpage, pageNumeber, Models.Count, "");//頁碼                   
                }
                return View();
            }
            else
            {
                 return RedirectPermanent(Url.Content("~/" + defLang));
               
            }

        }

        /// <summary>
        /// 提供文章內容，包含聚焦點、新聞與觀點、社會公益、研究教學 (詳細內容)
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ArticleInfo()
        {

            PathTitle = ViewBag.ResLang["聚焦點"];
            Data = null;

            if (ID != "" && Guid != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleId", Guid);

                List<GlobalArticleModel> Models = OfficeAppData.GetGlobalArticle(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    if (ID == "A1")
                    {
                        Title = ViewBag.ResLang["新聞與觀點"] + "|";
                        PathTitle = ViewBag.ResLang["新聞與觀點"];
                    }

                    if (ID == "SD2")
                    {
                        PathTitle = Models[0].typeName;
                    }

                    Title = Models[0].title + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("articleType", ID);
                    callApiData.Add("keyword", SearchKeyword);
                    callApiData.Add("articleId", Guid);
                   var ModelList = OfficeAppData.GetGlobalArticleList(callApiData);
                    if (ModelList != null && ModelList.GetType().Name != "JObject")
                    {
                        ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                        ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 記者會資訊
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult PressNewsList(int? page)
        {
            Title = ViewBag.ResLang["記者會資訊"] + "|";
            PathTitle = ViewBag.ResLang["記者會資訊"];
            Data = null;
            Banner = Url.Content("~/images/05-system/news/hero.jpg");
            ViewBag.pageView = "";
            int pageWidth = 10;//預設每頁長度
                      

                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);                
                callApiData.Add("keyword", SearchKeyword);

                List<PressNewsListModel> Models = OfficeAppData.GetPressNewsList(callApiData);

                if (Models != null && Models.GetType().Name != "JObject")
                {
                    var pageNumeber = page ?? 1;
                    Data = Models.ToPagedList(pageNumeber, pageWidth);
                    int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/News/PressNewsList/" + ID, Allpage, pageNumeber, Models.Count, "");//頁碼                
                }
                return View();
           
        }


        /// <summary>
        /// 記者會資訊 (詳細內容)
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult PressNews()
        {

            PathTitle = ViewBag.ResLang["記者會資訊"];
            Data = null;
            
            if (ID != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("pressID", ID);

                List<PressNewsModel> Models = OfficeAppData.GetPressNews(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {   
                    Title = Models[0].Title + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("keyword", SearchKeyword);
                    callApiData.Add("ID", ID);
                    var ModelList = OfficeAppData.GetPressNewsList(callApiData);
                    if (ModelList != null && ModelList.GetType().Name != "JObject")
                    {
                        ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                        ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }


        /// <summary>
        /// 得獎紀錄
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult AwardList(int? page)
        {
            Title = ViewBag.ResLang["得獎紀錄"] + "|";
            PathTitle = ViewBag.ResLang["得獎紀錄"];
            Data = null;
            Banner = Url.Content("~/images/05-system/news/hero.jpg");
            ViewBag.pageView = "";
            int pageWidth = 10;//預設每頁長度


            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);
            callApiData.Add("keyword", SearchKeyword);

            List<AwardListModel> Models = OfficeAppData.GetAwardList(callApiData);

            if (Models != null && Models.GetType().Name != "JObject")
            {
                var pageNumeber = page ?? 1;
                Data = Models.ToPagedList(pageNumeber, pageWidth);
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/News/AwardList/" + ID, Allpage, pageNumeber, Models.Count, "");//頁碼                
            }
            return View();

        }


        /// <summary>
        /// 得獎紀錄 (詳細內容)
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Award()
        {

            PathTitle = ViewBag.ResLang["得獎紀錄"];
            Data = null;

            if (ID != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("awardId", ID);

               var Models = OfficeAppData.GetAward(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    Title = Models[0].Title + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("keyword", SearchKeyword);
                    callApiData.Add("ID", ID);
                    var ModelList = OfficeAppData.GetAwardList(callApiData);
                    if (ModelList != null && ModelList.GetType().Name != "JObject")
                    {
                        ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                        ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }


        /// <summary>
        /// 藥品異動訊息
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult DrugChangeList()
        {
            Title = ViewBag.ResLang["藥品異動訊息"] + "|";
            PathTitle = ViewBag.ResLang["藥品異動訊息"];
            Data = null;
            ViewBag.YearLen = 0;
            ViewBag.LastYear = 0;
            ViewBag.FirstYear = 0;

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);

                var Models = OfficeAppData.GetDrugChangeList(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {

                    List<DrugChangeListModel> DrugChangeList = Models;

                    List<string> lastYear = DrugChangeList[0].yearMonth.ToString().Split('年').ToList();
                    List<string> firstYear = DrugChangeList[DrugChangeList.Count-1].yearMonth.ToString().Split('年').ToList();

                    ViewBag.LastYear = int.Parse(lastYear[0].ToString());
                    ViewBag.FirstYear = int.Parse(firstYear[0].ToString());

                    int yearLen = int.Parse(lastYear[0].ToString()) - int.Parse(firstYear[0].ToString());
                    ViewBag.YearLen = yearLen;
                    Data = Models;


                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

        }



        /// <summary>
        ///  醫療品質
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Quality()
        {
            Title = ViewBag.ResLang["醫療品質"] + "|";
            PathTitle = ViewBag.ResLang["醫療品質"];
            Data = null;

            ViewBag.MedicalHospital = null;

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);

            var Models = OfficeAppData.GetMedicalQualityList(callApiData);
            if (Models != null && Models.GetType().Name != "JObject")
            {
                Data = Models;
                List<MedicalQualityListModel> Medical = Models;
                ViewBag.MedicalHospital = Medical.GroupBy(m => m.hospitalName).ToList();
            }

            ViewBag.MsgData = DB.notes_data.Where(m => m.lang == defLang).Where(m => m.guid == "7").FirstOrDefault();
            return View();
          

        }

        /// <summary>
        ///  醫療品質-各分院服務滿意度問卷調查結果
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult QualityResult()
        {
            Title = ViewBag.ResLang["各分院服務滿意度問卷調查結果"] + "|";
            PathTitle = ViewBag.ResLang["各分院服務滿意度問卷調查結果"];
            Data = null;

            ViewBag.ThisHospital = "";
            ViewBag.ReportData = null;
            ViewBag.HospitalTitle = null;

            ViewBag.Data2 = null;

            List<string> idList = new List<string>();//取得圖表ID

            List<string> HospitalTitle = new List<string>();//取得圖表院區

            if (ID != null && ID != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("hospitalID", ID);

                var Models = OfficeAppData.GetSatisfactionList(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    Data = Models;
                    //目前醫院名稱
                    var tmp =  Hospitals.Find(m => m.hospitalID == ID);
                    if(tmp != null)
                    {
                        idList.Add(tmp.guid);

                        ViewBag.ThisHospital = tmp.title;
                        if (!string.IsNullOrEmpty(tmp.join_title))
                        {
                            ViewBag.ThisHospital = tmp.join_title;
                            idList.Add(tmp.join_guid);
                        }
                        //取得組合院區資訊
                        if (!string.IsNullOrEmpty(tmp.join_guid))
                        {

                            var tmp2 = Hospitals.Find(m => m.guid == tmp.join_guid);

                            callApiData.Clear();
                            callApiData.Add("language", defApiLang);
                            callApiData.Add("hospitalID", tmp2.hospitalID);

                            Models = OfficeAppData.GetSatisfactionList(callApiData);
                            if (Models != null && Models.GetType().Name != "JObject")
                            {
                                ViewBag.Data2 = Models;
                            }
                        }


                    }


                    //圖表
                    Dictionary<int, string> ReportData = new Dictionary<int, string>();




                    var quality_result = DB.quality_result.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m=>idList.Contains(m.category)).OrderBy(m => m.sortIndex).ToList();




                    if (quality_result != null && quality_result.Count > 0)
                    {
                      
                        var subData = DB.quality_result_data.Where(m => m.lang == defLang).Where(m => m.status == "Y").OrderBy(m => m.title).ToList();


                    

                        int i = 1;
                        foreach (quality_result item in quality_result)
                        {
                            var tmps = Hospitals.Find(m => m.guid == item.category);
                            if(tmps != null)
                            {
                                HospitalTitle.Add(tmps.title);//取得圖表院區
                            }

                            var temp = FunctionService.InfinityJsonFormat("[{\"key\":\"X\",\"val\":[\"門診\",\"急診\",\"住院\"]},{\"key\":\"Color\",\"val\":[\"#63AAD6\",\"#90B64C\",\"#F38826\"]}]");//取得json 數量
                            int qty = temp["X"].ToList().Count();
                            string labels = "";
                            List<string> numData = new List<string>();
                            foreach (quality_result_data subItem in subData.FindAll(m => m.category == item.guid).OrderBy(m => m.title).ToList())
                            {
                                // labels.Add(subItem.title);//年分
                                labels = labels + "'" + subItem.title + "',";//年分
                                List<string> numConcent = subItem.content.Split(',').ToList();//各年分資料
                                for (int s = 0; s < qty; s++)
                                {
                                    if (numData.Count >= (s + 1))
                                    {
                                        numData[s] = numData[s] + numConcent[s].ToString() + ",";
                                    }
                                    else
                                    {
                                        numData.Add(numConcent[s].ToString() + ",");
                                    }
                                }

                            }

                            //SubReportData.Add("labels", labels);
                            // SubReportData.Add("data", numData);


                            string viewData = "var data" + i + " = {" +
                                              "labels: [" + labels.Substring(0, labels.Length - 1) + "]," +
                                              "datasets: [";

                            for (int s = 0; s < qty; s++)
                            {
                                viewData = viewData + "{" +
                                                       "label: '" + temp["X"][s].ToString() + "'," +
                                                       "data: [" + numData[s].Substring(0, numData[s].Length - 1) + "]," +
                                                       "borderColor: '" + temp["Color"][s].ToString() + "'," +
                                                       "backgroundColor: '" + temp["Color"][s].ToString() + "',},";
                            }
                            viewData = viewData + "]}" + "\r\n";


                            ReportData.Add(i, viewData);

                            i++;

                        }
                        ViewBag.ReportData = ReportData;
                    }


                    ViewBag.HospitalTitle = HospitalTitle;//取得圖表院區


                }
            }           


            return View();


        }


        /// <summary>
        ///  醫療品質-各分院服務滿意度問卷調查結果
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult QualityInfo()
        {
            Title = ViewBag.ResLang["品質績優事項"] + "|";
            PathTitle = ViewBag.ResLang["品質績優事項"];
            Data = null;

            ViewBag.ThisHospital = "";

            ViewBag.YearLen = 0;
            ViewBag.LastYear = 0;
            ViewBag.FirstYear = 0;


            if (ID != null && ID != "")
            {
               Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("hospitalID", ID);

                var Models = OfficeAppData.GetExcellentQualityList(callApiData);
                Data = Models;
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    List<ExcellentQualityListModel> QualityList = Models;
                    string lastYear = QualityList[0].year.ToString();
                    string firstYear = QualityList[QualityList.Count - 1].year.ToString();
                    ViewBag.LastYear = int.Parse(lastYear.ToString());
                    ViewBag.FirstYear = int.Parse(firstYear.ToString());

                    int yearLen = int.Parse(lastYear.ToString()) - int.Parse(firstYear.ToString());
                    ViewBag.YearLen = yearLen;
                }


                //目前醫院名稱
                var tmp = Hospitals.Find(m => m.hospitalID == ID);
                if (tmp != null)
                {
                    ViewBag.ThisHospital = tmp.title;
                    if (!string.IsNullOrEmpty(Hospitals[0].join_title))
                    {
                        ViewBag.ThisHospital = tmp.join_title;
                    }
                }
            }
            
            

            return View();


        }
    }
}