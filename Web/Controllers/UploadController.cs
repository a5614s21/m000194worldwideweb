﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Collections;
using System.IO;
using System.Web.Script.Serialization;

namespace Web.Controllers
{
    public class UploadController : Controller
    {
        //
        // GET: /Upload/

        public ActionResult Upload()
        {
            bool isSavedSuccessfully = true;

            string output = "";
            string outputImageFile = "";
            string extOpt = "";
            int i = 1;

            //目前使用者ID
            string[] tempID = User.Identity.Name.ToString().Split('\\');
            string UserID = tempID[tempID.Length - 1].ToString();

            try
            {
                Dictionary<String, Object> dic = new Dictionary<string, object>();

                Response.ContentType = "application/json";
                //post
                string Field = Request.Form["Field"];
                string fileTypes = Request.Form["types"];//需求格式


                //檔案格式
                string uploadRoot = "";
                ArrayList checkFileTypes = new ArrayList();
                if (fileTypes.IndexOf("image") != -1)
                {
                    checkFileTypes.Add(".jpg");
                    checkFileTypes.Add(".png");
                    checkFileTypes.Add(".gif");
                    uploadRoot = "images";
                }
                else if (fileTypes.IndexOf("video") != -1)
                {                  
                    checkFileTypes.Add(".mp4");
                    uploadRoot = "videos";
                }
                else
                {
                    checkFileTypes.Add(".pdf");
                    checkFileTypes.Add(".doc");
                    checkFileTypes.Add(".docx");
                    checkFileTypes.Add(".xls");
                    checkFileTypes.Add(".xlsx");
                    checkFileTypes.Add(".ppt");
                    checkFileTypes.Add(".pptx");
                    uploadRoot = "files";
                }

                string FilePath = RouteData.Values["FilePath"].ToString();//上傳來源

                /*
                if(FilePath != "system")
                {
                    uploadRoot = uploadRoot + "\\" + UserID;//AD使用者資料夾
                    if(!System.IO.Directory.Exists(Server.MapPath("~/") + "\\Content\\Upload\\" + FilePath + "\\" + uploadRoot))
                    {
                        Directory.CreateDirectory(Server.MapPath("~/") + "\\Content\\Upload\\" + FilePath + "\\" + uploadRoot);
                    }

                }
                */

                //上傳圖片

                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase uploads = Request.Files[fileName];
                    float fileSize = uploads.ContentLength / 1000;
                    string file = System.IO.Path.GetFileName(uploads.FileName);
                    string[] tempName = uploads.FileName.ToString().Split('.');//將檔名切開取得附檔名

                    string ext = System.IO.Path.GetExtension(file); //副檔名


                    if (checkFileTypes.IndexOf(ext.ToLower()) != -1)
                    {
                        var newFilename = DateTime.Now.ToString("yyMMddhhmmss") + i.ToString() + ext.ToString();//更換檔名
                        if(uploadRoot.IndexOf("images") == -1)
                        {
                            newFilename = uploads.FileName.ToString();
                        }

                        if (Directory.Exists(Server.MapPath("~/Uploads")))
                        {
                            var filenames = Server.MapPath("~/Uploads") + "\\" + FilePath + "\\" + uploadRoot + "\\" + newFilename;//設定路徑與檔案名稱(file)
                            uploads.SaveAs(filenames);//儲存檔案
                            filenames = filenames.Replace(Server.MapPath("~/Uploads"), "").Replace("\\Upload\\" + FilePath + "\\" + uploadRoot + "\\", "");//將檔案的路徑全部取代清空
                            tempName = newFilename.ToString().Split('.');//將檔名切開取得附檔名
                            outputImageFile += filenames + ",";//紀錄回傳檔案
                            output += Url.Content("~/Uploads/" + filenames) + "§";
                        }
                        else
                        {
                            var filenames = Server.MapPath("~/") + "\\Content\\Upload\\" + FilePath + "\\" + uploadRoot + "\\" + newFilename;//設定路徑與檔案名稱(file)
                            uploads.SaveAs(filenames);//儲存檔案
                            filenames = filenames.Replace(Server.MapPath("~/"), "").Replace("\\Content\\Upload\\" + FilePath + "\\" + uploadRoot + "\\", "");//將檔案的路徑全部取代清空
                            tempName = newFilename.ToString().Split('.');//將檔名切開取得附檔名
                            outputImageFile += filenames + ",";//紀錄回傳檔案
                            output += Url.Content("~/Content/Upload/" + FilePath + "/" + uploadRoot + "/" + filenames) + "§";
                        }

                    
                    }

                    i++;
                }




                JavaScriptSerializer serializer = new JavaScriptSerializer();

                //輸出json格式
               // Response.Write(serializer.Serialize(output + '§' + outputImageFile));
            }
            catch (Exception ex)
            {
                isSavedSuccessfully = false;
            }



            if (isSavedSuccessfully)
            {

                return Json(new { Message = output.ToString() });
            }
            else
            {
                return Json(new { Message = "Error in saving file" });
            }



        }

    }
}
