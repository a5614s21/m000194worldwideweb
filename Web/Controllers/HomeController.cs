﻿using ElFinder;
using Newtonsoft.Json.Linq;
using PagedList;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Web.Models;
using Web.Service;
using Web.ServiceModels;


namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            Title = "";
            ViewBag.IndexBanner = null;
            ViewBag.S02 = null;
            ViewBag.S03 = null;
            ViewBag.S04 = null;
            ViewBag.News1 = null;
            ViewBag.News2 = null;

            Dictionary<string, string> callApiData = new Dictionary<string, string>();

            #region 首頁-主視覺 (S01)

            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "S01");
            callApiData.Add("keyword", "");

            var ModelsBanner = OfficeAppData.GetGlobalArticleList(callApiData, "Y");

            if (ModelsBanner != null && ModelsBanner.GetType().Name != "JObject")
            {
                ViewBag.IndexBanner = ModelsBanner;
            }

            #endregion

            #region 首頁-聚焦點 (S02)

            callApiData.Clear();
            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "S02");
            callApiData.Add("keyword", "");

            var Models = OfficeAppData.GetGlobalArticleList(callApiData, "Y");

            if (Models != null && Models.GetType().Name != "JObject")
            {
                ViewBag.S02 = Models;
            }

            #endregion

            #region 首頁-新聞與觀點 (S03)

            callApiData.Clear();
            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "S03");
            callApiData.Add("keyword", "");

            var ModelsS03 = OfficeAppData.GetGlobalArticleList(callApiData, "Y");

            if (ModelsS03 != null && ModelsS03.GetType().Name != "JObject")
            {
                ViewBag.S03 = ModelsS03;
            }

            #endregion

            #region 重點新聞

            callApiData.Clear();
            callApiData.Add("language", defApiLang);
            callApiData.Add("newsType", "A");

            var Models1 = OfficeAppData.GetHomePageBulletinList(callApiData);//最新消息

            if (Models1 != null && Models1.GetType().Name != "JObject")
            {
                ViewBag.News1 = Models1;
            }

            callApiData.Remove("newsType");
            callApiData.Add("newsType", "B");
            var Models2 = OfficeAppData.GetHomePageBulletinList(callApiData);//活動訊息
            if (Models2 != null && Models2.GetType().Name != "JObject")
            {
                ViewBag.News2 = Models2;
            }

            #endregion

            #region 首頁-社會公益 (S04)

            callApiData.Clear();
            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "S04");
            callApiData.Add("keyword", "");

            var ModelsS04 = OfficeAppData.GetGlobalArticleList(callApiData, "Y");

            if (ModelsS04 != null && ModelsS04.GetType().Name != "JObject")
            {
                ViewBag.S04 = ModelsS04;
            }

            #endregion

            try
            {
                ViewBag.test = new Root(new DirectoryInfo(Server.MapPath("~/")), Url.Content("~/")).Directory.FullName;
            }
            catch
            {
                ViewBag.test = "null";
            }

            try
            {
                ViewBag.test1 = new Root(new DirectoryInfo(Server.MapPath("~/Uploads/")), Url.Content("~/Uploads/")).Directory.FullName;
            }
            catch
            {
                ViewBag.test1 = "null";
            }

            try
            {
                ViewBag.test2 = new Root(new DirectoryInfo(Server.MapPath("~/Content/Upload/")), Url.Content("~/Content/Upload/")).Directory.FullName;
            }
            catch
            {
                ViewBag.test2 = "null";
            }
            
            return View();
        }

        public ActionResult Contact()
        {
            Title = ViewBag.ResLang["意見信箱"] + "|";
            PathTitle = ViewBag.ResLang["意見信箱"];
            Data = null;
            List<hospitals> hospitalsList = DB.hospitals.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.hospitalID != "").ToList();
            ViewBag.hospitalsList = hospitalsList;

            ViewBag.Contact = null;

            if (Session["Contact"] != null)
            {
                ViewBag.Contact = Session["Contact"];
            }

            return View();
        }

        /// <summary>
        /// 送出意見信箱
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ContactForm(FormCollection form)
        {

            string filePath = Server.MapPath("~/") + "/Log/Contact.txt";
           

            try
            {
                /*
                var response = form["g-recaptcha-response"];
                string secretKey = "6Ld_NxUTAAAAAAzX2OqL4afKeAOirTpUlT_ExxR-";
                var client = new WebClient();
                var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
                var obj = JObject.Parse(result);
                var status = (bool)obj.SelectToken("success");
                    */
                //var status = true;


                //註冊資料暫存
                Session.Remove("Contact");
                Session.Add("Contact", form);

                if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                    alertData.Add("text", "");
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Home/Contact"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");

                }
                else
                {
                    Dictionary<string, string> callApiData = new Dictionary<string, string>();
                    callApiData.Add("cName", form["cName"].ToString());
                    callApiData.Add("tel", form["tel"].ToString());
                    callApiData.Add("eMail", form["eMail"].ToString());
                    callApiData.Add("hospitalID", form["hospitalID"].ToString());
                    callApiData.Add("matter", form["matter"].ToString());
                    callApiData.Add("description", form["description"].ToString());
                    callApiData.Add("remoteIP", FunctionService.GetIP());

                    var ResponseData = OfficeAppData.SendOpinionMail(callApiData);

                    if (ResponseData["isSuccess"].ToString() == "Y")
                    {
                        Session.Remove("Contact");
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["送出完成"].ToString() + "!");
                        alertData.Add("text", langData["我們會盡快回覆您謝謝您"].ToString());
                        alertData.Add("type", "success");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Home/Contact"));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                    }
                    else
                    {
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["送出失敗"].ToString() + "!");
                        alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                        alertData.Add("type", "error");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Home/Contact"));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                    }
                }
           
            }
            catch(Exception ex)
            {
               // string filePath = Server.MapPath("~/") + "/Log/Contact.txt";
                try
                {
                    using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        StreamWriter sw = new StreamWriter(fs);
                        sw.BaseStream.Seek(0, SeekOrigin.End);
                        sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "：" + ex.Message.ToString());
                        sw.WriteLine("\r\n");//換行                 
                        sw.Flush();
                        sw.Close();
                        fs.Close();
                    }
                }
                catch
                {

                }


                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["送出失敗"].ToString() + "!");
                alertData.Add("text", "系統錯誤，請洽網站管理員!");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Home/Contact"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
           
        }

        public ActionResult Sitemap()
        {
            Title = ViewBag.ResLang["網站地圖"] + "|";
            PathTitle = ViewBag.ResLang["網站地圖"];
            Data = null;

            return View();
        }

        public ActionResult Staff()
        {
            Title = ViewBag.ResLang["員工專區"] + "|";
            PathTitle = ViewBag.ResLang["員工專區"];
            Data = DB.staff_area.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.content != "").ToList();

            return View();
        }

        public ActionResult Privacy()
        {
            Title = ViewBag.ResLang["隱私權聲明與網站資安政策"] + "|";
            PathTitle = ViewBag.ResLang["隱私權聲明與網站資安政策"];
            Data = DB.notes_data.Where(m => m.lang == defLang).Where(m => m.guid == "5").FirstOrDefault();

            return View();
        }

        public ActionResult Cm()
        {
            Title = ViewBag.ResLang["特色醫療中心"] + "|";
            PathTitle = ViewBag.ResLang["特色醫療中心"];
            Data = DB.cm_list.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.content != "").ToList();

            return View();
        }

        /// <summary>
        /// 搜尋結果
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult Searchs(int? page)
        {
            Title = ViewBag.ResLang["搜尋結果"] + "|";
            PathTitle = ViewBag.ResLang["搜尋結果"];

            ViewBag.pageView = "";
            int pageWidth = 10;//預設每頁長度

            var keyword = new SqlParameter("keyword", "%" + SearchKeyword + "%");
            var lang = new SqlParameter("lang", defLang);
            var parameter = new SqlParameter[] { lang, keyword };

            List<full_search> full_search = DB.Database.SqlQuery<full_search>("select * from full_search where lang = @lang and (title like @keyword or notes like @keyword or content like @keyword) ", parameter).ToList();

            ViewBag.searchCount = full_search.Count;

            if (full_search != null && full_search.Count > 0)
            {
                var pageNumeber = page ?? 1;
                Data = full_search.ToPagedList(pageNumeber, pageWidth);
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)full_search.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/Home/Searchs/" + ID, Allpage, pageNumeber, full_search.Count, "&keyword=" + SearchKeyword);//頁碼
            }

            return View();
        }

        public ActionResult Alert()
        {
            if (Session["alertData"] != null)
            {
                ViewBag.alertData = Session["alertData"];
                Session.Remove("alertData");
                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 清除暫存 
        /// </summary>
        public void ClearCache()
        {
            List<string> cacheKeys = new List<string>();
            IDictionaryEnumerator cacheEnum = HttpRuntime.Cache.GetEnumerator();
            while (cacheEnum.MoveNext())
            {
                cacheKeys.Add(cacheEnum.Key.ToString());
            }
            foreach (string cacheKey in cacheKeys)
            {
                Response.Write(cacheKey.ToString() + "：Clear!<br>");

                HttpRuntime.Cache.Remove(cacheKey);
            }

            Response.Write("All Cache is Clear!<br>");
        }

        public void AppleApp()
        {
            string json = "{\"applinks\": {\"apps\": [],\"details\": [ { \"appID\": \"id619682038\", \"paths\": [\"*\"] }]}}";
            Response.Write(json);
        }

    }
}