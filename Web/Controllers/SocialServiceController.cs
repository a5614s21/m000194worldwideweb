﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.ServiceModels;
using Web.Models;
using Web.Service;
using System.Web.UI;

namespace Web.Controllers
{
    public class SocialServiceController : BaseController
    {
        // GET: SocialService
        public ActionResult Index()
        {
            Title = ViewBag.ResLang["社會公益"] + "|";
            PathTitle = ViewBag.ResLang["社會公益"];

            ViewBag.SC1 = null;
            ViewBag.SC2 = null;

            Dictionary<string, string> callApiData = new Dictionary<string, string>();


            #region 社會公益-圖文集 (SC2)
            callApiData.Clear();
            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "SC2");
            callApiData.Add("keyword", "");

            var Models = OfficeAppData.GetGlobalArticleList(callApiData, "Y");

            if (Models != null && Models.GetType().Name != "JObject")
            {
                ViewBag.SC2 = Models;
            }
            #endregion

            return View();
        }

        /// <summary>
        /// 社會公益內文
        /// </summary>
        /// <returns></returns>
        public ActionResult NewsInfo()
        {

            PathTitle = ViewBag.ResLang["社會公益"];
            Data = null;
            ViewBag.Action = "PublicWelfare";

            if (ID != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleId", ID);

                List<GlobalArticleModel> Models = OfficeAppData.GetGlobalArticle(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {                  
                    Title = Models[0].title + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("articleType", "SC2");
                    callApiData.Add("keyword", SearchKeyword);
                    callApiData.Add("articleId", ID);
                    var ModelList = OfficeAppData.GetGlobalArticleList(callApiData);
                    if (ModelList != null && ModelList.GetType().Name != "JObject")
                    {
                        ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                        ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 社會關懷
        /// </summary>
        /// <returns></returns>
        public ActionResult PublicWelfare(int? page)
        {
            Title = ViewBag.ResLang["社會關懷"] + "|";
            PathTitle = ViewBag.ResLang["社會關懷"];

            int pageWidth = 10;//預設每頁長度
            var pageNumeber = page ?? 1;

            var Models = DB.public_welfare.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").OrderBy(m=>m.sortIndex).ToList();

            if(Models != null && Models.Count > 0)
            {
                Data = Models.ToPagedList(pageNumeber, pageWidth);
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/SocialService/PublicWelfare/" + ID, Allpage, pageNumeber, Models.Count, "");//頁碼   
            }     
            return View();
        }

        /// <summary>
        /// 公益活動內文
        /// </summary>
        /// <returns></returns>
        public ActionResult PublicWelfareInfo()
        {

            PathTitle = ViewBag.ResLang["公益活動"];
            Data = null;
            ViewBag.Action = "PublicWelfare";
            if (ID != "" )
            {

                public_welfare Models = DB.public_welfare.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.guid == ID).FirstOrDefault();

                if (Models != null)
                {
                  
                    Title = Models.title + "|";
                    Data = Models;

                    //上下筆---------------------------------------------------------------------------
                   
                    var ModelList = DB.public_welfare.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").OrderBy(m => m.sortIndex).ToList();

                    if (ModelList != null && ModelList.Count > 0)
                    {
                        int nowIndex = ModelList.FindIndex(m => m.guid == ID);
                        if ((nowIndex - 1) >= 0 && ModelList[nowIndex - 1] != null)
                        {
                            ViewBag.prevID = ModelList[nowIndex - 1].guid;
                        }

                        if ((nowIndex + 1) < ModelList.Count && ModelList[nowIndex + 1] != null)
                        {
                            ViewBag.nextID = ModelList[nowIndex + 1].guid;
                        }                    
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 社會服務
        /// </summary>
        /// <returns></returns>
        public ActionResult Service()
        {
            Title = ViewBag.ResLang["社會服務"] + "|";
            PathTitle = ViewBag.ResLang["社會服務"];
            Data = DB.notes_data.Where(m => m.lang == defLang).Where(m => m.guid == "3").FirstOrDefault();
            ViewBag.SocialService = DB.social_service.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").OrderBy(m => m.sortIndex).ToList();
            return View();
        }


        /// <summary>
        /// 社會服務內文
        /// </summary>
        /// <returns></returns>
        public ActionResult ServiceInfo()
        {

            PathTitle = ViewBag.ResLang["社會服務"];
            Data = null;
            ViewBag.Action = "Service";
            if (ID != "")
            {

                social_service Models = DB.social_service.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.guid == ID).FirstOrDefault();

                if (Models != null)
                {

                    Title = Models.title + "|";
                    Data = Models;

                    //上下筆---------------------------------------------------------------------------

                    var ModelList = DB.social_service.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.title != "").OrderBy(m => m.sortIndex).ToList();

                    if (ModelList != null && ModelList.Count > 0)
                    {
                        int nowIndex = ModelList.FindIndex(m => m.guid == ID);
                        if ((nowIndex - 1) >= 0 && ModelList[nowIndex - 1] != null)
                        {
                            ViewBag.prevID = ModelList[nowIndex - 1].guid;
                        }

                        if ((nowIndex + 1) < ModelList.Count && ModelList[nowIndex + 1] != null)
                        {
                            ViewBag.nextID = ModelList[nowIndex + 1].guid;
                        }
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }

        /// <summary>
        /// 社服活動
        /// </summary>
        /// <returns></returns>
        public ActionResult Activitys()
        {
            Title = ViewBag.ResLang["社服活動"] + "|";
            PathTitle = ViewBag.ResLang["社服活動"];

            Data = null;
            dynamic Models = null;

            string cacheKey = "Activitys" + ID;

            ViewBag.CacheKey = cacheKey;
            ViewBag.AddPath = "";

            

            if (ID != "")
            {
                ViewBag.AddPath = "/" + ID;
            }
            else
            {
                ID = "3";
            }

            ViewBag.CheckID = ID;

            /*    if (cacheContainer.Get(cacheKey) == null)
                {
                    Dictionary<string, string> callApiData = new Dictionary<string, string>();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("hospitalID", ID);
                    Models = OfficeAppData.GetSocialServiceList(callApiData);                
                }
                else
                {
                    Models = cacheContainer.Get(cacheKey);//自cache取出
                }
                */

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);
            callApiData.Add("hospitalID", ID);
            Models = OfficeAppData.GetSocialServiceList(callApiData);

            if (Models != null && Models.GetType().Name != "JObject")
            {
                Data = Models;
                //若無任何傳遞分院，取得預設院區
               /* if (ID == "")
                {                  
                    var hospitaName = Hospitals[0].title;
                    if(!string.IsNullOrEmpty(Hospitals[0].join_title))
                    {
                        hospitaName = Hospitals[0].join_title;
                    }
                    List<SocialServiceListModel> SocialServiceList = Models;
                    Data = SocialServiceList.FindAll(m => m.hospitalName == hospitaName).ToList();
                }*/
            }


            callApiData.Clear();
            callApiData.Add("language", defApiLang);
            Models = OfficeAppData.GetSocialServiceLocList(callApiData);
            ViewBag.HhospitalOfSocial = null;
            if (Models != null && Models.GetType().Name != "JObject")
            {
                ViewBag.HhospitalOfSocial = Models;
            }


            if (Request["v"] != null && Request["v"].ToString() != "")
            {

                
                return View("ActivitysList");//切換至列表顯示
            }
            else
            {
                return View();
            }
           
        }



        /// <summary>
        /// 新聞與觀點
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ArticleList(int? page)
        {

            Title = ViewBag.ResLang["新聞與觀點"] + "|";
            PathTitle = ViewBag.ResLang["新聞與觀點"];
            Data = null;

            ViewBag.pageView = "";
            int pageWidth = 12;//預設每頁長度
            if (ID != "")
            {


                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleType", ID);
                callApiData.Add("keyword", SearchKeyword);

                List<GlobalArticleListModel> Models = OfficeAppData.GetGlobalArticleList(callApiData);

             

                if (Models != null && Models.GetType().Name != "JObject")
                {
                    var pageNumeber = page ?? 1;

                    string addPath = "";
                    //有關鍵字
                    if(!string.IsNullOrEmpty(SearchKeyword))
                    {
                        Models = Models.Where(m => m.title.Contains(SearchKeyword) || m.description.Contains(SearchKeyword)).ToList();
                        addPath = "&keyword=" + SearchKeyword;
                    }


                    Data = Models.ToPagedList(pageNumeber, pageWidth);
                    int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                    ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/News/ArticleList/" + ID, Allpage, pageNumeber, Models.Count, addPath);//頁碼                   
                }
                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));

            }

        }

        /// <summary>
        /// 新聞與觀點 (詳細內容)
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult ArticleInfo()
        {

            PathTitle = ViewBag.ResLang["新聞與觀點"];
            Data = null;

            if (ID != "" && Guid != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("articleId", Guid);

                List<GlobalArticleModel> Models = OfficeAppData.GetGlobalArticle(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {

                    Title = Models[0].title + "|";
                    Data = Models[0];

                    //上下筆---------------------------------------------------------------------------
                    callApiData.Clear();
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("articleType", ID);
                    callApiData.Add("keyword", SearchKeyword);
                    callApiData.Add("articleId", Guid);
                    var ModelList = OfficeAppData.GetGlobalArticleList(callApiData);
                    if (ModelList != null && ModelList.GetType().Name != "JObject")
                    {
                        ViewBag.prevID = ModelList[0].PrevNextID["PrevID"];
                        ViewBag.nextID = ModelList[0].PrevNextID["NextID"];
                    }
                    //--------------------------------------------------------------------------------

                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang));
                }

            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
        }
    }
}