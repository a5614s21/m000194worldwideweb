﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Service;
using Web.ServiceModels;
using System.Web.Caching;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace Web.Controllers
{
    public class AnalyticsDatatableController : Controller
    {
        public GoogleAnalyticsService GaData = new GoogleAnalyticsService();
        protected GaParameterModel GaParameter = new GaParameterModel();//宣告參數使用
        protected System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;//更改較少資訊Cache使用
        public int cacheTime = 15;
        protected string sDate = DateTime.Now.ToString("yyyy") + "-01-01";
        protected string eDate = DateTime.Now.ToString("yyyy") + "-12-31";
        protected Dictionary<string , object> dataTableVal = new Dictionary<string, object>();


        // GET: AnalyticsDatatable
        public string Index(string key , string subkey = null)
        {
            NameValueCollection requests = Request.Params;
            int skip = int.Parse(requests["iDisplayStart"].ToString());
            int take = int.Parse(requests["iDisplayLength"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if (requests["sSearch"].ToString() != "")
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["sSearch"].ToString());
                sSearch = sSearch[0];
            }

            string orderByKey = requests["mDataProp_" + requests["iSortCol_0"]].ToString();
            string orderByType = requests["sSortDir_0"].ToString();
            /*string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            if (tables == "historys_data" && orderByKey == "category")
            {
                orderBy = "category desc ,day " + orderByType.ToUpper();
            }
            */


            dataTableVal.Add("skip", skip);
            dataTableVal.Add("take", take);
            dataTableVal.Add("sSearch", sSearch);
            dataTableVal.Add("orderByKey", orderByKey);
            dataTableVal.Add("orderByType", orderByType);

            if (Session["sDate"] != null && Session["eDate"] != null)
            {
                sDate = Session["sDate"].ToString();
                eDate = Session["eDate"].ToString();
            }

            GaParameter.startDate = sDate;//預設起始日
            GaParameter.endDate = eDate;//預設結束日

            Dictionary<string, object> Opt = new Dictionary<string, object>();//輸出用
            switch (key)
            {
                //熱門頁面
                case "hot":
                    Opt = HotPage();  
                    break;
                //查詢關鍵字
                case "keyword":
                    Opt = HotKeyword();
                    break;
                //流量來源
                case "sessions":
                    Opt = HotSessions();
                    break;
                //瀏覽器使用
                case "browser":
                    Opt = HotBrowser();
                    break;
            }

            return JsonConvert.SerializeObject(Opt);
        }


        /// <summary>
        /// 瀏覽器使用
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> HotBrowser()
        {
            Dictionary<string, object> opt = new Dictionary<string, object>();//輸出用

            #region 瀏覽器使用
            try
            {

                GaParameter.metrics = "ga:pageviews";
                GaParameter.Dimensions = "ga:browser";


                string CacheName = "Browser" + sDate + eDate;
                dynamic tmp = null;
                if (cacheContainer.Get(CacheName) == null)
                {
                    tmp = GaData.ReGaContent(GaParameter);
                    cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                }
                else
                {
                    tmp = cacheContainer.Get(CacheName);//自cache取出
                }

             
                List<BrowserViewModel> BrowserViewModel = new List<BrowserViewModel>();
                int s = 0;
                foreach (List<string> item in tmp.Rows)
                {
                    BrowserViewModel m = new BrowserViewModel();
                    m.name = item[0];//名稱              
                    m.session = int.Parse(item[1].ToString());
                    BrowserViewModel.Add(m);
                   // totalPageView += int.Parse(item[1].ToString());
                    s++;
                }


                if (dataTableVal["sSearch"] != null)
                {
                    dynamic sSearch = dataTableVal["sSearch"];
                    string keyword = sSearch.keyword.ToString();
                    BrowserViewModel = BrowserViewModel.Where(m => m.name.Contains(keyword)).ToList();
                }

                opt.Add("iTotalDisplayRecords", BrowserViewModel.Count);

                List<BrowserViewModel> data = BrowserViewModel.OrderByDescending(m => m.session).Skip(int.Parse(dataTableVal["skip"].ToString())).Take(int.Parse(dataTableVal["take"].ToString())).ToList();

                List<object> source = new List<object>();//紀錄列表資訊
                int a = 0;
                foreach (BrowserViewModel item in data)
                {
                    Dictionary<string, object> subOpt = new Dictionary<string, object>();//輸出用             
                    int num = int.Parse(dataTableVal["skip"].ToString()) + a + 1;

                    subOpt.Add("no", num.ToString());
                    subOpt.Add("name", item.name.ToString());
                    subOpt.Add("session", item.session.ToString());

                    source.Add(subOpt);
                    a++;
                }
                opt.Add("data", source);
            }
            catch
            {
                opt.Clear();
                opt.Add("iTotalDisplayRecords", "0");
                opt.Add("data", "");
            }

            return opt;

            #endregion

        }



        /// <summary>
        /// 流量來源
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> HotSessions()
        {
            Dictionary<string, object> opt = new Dictionary<string, object>();//輸出用
                                                                              ////查詢關鍵字
                                                                              //ViewBag.HotPages = null;
            #region 流量來源
            try
            {

                GaParameter.metrics = "ga:organicSearches";
                GaParameter.Dimensions = "ga:source";

                // var tmp = GaData.ReGaContent(GaParameter);

                string CacheName = "Sessions" + sDate + eDate;
                dynamic tmp = null;
                 if (cacheContainer.Get(CacheName) == null)
                  {
                      tmp = GaData.ReGaContent(GaParameter);
                      cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                  }
                  else
                  {
                      tmp = cacheContainer.Get(CacheName);//自cache取出
                  }
              
                List<SessionsViewModel> SessionsViewModel = new List<SessionsViewModel>();
                int s = 0;
                foreach (List<string> item in tmp.Rows)
                {
                     SessionsViewModel m = new SessionsViewModel();

                    m.source = item[0];//名稱              
                    m.num = int.Parse(item[1].ToString());
                    SessionsViewModel.Add(m);

                    s++;
                }

                if (dataTableVal["sSearch"] != null)
                {
                    dynamic sSearch = dataTableVal["sSearch"];
                    string keyword = sSearch.keyword.ToString();
                    SessionsViewModel = SessionsViewModel.Where(m => m.source.Contains(keyword)).ToList();
                }

                opt.Add("iTotalDisplayRecords", SessionsViewModel.Count);

                List<SessionsViewModel> data = SessionsViewModel.OrderByDescending(m => m.num).Skip(int.Parse(dataTableVal["skip"].ToString())).Take(int.Parse(dataTableVal["take"].ToString())).ToList();

                List<object> source = new List<object>();//紀錄列表資訊
                int a = 0;
                foreach (SessionsViewModel item in data)
                {
                    Dictionary<string, object> subOpt = new Dictionary<string, object>();//輸出用             
                    int num = int.Parse(dataTableVal["skip"].ToString()) + a + 1;

                    subOpt.Add("no", num.ToString());
                    subOpt.Add("source", item.source.ToString());
                    subOpt.Add("num", item.num.ToString());

                    source.Add(subOpt);
                    a++;
                }
                opt.Add("data", source);
            }
            catch
            {
                opt.Clear();
                opt.Add("iTotalDisplayRecords", "0");
                opt.Add("data", "");
            }

            return opt;

            #endregion

        }


        /// <summary>
        /// 關鍵字
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> HotKeyword()
        {
            Dictionary<string, object> opt = new Dictionary<string, object>();//輸出用
            ////查詢關鍵字
            //ViewBag.HotPages = null;
            #region 查詢關鍵字

            try
            {
                GaParameter.metrics = "ga:organicSearches";
                GaParameter.Dimensions = "ga:keyword";

                // var tmp = GaData.ReGaContent(GaParameter);

                string CacheName = "Keywords" + sDate + eDate;
                dynamic tmp = null;
                if (cacheContainer.Get(CacheName) == null)
                {
                    tmp = GaData.ReGaContent(GaParameter);
                    cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                }
                else
                {
                    tmp = cacheContainer.Get(CacheName);//自cache取出
                }
                List<KeywordViewModel> KeywordViewModel = new List<KeywordViewModel>();
                int s = 0;
                foreach (List<string> item in tmp.Rows)
                {
                    KeywordViewModel m = new KeywordViewModel();
                
                    m.keyword = item[0];//名稱              
                    m.num = int.Parse(item[1].ToString());
                    KeywordViewModel.Add(m);                  

                    s++;
                }

                if (dataTableVal["sSearch"] != null)
                {
                    dynamic sSearch = dataTableVal["sSearch"];
                    string keyword = sSearch.keyword.ToString();
                    KeywordViewModel = KeywordViewModel.Where(m => m.keyword.Contains(keyword)).ToList();
                }

                opt.Add("iTotalDisplayRecords", KeywordViewModel.Count);

                List<KeywordViewModel> data = KeywordViewModel.OrderByDescending(m => m.num).Skip(int.Parse(dataTableVal["skip"].ToString())).Take(int.Parse(dataTableVal["take"].ToString())).ToList();

                List<object> source = new List<object>();//紀錄列表資訊
                int a = 0;
                foreach (KeywordViewModel item in data)
                {
                    Dictionary<string, object> subOpt = new Dictionary<string, object>();//輸出用             
                    int num = int.Parse(dataTableVal["skip"].ToString()) + a + 1;

                    subOpt.Add("no", num.ToString());
                    subOpt.Add("keyword", item.keyword.ToString());
                    subOpt.Add("num", item.num.ToString());

                    source.Add(subOpt);
                    a++;
                }
                opt.Add("data", source);
            }
            catch
            {
                opt.Clear();
                opt.Add("iTotalDisplayRecords", "0");
                opt.Add("data", "");
            }

            return opt;

            #endregion

        }

        /// <summary>
        /// 熱門頁面
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> HotPage()
        {
            Dictionary<string, object> opt = new Dictionary<string, object>();//輸出用
            ////相關頁面
            ViewBag.HotPages = null;
            #region 相關頁面
            
            try
            {
                GaParameter.metrics = "ga:pageValue,ga:pageviews,ga:pageviewsPerSession,ga:timeOnPage";
                GaParameter.Dimensions = "ga:pagePath,ga:pageTitle";

                // var tmp = GaData.ReGaContent(GaParameter);

                string CacheName = "OtherPage" + sDate + eDate;
                dynamic tmp = null;
                if (cacheContainer.Get(CacheName) == null)
                {
                    tmp = GaData.ReGaContent(GaParameter);
                    cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                }
                else
                {
                    tmp = cacheContainer.Get(CacheName);//自cache取出
                }
                List<PageViewModel> PageViewModel = new List<PageViewModel>();
                int s = 0;
                foreach (List<string> item in tmp.Rows)
                {
                    PageViewModel m = new PageViewModel();
                    m.url = item[0];//網頁
                    m.title = item[1];//標題
                    m.val = (float.Parse(item[2].ToString())).ToString("#0.0");
                    m.views = int.Parse(item[3].ToString());
                    m.session = (float.Parse(item[4].ToString())).ToString("#0.00");

                    double mach = double.Parse(item[5].ToString()) / m.views;

                    m.times = FunctionService.secToTime(mach.ToString());

                    PageViewModel.Add(m);
                    s++;
                }

                if (dataTableVal["sSearch"] != null)
                {
                    dynamic sSearch = dataTableVal["sSearch"];
                    string keyword = sSearch.keyword.ToString();
                    PageViewModel = PageViewModel.Where(m => m.url.Contains(keyword) || m.title.Contains(keyword)).ToList();
                }

                opt.Add("iTotalDisplayRecords", PageViewModel.Count);

                List<PageViewModel>  HotPages = PageViewModel.OrderByDescending(m => m.views).Skip(int.Parse(dataTableVal["skip"].ToString())).Take(int.Parse(dataTableVal["take"].ToString())).ToList();

                List<object> source = new List<object>();//紀錄列表資訊
                int a = 0;
                foreach(PageViewModel item in HotPages)
                {
                    Dictionary<string, object> subOpt = new Dictionary<string, object>();//輸出用             
                    int num = int.Parse(dataTableVal["skip"].ToString()) + a + 1;

                    subOpt.Add("num", num.ToString());
                    subOpt.Add("title", item.title.ToString());
                    subOpt.Add("url", item.url.ToString());
                    subOpt.Add("views", item.views.ToString());
                    subOpt.Add("times", item.times.ToString());

                    source.Add(subOpt);
                    a++;
                }
                opt.Add("data", source);
            }
            catch {
                opt.Clear();
                opt.Add("iTotalDisplayRecords", "0");
                opt.Add("data", "");
            }

            return opt;

            #endregion

        }

    }
}