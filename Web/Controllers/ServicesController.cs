﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.ServiceModels;
using PagedList;
using Web.Service;
using System.Web.UI;
using System.Web.Caching;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace Web.Controllers
{
    public class ServicesController : BaseController
    {
        // GET: Medical
        public ActionResult Index()
        {
            Title = ViewBag.ResLang["醫療服務"] + "|";

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);
            callApiData.Add("articleType", "SA1");
            callApiData.Add("keyword", SearchKeyword);

            //研究教學-主視覺
            var Models = OfficeAppData.GetGlobalArticleList(callApiData);

            ViewBag.SA1 = null;
            if (Models != null && Models.GetType().Name != "JObject")
            {
                ViewBag.SA1 = Models;
            }

            //頁面資訊
            Models = DB.index_info.Where(m => m.lang == defLang).Where(m => m.guid == "1").FirstOrDefault();
            Data = Models;
            Description = Models.seo_description;
            Keywords = Models.seo_keywords;

            return View();
        }

        public ActionResult Team()
        {
            Title = ViewBag.ResLang["醫療團隊"] + "|";
            return View();
        }

        /// <summary>
        /// 醫師介紹
        /// </summary>
        /// <returns></returns>
        public ActionResult DeptList()
        {
            Title = ViewBag.ResLang["醫師介紹"] + "|";
            PathTitle = ViewBag.ResLang["醫師介紹"];
            Data = null;
            ViewBag.Action = "Team";
            ViewBag.ThisHospital = "";
            ViewBag.ThisHospitalIID = "";
            if (ID != null && ID != "")
            {
                ViewBag.ThisHospitalIID = ID;

                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("hospitalID", ID);

                var Models = OfficeAppData.GetDoctorDeptList(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    Data = Models;

                    //目前醫院名稱
                    var tmp = Hospitals.Find(m => m.hospitalID == ID);
                    if (tmp != null)
                    {
                        ViewBag.ThisHospital = tmp.title;
                        if (!string.IsNullOrEmpty(tmp.join_title))
                        {
                            ViewBag.ThisHospital = tmp.join_title;
                        }
                    }
                    Title = ViewBag.ThisHospital + "|" + ViewBag.ResLang["醫師介紹"] + "|";
                }
            }

            return View();
        }

        /// <summary>
        /// 醫生介紹
        /// </summary>
        /// <returns></returns>
        public ActionResult DeptInfo()
        {
            Title = ViewBag.ResLang["醫師介紹"] + "|";
            PathTitle = ViewBag.ResLang["醫師介紹"];
            Data = null;

            ViewBag.ThisHospital = "";
            ViewBag.ThisHospitalIID = "";
            ViewBag.Action = "Team";
            ViewBag.deptName = "";

            if (ID != null && ID != "" && Guid != null && Guid != "")
            {
                ViewBag.ThisHospitalIID = ID;

                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("hospitalID", ID);

                var Models = OfficeAppData.GetDoctorDeptList(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    List<DoctorDeptListModel> DoctorDeptList = Models;

                    DoctorDeptList = DoctorDeptList.Where(m => m.deptId == Guid).ToList();

                    Data = DoctorDeptList[0];

                    string tempTitle =  DoctorDeptList[0].deptName + "|" + ViewBag.ResLang["醫師介紹"] + "|";

                    if (!string.IsNullOrEmpty(SubID))
                    {
                        ViewBag.deptName = DoctorDeptList[0].subDept.Find(m => m.deptId == ViewBag.SubID).deptName;
                    }
                    else
                    {
                        ViewBag.deptName = DoctorDeptList[0].deptName;
                    }

                    if (!string.IsNullOrEmpty(LastID))
                    {
                        ViewBag.deptName = DoctorDeptList[0].subDept.Find(m => m.deptId == ViewBag.SubID).subDept.Find(m => m.deptId == ViewBag.LastID).deptName;
                    }

                    Title = ViewBag.deptName + "|" + tempTitle;
                    //目前醫院名稱
                    var tmp = Hospitals.Find(m => m.hospitalID == ID);
                    if (tmp != null)
                    {
                        ViewBag.ThisHospital = tmp.title;
                        if (!string.IsNullOrEmpty(tmp.join_title))
                        {
                            ViewBag.ThisHospital = tmp.join_title;
                        }
                    }
                }

                Dictionary<string, string> callApiData2 = new Dictionary<string, string>();
                callApiData2.Add("language", defApiLang);
                callApiData2.Add("hospitalID", ID);

                if (!string.IsNullOrEmpty(LastID))
                {
                    callApiData2.Add("deptId", LastID);
                }
                else if (string.IsNullOrEmpty(LastID) && !string.IsNullOrEmpty(SubID))
                {
                    callApiData2.Add("deptId", SubID);
                }
                else
                {
                    callApiData2.Add("deptId", Guid);
                }

                var Models2 = OfficeAppData.GetDeptDoctor(callApiData2);
                 //if (Models2 != null && Models2.GetType().Name != "JObject")
                if (Models2 != null)
                {
                    try
                    {
                        //ViewBag.Dept = Models2.resultList;
                        ViewBag.Dept = Models2;
                    }
                    catch
                    {
                        ViewBag.Dept = null;
                    }
                  
                }

                ViewBag.DeptId = SubID.ToString();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
            return View();
        }

        /// <summary>
        /// 醫師介紹
        /// </summary>
        /// <returns></returns>
        public ActionResult DoctorInfo()
        {
            Title = ViewBag.ResLang["醫師介紹"] + "|";
            PathTitle = ViewBag.ResLang["醫師介紹"];
            Data = null;
            ViewBag.Action = "Team";
            ViewBag.register = null;

            if (ID != null && ID != "")
            {
                dynamic Models = null;

                string cacheKey = "DoctorInfo" + ID;

                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("doctorId", ID);
                Models = OfficeAppData.GetDoctor(callApiData);

                if (Models != null && Models.GetType().Name != "JObject")
                {
                    Data = Models[0];
                    Title = Models[0].doctorName + ViewBag.ResLang["醫師"] + "|" + ViewBag.ResLang["醫師介紹"] + "|";

                    callApiData.Clear();
                    callApiData.Add("hospitalID", "");
                    callApiData.Add("language", defApiLang);
                    callApiData.Add("drid", ID);

                    var register = AppData.GetDoctorDep(callApiData, Hospitals);
                    ViewBag.register = register;
                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang));
            }
            return View();
        }

        #region 會員專區

        /// <summary>
        /// 會員登入
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            Title = ViewBag.ResLang["會員登入"] + "|";
            PathTitle = ViewBag.ResLang["會員登入"];
            ViewBag.Action = "Online";
            if (Session["MemberData"] != null)
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Services/Member"));
            }
            else
            {
                return View();
            }
        }

        /// <summary>
        /// 會員登入判斷
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void LoginCheck(FormCollection form)
        {
            /*var response = form["g-recaptcha-response"];
            string secretKey = "6Ld_NxUTAAAAAAzX2OqL4afKeAOirTpUlT_ExxR-";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");
            */
            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Services/Login"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
            else
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();

                callApiData.Add("account", form["account"].ToString());
                callApiData.Add("password", form["password"].ToString());

                var Models = OfficeAppData.CheckMemberLogin(callApiData);
                if (Models != null && Models.GetType().Name != "JObject")
                {
                    //登入成功
                    if (Models[0].isPass == "Y")
                    {
                        //取基本資料
                        var Member = OfficeAppData.GetMemberData(callApiData);
                        if (Member != null && Member.GetType().Name != "JObject")
                        {
                            Session.Remove("MemberData");
                            Session.Add("MemberData", Member[0]);
                            Response.Write("<script>window.location.href='" + Url.Content("~/" + defLang + "/Services/Member';</script>"));
                        }
                    }
                    //登入失敗
                    else
                    {
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["登入失敗"].ToString() + "!");
                        alertData.Add("text", langData["請檢查帳號或密碼是否輸入錯誤"].ToString());
                        alertData.Add("type", "error");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Services/Login"));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                    }
                }
                else
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["登入失敗"].ToString() + "!");
                    alertData.Add("text", langData["請檢查帳號或密碼是否輸入錯誤"].ToString());
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Login"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
            }
        }

        /// <summary>
        /// 會員服務
        /// </summary>
        /// <returns></returns>
        public ActionResult Member()
        {
            Title = ViewBag.ResLang["會員服務"] + "|";
            PathTitle = ViewBag.ResLang["會員服務"];
            ViewBag.Action = "Online";

            ViewBag.MemberData = null;

            if (Session["MemberData"] != null)
            {
                ViewBag.MemberData = Session["MemberData"];
                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Services/Login"));
            }
        }

        /// <summary>
        /// 修改會員資料
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void MemberEditForm(FormCollection form)
        {
            /* var response = form["g-recaptcha-response"];
             string secretKey = "6Ld_NxUTAAAAAAzX2OqL4afKeAOirTpUlT_ExxR-";
             var client = new WebClient();
             var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
             var obj = JObject.Parse(result);
             var status = (bool)obj.SelectToken("success");*/

            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Services/Member"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
            else
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("saveType", "U");//N(新註冊)、U(修改會員資料)
                callApiData.Add("account", form["account"].ToString());
                if (!string.IsNullOrEmpty(form["password"]))
                {
                    callApiData.Add("password", form["password"].ToString());
                }
                else
                {
                    callApiData.Add("password", "");
                }

                callApiData.Add("cName", form["cName"].ToString());
                callApiData.Add("sex", form["sex"].ToString());
                callApiData.Add("birthday", form["birthday"].ToString());
                callApiData.Add("tel", form["tel"].ToString());
                callApiData.Add("address", form["address"].ToString());

                callApiData.Add("ePaper", "[{'paperId': '" + form["paperId"].ToString() + "','isOrder': '" + form["isOrder"].ToString() + "'}]");
                // callApiData.Add("ePaper", "");
                var ResponseData = OfficeAppData.SetMemberData(callApiData);

                if (ResponseData["isSuccess"].ToString() == "Y")
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["修改完成"].ToString() + "!");
                    alertData.Add("text", langData["會員資料已修改"].ToString());
                    alertData.Add("type", "success");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Member"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    //取基本資料
                    callApiData.Clear();
                    callApiData.Add("account", form["account"].ToString());
                    var Member = OfficeAppData.GetMemberData(callApiData);
                    if (Member != null && Member.GetType().Name != "JObject")
                    {
                        Session.Remove("MemberData");
                        Session.Add("MemberData", Member[0]);
                    }

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
                else
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["修改失敗"].ToString() + "!");
                    alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Member"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
            }
        }

        /// <summary>
        /// 加入會員
        /// </summary>
        /// <returns></returns>
        public ActionResult Registered()
        {
            Title = ViewBag.ResLang["加入會員"] + "|";
            PathTitle = ViewBag.ResLang["加入會員"];
            ViewBag.Action = "Online";

            ViewBag.Taiwan = DB.taiwan_city.Where(m => m.lay == "1").Where(m => m.lang == defLang).OrderBy(m => m.sortIndex).ToList();

            ViewBag.Registered = null;
            ViewBag.TaiwanDistrict = null;

            if (Session["Registered"] != null)
            {
                ViewBag.Registered = Session["Registered"];
                if (!string.IsNullOrEmpty(ViewBag.Registered["city"]))
                {
                    string city = ViewBag.Registered["city"].ToString();
                    ViewBag.TaiwanDistrict = DB.taiwan_city.Where(m => m.lay == "2").Where(m => m.city == city).Where(m => m.lang == defLang).OrderBy(m => m.sortIndex).ToList();
                }
            }

            return View();
        }

        /// <summary>
        /// 送出註冊會員
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void RegisteredForm(FormCollection form)
        {
            /* var response = form["g-recaptcha-response"];
             string secretKey = "6Ld_NxUTAAAAAAzX2OqL4afKeAOirTpUlT_ExxR-";
             var client = new WebClient();
             var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
             var obj = JObject.Parse(result);
             var status = (bool)obj.SelectToken("success");
             */
            //註冊資料暫存
            Session.Remove("Registered");
            Session.Add("Registered", form);

            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Services/Registered"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
            else
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("saveType", "N");//N(新註冊)、U(修改會員資料)
                callApiData.Add("account", form["account"].ToString());
                callApiData.Add("password", form["password"].ToString());
                callApiData.Add("cName", form["cName"].ToString());
                callApiData.Add("sex", form["sex"].ToString());
                callApiData.Add("birthday", form["birthday"].ToString());
                callApiData.Add("tel", form["tel"].ToString());

                string address = "";

                if (!string.IsNullOrEmpty(form["city"]))
                {
                    address += form["city"].ToString();
                }
                if (!string.IsNullOrEmpty(form["district"]))
                {
                    address += form["district"].ToString();
                }
                if (!string.IsNullOrEmpty(form["rd"]))
                {
                    address += form["rd"].ToString() + "路";
                }

                if (!string.IsNullOrEmpty(form["lane"]))
                {
                    address += form["lane"].ToString() + ViewBag.ResLang["巷"];
                }
                if (!string.IsNullOrEmpty(form["do"]))
                {
                    address += form["do"].ToString() + ViewBag.ResLang["弄"];
                }
                if (!string.IsNullOrEmpty(form["no"]))
                {
                    address += form["no"].ToString() + ViewBag.ResLang["號"];
                }
                if (!string.IsNullOrEmpty(form["floor"]))
                {
                    address += form["floor"].ToString() + ViewBag.ResLang["樓"];
                }
                if (!string.IsNullOrEmpty(form["It"]))
                {
                    address += ViewBag.ResLang["之"] + form["It"].ToString();
                }

                callApiData.Add("address", address);

                callApiData.Add("ePaper", "[{'paperId': '" + form["paperId"].ToString() + "','isOrder': '" + form["isOrder"].ToString() + "'}]");
                // callApiData.Add("ePaper", "");
                var ResponseData = OfficeAppData.SetMemberData(callApiData);

                if (ResponseData != null && ResponseData.GetType().Name != "JObject")
                {
                    //發送驗證碼
                    NameValueCollection sendContent = new NameValueCollection();

                    string url = webURL + "/" + defLang + "/Services/EnableMember/" + ResponseData[0].activeCode.ToString();

                    sendContent.Add("cName", form["cName"].ToString());
                    sendContent.Add("Content", "<p>" + langData["加入完成"].ToString() + "：<span style=\"color:#FF0000; font-weight:bold; font-size:15px;\"><a href=\"" + url + "\" target=\"_blank\">" + url + "</a> </span></p>");

                    List<string> MailList = new List<string>();
                    MailList.Add(form["account"].ToString());
                    FunctionService.sendMail(MailList, "2", defLang, webURL, sendContent);

                    Session.Remove("Registered");
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["加入完成"].ToString() + "!");
                    alertData.Add("text", langData["您已完成加入長庚會員"].ToString());
                    alertData.Add("type", "success");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Login"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
                else
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["加入失敗"].ToString() + "!");
                    alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Registered"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
            }
        }

        /// <summary>
        /// 啟用會員
        /// </summary>
        public void EnableMember()
        {
            if (ID != "")
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("activeCode", ID);
                var ResponseData = OfficeAppData.SetMemberActive(callApiData);

                if (ResponseData["isSuccess"].ToString() == "Y")
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["加入完成"].ToString() + "!");
                    alertData.Add("text", langData["您已完成加入長庚會員您可以直接輸入帳號密碼登入"].ToString());
                    alertData.Add("type", "success");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Login"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
                else
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["啟用錯誤"].ToString() + "!");
                    alertData.Add("text", langData["啟用錯誤請確認是否已啟用過或資訊錯誤"].ToString());
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Login"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
            }
            else
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["未知錯誤"].ToString() + "!");
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
        }

        #endregion

        #region 用藥諮詢

        /// <summary>
        /// 用藥諮詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Consultation(int? page)
        {
            Title = ViewBag.ResLang["用藥諮詢"] + "|";
            PathTitle = ViewBag.ResLang["用藥諮詢"];
            Data = null;
            ViewBag.Action = "Online";
            ViewBag.pageView = "";
            int pageWidth = 5;//預設每頁長度

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);

            var Models = OfficeAppData.GetDrugConsultList(callApiData);

            if (Models != null && Models.GetType().Name != "JObject")
            {
                List<DrugConsultListModel> List = Models;

                var pageNumeber = page ?? 1;
                Data = List.ToPagedList(pageNumeber, pageWidth);
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/Services/Consultation/" + ID, Allpage, pageNumeber, Models.Count, "");//頁碼
            }
            return View();
        }

        /// <summary>
        /// 用藥諮詢-我要發問
        /// </summary>
        /// <returns></returns>
        public ActionResult ConsultationForm()
        {
            Title = ViewBag.ResLang["用藥諮詢"] + "|";
            PathTitle = ViewBag.ResLang["用藥諮詢"];
            ViewBag.Action = "Online";
            ViewBag.ActionDefult = "Consultation";

            ViewBag.Consultation = null;

            if (Session["Consultation"] != null)
            {
                ViewBag.Consultation = Session["Consultation"];
            }

            return View();
        }

        /// <summary>
        /// 用藥諮詢-我要發問 - 送出發問
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ConsultationSend(FormCollection form)
        {
            /*  var response = form["g-recaptcha-response"];
              string secretKey = "6Ld_NxUTAAAAAAzX2OqL4afKeAOirTpUlT_ExxR-";
              var client = new WebClient();
              var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
              var obj = JObject.Parse(result);
              var status = (bool)obj.SelectToken("success");
              */
            //註冊資料暫存
            Session.Remove("Consultation");
            Session.Add("Consultation", form);

            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Services/ConsultationForm"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
            else
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("language", defApiLang);
                callApiData.Add("nickName", form["nickName"].ToString());
                callApiData.Add("postTime", form["postTime"].ToString());
                callApiData.Add("sex", form["sex"].ToString());
                callApiData.Add("age", form["age"].ToString());
                callApiData.Add("profession", form["profession"].ToString());
                callApiData.Add("eMail", form["eMail"].ToString());
                callApiData.Add("description", form["description"].ToString());

                // callApiData.Add("ePaper", "");
                var ResponseData = OfficeAppData.SetDrugConsult(callApiData);

                if (ResponseData["isSuccess"].ToString() == "Y")
                {
                    Session.Remove("Consultation");
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["發問完成"].ToString() + "!");
                    alertData.Add("text", langData["我們會盡快回覆您的諮詢"].ToString());
                    alertData.Add("type", "success");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Consultation"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
                else
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["發問失敗"].ToString() + "!");
                    alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/ConsultationForm"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
            }
        }

        #endregion

        /// <summary>
        /// 衛教園地
        /// </summary>
        /// <returns></returns>
        public ActionResult Guardian()
        {
            Title = ViewBag.ResLang["衛教園地"] + "|";
            PathTitle = ViewBag.ResLang["衛教園地"];
            ViewBag.Action = "Online";
            Dictionary<string, string> postParams = new Dictionary<string, string>();
            var Models = OfficeAppData.GetHealthEduList(postParams);//API

            if (Models != null && Models.GetType().Name != "JObject")
            {
                Data = Models;
            }

            return View();
        }

        /// <summary>
        /// 衛教園地-搜尋
        /// </summary>
        /// <returns></returns>
        public ActionResult GuardianSearch()
        {
            Title = ViewBag.ResLang["衛教園地"] + "|";
            PathTitle = ViewBag.ResLang["衛教園地"];
            ViewBag.Action = "Online";

            Dictionary<string, string> postParams = new Dictionary<string, string>();
            postParams.Add("kwd", SearchKeyword);
            var Models = OfficeAppData.GetHealthEduContent4Search(postParams);//API

            if (Models != null && Models.GetType().Name != "JObject")
            {
                Data = Models;
            }
            return View();
        }

        /// <summary>
        /// 用藥指導單張
        /// </summary>
        /// <returns></returns>
        public ActionResult DrugGuide(int? page)
        {
            Title = ViewBag.ResLang["用藥指導單張"] + "|";
            PathTitle = ViewBag.ResLang["用藥指導單張"];
            ViewBag.Action = "Online";
            int pageWidth = 10;//預設每頁長度
            ViewBag.pageView = "";

            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);

            List<DrugGuideListModel> Models = OfficeAppData.GetDrugGuideList(callApiData);

            if (Models != null && Models.GetType().Name != "JObject")
            {
                var pageNumeber = page ?? 1;
                Data = Models.ToPagedList(pageNumeber, pageWidth);
                int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)Models.Count / pageWidth));//總頁數
                ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + defLang + "/Services/DrugGuide", Allpage, pageNumeber, Models.Count, "");//頁碼
            }

            return View();
        }

        /// <summary>
        /// 藥品綜合查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Drug()
        {
            Title = ViewBag.ResLang["藥品綜合查詢"] + "|";
            PathTitle = ViewBag.ResLang["藥品綜合查詢"];
            ViewBag.Action = "Online";

            return View();
        }

        /// <summary>
        /// 藥品綜合查詢-查詢後
        /// </summary>
        /// <returns></returns>
        public ActionResult DrugSearch()
        {
            Title = ViewBag.ResLang["藥品綜合查詢"] + "|";
            PathTitle = ViewBag.ResLang["藥品綜合查詢"];
            ViewBag.Action = "Online";
            ViewBag.ActionDefult = "Drug";

            Dictionary<string, string> postParams = new Dictionary<string, string>();
            postParams.Add("kwd", SearchKeyword);
            postParams.Add("bcode", SearchCode);
            var Models = OfficeAppData.GetDrugQueryResult(postParams);//API

            if (Models != null && !Models.ContainsKey("Status"))
            {
                Data = Models;
            }

            return View();
        }

        /// <summary>
        /// 醫療諮詢
        /// </summary>
        /// <returns></returns>
        public ActionResult Advisory()
        {
            Title = ViewBag.ResLang["醫療諮詢"] + "|";
            PathTitle = ViewBag.ResLang["醫療諮詢"];
            ViewBag.Action = "Online";

            List<hospitals> hospitalsList = DB.hospitals.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();
            ViewBag.hospitalsList = hospitalsList;

            return View();
        }

        /// <summary>
        /// 醫療諮詢內容
        /// </summary>
        /// <returns></returns>
        public ActionResult AdvisoryInfo()
        {
            Title = ViewBag.ResLang["醫療諮詢"] + "|";
            PathTitle = ViewBag.ResLang["醫療諮詢"];
            ViewBag.Action = "Online";
            if (ID != "")
            {
                List<hospitals> hospitalsList = DB.hospitals.Where(m => m.lang == defLang).Where(m => m.status == "Y").ToList();
                ViewBag.hospitalsList = hospitalsList;
                Title = hospitalsList.Find(m => m.guid == ID).title + "|";
                PathTitle = hospitalsList.Find(m => m.guid == ID).title;
                var Models = DB.hospitals_advisory.Where(m => m.lang == defLang).Where(m => m.status == "Y").Where(m => m.category == ID).ToList();

                if (Models != null && Models.Count > 0)
                {
                    Data = Models;
                }

                return View();
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Services/Advisory"));
            }
        }

        /// <summary>
        /// 服務滿意度問卷調查
        /// </summary>
        /// <returns></returns>
        public ActionResult Questionnaire()
        {
            Title = ViewBag.ResLang["服務滿意度問卷調查"] + "|";
            PathTitle = ViewBag.ResLang["服務滿意度問卷調查"];
            ViewBag.Action = "Online";

            Data = DB.notes_data.Where(m => m.lang == defLang).Where(m => m.guid == "4").FirstOrDefault();

            return View();
        }

        /// <summary>
        /// 忘記密碼
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPassword()
        {
            Title = ViewBag.ResLang["忘記密碼"] + "|";
            PathTitle = ViewBag.ResLang["忘記密碼"];
            ViewBag.Action = "Online";

            ViewBag.ActionDefult = "Login";

            return View();
        }

        /// <summary>
        /// 忘記密碼2
        /// </summary>
        /// <returns></returns>
        public ActionResult ForgotPassword2()
        {
            Title = ViewBag.ResLang["忘記密碼"] + "|";
            PathTitle = ViewBag.ResLang["忘記密碼"];
            ViewBag.Action = "Online";

            ViewBag.ActionDefult = "Login";

            if (Request.Cookies["tempAccount"] != null)
            {
                HttpCookie CookieAccount = Request.Cookies["tempAccount"];

                if (CookieAccount != null && CookieAccount.Value != "" && CookieAccount.Value.IndexOf("&") != -1)
                {
                    return View();
                }
                else
                {
                    return RedirectPermanent(Url.Content("~/" + defLang + "/Services/ForgotPassword"));
                }
            }
            else
            {
                return RedirectPermanent(Url.Content("~/" + defLang + "/Services/ForgotPassword"));
            }
        }

        /// <summary>
        /// 重新寄送會員啟用信
        /// </summary>
        /// <returns></returns>
        public ActionResult SendActive()
        {
            Title = ViewBag.ResLang["重新寄送會員啟用信"] + "|";
            PathTitle = ViewBag.ResLang["重新寄送會員啟用信"];
            ViewBag.Action = "Online";

            ViewBag.ActionDefult = "Login";

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void SendActiveForm(FormCollection form)
        {
            if(form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Services/SendActive"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert") + ";</script>");
            }
            else
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("saveType", "R");
                callApiData.Add("account", form["account"].ToString());

                //取基本資料
                //var Member = OfficeAppData.SetMemberDataActive(callApiData);
                var ResponseData = OfficeAppData.SetMemberDataActive(callApiData);

                if (ResponseData != null && ResponseData.GetType().Name != "JObject")
                {
                    //發送驗證碼
                    NameValueCollection sendContent = new NameValueCollection();

                    string url = webURL + "/" + defLang + "/Services/EnableMember/" + ResponseData[0].activeCode.ToString();
                    //string url = "http://iis.24241872.tw/projects/public/m000194/test" + "/" + defLang + "/Services/EnableMember/" + ResponseData[0].activeCode.ToString();

                    sendContent.Add("cName", "會員");
                    sendContent.Add("Content", "<p>" + langData["加入完成"].ToString() + "：<span style=\"color:#FF0000; font-weight:bold; font-size:15px;\"><a href=\"" + url + "\" target=\"_blank\">" + url + "</a> </span></p>");

                    List<string> MailList = new List<string>();
                    MailList.Add(form["account"].ToString());
                    FunctionService.sendMail(MailList, "2", defLang, webURL, sendContent);


                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["重寄完成"].ToString() + "!");
                    alertData.Add("text", langData["會員啟用信已重新寄送"].ToString());
                    alertData.Add("type", "success");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Login"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");

                }
                else
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["重新寄送會員啟用信失敗"].ToString() + "!");
                    alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/SendActive"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
            }
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePassword()
        {
            Title = ViewBag.ResLang["修改密碼"] + "|";
            PathTitle = ViewBag.ResLang["修改密碼"];
            ViewBag.Action = "Online";

            ViewBag.ActionDefult = "Login";

            return View();
        }

        /// <summary>
        /// 團隊搜尋
        /// </summary>
        /// <returns></returns>
        public ActionResult TeamSearch()
        {
            Title = ViewBag.ResLang["團隊搜尋"] + "|";
            PathTitle = ViewBag.ResLang["團隊搜尋"];

            ViewBag.Action = "Team";
            return View();
        }

        /// <summary>
        /// 團隊搜尋結果
        /// </summary>
        /// <returns></returns>
        public ActionResult TeamList()
        {
            Title = ViewBag.ResLang["團隊搜尋"] + "|";
            PathTitle = ViewBag.ResLang["團隊搜尋"];
            ViewBag.Action = "Team";
            ViewBag.Dept = null;
            ViewBag.keyword = "";
            ViewBag.DeptId = null;
            dynamic Models = null;
            ViewBag.showHospital = "N";
            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);



            if (!string.IsNullOrEmpty(Request["keyword"]))
            {
                ViewBag.keyword = Request["keyword"].ToString(); 
                callApiData.Add("keyword", Request["keyword"].ToString());
            }
            if (!string.IsNullOrEmpty(Request["hospital"]))
            {
                callApiData.Add("hospitalID", Request["hospital"].ToString());
            }
            if (!string.IsNullOrEmpty(Request["dept"]))
            {

                ViewBag.DeptId = Request["dept"].ToString().Split('_').ToList();
                //callApiData.Add("deptId", Request["deptVal"].ToString());

                // var test = Request["deptVal"].ToString();
                if(!string.IsNullOrEmpty(Request["dept"].ToString()))
                {
                    if (Request["dept"].ToString().IndexOf("_") == -1)
                    {
                        callApiData.Add("deptId", ViewBag.DeptId[0]);

                    }
                    else
                    {
                        callApiData.Add("deptId", ViewBag.DeptId[1]);
                    }

                }


            }

            Models = OfficeAppData.GetSearchDoctor(callApiData);

            if (Models != null && Models.GetType().Name != "JObject" && Models.Count > 0)
            {
                //ViewBag.Dept = Models;
                Data = Models[0];
            }
            else
            {
                callApiData.Remove("hospitalID");
                callApiData.Remove("deptId");

                Models = OfficeAppData.GetSearchDoctor(callApiData);
                if (Models != null && Models.GetType().Name != "JObject" && Models.Count > 0)
                {
                    //ViewBag.Dept = Models;
                    Data = Models[0];
                }
                ViewBag.showHospital = "Y";
            }

            List<SearchDoctorModel> DoctorListTemp = Models;


            Dictionary<string, List<SearchDoctorModel>> DoctorList = new Dictionary<string, List<SearchDoctorModel>>();

            foreach (SearchDoctorModel Doctors in DoctorListTemp)
            {
                if (!DoctorList.ContainsKey(Doctors.deptName))
                {
                    List<SearchDoctorModel> temp = new List<SearchDoctorModel>();
                    temp.Add(Doctors);
                    DoctorList.Add(Doctors.deptName, temp);
                }
                else
                {
                    List<SearchDoctorModel> temp = DoctorList[Doctors.deptName];
                    temp.Add(Doctors);
                    DoctorList[Doctors.deptName] = temp;

                }
            }
            ViewBag.DoctorList = DoctorList;


          


            if (!string.IsNullOrEmpty(Request["hospital"]))
             {
                 callApiData.Clear();
                 callApiData.Add("language", defApiLang);
                 callApiData.Add("hospitalID", Request["hospital"].ToString());

                 Models = OfficeAppData.GetDoctorDeptList(callApiData);
                 if (Models != null && Models.GetType().Name != "JObject")
                 {
                     ViewBag.Dept = Models;
                 }

                 if (!string.IsNullOrEmpty(Request["dept"]))
                 {
                     var test = Request["deptVal"].ToString();
                     //取得團隊資料
                     callApiData.Clear();
                     callApiData.Add("language", defApiLang);
                     callApiData.Add("deptId", Request["dept"].ToString());

                     ViewBag.DeptId = Request["dept"].ToString().Split('_').ToList();

                     Models = OfficeAppData.GetDeptDoctor(callApiData);
                     if (Models != null && Models.GetType().Name != "JObject")
                     {
                         //Data = Models[0];
                     }
                  
                }
             
            }

            return View();
        }

        /// <summary>
        /// 檢查帳號(email是否存在)
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ForgotForm(FormCollection form)
        {
            /*var response = form["g-recaptcha-response"];
            string secretKey = "6Ld_NxUTAAAAAAzX2OqL4afKeAOirTpUlT_ExxR-";
            var client = new WebClient();
            var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
            var obj = JObject.Parse(result);
            var status = (bool)obj.SelectToken("success");
            */
            if (form["verification"].ToString() != Session["_ValCheckCode"].ToString())
            {
                Dictionary<String, Object> alertData = new Dictionary<string, object>();
                alertData.Add("title", langData["驗證碼比對錯誤"].ToString());
                alertData.Add("text", "");
                alertData.Add("type", "error");
                alertData.Add("url", Url.Content("~/" + defLang + "/Services/ForgotPassword"));

                Session.Remove("alertData");
                Session.Add("alertData", alertData);

                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
            }
            else
            {
                Dictionary<string, string> callApiData = new Dictionary<string, string>();

                callApiData.Add("account", form["account"].ToString());

                //取基本資料
                var Member = OfficeAppData.GetMemberData(callApiData);
                if (Member != null && Member.GetType().Name != "JObject")
                {
                    HttpCookie cookie = new HttpCookie("tempAccount");
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(cookie);

                    //產生一個Cookie

                    //設定過期日
                    cookie.Expires = DateTime.Now.AddMinutes(2);//10分鐘有效
                    cookie.Values.Add("account", form["account"].ToString());//增加属性
                    string Codes = FunctionService.RandomPassword.Generate(5, 5);//驗證碼
                    cookie.Values.Add("codes", Codes.ToString());//增加属性

                    Response.AppendCookie(cookie);//确定写入cookie中

                    //發送驗證碼
                    NameValueCollection sendContent = new NameValueCollection();

                    sendContent.Add("cName", "會員");
                    sendContent.Add("Content", "<p>您的修改密碼驗證碼為：<span style=\"color:#FF0000; font-weight:bold; font-size:15px;\">" + Codes + "</span></p><p>請在10分鐘內於網站中輸入!</p>");

                    List<string> MailList = new List<string>();
                    MailList.Add(form["account"].ToString());
                    FunctionService.sendMail(MailList, "1", defLang, webURL, sendContent);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Services/ForgotPassword2'") + ";</script>");
                }
                else
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["查詢失敗"].ToString() + "!");
                    alertData.Add("text", langData["查無此Email資料"].ToString());
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/ForgotPassword"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
            }
        }

        /// <summary>
        /// 比對驗證碼
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ForgotForm2(FormCollection form)
        {
            if (Request.Cookies["tempAccount"] != null)
            {
                HttpCookie CookieAccount = Request.Cookies["tempAccount"];

                if (CookieAccount != null && CookieAccount.Value != "" && CookieAccount.Value.IndexOf("&") != -1)
                {
                    List<string> AccountData = CookieAccount.Value.Split('&').ToList();
                    string account = Server.HtmlEncode(AccountData[0].Replace("account=", ""));
                    string codes = Server.HtmlEncode(AccountData[1].Replace("codes=", ""));

                    if (codes == form["codes"].ToString().ToLower())
                    {
                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Services/ChangePassword'") + ";</script>");
                    }
                    else
                    {
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["驗證錯誤"].ToString() + "!");
                        alertData.Add("text", langData["驗證碼比對錯誤"].ToString());
                        alertData.Add("type", "error");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Services/ForgotPassword2"));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                    }
                }
                else
                {
                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Services/ForgotPassword'") + ";</script>");
                }
            }
            else
            {
                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Services/ForgotPassword'") + ";</script>");
            }
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <param name="form"></param>
        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public void ModifyPassword(FormCollection form)
        {
            string account = "";
            string reUrl = "";
            if (Session["MemberData"] != null)
            {
                var response = form["g-recaptcha-response"];
                string secretKey = "6Ld_NxUTAAAAAAzX2OqL4afKeAOirTpUlT_ExxR-";
                var client = new WebClient();
                var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response));
                var obj = JObject.Parse(result);
                var status = (bool)obj.SelectToken("success");

                if (status)
                {
                    MemberDataModel MemberData = (MemberDataModel)Session["MemberData"];
                    account = MemberData.account;
                    reUrl = "Member";
                }
                else
                {
                    Dictionary<String, Object> alertData = new Dictionary<string, object>();
                    alertData.Add("title", langData["請勾選驗證"].ToString() + "!");
                    alertData.Add("text", "");
                    alertData.Add("type", "error");
                    alertData.Add("url", Url.Content("~/" + defLang + "/Services/Member"));

                    Session.Remove("alertData");
                    Session.Add("alertData", alertData);

                    Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                }
            }
            else
            {
                HttpCookie CookieAccount = Request.Cookies["tempAccount"];

                if (CookieAccount != null && CookieAccount.Value != "" && CookieAccount.Value.IndexOf("&") != -1)
                {
                    List<string> AccountData = CookieAccount.Value.Split('&').ToList();
                    account = Server.HtmlEncode(AccountData[0].Replace("account=", ""));
                    reUrl = "Login";
                }
            }
            if (account != "")
            {
                //取基本資料
                Dictionary<string, string> callApiData = new Dictionary<string, string>();
                callApiData.Add("account", account);
                var Member = OfficeAppData.GetMemberData(callApiData);
                if (Member != null && Member.GetType().Name != "JObject")
                {
                    //Member[0]
                    MemberDataModel temp = Member[0];
                    callApiData.Add("saveType", "U");//N(新註冊)、U(修改會員資料)
                    callApiData.Add("password", form["password"].ToString());

                    callApiData.Add("cName", temp.cName.ToString());
                    callApiData.Add("sex", temp.sex.ToString());
                    callApiData.Add("birthday", temp.birthday.ToString());
                    callApiData.Add("tel", temp.tel.ToString());
                    callApiData.Add("address", temp.address.ToString());

                    callApiData.Add("ePaper", "[{'paperId': '" + temp.ePaper.FirstOrDefault().paperId + "','isOrder': '" + temp.ePaper.FirstOrDefault().isOrder + "'}]");
                    // callApiData.Add("ePaper", "");
                    var ResponseData = OfficeAppData.SetMemberData(callApiData);

                    if (ResponseData["isSuccess"].ToString() == "Y")
                    {
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["修改完成"].ToString() + "!");
                        alertData.Add("text", langData["密碼已修改"].ToString());
                        alertData.Add("type", "success");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Services/" + reUrl));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        HttpCookie CookieAccount = Request.Cookies["tempAccount"];

                        if (CookieAccount != null && CookieAccount.Value != "" && CookieAccount.Value.IndexOf("&") != -1)
                        {
                            HttpCookie cookie = new HttpCookie("tempAccount");
                            cookie.Expires = DateTime.Now.AddDays(-1);
                            Response.Cookies.Add(cookie);
                        }

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                    }
                    else
                    {
                        Dictionary<String, Object> alertData = new Dictionary<string, object>();
                        alertData.Add("title", langData["修改失敗"].ToString() + "!");
                        alertData.Add("text", langData["請檢查檢查相關資訊或洽官網管理員"].ToString());
                        alertData.Add("type", "error");
                        alertData.Add("url", Url.Content("~/" + defLang + "/Services/" + reUrl));

                        Session.Remove("alertData");
                        Session.Add("alertData", alertData);

                        Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Home/Alert'") + ";</script>");
                    }
                }
            }
            else
            {
                Response.Write("<script> window.location.href='" + Url.Content("~/" + defLang + "/Services/Login'") + ";</script>");
            }
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            Session.Remove("MemberData");
            return RedirectPermanent(Url.Content("~/" + defLang + "/Services/Login"));
        }

        /// <summary>
        /// 取得區鄉鎮
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        public string GetDistrict(FormCollection form)
        {
            string city = form["city"].ToString();
            var Models = DB.taiwan_city.Where(m => m.lay == "2").Where(m => m.city == city).Where(m => m.lang == defLang).OrderBy(m => m.sortIndex).ToList();
            if (Models != null && Models.GetType().Name != "JObject")
            {
                //輸出json格式
                return JsonConvert.SerializeObject(Models, Formatting.Indented);
            }
            else
            {
                return "";
            }
        }

        [HttpPost]
        public string Ajax(FormCollection form)
        {
            string codeNo = form["codeNo"].ToString();
            Dictionary<string, string> postParams = new Dictionary<string, string>();
            postParams.Add("articleNumber", codeNo);
            var Models = OfficeAppData.GetHealthEduContent(postParams);//API

            Dictionary<String, Object> dic = new Dictionary<string, object>();

            if (Models != null && Models.GetType().Name != "JObject")
            {
                //json = JsonConvert.SerializeObject(Models[0], Formatting.Indented);
                dic.Add("url", Models[0].閱讀網址);
            }
            //輸出json格式
            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            return json;
        }

        /// <summary>
        /// 動態取得科別
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        public string GetDept(FormCollection form)
        {
            Dictionary<string, string> callApiData = new Dictionary<string, string>();
            callApiData.Add("language", defApiLang);
            callApiData.Add("hospitalID", form["hospital"]);

            string json = JsonConvert.SerializeObject("", Formatting.Indented);

            var Models = OfficeAppData.GetDoctorDeptList(callApiData);
            if (Models != null && Models.GetType().Name != "JObject")
            {
                json = JsonConvert.SerializeObject(Models, Formatting.Indented);
            }

            //輸出json格式

            return json;
        }

        /// <summary>
        /// 判斷帳號是否存在
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpGet]
        public string CheckAccount(string account)
        {
            string re = "true";

            Dictionary<string, string> callApiData = new Dictionary<string, string>();

            callApiData.Add("account", account.ToString());

            //取基本資料
            var Member = OfficeAppData.GetMemberData(callApiData);
            if (Member != null && Member.GetType().Name != "JObject")
            {
                re = "false";
            }

            return re;
        }
    }
}